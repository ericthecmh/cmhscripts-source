import java.util.ArrayList;
import java.util.List;

import org.osbot.script.Script;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.model.GroundItem;
import org.osbot.script.rs2.model.NPC;
import org.osbot.script.rs2.model.RS2Object;
import org.osbot.script.rs2.randoms.RandomBehaviourHook;
import org.osbot.script.rs2.randoms.RandomManager;
import org.osbot.script.rs2.ui.RS2Interface;
import org.osbot.script.rs2.ui.RS2InterfaceChild;

public class CMHEvilBobHandler extends RandomBehaviourHook{

	private Script loader;
	private NPC servant, evilBob;
	private RS2Object portal;
	private boolean failed;
	private ArrayList<Integer> droppedItems;

	public CMHEvilBobHandler(Script loader) {
		super(RandomManager.EVIL_BOB);
		this.loader = loader;
		droppedItems = new ArrayList<Integer>();
	}

	public boolean shouldActivate() {
		return loader.closestNPCForName("Evil Bob") != null
				&& loader.closestNPCForName("Servant") != null;

	}
	@Override
	public boolean shouldActivatePreLoop(){
		return true;
	}

	@Override
	public int preLoop() {
		failed = false;
		loader.log("Activating Evil Bob random solver!");
		servant = loader.closestNPCForName("Servant");
		if (servant == null) {
			loader.log("Unable to find servant. Terminating.");
			failed = true;
		}
		evilBob = loader.closestNPCForName("Evil Bob");
		if (evilBob == null) {
			loader.log("Unable to find Evil Bob. Terminating.");
			failed = true;
		}
		portal = loader.closestObjectForName("Portal");
		if (portal == null) {
			loader.log("Unable to find portal. Terminating.");
			failed = true;
		}
		return 0;
	}

	public int onLoop() {
		if (!failed) {
			while (loader.client.getInventory().getFullSlots() > 26) {
				loader.log("Inventory too full. Dropping items");
				int it = loader.client.getInventory().getFirstNonEmptySlot();
				droppedItems.add(loader.client.getInventory().getItems()[it]
						.getId());
				loader.log("Dropping a "
						+ loader.client.getInventory().getItems()[it].getName());
				try {
					int oldcount = loader.client.getInventory().getFullSlots();
					for (int i = 0; i < 10
							&& !loader.client.getInventory().interactWithSlot(
									it, "Drop")
							&& oldcount == loader.client.getInventory()
									.getFullSlots(); i++) {
						loader.sleep(loader.random(1500, 2000));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			loader.log("Determining fishing spot location");
			try {
				for (int i = 0; i < 10 && !servant.interact("Talk-to"); i++)
					loader.sleep(loader.random(500, 600));
				int id = getDialogInterface();
				for (int i = 0; i < 10 && id == -1; i++) {
					id = getDialogInterface();
					loader.sleep(loader.random(500, 600));
				}
				if (id == -1) {
					loader.log("Unable to talk to servant. Terminating.");
					return 0;
				}
				while (id != -1) {
					clickContinue(loader.client.getInterface(id));
					id = getDialogInterface();
				}
				int angle = loader.client.getCameraYawAngle();
				loader.sleep(loader.random(5000, 6000));
				Position p2 = null;
				if (Math.abs(angle - 0) < 10) {
					loader.log("Fishing spot to the North");
					p2 = new Position(loader.myX(), loader.myY() + 10, 0);
				} else if (Math.abs(angle - 180) < 10) {
					loader.log("Fishing spot to the South");
					p2 = new Position(loader.myX(), loader.myY() - 10, 0);
				} else if (Math.abs(angle - 90) < 10) {
					loader.log("Fishing spot to the East");
					p2 = new Position(loader.myX() - 10, loader.myY(), 0);
				} else if (Math.abs(angle - 270) < 10) {
					loader.log("Fishing spot to the West");
					p2 = new Position(loader.myX() + 10, loader.myY(), 0);
				}
				if (p2 == null) {
					loader.log("Could not figure location of spot");
				}
				RS2Object spot = closestObjectTo("Fishing spot", p2);
				if (spot == null) {
					loader.log("Can't find spot! Terminating.");
					return 0;
				}
				loader.log("Found fishing spot.");
				while (loader.distance(p2) > 4) {
					loader.walkMiniMap(p2);
					while (loader.myPlayer().isMoving())
						loader.sleep(loader.random(500, 600));
				}
				loader.log("Getting small fishing net");
				GroundItem net = loader
						.closestGroundItemForName("Small fishing net");
				while (loader.distance(net.getPosition()) > 4) {
					loader.walkMiniMap(net.getPosition());
					while (loader.myPlayer().isMoving())
						loader.sleep(loader.random(500, 600));
				}
				for (int i = 0; i < 10 && !net.interact("Take"); i++)
					loader.sleep(loader.random(500, 600));
				for (int i = 0; i < 10
						&& !loader.client.getInventory().contains(
								"Small fishing net"); i++) {
					loader.sleep(loader.random(500, 600));
				}
				loader.sleep(loader.random(2000, 2100));
				while (!loader.client.getInventory().contains(
						"Small fishing net")) {
					loader.log("Getting small fishing net");
					net = loader.closestGroundItemForName("Small fishing net");
					while (loader.distance(net.getPosition()) > 4) {
						loader.walkMiniMap(net.getPosition());
						while (loader.myPlayer().isMoving())
							loader.sleep(loader.random(500, 600));
					}
					for (int i = 0; i < 10 && !net.interact("Take"); i++)
						loader.sleep(loader.random(500, 600));
					for (int i = 0; i < 10
							&& !loader.client.getInventory().contains(
									"Small fishing net"); i++) {
						loader.sleep(loader.random(500, 600));
					}
					loader.sleep(loader.random(2000, 2100));
				}
				loader.log("Got a fishing net");
				while (loader.distance(spot.getPosition()) > 4) {
					loader.walkMiniMap(spot.getPosition());
					while (loader.myPlayer().isMoving())
						loader.sleep(loader.random(500, 600));
				}
				for (int i = 0; i < 10 && !spot.interact("Net"); i++) {
					loader.sleep(loader.random(500, 600));
				}
				if (!loader.client.getInventory().contains("Fishlike thing")) {
					for (int i = 0; i < 10 && !spot.interact("Net"); i++) {
						loader.sleep(loader.random(500, 600));
					}
				}
				loader.sleep(loader.random(2000, 2100));
				while (loader.myPlayer().getAnimation() != -1)
					;
				loader.sleep(loader.random(500, 600));
				loader.log("Got a fishlike thing");
				while (loader.distance(evilBob.getPosition()) > 4) {
					loader.walkMiniMap(evilBob.getPosition());
					while (loader.myPlayer().isMoving())
						loader.sleep(loader.random(500, 600));
				}
				loader.client.rotateCameraPitch(90);
				loader.sleep(loader.random(500, 600));
				RS2Object pot = loader.closestObjectForName("Uncooking pot");
				if (pot == null) {
					loader.log("Could not find pot");
					return 0;
				}
				loader.client.rotateCameraPitch(90);
				loader.sleep(loader.random(500, 600));
				while (!loader.client.getInventory().contains(
						"Raw fishlike thing")) {
					for (int i = 0; i < 10
							&& !loader.client.getInventory().interactWithName(
									"Fishlike thing", "Use"); i++) {
						loader.sleep(loader.random(500, 600));
					}

					loader.client.rotateCameraPitch(90);
					loader.sleep(loader.random(2000, 2100));
					for (int i = 0; i < 10
							&& !loader.client.moveMouseTo(
									pot.getMouseDestination(), false, true,
									false); i++) {
						loader.sleep(loader.random(500, 600));
					}
					loader.sleep(loader.random(2000, 3000));
				}
				for (int i = 0; i < 10
						&& !loader.client.getInventory().interactWithName(
								"Raw fishlike thing", "Use"); i++) {
					loader.sleep(loader.random(500, 600));
				}

				loader.client.rotateCameraPitch(90);
				loader.sleep(loader.random(1000, 1100));
				for (int i = 0; i < 10
						&& !loader.client.moveMouseTo(
								evilBob.getMouseDestination(), false, true,
								false); i++) {
					loader.sleep(loader.random(500, 600));
				}
				id = getDialogInterface();
				for (int i = 0; i < 10 && id == -1; i++) {
					id = getDialogInterface();
					loader.sleep(loader.random(500, 600));
				}
				if (id == -1) {
					loader.log("Unable to talk to give fish to the dumb Bob. Terminating.");
					return 0;
				}
				while (id != -1) {
					clickContinue(loader.client.getInterface(id));
					id = getDialogInterface();
				}
				loader.client.rotateCameraPitch(90);
				loader.sleep(loader.random(500, 600));
				if (droppedItems.size() > 0) {
					loader.log("Picking up items");
					for (int i = 0; i < droppedItems.size(); i++) {
						int oldcount = loader.client.getInventory()
								.getFullSlots();
						GroundItem item = loader.closestGroundItem(droppedItems
								.get(i));
						if (item != null) {
							for (int j = 0; j < 10 && !item.interact("Take"); j++) {
								loader.sleep(loader.random(500, 600));
							}

							for (int j = 0; j < 10
									&& oldcount == loader.client.getInventory()
											.getFullSlots(); j++) {
								loader.sleep(loader.random(500, 600));
							}
							if (oldcount == loader.client.getInventory()
									.getFullSlots())
								i--;
						}
					}
				}
				while (loader.distance(portal) < 10) {
					loader.walkMiniMap(portal.getPosition());
					for (int i = 0; i < 10 && !portal.interact("Enter"); i++) {
						loader.sleep(loader.random(500, 600));
					}
					for (int i = 0; i < 30
							&& (loader.distance(evilBob.getPosition()) < 20); i++) {
						loader.sleep(loader.random(500, 600));
					}
				}
			} catch (Exception e) {

			}
		}
		return 0;
	}
	
	public boolean shouldActivatePostLoop(){
		return true;
	}

	private RS2Object closestObjectTo(String name, Position p) {
		List<RS2Object> objects = loader.client.getCurrentRegion().getObjects();
		RS2Object object = null;
		int dist = 20000000;
		for (RS2Object o : objects) {
			if (o.getName().equals(name)) {
				if (object == null) {
					object = o;
					dist = loader.distance(p.getX(), p.getY(), object.getX(),
							object.getY());
				} else {
					if (loader.distance(p.getX(), p.getY(), o.getX(), o.getY()) < dist) {
						dist = loader.distance(p.getX(), p.getY(), o.getX(),
								o.getY());
						object = o;
					}
				}
			}
		}
		return object;
	}

	private void clickContinue(RS2Interface inte) throws InterruptedException {
		for (RS2InterfaceChild c : inte.getChildren()) {
			if (c.getMessage().contains("here to cont")) {
				for (int i = 0; i < 10 && !c.interact(); i++) {
					loader.sleep(loader.random(500, 600));
				}
			}
		}
		loader.sleep(loader.random(1500, 2000));
	}

	private int getDialogInterface() {
		if (loader.client.getInterface(228) != null)
			return 228;
		if (loader.client.getInterface(230) != null)
			return 230;
		if (loader.client.getInterface(240) != null)
			return 240;
		if (loader.client.getInterface(241) != null)
			return 241;
		if (loader.client.getInterface(242) != null)
			return 242;
		if (loader.client.getInterface(243) != null)
			return 243;
		if (loader.client.getInterface(244) != null)
			return 244;
		if (loader.client.getInterface(64) != null)
			return 64;
		if (loader.client.getInterface(65) != null)
			return 65;
		return -1;
	}

	public int postLoop() {
		loader.log("Finished Bob Random");
		return 0;
	}
}
