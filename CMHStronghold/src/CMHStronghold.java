import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.table.DefaultTableModel;

import org.osbot.script.MethodProvider;
import org.osbot.script.RandomEventSolver;
import org.osbot.script.Script;
import org.osbot.script.mouse.RectangleDestination;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.model.Entity;
import org.osbot.script.rs2.model.GroundItem;
import org.osbot.script.rs2.model.Item;
import org.osbot.script.rs2.model.NPC;
import org.osbot.script.rs2.model.Player;
import org.osbot.script.rs2.model.RS2Object;
import org.osbot.script.rs2.randoms.RandomBehaviourHook;
import org.osbot.script.rs2.randoms.RandomManager;
import org.osbot.script.rs2.skill.Skill;
import org.osbot.script.rs2.skill.Skills;
import org.osbot.script.rs2.ui.Bank;
import org.osbot.script.rs2.ui.RS2Interface;
import org.osbot.script.rs2.ui.RS2InterfaceChild;
import org.osbot.script.rs2.ui.Tab;

public class CMHStronghold extends Script {
	private final boolean debugmsg = false;
	private boolean shouldHop = false;
	// CONSTANTS
	private final String currentVersion = "2.5.3";
	private final String settingsVersion = "1.8.0";

	private final int[] blacklistedWorlds = new int[] { 308, 316, 307, 315,
			323, 324, 325, 331, 332, 337, 339, 340, 347, 348, 355, 356, 363,
			364, 371, 372 };

	private final int CONST = 1;
	private final int[] portalIDs = { 21779, 23638, 23699 }; // UPDATE
	private final int[] ladderIDs = { 21778, 23571, 23671 }; // UPDATE
	private final int entranceID = 23703; // UPDATE
	private final int[][] floodFillArray = new int[][] { new int[] { 21762 },
			new int[] { 329, 330, 331 }, new int[] { 23663, 23664, 23662 },
			new int[] { 333, 332, 334 } }; // UPDATE
	private boolean debug = false;
	private final Area lumbyArea = new Area(3210, 3230, 3230, 3210);
	private final Position varrockCenter = new Position(3212, 3428, 0);
	private final Position[][] pathToBank = new Position[][] {
			new Position[] { new Position(3087, 3427, 0),
					new Position(3090, 3435, 0), new Position(3091, 3443, 0),
					new Position(3090, 3451, 0), new Position(3088, 3458, 0),
					new Position(3094, 3465, 0), new Position(3099, 3473, 0),
					new Position(3099, 3480, 0), new Position(3094, 3490, 0) },
			new Position[] { new Position(3087, 3427, 0),
					new Position(3090, 3435, 0), new Position(3091, 3443, 0),
					new Position(3090, 3451, 0), new Position(3088, 3458, 0),
					new Position(3081, 3466, 0), new Position(3080, 3474, 0),
					new Position(3082, 3484, 0), new Position(3090, 3487, 0),
					new Position(3094, 3490, 0) },
			new Position[] { new Position(3208, 3429, 0),
					new Position(3201, 3429, 0), new Position(3194, 3431, 0),
					new Position(3185, 3435, 0) } };
	private final Position[] pathFromVarrock = new Position[] {
			new Position(3178, 3430, 0), new Position(3167, 3429, 0),
			new Position(3163, 3421, 0), new Position(3152, 3416, 0),
			new Position(3145, 3420, 0), new Position(3135, 3422, 0),
			new Position(3127, 3422, 0), new Position(3118, 3424, 0),
			new Position(3108, 3421, 0), new Position(3097, 3420, 0),
			new Position(3088, 3420, 0), new Position(3080, 3423, 0) };

	// Operating variables
	private Script loader;
	private Skills skills;
	private boolean canStart;
	private int paintI = 0;
	private boolean hide = false;
	private BufferedImage[] paintImages = new BufferedImage[4];
	private boolean doneLoading;
	private State state;
	private int room, floor, walkI;
	private State returnTo;
	private boolean atMiddle;
	private String scriptMessage;
	private long messageStamp;
	private char[][] freeTiles;
	private int floodX, floodY;
	private boolean showFlood = true;
	private ArrayList<AttackNPC> attackNPCs;
	private NPC attackingNPC;
	private ArrayList<Food> eatFoods;
	private int[] keepIDs;
	private int setRun;
	private boolean isPrem;
	private boolean updated;
	private ArrayList<Loot> lootItems;
	private boolean buryBones;
	private int buryBonesLevel;
	private boolean firstDoor;
	private boolean doNotMark;
	private boolean equipArrow;
	private String arrowName;
	private int arrowAmount;
	private boolean scriptStopped;
	private CMHStrongholdLogin login;
	private int bankFailsafe;
	private long lastChange;
	private Position lastPos;
	private int walkFailsafe;
	private boolean antiban;
	private long lastAnim;
	private boolean customRandomEvent;
	private boolean attack;
	private ArrayList<Action> actionsList;
	private boolean attackingLock;
	private boolean banking;
	private int responsesI;
	private ArrayList<String> responses;
	private CMHCleverbot cleverbot;
	private boolean useCleverbot;
	private int messageCount;
	private int threshold;
	private boolean passedDoor2;
	private boolean useSpecialAttack;
	private int useSpecialThresh;
	private int setSpecialAttack;
	private ArrayList<Potion> drinkPots;
	private boolean solvingRandom;
	private long lastNPCHealthDrop;
	private int oldNPCHealth;
	private boolean bankPots;
	private boolean switchingWorlds;
	private boolean hopMod, hopPeople;
	private int hopPeopleAmount;
	private boolean useVarrockTab;
	private boolean useB2P;
	private int b2pThresh;
	private ArrayList<Break> breaks;
	private boolean takeBreaks;
	private long lastRuntimeUpdate;
	private int lastExpUpdate;

	// Paint stuff
	private int startAttackEXP, startDefenceEXP, startStrengthEXP,
			startRangeEXP, startMagicEXP, startHitpointsEXP, startPrayerEXP;

	private int startAttackLevel, startDefenceLevel, startStrengthLevel,
			startRangeLevel, startMagicLevel, startHitpointsLevel,
			startPrayerLevel;
	private int gainedAttackEXP, gainedDefenceEXP, gainedStrengthEXP,
			gainedRangeEXP, gainedMagicEXP, gainedHitpointsEXP,
			gainedPrayerEXP;
	private int gainedAttackLevel, gainedDefenceLevel, gainedStrengthLevel,
			gainedRangeLevel, gainedMagicLevel, gainedHitpointLevel,
			gainedPrayerLevel;
	private int attackPerHour, defencePerHour, strengthPerHour, rangePerHour,
			magicPerHour, prayerPerHour, hitpointsPerHour;
	private int hoursToLevel, minutesToLevel, secondsToLevel;
	private long current, start;
	private int hours, minutes, seconds;
	private final int tabX = -2;
	private final int tabY = 342;
	private final int offsetX = 0;
	private final int offsetY = 0;

	// Floor and Graph info
	private final String floor1cfg = "0 1 -100\n1 2 2\n2 3 -100\n3 4 D\n4 15 D\n4 5 D\n5 6 D\n6 7 -100\n7 8 2\n8 9 -100\n9 10 D\n10 11 D\n11 12 -100\n12 13 2\n13 14 -100\n15 16 -100\n16 17 2\n17 18 -100\n18 19 D\n19 20 D\n20 21 D\n21 22 -100\n22 23 2\n23 24 -100\n24 25 D\n25 26 -100\n26 27 2\n27 28 -100\n28 29 D\n29 30 D\n30 31 D\n31 32 -100\n32 33 2\n33 34 -100\n34 35 D\n35 36 D\n36 37 D\n37 38 -100\n38 39 2\n39 40 -100\n29 41 D\n41 42 D\n42 43 -100\n43 44 2\n44 45 -100\n45 46 D\n46 47 D\n47 48 -100\n48 49 2\n49 50 -100\n50 51 D\n51 52 D\n52 53 -100\n53 54 2\n54 55 -100\n51 56 D\n56 57 -100\n57 58 2\n58 59 -100\n59 60 D\n60 61 -100\n61 62 2\n62 63 -100\n63 64 D\n64 65 D\n14 65 D\n63 66 D\n66 67 -100\n67 68 2\n68 69 -100\n69 70 D\n70 71 D\n72 73 D\n73 74 -100\n74 75 2\n75 76 -100\n76 77 D\n77 78 -100\n78 79 2\n79 80 -100\n73 81 D\n81 82 D\n82 83 -100\n83 84 2\n84 85 -100\n41 88 D\n88 87 D\n87 86 D\n86 89 D\n89 90 -100\n90 91 2\n91 92 -100\n92 93 D\n93 60 D\n5 55 D\n71 72 D\n85 89 D\n";
	private final String floor2cfg = "0 1 D\n0 77 D\n1 2 -100\n2 3 2\n3 4 -100\n4 5 D\n5 6 D\n6 7 D\n6 71 D\n7 8 -100\n8 9 2\n9 10 -100\n10 11 D\n11 12 -100\n12 13 2\n13 14 -100\n14 15 D\n15 16 -100\n16 17 2\n17 18 -100\n18 19 D\n19 20 D\n20 21 -100\n21 22 2\n22 23 -100\n23 24 D\n24 25 D\n25 26 D\n26 27 -100\n27 28 2\n28 29 -100\n29 30 D\n30 31 D\n31 32 D\n32 33 D\n33 34 D\n34 19 D\n29 35 D\n35 36 D\n36 37 D\n37 38 D\n38 39 -100\n39 40 2\n40 41 -100\n41 42 D\n42 43 D\n41 43 D\n43 44 D\n44 45 -100\n45 46 2\n46 47 -100\n47 48 D\n48 49 D\n49 50 D\n50 26 D\n71 70 -100\n70 69 2\n69 68 -100\n68 67 D\n67 66 -100\n66 65 2\n65 64 -100\n64 63 D\n63 62 D\n62 61 D\n61 60 D\n60 59 -100\n59 58 2\n58 57 -100\n57 56 D\n56 55 D\n55 54 -100\n54 53 2\n53 52 -100\n52 51 D\n77 76 -100\n76 75 2\n75 74 -100\n74 73 D\n73 72 D\n72 64 D\n24 78 D\n78 79 D\n79 80 -100\n80 81 2\n81 82 -100\n82 83 D\n";
	private final String floor3cfg = "0 1 -100\n1 2 2\n2 3 -100\n3 4 D\n4 5 -100\n5 6 2\n6 7 -100\n7 8 D\n8 9 D\n8 61 D\n8 10 D\n10 11 -100\n11 12 2\n12 13 -100\n13 14 D\n14 15 D\n15 16 -100\n16 17 2\n17 18 -100\n18 19 D\n14 20 D\n20 21 -100\n21 22 2\n22 23 -100\n23 24 D\n24 25 D\n25 26 D\n26 27 -100\n27 28 2\n28 29 -100\n29 30 D\n30 31 D\n29 31 D\n31 32 D\n32 33 D\n33 34 -100\n34 35 2\n35 36 -100\n36 37 D\n37 38 -100\n38 39 2\n39 40 -100\n40 14 D\n32 41 D\n31 41 D\n41 42 D\n42 43 -100\n43 44 2\n44 45 -100\n45 46 D\n42 47 D\n47 48 -100\n48 49 2\n49 50 -100\n50 51 D\n51 52 D\n52 53 -100\n53 54 2\n54 55 -100\n51 56 D\n56 57 -100\n57 58 2\n58 59 -100\n59 60 D\n60 3 D\n62 37 D\n36 62 D\n62 63 -100\n63 64 2\n64 65 -100\n65 66 D\n66 67 -100\n67 68 2\n68 69 -100\n37 70 D\n";
	private final String floor4cfg = "0 1 D\n0 11 D\n0 35 D\n1 2 -100\n2 3 2\n3 4 -100\n4 5 D\n5 6 D\n6 7 D\n7 8 D\n8 9 -100\n9 10 2\n10 11 -100\n6 12 -100\n12 13 2\n13 14 -100\n14 15 D\n15 16 D\n16 17 -100\n17 18 2\n18 19 -100\n19 20 D\n20 21 D\n21 24 D\n23 24 D\n23 4 D\n24 25 -100\n25 26 2\n26 27 -100\n27 28 D\n28 29 D\n29 30 -100\n30 31 2\n31 32 -100\n32 33 D\n33 34 D\n35 36 -100\n36 37 2\n37 38 -100\n38 39 D\n39 40 -100\n40 41 2\n41 42 -100\n42 43 D\n43 44 D\n44 45 -100\n45 46 2\n46 47 -100\n47 48 D\n48 49 -100\n49 50 2\n50 51 -100\n51 52 D\n52 53 D\n53 54 D\n";
	private Floor[] floors;
	private int[][][] edges;

	// Walking stuff
	private ArrayList<Integer> pathToWalk;
	private ArrayList<ArrayList<Integer>> path;
	private int[] best;
	private int currentFloor;
	private int pathI;

	private Position[] pathToSS;
	private Area bankArea = new Area(3098, 3499, 3091, 3488);

	// Security Questions!
	private HashMap<String, Integer> answers;

	/****************************** RANDOM HANDLERS ******************************/

	CMHEvilBobHandler bobHandler;
	RandomEventSolver loginHandler;

	public CMHStronghold(Script s) {
		loader = s;
		answers = new HashMap<String, Integer>();
	}

	private enum State {
		START, DESCEND, WALKTOROOM, FIGHT, ASCEND, WALKTOBANK, OPENDOOR_WALK, BANK, WALKTOSS, REWARD, STOP;
	}

	private class CleverbotThread implements Runnable {

		public void run() {
			while (true) {
				if (scriptStopped)
					break;
				if (responses.size() > responsesI) {
					for (; responsesI < responses.size(); responsesI++) {
						try {
							if (Math.random() < .25)
								loader.type(responses.get(responsesI)
										.toLowerCase());
							else if (Math.random() < .50)
								loader.type(responses
										.get(responsesI)
										.toLowerCase()
										.substring(
												0,
												responses.get(responsesI)
														.length() - 1));
							else if (Math.random() < .75)
								loader.type(responses.get(responsesI));
							else
								loader.type(responses.get(responsesI)
										.substring(
												0,
												responses.get(responsesI)
														.length() - 1));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				try {
					loader.sleep(random(1000));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private class AttackingThread implements Runnable {
		public void run() {
			while (true) {
				if (scriptStopped)
					break;
				try {
					List<NPC> allList = loader.client.getLocalNPCs();
					NPC ret = null;
					for (NPC n : allList) {
						if (n != null && n.getHealth() > 0) {
							if (n.isFacing(loader.myPlayer())
									&& n.getAnimation() != -1)
								attackingNPC = n;
						}
					}
					if (state == State.FIGHT) {
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} catch (Exception e) {
				}

			}
		}
	}

	public void onPlayerMessage(String name, String message) {
		if (useCleverbot && !name.equals(loader.myPlayer().getName())
				&& state == State.FIGHT) {
			cleverbot.getResponse(message);
			if (++messageCount > threshold) {
				cleverbot.recreateSession();
				threshold = (threshold - 2) + random(0, 4);
			}
		}
	}

	public void onStart() {
		log("Loading paint");

		new Thread(new CalculatePairs()).start();
		skills = loader.client.getSkills();
		freeTiles = new char[21][21];
		Scanner scan = new Scanner(
				"What do you do if someone asks you for your password or recoveries to make you a member for free? Click here to continue \n3\nWhere can I find cheats for RuneScape? Click here to continue \n3\nWill Jagex block me from saying my PIN in game? Click here to continue \n2\nWho is it ok to share my account with? Click here to continue \n3\nRecovery answers should be... Click here to continue \n1\nWhat do I do if a moderator asks me for my account details? Click here to continue \n3\nWhat do you do if someone asks you for your password or recoveries to make you a player moderator? Click here to continue \n3\nWhere can I find cheats for RuneScape? Click here to continue \n3\nCan I leave my account logged in while I'm out of the room? Click here to continue \n2\nWhat do I do if I think I have a keylogger or virus? Click here to continue \n1\nMy friend asks me for my password so that he can do a difficult quest for me. Do I give it to him? Click here to continue \n2\nWhy do I need to type in recovery questions? Click here to continue \n1\nMy friend uses this great add-on program he got from a website, should I? Click here to continue \n1\nWhat are recovery questions used for? Click here to continue \n1\nA website says I can become a player moderator by giving them my password, what do I do? Click here to continue \n3\nHow will Jagex contact me if I have been chosen to be a moderator? Click here to continue \n3\nHow often should you change your recovery questions? Click here to continue \n2\nWho can I give my password to? Click here to continue \n3\nWhat is an example of a good bank PIN? Click here to continue \n3\nHow do I set a bank PIN? Click here to continue \n1\nWhere should I enter my RuneScape password? Click here to continue \n2\nWhat should I do if I think someone knows my recovery answers? Click here to continue \n3\n");
		while (scan.hasNext()) {
			String key = scan.nextLine();
			int answer = Integer.parseInt(scan.nextLine());
			answers.put(key, answer);
			// log(key + " " + answer);
		}
		startAttackLevel = skills.getLevel(Skill.ATTACK);
		startDefenceLevel = skills.getLevel(Skill.DEFENCE);
		startStrengthLevel = skills.getLevel(Skill.STRENGTH);
		startRangeLevel = skills.getLevel(Skill.RANGED);
		startMagicLevel = skills.getLevel(Skill.MAGIC);
		startHitpointsLevel = skills.getLevel(Skill.HITPOINTS);
		startPrayerLevel = skills.getLevel(Skill.PRAYER);

		startAttackEXP = skills.getExperience(Skill.ATTACK);
		startDefenceEXP = skills.getExperience(Skill.DEFENCE);
		startStrengthEXP = skills.getExperience(Skill.STRENGTH);
		startRangeEXP = skills.getExperience(Skill.RANGED);
		startMagicEXP = skills.getExperience(Skill.MAGIC);
		startHitpointsEXP = skills.getExperience(Skill.HITPOINTS);
		startPrayerEXP = skills.getExperience(Skill.PRAYER);
		new Thread(new Runnable() {
			public void run() {
				try {
					paintImages[0] = ImageIO.read(new URL(
							"http://ericthecmh.com/osbot/paint/main.png"));
					paintImages[1] = ImageIO.read(new URL(
							"http://ericthecmh.com/osbot/paint/stats.png"));
					paintImages[2] = ImageIO.read(new URL(
							"http://ericthecmh.com/osbot/paint/script.png"));
					paintImages[3] = ImageIO.read(new URL(
							"http://ericthecmh.com/osbot/paint/feedback.png"));
					doneLoading = true;
				} catch (Exception e) {
				}
			}
		}).start();
		try {
			for (int i = 0; i < 10; i++)
				loader.sleep(random(400, 500));
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		log("Loading gui");

		login = new CMHStrongholdLogin();
		login.isAuth = true;
		login.isPrem = true;

		if (!login.isAuth) {
			JOptionPane.showMessageDialog(null,
					"You must be logged in to run this script");
			return;
		}
		isPrem = login.isPrem;
		CMHStrongholdGUI gui = new CMHStrongholdGUI(
				loader.myPlayer().getName(), login.isPrem, settingsVersion);
		gui.setVisible(true);
		while (true) {
			if (gui.ready || !gui.isVisible())
				break;
			try {
				Thread.sleep(500);
			} catch (InterruptedException ex) {
			}
		}

		try {
			room = gui.getRoom();
			floor = gui.getFloor();
			int npcs = gui.model2.getSize();
			attackNPCs = new ArrayList<AttackNPC>();
			for (int i = 0; i < npcs; i++) {
				String npc = (String) gui.model2.get(i);
				attackNPCs.add(new AttackNPC(npc.substring(0,
						npc.indexOf("[") - 1), Integer.parseInt(npc.substring(
						npc.lastIndexOf(' ') + 1, npc.indexOf("]")))));
			}
			useB2P = gui.useB2P.isSelected();
			b2pThresh = Integer.parseInt(gui.b2pThreshText.getText());
			int foods = gui.foodTableModel.getRowCount();
			// log(foods + "");
			eatFoods = new ArrayList<Food>();
			if (useB2P) {
				eatFoods.add(new Food("Peach", 0, Integer
						.parseInt(gui.b2pEatText.getText())));
			}
			for (int i = 0; i < foods; i++) {
				// log(gui.foodTableModel.getValueAt(0, 2) + "");
				eatFoods.add(new Food((String) gui.foodTableModel.getValueAt(i,
						0), (Integer) gui.foodTableModel.getValueAt(i, 1),
						(Integer) gui.foodTableModel.getValueAt(i, 2)));
			}
			int pots = gui.potionTableModel.getRowCount();
			drinkPots = new ArrayList<Potion>();
			for (int i = 0; i < pots; i++) {
				// log(gui.foodTableModel.getValueAt(0, 2) + "");
				drinkPots.add(new Potion((String) gui.potionTableModel
						.getValueAt(i, 0), (Integer) gui.potionTableModel
						.getValueAt(i, 1), (Integer) gui.potionTableModel
						.getValueAt(i, 2)));
			}
			int loots = gui.lootTableModel.getRowCount();
			lootItems = new ArrayList<Loot>();
			for (int i = 0; i < loots; i++) {
				lootItems.add(new Loot((String) gui.lootTableModel.getValueAt(
						i, 0), (Integer) gui.lootTableModel.getValueAt(i, 1),
						(Boolean) gui.lootTableModel.getValueAt(i, 2),
						(Boolean) gui.lootTableModel.getValueAt(i, 3),
						(Boolean) gui.lootTableModel.getValueAt(i, 4)));
			}
			int actions = gui.actionTableModel.getRowCount();
			actionsList = new ArrayList<Action>();
			for (int i = 0; i < actions; i++) {
				actionsList.add(new Action((String) gui.actionTableModel
						.getValueAt(i, 0), (Integer) gui.actionTableModel
						.getValueAt(i, 1), (String) gui.actionTableModel
						.getValueAt(i, 2), (String) gui.actionTableModel
						.getValueAt(i, 3)));
			}
			// keepIDs = new int[] { 562, 557, 556, 558, 555, 559, 560, 554,
			// 563,
			// 561, 564, 565 };
			keepIDs = parseList(gui.keepIDs.getText());
			if (gui.buryBones.isSelected()) {
				buryBones = true;
				buryBonesLevel = Integer.parseInt(gui.buryBonesLevel.getText());
			}
			if (gui.equipArrows.isSelected()) {
				equipArrow = true;
				arrowName = gui.arrowName.getText();
				arrowAmount = Integer.parseInt(gui.arrowAmount.getText());
			}
		} catch (Exception e) {
			log("Error when parsing GUI. Please make sure your input is valid.");
		}
		setRun = random(45, 55);
		state = State.START;
		canStart = true;
		lastAnim = System.currentTimeMillis();
		customRandomEvent = gui.randomEvent.isSelected();

		bobHandler = new CMHEvilBobHandler(loader);
		banking = gui.banking.isSelected();
		responses = new ArrayList<String>();
		useCleverbot = gui.cleverbot.isSelected();
		threshold = Integer.parseInt(gui.cleverbotThreshold.getText());
		useSpecialAttack = gui.specialAttack.isSelected();
		isPrem = gui.isPrem;
		if (useSpecialAttack) {
			useSpecialThresh = Integer
					.parseInt(gui.specialAttackText.getText());
			setSpecialAttack = Math.min(100,
					useSpecialThresh + MethodProvider.random(0, 10));
		}
		messageCount = 0;
		if (useCleverbot) {
			cleverbot = new CMHCleverbot(responses, 20000);
			new Thread(new CleverbotThread()).start();
		}
		bankPots = gui.bankingPots.isSelected();
		hopMod = gui.hopMod.isSelected();
		hopPeople = gui.hopPeople.isSelected();
		hopPeopleAmount = Integer.parseInt(gui.hopPeopleText.getText());
		useVarrockTab = gui.varrockTab.isSelected();
		breaks = new ArrayList<Break>();
		if (gui.takeBreaks.isSelected()) {
			takeBreaks = true;
			int minutes = 0;
			do {
				int duration = gui.avgBreakLength.getValue()
						- gui.stdBreakLength.getValue()
						+ random(0, 2 * gui.stdBreakLength.getValue());
				minutes += gui.avgBreakInt.getValue()
						- gui.stdBreakInt.getValue()
						+ random(0, 2 * gui.stdBreakInt.getValue());
				breaks.add(new Break(minutes, duration));
				minutes += duration;
			} while (minutes < 7200);
		}
		new Thread(new AttackingThread()).start();
		loader.randomManager.unregisterHook(RandomManager.STRANGE_BOX);
		loader.randomManager.registerHook(new RandomBehaviourHook(
				RandomManager.STRANGE_BOX) {

			boolean isRunning = false;

			@Override
			public boolean shouldActivate() {
				return loader.client.getInventory().contains("Strange box");
			}

			@Override
			public boolean shouldActivatePreLoop() {
				return false;
			}

			@Override
			public int preLoop() {
				return 0;
			}

			@Override
			public int onLoop() {
				isRunning = true;
				Thread rThread = new Thread(new Runnable() {
					public void run() {
						isRunning = true;
						solvingRandom = true;
						loader.log("Solving random");
						try {
							lastChange = System.currentTimeMillis();
							lastAnim = System.currentTimeMillis();
							if (loader.myPlayer().isUnderAttack()) {
								passedDoor2 = false;
								firstDoor = true;
								RS2Object door = loader
										.closestObjectForName(getDoorName(currentFloor));
								for (int i = 0; i < 10 && door == null; i++) {
									door = loader
											.closestObjectForName(getDoorName(currentFloor));
									loader.sleep(random(300, 400));
								}
								if (door != null) {
									for (int i = 0; i < 10
											&& !door.interact("Open"); i++)
										sleep(random(400, 500));
									for (int i = 0; i < 10
											&& !loader.myPlayer().isMoving(); i++)
										sleep(random(400, 500));
									for (int i = 0; i < 10
											&& loader.myPlayer().isMoving(); i++)
										sleep(random(400, 500));
									for (int i = 0; i < 15; i++)
										sleep(random(400, 500));
								} else {
									isRunning = false;
									return;
								}
							}
							if (loader.client.getInventory().contains(
									"Strange box")) {
								loader.log("Solving strange box. If something goes wrong, please contact me.");
								int oldconfig = loader.client.getConfig(312);
								for (int i = 0; i < 10
										&& !loader.client.getInventory()
												.interactWithName(
														"Strange box", "Open"); i++) {
									loader.sleep(random(500, 600));
								}
								for (int i = 0; loader.client.getConfig(312) == oldconfig; i++)
									loader.sleep(random(500, 600));
								int childid = (loader.client.getConfig(312) >> 24) + 10;
								loader.sleep(random(2000, 4000));
								RS2Interface parent = loader.client
										.getInterface(190);
								for (int i = 0; i < 10
										&& !parent.getChild(childid).interact(); i++)
									loader.sleep(random(500, 600));
								sleep(random(2000, 3000));
							}
						} catch (Exception e) {

						}
						solvingRandom = false;
						isRunning = false;
						loader.log("Done solving.");
					}
				});
				rThread.start();
				while (loader.client.getInventory().contains("Strange box")
						&& isRunning) {
					loader.log("Still solving");
					try {
						sleep(random(1000, 1200));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				rThread.interrupt();
				rThread.stop();
				solvingRandom = false;
				lastAnim = System.currentTimeMillis();
				return 0;
			}

			@Override
			public boolean shouldActivatePostLoop() {
				return false;
			}

			@Override
			public int postLoop() {
				return 0;
			}

		});

		loader.randomManager.unregisterHook(RandomManager.EXP_REWARDS);
		loader.randomManager.registerHook(new RandomBehaviourHook(
				RandomManager.EXP_REWARDS) {

			boolean isRunning = false;

			@Override
			public boolean shouldActivate() {
				return loader.client.getInventory().contains(
						"Book of knowledge");
			}

			@Override
			public boolean shouldActivatePreLoop() {
				return false;
			}

			@Override
			public int preLoop() {
				return 0;
			}

			@Override
			public int onLoop() {
				isRunning = true;
				Thread rThread = new Thread(new Runnable() {
					public void run() {
						isRunning = true;
						solvingRandom = true;
						loader.log("Solving random");
						try {
							lastChange = System.currentTimeMillis();
							lastAnim = System.currentTimeMillis();
							if (loader.myPlayer().isUnderAttack()) {
								passedDoor2 = false;
								firstDoor = true;
								RS2Object door = loader
										.closestObjectForName(getDoorName(currentFloor));
								for (int i = 0; i < 10 && door == null; i++) {
									door = loader
											.closestObjectForName(getDoorName(currentFloor));
									loader.sleep(random(300, 400));
								}
								if (door != null) {
									for (int i = 0; i < 10
											&& !door.interact("Open"); i++)
										sleep(random(400, 500));
									for (int i = 0; i < 10
											&& !loader.myPlayer().isMoving(); i++)
										sleep(random(400, 500));
									for (int i = 0; i < 10
											&& loader.myPlayer().isMoving(); i++)
										sleep(random(400, 500));
									for (int i = 0; i < 15; i++)
										sleep(random(400, 500));
								} else {
									isRunning = false;
									return;
								}
							}
							if (loader.client.getInventory().contains(
									"Book of knowledge")) {
								loader.log("Solving book of knowledge. If something goes wrong, please contact me.");
								int oldconfig = loader.client.getConfig(312);
								for (int i = 0; i < 10
										&& !loader.client.getInventory()
												.interactWithName(
														"Book of knowledge",
														"Read"); i++) {
									loader.sleep(random(500, 600));
								}
								loader.sleep(random(1000, 1200));
								RS2Interface parent = loader.client
										.getInterface(134);
								int highest = skills.getLevel(Skill.ATTACK);
								int childid = 3;
								if (skills.getLevel(Skill.DEFENCE) > highest) {
									highest = skills.getLevel(Skill.DEFENCE);
									childid = 7;
								}
								if (skills.getLevel(Skill.STRENGTH) > highest) {
									highest = skills.getLevel(Skill.STRENGTH);
									childid = 4;
								}
								if (skills.getLevel(Skill.MAGIC) > highest) {
									highest = skills.getLevel(Skill.MAGIC);
									childid = 6;
								}
								if (skills.getLevel(Skill.RANGED) > highest) {
									highest = skills.getLevel(Skill.RANGED);
									childid = 5;
								}
								if (skills.getLevel(Skill.HITPOINTS) > highest) {
									highest = skills.getLevel(Skill.HITPOINTS);
									childid = 8;
								}
								for (int i = 0; i < 10
										&& !parent.getChild(childid).interact(); i++)
									loader.sleep(random(500, 600));
								sleep(random(2000, 3000));
								for (int i = 0; i < 10
										&& !parent.getChild(26).interact(); i++)
									loader.sleep(random(500, 600));
								sleep(random(2000, 3000));
							}
						} catch (Exception e) {

						}
						solvingRandom = false;
						loader.log("Done solving.");
						isRunning = false;
					}
				});
				rThread.start();
				while (loader.client.getInventory().contains(
						"Book of knowledge")) {
					try {
						sleep(random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				rThread.interrupt();
				rThread.stop();
				solvingRandom = false;
				lastAnim = System.currentTimeMillis();
				return 0;
			}

			@Override
			public boolean shouldActivatePostLoop() {
				return true;
			}

			@Override
			public int postLoop() {
				solvingRandom = false;
				lastAnim = System.currentTimeMillis();
				return 0;
			}

		});
		/*
		 * loginHandler =
		 * loader.randomManager.forId(RandomManager.LOGIN_SCRIPT);
		 * loader.randomManager.unregisterHook(RandomManager.LOGIN_SCRIPT);
		 * loader.randomManager.registerHook(new RandomBehaviourHook(
		 * RandomManager.LOGIN_SCRIPT) {
		 * 
		 * @Override public boolean shouldActivate() throws InterruptedException
		 * { try { return !switchingWorlds && System.currentTimeMillis() -
		 * lastLoggedIn > 20000 && (loader.client.getInterface(548).getChild(85)
		 * .getPosition().getX() == -1 && loader.client
		 * .getInterface(548).getChild(85) .getPosition().getY() == -1) &&
		 * loader.client.getInterface(378).getChild(17) .getX() == -1; } catch
		 * (Exception e) { return true; } }
		 * 
		 * @Override public boolean shouldActivatePreLoop() { return false; }
		 * 
		 * @Override public int preLoop() { return 0; }
		 * 
		 * @Override public int onLoop() { solvingRandom = true; int pause = 0;
		 * System.out.println("Logging in."); try { loginHandler.onActivate();
		 * loginHandler.onLoop(); } catch (InterruptedException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } try {
		 * loader.sleep(random(1000, 2000)); if
		 * (!(loader.client.getInterface(548).getChild(85) .getPosition().getX()
		 * == -1 && loader.client .getInterface(548).getChild(85).getPosition()
		 * .getY() == -1)) lastLoggedIn = System.currentTimeMillis(); } catch
		 * (Exception e) { } solvingRandom = false; return pause; }
		 * 
		 * @Override public boolean shouldActivatePostLoop() { return false; }
		 * 
		 * @Override public int postLoop() { return 0; }
		 * 
		 * });
		 */
		/*
		 * if (customRandomEvent) {
		 * loader.randomManager.unregisterHook(RandomManager.EVIL_BOB);
		 * loader.randomManager.registerHook(bobHandler); }
		 */
		new Thread(new MonitorThread()).start();
		start = System.currentTimeMillis();
		lastRuntimeUpdate = start;
	}

	private int[] parseList(String s) {
		StringTokenizer st = new StringTokenizer(s, ",");
		ArrayList<Integer> list = new ArrayList<Integer>();
		try {
			while (st.hasMoreTokens()) {
				list.add(Integer.parseInt(st.nextToken().trim()));
			}
		} catch (Exception e) {
			log("Keep Items list has incorrect syntax");
		}
		int[] ret = new int[list.size()];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = list.get(i);
		}
		return ret;
	}

	private void executeAction(Action a) {
		if (a.action.equals("SWITCHSTYLE")) {
			try {
				int style = Integer.parseInt(a.details);
				if (loader.client.getConfig(43) != style - 2) {
					for (int i = 0; i < 10 && !loader.openTab(Tab.ATTACK); i++) {
						sleep(random(500, 600));
					}

					RS2Interface inte = loader.client.getInterface(81);
					if (inte != null) {
						RS2InterfaceChild child = inte.getChild(style);
						if (child != null) {
							child.hover();
							sleep(random(1000, 1100));
							child.interact();
							sleep(random(1000, 1100));
						}
					}
				}
			} catch (Exception e) {

			}
		} else if (a.action.equals("SWITCHROOM") && state != State.WALKTOROOM
				&& !a.triggered) {
			a.triggered = true;
			floor = Integer.parseInt(a.details.charAt(0) + "");
			floor--;
			room = Integer.parseInt(a.details.substring(2));
			if (loader.canReach(floors[floor].tilearr[room]))
				return;
			state = State.WALKTOROOM;
			Dijkstra(room, floor);
			pathToWalk = path.get(0);
			walkI = 0;
			firstDoor = true;
			lastChange = System.currentTimeMillis();
			lastPos = loader.myPosition();
			walkFailsafe = 0;
			returnTo = State.DESCEND;
		} else if (a.action.equals("STOPSCRIPT")) {
			if (loader.myPlayer().isUnderAttack()) {
				firstDoor = true;
				RS2Object door = loader
						.closestObjectForName(getDoorName(currentFloor));
				for (int i = 0; i < 10 && door == null; i++) {
					door = loader
							.closestObjectForName(getDoorName(currentFloor));
					try {
						loader.sleep(random(300, 400));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (door != null) {
					try {
						for (int i = 0; i < 10 && !door.interact("Open"); i++)
							sleep(random(400, 500));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					boolean question = false;
					boolean passedDoor = false;
					for (int i = 0; i < 20; i++) {
						if (!(loader.client.getInterface(240) == null
								&& loader.client.getInterface(241) == null
								&& loader.client.getInterface(242) == null
								&& loader.client.getInterface(243) == null && loader.client
									.getInterface(244) == null)) {
							question = true;
							break;
						}

						if (firstDoor
								&& loader.myPlayer().getAnimation() == 4283) {
							passedDoor = true;
							break;
						}
						if (loader.myPlayer().getAnimation() == 4282) {
							passedDoor = true;
							break;
						}
						try {
							sleep(random(500, 600));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (!question && !passedDoor)
						return;
					else if (!question && passedDoor) {
						if (!firstDoor)
							passedDoor = false;
						for (int i = 0; i < 10; i++) {
							if (loader.myPlayer().getAnimation() == 4283) {
								passedDoor = true;
								break;
							}
							try {
								sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						for (int i = 0; firstDoor && i < 10
								&& loader.myPlayer().getAnimation() == 4283; i++) {
							try {
								sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						if (passedDoor) {
							firstDoor = false;
							doNotMark = true;
							// log(walkI + "");
						} else {
							return;
						}
					}
				}
				try {
					loader.sleep(random(12000, 13000));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			scriptStopped = true;
			try {
				loader.stop();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private long updateTime;

	public int onLoop() {
		if (solvingRandom)
			return random(300, 400);
		if (canStart) {
			for (final Break b : breaks) {
				if (System.currentTimeMillis() > b.startTime
						&& System.currentTimeMillis() < (b.startTime + b.duration)) {
					loader.randomManager.registerHook(new RandomBehaviourHook(
							RandomManager.LOGIN_SCRIPT) {

						@Override
						public boolean shouldActivate() {
							return System.currentTimeMillis() > b.startTime
									&& System.currentTimeMillis() < (b.startTime + b.duration);
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							loader.log("Beginning break for " + b.duration
									/ 1000 / 60 + " minutes");
							if (loader.myPlayer().isUnderAttack()) {
								firstDoor = true;
								RS2Object door = loader
										.closestObjectForName(getDoorName(currentFloor));
								for (int i = 0; i < 10 && door == null; i++) {
									door = loader
											.closestObjectForName(getDoorName(currentFloor));
									try {
										loader.sleep(random(300, 400));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								if (door != null) {
									try {
										for (int i = 0; i < 10
												&& !door.interact("Open"); i++)
											sleep(random(400, 500));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									boolean question = false;
									boolean passedDoor = false;
									for (int i = 0; i < 20; i++) {
										if (!(loader.client.getInterface(240) == null
												&& loader.client
														.getInterface(241) == null
												&& loader.client
														.getInterface(242) == null
												&& loader.client
														.getInterface(243) == null && loader.client
												.getInterface(244) == null)) {
											question = true;
											break;
										}

										if (firstDoor
												&& loader.myPlayer()
														.getAnimation() == 4283) {
											passedDoor = true;
											break;
										}
										if (loader.myPlayer().getAnimation() == 4282) {
											passedDoor = true;
											break;
										}
										try {
											sleep(random(500, 600));
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
									if (!question && !passedDoor) {
										loader.randomManager
												.unregisterHook(RandomManager.LOGIN_SCRIPT);
										return 0;
									} else if (!question && passedDoor) {
										if (!firstDoor)
											passedDoor = false;
										for (int i = 0; i < 10; i++) {
											if (loader.myPlayer()
													.getAnimation() == 4283) {
												passedDoor = true;
												break;
											}
											try {
												sleep(random(500, 600));
											} catch (InterruptedException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										}
										for (int i = 0; firstDoor
												&& i < 10
												&& loader.myPlayer()
														.getAnimation() == 4283; i++) {
											try {
												sleep(random(500, 600));
											} catch (InterruptedException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										}
										if (passedDoor) {
											firstDoor = false;
											doNotMark = true;
											// log(walkI + "");
										} else {
											loader.randomManager
													.unregisterHook(RandomManager.LOGIN_SCRIPT);
											return 0;
										}
									}
								}
								try {
									loader.sleep(random(12000, 13000));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							loader.log("Logging out");
							try {
								for (int i = 0; i < 10
										&& !loader.logoutTab.logOut(); i++)
									loader.sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							try {
								while (!scriptStopped
										&& System.currentTimeMillis() > b.startTime
										&& System.currentTimeMillis() < (b.startTime + b.duration)) {
									loader.sleep(random(500, 600));
								}
							} catch (Exception e) {
							}
							loader.log("Break over!");
							loader.randomManager
									.unregisterHook(RandomManager.LOGIN_SCRIPT);
							return -1;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}

					});
				}
			}
			if (useB2P
					&& System.currentTimeMillis() - start > (1000 * 60 * 60 * 3)
					&& !isPrem) {
				loader.log("You have reached the 3 hour limit for free users.");
				Dijkstra(room, currentFloor);
				pathToWalk = path.get(0);
				walkI = 0;
				returnTo = State.STOP;
				state = State.OPENDOOR_WALK;
			}

			if (lumbyArea.currentlyInArea()) {
				loader.log("I've been idle too long. Shutting down.");
				loader.log("Your current status is " + state.toString());
				loader.log("If you have received this in error, please post the status above and what your status should be.");
				scriptStopped = true;
				try {
					loader.stop();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				if (state == State.FIGHT) {
					boolean actionTriggered = false;
					for (int i = actionsList.size() - 1; !actionTriggered
							&& i >= 0; i--) {
						Action a = actionsList.get(i);
						switch (a.skill.charAt(0)) {
						case 'A':
							if (skills.getLevel(Skill.ATTACK) >= a.level) {
								executeAction(a);
								actionTriggered = true;
							}
							break;
						case 'S':
							if (skills.getLevel(Skill.STRENGTH) >= a.level) {
								executeAction(a);
								actionTriggered = true;
							}
							break;
						case 'D':
							if (skills.getLevel(Skill.DEFENCE) >= a.level) {
								executeAction(a);
								actionTriggered = true;
							}
							break;
						case 'R':
							if (skills.getLevel(Skill.RANGED) >= a.level) {
								executeAction(a);
								actionTriggered = true;
							}
							break;
						case 'M':
							if (skills.getLevel(Skill.MAGIC) >= a.level) {
								executeAction(a);
								actionTriggered = true;
							}
							break;
						case 'T':
							if (System.currentTimeMillis() - start > a.level * 60 * 1000) {
								executeAction(a);
								actionTriggered = true;
							}
						}
					}
				}
			} catch (Exception e) {

			}
			try {
				RS2Interface inte = loader.client.getInterface(499);
				if (inte != null) {
					RS2InterfaceChild c = inte.getChild(24);
					if (c != null) {
						try {
							c.hover();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							c.interact();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

				if (!loader.isRunning()
						&& loader.client.getRunEnergy() > setRun) {
					try {
						setRunning2(true);
						setRun = random(45, 55);
						loader.sleep(random(500, 700));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				new Thread(new Runnable() {
					public void run() {
						try {
							loader.client.rotateCameraPitch(90);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}).start();
				switch (state) {
				case START:
					firstDoor = true;
					if (bankArea.currentlyInArea()
							|| loader.distance(new Position(3185, 3436, 0)) < 20) {
						bankFailsafe = 0;
						state = State.BANK;
						if (loader.distance(new Position(3185, 3436, 0)) < 20)
							pathI = 2;
						break;
					}

					state = State.DESCEND;

					if (loader.canReach(floors[floor].tilearr[room])) {
						firstDoor = true;
						state = State.FIGHT;
						freeTiles = new char[31][31];
						// log("Running flood fill");
						currentFloor = floor;
						floodFill(floodFillArray[currentFloor]);
						break;
					}
					for (int i = 0; i < 4; i++) {
						if (loader.distance(floors[i].tilearr[0]) < 10) {
							currentFloor = i;
							break;
						}
					}
					break;
				case DESCEND:
					RS2Object entrance = loader.closestObject(entranceID);
					if (entrance != null) {
						for (int i = 0; i < 10
								&& !entrance.interact("Climb-down"); i++)
							sleep(random(500, 600));
						for (int i = 0; i < 10
								&& distanceBetween(entrance.getPosition(),
										loader.myPosition()) < 10; i++) {

							sleep(random(500, 600));
						}
					}

					if (currentFloor == floor) {
						Dijkstra(0, currentFloor);
						pathToWalk = path.get(room);
						walkI = 0;
						firstDoor = true;
						lastChange = System.currentTimeMillis();
						lastPos = loader.myPosition();
						state = State.WALKTOROOM;
						walkFailsafe = 0;
						break;
					}
					if (Math.abs(System.currentTimeMillis() - lastChange) > 20000) {
						boolean foundLocation = false;
						for (int i = 0; i < 4; i++) {
							if (loader.distance(floors[i].tilearr[0]) < 10) {
								atMiddle = false;
								currentFloor = i;
								foundLocation = true;
								break;
							}
						}
						if (!foundLocation)
							atMiddle = true;
						lastChange = System.currentTimeMillis();
					}
					if (atMiddle) {
						RS2Object ladder = loader
								.closestObject(ladderIDs[currentFloor]);
						for (int i = 0; i < 10 && ladder == null; i++) {
							ladder = loader
									.closestObject(ladderIDs[currentFloor]);
							sleep(random(500, 600));
						}
						if (loader.distance(ladder.getPosition()) > 6) {
							walkMiniMap2(ladder.getPosition());
							for (int i = 0; i < 5
									&& !loader.myPlayer().isMoving(); i++) {
								sleep(random(500, 600));
							}
							for (int i = 0; i < 10
									&& loader.myPlayer().isMoving(); i++) {
								sleep(random(500, 600));
							}
						}
						for (int i = 0; i < 10
								&& !ladder.interact("Climb-down"); i++) {
							sleep(random(500, 600));
						}
						int id = getPopupInterface();
						Position p = loader.myPosition();
						for (int i = 0; i < 10
								&& (id == -1 && distanceBetween(p,
										loader.myPosition()) < 10); i++) {
							id = getPopupInterface();
							sleep(random(500, 600));
						}
						if (id != -1) {
							loader.client.getInterface(id).getChild(17)
									.interact();
							for (int i = 0; i < 10
									&& (distanceBetween(p, loader.myPosition()) < 20); i++) {
								sleep(random(500, 600));
							}
						}
						for (int i = 0; i < 10
								&& distanceBetween(p, loader.myPosition()) <= 10; i++)
							sleep(random(500, 600));
						if (distanceBetween(p, loader.myPosition()) > 10) {
							currentFloor++;
							atMiddle = false;
						}

					} else {
						if (currentFloor == 0) {
							RS2Object portal = loader
									.closestObject(portalIDs[currentFloor]);
							for (int i = 0; i < 10 && portal == null; i++) {
								portal = loader
										.closestObject(portalIDs[currentFloor]);
								sleep(random(500, 600));
							}
							if (portal != null) {
								if (loader.distance(portal) > 4) {
									walkMiniMap2(portal.getPosition());
									sleep(random(2000, 2100));
								}
								Position p = loader.myPosition();
								for (int i = 0; i < 10
										&& !portal.interact("Use"); i++) {
									sleep(random(500, 600));
								}
								for (int i = 0; i < 10
										&& distanceBetween(p,
												loader.myPosition()) < 10; i++) {

									sleep(random(500, 600));
								}
								atMiddle = true;
								break;
							}
						} else if (currentFloor == 1) {
							RS2Object portal = loader
									.closestObject(portalIDs[currentFloor]);
							for (int i = 0; i < 10 && portal == null; i++) {
								portal = loader
										.closestObject(portalIDs[currentFloor]);
								sleep(random(500, 600));
							}
							if (portal != null) {
								if (loader.distance(portal) > 4) {
									walkMiniMap2(portal.getPosition());
								}
								Position p = loader.myPosition();
								for (int i = 0; i < 10
										&& !portal.interact("Use"); i++) {
									sleep(random(500, 600));
								}
								for (int i = 0; i < 10
										&& distanceBetween(p,
												loader.myPosition()) < 10; i++) {
									if (Math.abs(System.currentTimeMillis()
											- messageStamp) < 5000
											&& scriptMessage
													.contains("sufficient"))
										break;
									sleep(random(500, 600));
								}
								if (distanceBetween(p, loader.myPosition()) < 10) {
									Dijkstra(0, currentFloor);
									pathToWalk = path.get(83);
									walkI = 0;
									returnTo = State.DESCEND;
									firstDoor = true;
									lastChange = System.currentTimeMillis();
									lastPos = loader.myPosition();
									state = State.WALKTOROOM;
									walkFailsafe = 0;
								}
								atMiddle = true;
								break;
							}
						} else if (currentFloor == 2) {
							RS2Object portal = loader
									.closestObject(portalIDs[currentFloor]);
							for (int i = 0; i < 10 && portal == null; i++) {
								portal = loader
										.closestObject(portalIDs[currentFloor]);
								sleep(random(500, 600));
							}
							if (portal != null) {
								if (loader.distance(portal) > 4) {
									walkMiniMap2(portal.getPosition());
								}
								Position p = loader.myPosition();
								for (int i = 0; i < 10
										&& !portal.interact("Use"); i++) {
									sleep(random(500, 600));
								}
								for (int i = 0; i < 10
										&& distanceBetween(p,
												loader.myPosition()) < 10; i++) {
									if (Math.abs(System.currentTimeMillis()
											- messageStamp) < 5000
											&& scriptMessage
													.contains("sufficient"))
										break;
									sleep(random(500, 600));
								}
								if (distanceBetween(p, loader.myPosition()) < 10) {
									Dijkstra(0, currentFloor);
									pathToWalk = path.get(69);
									walkI = 0;
									returnTo = State.DESCEND;
									firstDoor = true;
									lastChange = System.currentTimeMillis();
									lastPos = loader.myPosition();
									state = State.WALKTOROOM;
									walkFailsafe = 0;
								}
								atMiddle = true;
								break;
							}
						}
					}
					break;
				case STOP:
					loader.log("You are in a safe place. Terminating.");
					scriptStopped = true;
					loader.stop();
					break;
				case WALKTOROOM:
					if (System.currentTimeMillis() - lastChange > 30000) {
						walkI -= 2;
						if (walkI < 0)
							walkI = 0;
						walkFailsafe++;
						firstDoor = true;
					}
					if (pathToWalk.size() == walkI) {
						if (returnTo != null) {
							state = returnTo;
							returnTo = null;
							break;
						} else {
							sleep(random(2000, 3000));
							freeTiles = new char[31][31];
							// log("Running flood fill");

							floodFill(floodFillArray[currentFloor]);
							if (debug)
								JOptionPane.showMessageDialog(null, "At room");
							firstDoor = true;
							state = State.FIGHT;
							break;
						}
					}
					if (walkI > 0
							&& edges[pathToWalk.get(walkI - 1)][pathToWalk
									.get(walkI)][currentFloor] == -100) {
						RS2Object door = closestObjectTo(
								getDoorName(currentFloor),
								floors[currentFloor].tilearr[pathToWalk
										.get(walkI)]);
						for (int i = 0; i < 10 && door == null; i++) {
							door = closestObjectTo(getDoorName(currentFloor),
									floors[currentFloor].tilearr[pathToWalk
											.get(walkI)]);
							sleep(random(1000, 1100));
						}
						if (door != null) {
							for (int i = 0; i < 10 && !door.interact("Open"); i++)
								sleep(random(400, 500));
							boolean question = false;
							boolean passedDoor = false;
							for (int i = 0; i < 20; i++) {
								if (!(loader.client.getInterface(240) == null
										&& loader.client.getInterface(241) == null
										&& loader.client.getInterface(242) == null
										&& loader.client.getInterface(243) == null && loader.client
											.getInterface(244) == null)) {
									question = true;
									break;
								}

								if (firstDoor
										&& loader.myPlayer().getAnimation() == 4283) {
									passedDoor = true;
									break;
								}
								if (loader.myPlayer().getAnimation() == 4282) {
									passedDoor = true;
									break;
								}
								sleep(random(500, 600));
							}
							if (!question && !passedDoor)
								break;
							else if (!question && passedDoor) {
								if (!firstDoor)
									passedDoor = false;
								for (int i = 0; i < 10; i++) {
									if (loader.myPlayer().getAnimation() == 4283) {
										passedDoor = true;
										break;
									}
									sleep(random(500, 600));
								}
								for (int i = 0; firstDoor
										&& i < 10
										&& loader.myPlayer().getAnimation() == 4283; i++) {
									sleep(random(500, 600));
								}
								if (passedDoor) {
									firstDoor = !firstDoor;
									walkI++;
									// log(walkI + "");
								}
							} else if (question) {
								lastChange = System.currentTimeMillis();
								sleep(random(1000, 1100));
								solveSecurityQuestion();
								passedDoor = false;
								for (int i = 0; i < 100; i++) {
									if (loader.myPlayer().getAnimation() == 4283) {
										passedDoor = true;
										break;
									}
									sleep(random(100, 110));
								}
								if (passedDoor) {
									firstDoor = !firstDoor;
									walkI++;
									break;
									// log(walkI + "");
								}
								// JOptionPane.showMessageDialog(null,
								// "Question done, new walkI=" + walkI);
							}
						}
					} else {
						if (loader
								.distance(floors[currentFloor].tilearr[pathToWalk
										.get(walkI)]) > 4) {
							walkMiniMap2(randomize(floors[currentFloor].tilearr[pathToWalk
									.get(walkI++)]));
						} else
							walkI++;
						sleep(random(500, 600));
					}
					break;
				case FIGHT:
					entrance = loader.closestObject(entranceID);
					if (entrance != null) {
						state = State.DESCEND;
						walkI = 0;
					}
					if (attackingNPC != null && attackingNPC.getHealth() == 0) {
						updateServer();
					}
					try {
						doEat();
					} catch (Exception e) {
					}
					if (debugmsg)
						log("FIGHT");
					if (shouldHop()) {
						shouldHop = false;
						try {
							doHop();
						} catch (Exception e) {
						}
						break;
					}
					if (System.currentTimeMillis() - lastAnim > 10000) {
						attack = true;
					}
					if (attackingNPC != null
							&& attackingNPC.getHealth() != oldNPCHealth) {

						lastNPCHealthDrop = System.currentTimeMillis();
						oldNPCHealth = attackingNPC.getHealth();
					}
					if (System.currentTimeMillis() - lastNPCHealthDrop > 15000) {
						attack = true;
					}
					if (!inRange(loader.myPosition())) {
						try {
							openDoor2();
							loader.sleep(random(3000, 4000));
						} catch (Exception e) {
						}
					} else {
						firstDoor = true;
					}
					// if (attackingNPC != null)
					// log(attackingNPC.getHealth() + "");
					if (equipArrow) {
						// log(arrowName + " " +
						// loader.client.getInventory().getAmount(arrowName) +
						// " " + arrowAmount);
						try {
							equipArrows();
						} catch (Exception e) {
						}
					}
					if (shouldAttack()) {
						System.out.println("Attacking");
						attack = false;
						if (debugmsg)
							log("Checking all Combat Loot");
						try {
							takeLoot(false);
						} catch (Exception e) {
						}
						if (shouldBury()) {
							if (debugmsg)
								log("Burying bones");
							try {
								doBury();
							} catch (Exception e) {
							}
							try {
								for (int i = 0; i < 10
										&& loader.myPlayer().getAnimation() != -1; i++)
									loader.sleep(MethodProvider.random(1000,
											2000));
							} catch (Exception e) {
							}
						}
						if (debugmsg)
							log("Attacking");
						try {
							doAttack(0);
						} catch (Exception e) {
						}
					} else {
						if (debugmsg)
							log("Checking combat loot");
						try {
							takeLoot(true);
						} catch (Exception e) {
						}
						if (!shouldAttack()) {
							if (debugmsg)
								log("Antiban");
							Thread antibanThread = new Thread(new Runnable() {
								public void run() {
									doAntiban();
								}
							});
							antibanThread.start();
							for (int i = 0; i < 10 && antiban; i++) {
								loader.sleep(random(800, 900));
							}
							if (antiban) {
								antiban = false;
								antibanThread.interrupt();
								antibanThread.stop();
							}
						}
						if (shouldActivateSpecial()) {
							if (debugmsg)
								log("Activating special attack");
							try {
								activateSpecial();
							} catch (Exception e) {
							}
						}
					}

					if (System.currentTimeMillis() - lastAnim > 15000) {
						for (int i = 0; i < 10; i++) {
							if (loader.myPlayer().getAnimation() != -1) {
								lastAnim = System.currentTimeMillis();
								break;
							}
							loader.sleep(random(500, 600));
						}
						if (System.currentTimeMillis() - lastAnim > 15000) {
							try {
								openDoor1();
								attack = true;
							} catch (Exception e) {
							}
						}
					}
					if (useB2P) {
						if (debugmsg)
							log("Bones to peaches");
						try {
							doB2P();
						} catch (Exception e) {
						}
					}

					int foodleft = getFoodCount();
					int potsleft = getPotionsCount();
					try {
						doPots();
					} catch (Exception e) {
					}
					if (shouldBank(foodleft, potsleft)) {
						try {
							if (useVarrockTab
									&& loader.client.getInventory().contains(
											"Varrock teleport")) {
								for (int i = 0; i < 10
										&& !loader.client.getInventory()
												.interactWithName(
														"Varrock teleport",
														"Break"); i++) {
									loader.sleep(random(500, 600));
								}
								for (int i = 0; i < 100
										&& loader.distance(varrockCenter) > 20; i++) {
									loader.sleep(random(500, 600));
								}
								loader.sleep(random(1000, 2000));
								walkI = 0;
								pathI = 2;
								state = State.WALKTOBANK;
								break;
							}
							Dijkstra(room, currentFloor);
							pathToWalk = path.get(0);
							walkI = 0;
							returnTo = (banking ? State.ASCEND : State.STOP);
							state = State.OPENDOOR_WALK;
						} catch (Exception e) {
						}
					}
					break;
				case REWARD:

					break;
				case OPENDOOR_WALK:
					if (walkI > 0
							&& edges[pathToWalk.get(walkI - 1)][pathToWalk
									.get(walkI)][currentFloor] == -100) {
						RS2Object door = closestObjectTo(
								getDoorName(currentFloor),
								floors[currentFloor].tilearr[pathToWalk
										.get(walkI)]);
						for (int i = 0; i < 10 && door == null; i++) {
							door = closestObjectTo(getDoorName(currentFloor),
									floors[currentFloor].tilearr[pathToWalk
											.get(walkI)]);
							sleep(random(1000, 1100));
						}
						if (door != null) {
							for (int i = 0; i < 10 && !door.interact("Open"); i++)
								sleep(random(400, 500));
							boolean question = false;
							boolean passedDoor = false;
							for (int i = 0; i < 20; i++) {
								if (!(loader.client.getInterface(240) == null
										&& loader.client.getInterface(241) == null
										&& loader.client.getInterface(242) == null
										&& loader.client.getInterface(243) == null && loader.client
											.getInterface(244) == null)) {
									question = true;
									break;
								}
								if (loader.myPlayer().getAnimation() == 4282) {
									passedDoor = true;
									break;
								}
								sleep(random(500, 600));
							}
							if (!question && !passedDoor)
								break;
							else if (!question && passedDoor) {
								passedDoor = false;
								for (int i = 0; i < 10; i++) {
									if (loader.myPlayer().getAnimation() == 4283) {
										passedDoor = true;
										break;
									}
									sleep(random(500, 600));
								}
								for (int i = 0; i < 2
										&& loader.myPlayer().isMoving(); i++)
									sleep(random(1000, 1100));
								sleep(random(1000, 1100));
								walkI++;
							} else if (question) {
								lastChange = System.currentTimeMillis();
								sleep(random(1000, 1100));
								solveSecurityQuestion();
								passedDoor = false;
								for (int i = 0; i < 100; i++) {
									if (loader.myPlayer().getAnimation() == 4283) {
										passedDoor = true;
										break;
									}
									sleep(random(100, 110));
								}
								if (passedDoor) {
									walkI++;
									break;
									// log(walkI + "");
								}
								// JOptionPane.showMessageDialog(null,
								// "Question done, new walkI=" + walkI);
							}
						}
					}
					firstDoor = true;
					lastChange = System.currentTimeMillis();
					lastPos = loader.myPosition();
					state = State.WALKTOROOM;
					walkFailsafe = 0;
					break;
				case ASCEND:

					if (currentFloor == -1) {
						pathI = random(0, 1);
						walkI = 0;
						state = State.WALKTOBANK;
						break;
					}
					if (Math.abs(System.currentTimeMillis() - lastChange) > 20000) {
						boolean foundLocation = false;
						for (int i = 0; i < 4; i++) {
							if (loader.distance(floors[i].tilearr[0]) < 10) {
								foundLocation = true;
								currentFloor = i;
								break;
							}
						}
						if (!foundLocation)
							currentFloor = -1;
						lastChange = System.currentTimeMillis();
					}
					RS2Object ladder = loader
							.closestObjectForName(getLadderName(currentFloor));
					for (int i = 0; i < 10 && ladder == null; i++) {
						ladder = loader
								.closestObjectForName(getLadderName(currentFloor));
						sleep(random(500, 600));
					}
					if (ladder != null) {
						Position p = loader.myPosition();
						for (int i = 0; i < 10 && !ladder.interact("Climb-up"); i++) {
							sleep(random(500, 600));
						}
						for (int i = 0; i < 10
								&& loader.myPlayer().getAnimation() == -1; i++) {
							loader.sleep(random(500, 600));
						}
						for (int i = 0; i < 10
								&& distanceBetween(p, loader.myPosition()) < 10; i++) {

							sleep(random(500, 600));
						}
						if (currentFloor == 0) {
							if (loader.distance(pathToBank[0][0]) < 20) {
								currentFloor = -1;
							}
						} else {
							if (loader
									.distance(floors[currentFloor - 1].tilearr[0]) < 20)
								currentFloor--;
						}
						break;
					}
					break;
				case WALKTOBANK:
					foodleft = 0;
					for (Food f : eatFoods) {
						foodleft += loader.client.getInventory().getAmount(
								f.name);
					}
					if (foodleft > 0) {
						firstDoor = true;
						state = State.FIGHT;
					}
					if (loader.distance(floors[0].tilearr[0]) < 10) {
						state = State.ASCEND;
					}
					if (walkI == 0) {

						try {
							walkMiniMap2(randomize(pathToBank[pathI][walkI++]));
							break;
						} catch (InterruptedException e) {
						}
					}
					if (walkI == pathToBank[pathI].length) {
						walkI = 0;
						bankFailsafe = 0;
						state = State.BANK;
						break;
					}
					try {
						if (loader.realDistance(pathToBank[pathI][walkI - 1]) <= 6) {
							walkMiniMap2(randomize(pathToBank[pathI][walkI++]));
						} else
							walkMiniMap2(randomize(pathToBank[pathI][walkI - 1]));

					} catch (InterruptedException e) {

					}
					break;
				case BANK:
					if (loader.distance(floors[0].tilearr[0]) < 10) {
						state = State.ASCEND;
					}
					Bank b = loader.client.getBank();
					for (int i = 0; i < 10 && !b.isOpen(); i++) {
						RS2Object booth = loader
								.closestObjectForName("Bank booth");
						try {
							loader.sleep(random(300, 400));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (booth == null)
							break;
						try {
							booth.interact("Bank");
							loader.sleep(random(500, 700));
							for (int j = 0; j < 10 && !b.isOpen(); j++) {
								loader.sleep(random(200, 300));
								if (loader.myPlayer().isMoving())
									j--;
							}
						} catch (Exception e) {
						}
					}
					try {
						for (int i = 0; i < 10 && !b.depositAllExcept(keepIDs); i++)
							loader.sleep(random(900, 1100));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (useVarrockTab) {
						for (int i = 0; i < 10 && !b.withdraw1(8007); i++)
							loader.sleep(random(900, 1000));
					}
					Item[] items = b.getItems();
					boolean foodInBank = false;
					if (items != null) {
						try {
							for (int it = 0; it < items.length
									&& !loader.client.getInventory().isFull(); it++) {
								for (Food f : eatFoods) {
									if (f.amount == 0)
										continue;
									if (items[it].getName().equals(f.name)) {
										foodInBank = true;
										if (f.amount == 28)

											for (int i = 0; i < 10
													&& !b.withdrawAll(items[it]
															.getId()); i++)
												sleep(random(500, 600));
										else
											for (int i = 0; i < 10
													&& !b.withdrawX(
															items[it].getId(),
															f.amount); i++)
												sleep(random(500, 600));
										for (int i = 0; i < 10
												&& loader.client.getInventory()
														.getAmount(f.name) == 0; i++)
											sleep(random(500, 600));
									}
								}
								for (Potion f : drinkPots) {

									int potionsTaken = (int) loader.client
											.getInventory().getAmount(
													f.name + "(1)")
											+ (int) loader.client
													.getInventory().getAmount(
															f.name + "(2)")
											+ (int) loader.client
													.getInventory().getAmount(
															f.name + "(3)")
											+ (int) loader.client
													.getInventory().getAmount(
															f.name + "(4)")
											+ (int) loader.client
													.getInventory().getAmount(
															f.name + "(5)");
									for (int n = 5; n >= 1
											&& potionsTaken < f.amount; n--) {
										if (items[it].getName().equals(
												f.name + "(" + n + ")")) {
											if (f.amount == 28)
												for (int i = 0; i < 10
														&& !b.withdrawAll(items[it]
																.getId()); i++)
													sleep(random(500, 600));
											else
												for (int i = 0; i < 10
														&& !b.withdrawX(
																items[it]
																		.getId(),
																f.amount); i++)
													sleep(random(500, 600));
											for (int i = 0; i < 10
													&& loader.client
															.getInventory()
															.getAmount(
																	f.name
																			+ "("
																			+ n
																			+ ")") == 0; i++)
												sleep(random(500, 600));
											potionsTaken = (int) loader.client
													.getInventory().getAmount(
															f.name + "(1)")
													+ (int) loader.client
															.getInventory()
															.getAmount(
																	f.name
																			+ "(2)")
													+ (int) loader.client
															.getInventory()
															.getAmount(
																	f.name
																			+ "(3)")
													+ (int) loader.client
															.getInventory()
															.getAmount(
																	f.name
																			+ "(4)")
													+ (int) loader.client
															.getInventory()
															.getAmount(
																	f.name
																			+ "(5)");
										}
									}
								}
							}
						} catch (Exception e) {

						}
					}
					if (!foodInBank) {
						loader.log("Out of food in bank. Stopping.");
						scriptStopped = true;
						loader.stop();
						break;
					}
					// log("Checking inventory");
					if (true/* checkInventoryAmount() */) {
						pathToSS = (pathI == 2 ? pathFromVarrock
								: reverse(pathToBank[random(0, 1)]));
						state = State.WALKTOSS;
						currentFloor = 0;
						if (debug)
							JOptionPane.showMessageDialog(null,
									"Finished Banking");
						break;
					}
				case WALKTOSS:
					if (distanceBetween(floors[0].tilearr[0],
							loader.myPosition()) < 10)
						state = State.DESCEND;
					if (walkI == 0) {

						try {

							walkMiniMap2(randomize(pathToSS[walkI++]));
							break;
						} catch (InterruptedException e) {
						}
					}
					if (walkI == pathToSS.length) {
						if (loader.distance(pathToSS[walkI - 1]) <= 3) {
							log("Entrance Reached");
							for (int i = 0; i < 10
									&& loader.myPlayer().isMoving(); i++) {
								sleep(random(500, 600));
							}
							entrance = loader.closestObject(entranceID);
							for (int i = 0; i < 10 && entrance == null; i++) {
								entrance = loader.closestObject(entranceID);
								sleep(random(500, 600));
							}
							for (int i = 0; i < 10
									&& !entrance.interact("Climb-down"); i++)
								sleep(random(500, 600));
							for (int i = 0; i < 10
									&& distanceBetween(entrance.getPosition(),
											loader.myPosition()) < 10; i++) {

								sleep(random(500, 600));
							}

						} else {
							log("Waiting for Entrance");
							if (!loader.myPlayer().isMoving()) {
								walkMiniMap2(pathToSS[walkI - 1]);
								sleep(random(600, 700));
							}
						}
						break;
					}
					try {
						if (loader.realDistance(pathToSS[walkI - 1]) <= 6) {
							walkMiniMap2(randomize(pathToSS[walkI++]));
						} else
							walkMiniMap2(randomize(pathToSS[walkI - 1]));
						if (!loader.isRunning()
								&& loader.client.getRunEnergy() > 0) {
							try {
								setRunning2(true);
								loader.sleep(random(500, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} catch (InterruptedException e) {

					}
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return random(300, 400);
	}

	private void updateServer() {
		new Thread(new Runnable() {
			public void run() {
				try {
					int gainedTotal = gainedAttackEXP + gainedStrengthEXP
							+ gainedDefenceEXP + gainedRangeEXP
							+ gainedMagicEXP + gainedHitpointsEXP
							+ gainedPrayerEXP;
					URL url = new URL(
							"http://cmhscripts.no-ip.org/update.php?script=CMHStronghold&Username="
									+ loader.myPlayer().getName()
									+ "&Runtime="
									+ (System.currentTimeMillis() - lastRuntimeUpdate)
									+ "&ExpGained="
									+ (gainedTotal - lastExpUpdate)
									+ "&MonstersKilled=1" + "&OSBotName="
									+ loader.getBot().getUsername());
					lastRuntimeUpdate = System.currentTimeMillis();
					lastExpUpdate = gainedTotal;
					url.openConnection().getContent();
				} catch (Exception e) {

				}
			}
		}).start();
	}

	private boolean shouldBank(int foodleft, int potsleft) {
		return (foodleft == 0 && eatFoods.size() > (useB2P ? 1 : 0))
				|| (potsleft == 0 && bankPots && drinkPots.size() > 0)
				&& !(useB2P && loader.client.getInventory().getAmount("Bones") >= b2pThresh);
	}

	private void doPots() {
		for (Potion p : drinkPots) {
			for (int i = 1; i <= 5; i++) {
				if (skills.getCurrentLevel(p.skill) - skills.getLevel(p.skill) < p.eat) {
					Item food = loader.client.getInventory().getItemForName(
							p.name + "(" + i + ")");
					if (food != null) {
						int oldHealth = skills.getCurrentLevel(p.skill);
						try {
							for (int j = 0; j < 10
									&& !loader.client.getInventory()
											.interactWithName(
													p.name + "(" + i + ")",
													"Drink"); j++) {
								sleep(random(500, 600));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						loader.log("Drank " + p.name + " at level " + oldHealth);
						for (int j = 0; j < 10
								&& oldHealth == skills.getCurrentLevel(p.skill); j++) {
							try {
								sleep(random(200, 300));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						p.eat = p.realeat - 1 + MethodProvider.random(0, 2);
					}
				}
			}
		}
	}

	private int getPotionsCount() {
		int potsleft = 0;
		for (Potion p : drinkPots) {
			for (int i = 1; i <= 5; i++) {
				potsleft += loader.client.getInventory().getAmount(
						p.name + "(" + i + ")");
			}
		}
		return potsleft;
	}

	private void doEat() {
		for (Food f : eatFoods) {
			if (loader.myPlayer().getHealth() < f.eat) {
				Item food = loader.client.getInventory().getItemForName(f.name);
				if (food != null) {
					int oldHealth = loader.myPlayer().getHealth();
					try {
						for (int j = 0; j < 10
								&& !loader.client.getInventory()
										.interactWithName(f.name, "Eat"); j++) {
							sleep(random(500, 600));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					for (int j = 0; j < 10
							&& oldHealth == loader.myPlayer().getHealth(); j++) {
						try {
							sleep(random(200, 300));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					f.eat = f.realeat - 5 + MethodProvider.random(0, 10);
				}
			}
		}
	}

	private int getFoodCount() {
		int foodleft = 0;
		for (Food f : eatFoods) {
			foodleft += loader.client.getInventory().getAmount(f.name);
		}
		return foodleft;
	}

	private void doB2P() {
		if (loader.client.getInventory().getAmount("Bones") >= b2pThresh) {
			if (loader.client.getInventory().isFull()
					&& !loader.client.getInventory().contains("Peach")
					|| (loader.myPlayer().getHealth() <= eatFoods.get(0).eat && !loader.client
							.getInventory().contains("Peach"))) {
				if (loader.client.getInventory().contains("Bones to peaches")) {
					long oldcount = loader.client.getInventory().getAmount(
							"Peach");
					try {
						for (int i = 0; i < 10
								&& !loader.client.getInventory()
										.interactWithName("Bones to peaches",
												"Break"); i++) {
							loader.sleep(random(500, 600));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					for (int i = 0; i < 10
							&& (oldcount == loader.client.getInventory()
									.getAmount("Peach")); i++) {
						try {
							loader.sleep(random(500, 1000));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		} else {
			if ((loader.myPlayer().getHealth() <= eatFoods.get(0).eat && !loader.client
					.getInventory().contains("Peach"))) {
				for (int b = 0; b < 28
						&& !loader.client.getInventory().isFull(); b++) {
					GroundItem lootItem = getClosestGroundItemForName("Bones");
					if (lootItem == null)
						break;
					if (inRange(lootItem.getPosition())) {
						try {
							for (int i = 0; i < 10
									&& !lootItem.interact("Take"); i++) {
								sleep(random(500, 600));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							sleep(random(1000, 1100));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						for (int i = 0; loader.myPlayer().isMoving(); i++) {
							try {
								sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} else {
						break;
					}
				}
				try {
					loader.sleep(random(1000, 2000));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (loader.client.getInventory().getAmount("Bones") >= b2pThresh) {
					if (loader.client.getInventory().isFull()
							|| (loader.myPlayer().getHealth() <= eatFoods
									.get(0).eat && !loader.client
									.getInventory().contains("Peach"))) {
						if (loader.client.getInventory().contains(
								"Bones to peaches")) {
							long oldcount = loader.client.getInventory()
									.getAmount("Peach");
							try {
								for (int i = 0; i < 10
										&& !loader.client.getInventory()
												.interactWithName(
														"Bones to peaches",
														"Break"); i++) {
									loader.sleep(random(500, 600));
								}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							for (int i = 0; i < 10
									&& (oldcount == loader.client
											.getInventory().getAmount("Peach")); i++) {
								try {
									loader.sleep(random(500, 1000));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					}
				}
			}
		}
	}

	private void openDoor2() {
		if (!firstDoor && !doNotMark) {
			setRange(loader.closestObjectForName(getDoorName(currentFloor))
					.getPosition());
			setRange(loader.closestObjectForName(getDoorName(currentFloor))
					.getPosition());
		}
		doNotMark = false;
		firstDoor = !firstDoor;
		RS2Object door = loader.closestObjectForName(getDoorName(currentFloor));
		for (int i = 0; i < 10 && door == null; i++) {
			door = closestObjectTo(getDoorName(currentFloor),
					floors[currentFloor].tilearr[pathToWalk.get(walkI)]);
			try {
				sleep(random(1000, 1100));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (door != null) {
			try {
				for (int i = 0; i < 10 && !door.interact("Open"); i++)
					sleep(random(400, 500));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			boolean question = false;
			boolean passedDoor = false;
			for (int i = 0; i < 20; i++) {
				if (!(loader.client.getInterface(240) == null
						&& loader.client.getInterface(241) == null
						&& loader.client.getInterface(242) == null
						&& loader.client.getInterface(243) == null && loader.client
							.getInterface(244) == null)) {
					question = true;
					break;
				}
				if (loader.myPlayer().getAnimation() == 4282) {
					passedDoor = true;
					break;
				}
				try {
					sleep(random(500, 600));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (!question && !passedDoor)
				return;
			else if (!question && passedDoor) {
				passedDoor = false;
				for (int i = 0; i < 10; i++) {
					if (loader.myPlayer().getAnimation() != -1) {
						passedDoor = true;
						break;
					}
					try {
						sleep(random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (passedDoor) {
					try {
						sleep(random(2000, 3000));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return;
					// log(walkI + "");
				}
			} else if (question) {
				lastChange = System.currentTimeMillis();
				try {
					sleep(random(1000, 1100));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				solveSecurityQuestion();
				passedDoor = false;
				for (int i = 0; i < 100; i++) {
					if (loader.myPlayer().getAnimation() != -1) {
						passedDoor = true;
						break;
					}
					try {
						sleep(random(100, 110));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (passedDoor) {
					try {
						sleep(random(2000, 3000));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return;
					// log(walkI + "");
				}
				// JOptionPane.showMessageDialog(null,
				// "Question done, new walkI=" + walkI);
			}
		}
	}

	private void openDoor1() {
		lastAnim = System.currentTimeMillis();
		firstDoor = false;
		try {
			RS2Object door = loader
					.closestObjectForName(getDoorName(currentFloor));
			for (int i = 0; i < 10 && door == null; i++) {
				door = closestObjectTo(getDoorName(currentFloor),
						floors[currentFloor].tilearr[pathToWalk.get(walkI)]);
				sleep(random(1000, 1100));
			}
			if (door != null) {
				for (int i = 0; i < 10 && !door.interact("Open"); i++)
					sleep(random(400, 500));
				boolean question = false;
				boolean passedDoor = false;
				for (int i = 0; i < 20; i++) {
					if (!(loader.client.getInterface(240) == null
							&& loader.client.getInterface(241) == null
							&& loader.client.getInterface(242) == null
							&& loader.client.getInterface(243) == null && loader.client
								.getInterface(244) == null)) {
						question = true;
						break;
					}
					if (loader.myPlayer().getAnimation() == 4282) {
						passedDoor = true;
						break;
					}
					sleep(random(500, 600));
				}
				if (!question && !passedDoor)
					return;
				else if (!question && passedDoor) {
					passedDoor = false;
					for (int i = 0; i < 10; i++) {
						if (loader.myPlayer().getAnimation() != -1) {
							passedDoor = true;
							break;
						}
						sleep(random(500, 600));
					}
					if (passedDoor) {
						lastAnim = System.currentTimeMillis();
						sleep(random(2000, 3000));
						return;
						// log(walkI + "");
					}
				} else if (question) {
					lastChange = System.currentTimeMillis();
					sleep(random(1000, 1100));
					solveSecurityQuestion();
					passedDoor = false;
					for (int i = 0; i < 100; i++) {
						if (loader.myPlayer().getAnimation() != -1) {
							passedDoor = true;
							break;
						}
						sleep(random(100, 110));
					}
					if (passedDoor) {
						lastAnim = System.currentTimeMillis();
						sleep(random(2000, 3000));
						return;
						// log(walkI + "");
					}
					// JOptionPane.showMessageDialog(null,
					// "Question done, new walkI=" + walkI);
				}
			}
		} catch (Exception e) {
		}
	}

	private void activateSpecial() {
		try {
			for (int i = 0; i < 10 && !loader.openTab(Tab.ATTACK); i++)
				loader.sleep(random(500, 600));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		RectangleDestination specialAttack = new RectangleDestination(572, 416,
				140, 12);
		try {
			loader.client.moveMouseTo(specialAttack, false, false, false);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for (int i = 0; i < 10
				&& !(loader.client.getMousePosition().getX() > 570
						&& loader.client.getMousePosition().getX() < 714
						&& loader.client.getMousePosition().getY() > 414 && loader.client
						.getMousePosition().getY() < 430); i++) {
			try {
				loader.sleep(random(200, 300));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			loader.client.clickMouse(false);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		setSpecialAttack = useSpecialThresh + MethodProvider.random(0, 10);
		if (!(!loader.myPlayer().isUnderAttack() || (attack || (attackingNPC == null || attackingNPC
				.getHealth() == 0))))
			try {
				loader.sleep(random(1000, 1200));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	private boolean shouldActivateSpecial() {
		return useSpecialAttack && !loader.combat.isSpecialActivated()
				&& loader.combat.getSpecialPercentage() > setSpecialAttack;
	}

	private void doAntiban() {
		if (Math.random() < .10) {
			antiban = true;
			try {
				int r = random(9);
				Rectangle k = new Rectangle(1 + random(515), 1 + random(330),
						10, 10);
				switch (r) {
				case 0:
				case 1:
				case 2:
					loader.client.moveMouseTo(new RectangleDestination(k),
							false, false, false);
					break;
				case 3:
				case 6:
					int i = random(15);
					loader.openTab(Tab.SKILLS);
					loader.sleep(random(300, 400));
					try {
						loader.client.getInterface(320).getChild(122 + i)
								.hover();
					} catch (NullPointerException e) {
						loader.sleep(random(1000, 1200));
						return;
					}
					loader.sleep(random(2500, 3500));
					loader.openTab(Tab.INVENTORY);
					break;
				case 4:
				case 5:
				case 9:
				case 8:
					loader.client.rotateCameraToAngle(random(360));
					break;
				}
			} catch (InterruptedException e) {
			}
			antiban = false;
		}
	}

	private void doAttack(int count) {
		if (count == 5 || scriptStopped)
			return;
		NPC npc = closestAttackableNPC(attackNPCs);
		if (npc != null) {
			try {
				for (int i = 0; i < 5 && !npc.interact("Attack"); i++) {
					sleep(random(300, 400));
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			boolean attackSuccess = false;
			if (debugmsg)
				loader.log("Waiting for you to do something");
			for (int i = 0; i < 10; i++) {
				if (loader.myPlayer().getAnimation() != -1) {
					attackSuccess = true;
					break;
				}
				if (loader.myPlayer().isMoving())
					break;
				try {
					loader.sleep(random(60, 70));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (!attackSuccess) {
				if (loader.myPlayer().isMoving()) {
					if (debugmsg)
						loader.log("Waiting for you to stop moving");
					for (int i = 0; i < 20; i++) {
						if (!loader.myPlayer().isMoving())
							break;
						try {
							loader.sleep(random(250, 300));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				if (debugmsg)
					loader.log("Waiting for animation");
				for (int i = 0; i < 5; i++) {
					if (loader.myPlayer().getAnimation() != -1) {
						attackSuccess = true;
						break;
					}
					try {
						loader.sleep(random(200, 300));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			if (!attackSuccess) {
				if (debugmsg)
					loader.log("Attack failed. Reattacking");
				doAttack(count + 1);
				return;
			}
			attackingLock = true;
			attackingNPC = npc;
			attackingLock = false;
			for (int i = 0; i < 3
					&& !((attackingNPC == null || attackingNPC.getHealth() == 0 || !attackingNPC
							.isFacing(loader.myPlayer())) || loader.myPlayer()
							.getFacing() == null); i++)
				try {
					sleep(random(500, 600));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	private void doBury() {
		if (loader.client.getInventory().getAmount("Bones") > 0) {
			Item food = loader.client.getInventory().getItemForName("Bones");
			if (food != null) {
				long oldcount = loader.client.getInventory().getAmount("Bones");
				try {
					for (int j = 0; j < 10
							&& !loader.client.getInventory().interactWithName(
									"Bones", "Bury"); j++) {
						sleep(random(500, 600));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int j = 0; j < 10
						&& oldcount == loader.client.getInventory().getAmount(
								"Bones"); j++) {
					try {
						sleep(random(200, 300));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		if (loader.client.getInventory().getAmount("Wolf bones") > 0) {
			Item food = loader.client.getInventory().getItemForName(
					"Wolf bones");
			if (food != null) {
				long oldcount = loader.client.getInventory().getAmount(
						"Wolf bones");
				try {
					for (int j = 0; j < 10
							&& !loader.client.getInventory().interactWithName(
									"Wolf bones", "Bury"); j++) {
						sleep(random(500, 600));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int j = 0; j < 10
						&& oldcount == loader.client.getInventory().getAmount(
								"Wolf bones"); j++) {
					try {
						sleep(random(200, 300));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	private boolean shouldBury() {
		return buryBones
				&& loader.client.getSkills().getLevel(Skill.PRAYER) < buryBonesLevel;
	}

	private void takeLoot(boolean combat) {
		for (Loot l : lootItems) {
			if (combat && !l.combat)
				continue;
			GroundItem lootItem = getClosestGroundItemForName(l.name);
			if (lootItem == null)
				continue;
			if (inRange(lootItem.getPosition())) {
				if (lootItem.getAmount() >= l.number) {
					for (int i = 0; i < 10
							&& loader.client.getInventory().isFull()
							&& l.eat
							&& (!l.stackable || (!loader.client.getInventory()
									.contains(l.name) && !loader.client
									.getInventory().contains(getInt(l.name)))); i++) {
						for (Food f : eatFoods) {
							Item food = loader.client.getInventory()
									.getItemForName(f.name);
							if (food != null) {
								int oldHealth = loader.myPlayer().getHealth();
								try {
									for (int j = 0; j < 10
											&& !loader.client.getInventory()
													.interactWithName(f.name,
															"Eat"); j++) {
										sleep(random(500, 600));
									}
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								for (int j = 0; j < 10
										&& oldHealth == loader.myPlayer()
												.getHealth(); j++) {
									try {
										sleep(random(200, 300));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								break;
							}
						}
					}
					if (!loader.client.getInventory().isFull()
							|| (l.stackable && (loader.client.getInventory()
									.contains(l.name) || loader.client
									.getInventory().contains(getInt(l.name))))) {
						try {
							for (int i = 0; i < 10
									&& !lootItem.interact("Take"); i++) {
								sleep(random(500, 600));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							sleep(random(1000, 1100));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					for (int i = 0; i < 10 && loader.myPlayer().isMoving(); i++) {
						try {
							sleep(random(500, 600));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	private boolean shouldAttack() {
		return (attack || (attackingNPC == null || attackingNPC.getHealth() == 0));
	}

	private void equipArrows() {
		if (loader.client.getInventory().getAmount(arrowName) > arrowAmount) {
			try {
				for (int i = 0; i < 10
						&& !loader.client.getInventory().interactWithName(
								arrowName, "Wield"); i++) {
					sleep(random(500, 600));
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		for (int i = 0; i < 10
				&& loader.client.getInventory().getAmount(arrowName) > 0; i++)
			try {
				sleep(random(500, 600));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	private void doHop() {
		try {
			if (loader.myPlayer().isUnderAttack()) {
				firstDoor = true;
				RS2Object door = loader
						.closestObjectForName(getDoorName(currentFloor));
				for (int i = 0; i < 10 && door == null; i++) {
					door = loader
							.closestObjectForName(getDoorName(currentFloor));
					loader.sleep(random(300, 400));
				}
				if (door != null) {
					for (int i = 0; i < 10 && !door.interact("Open"); i++)
						sleep(random(400, 500));
					boolean question = false;
					boolean passedDoor = false;
					for (int i = 0; i < 20; i++) {
						if (!(loader.client.getInterface(240) == null
								&& loader.client.getInterface(241) == null
								&& loader.client.getInterface(242) == null
								&& loader.client.getInterface(243) == null && loader.client
									.getInterface(244) == null)) {
							question = true;
							break;
						}

						if (firstDoor
								&& loader.myPlayer().getAnimation() == 4283) {
							passedDoor = true;
							break;
						}
						if (loader.myPlayer().getAnimation() == 4282) {
							passedDoor = true;
							break;
						}
						sleep(random(500, 600));
					}
					if (!question && !passedDoor)
						return;
					else if (!question && passedDoor) {
						if (!firstDoor)
							passedDoor = false;
						for (int i = 0; i < 10; i++) {
							if (loader.myPlayer().getAnimation() == 4283) {
								passedDoor = true;
								break;
							}
							sleep(random(500, 600));
						}
						for (int i = 0; firstDoor && i < 10
								&& loader.myPlayer().getAnimation() == 4283; i++) {
							sleep(random(500, 600));
						}
						if (passedDoor) {
							firstDoor = false;
							doNotMark = true;
							// log(walkI + "");
						} else {
							return;
						}
					}
				}
				loader.sleep(random(12000, 13000));
			}

			loader.log("Hopping worlds...");
			doneLoading = false;
			Thread worldHopperThread = new Thread(new Runnable() {
				public void run() {
					try {
						loader.worldHopper.hopWorld(getRandomWorld());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					doneLoading = true;
				}
			});
			worldHopperThread.start();
			while (!doneLoading) {
				if(loader.myPlayer().isUnderAttack()){
					worldHopperThread.interrupt();
					worldHopperThread.stop();
					doneLoading = true;
					break;
				}
			}

		} catch (Exception e) {
		}
	}

	private boolean shouldHop() {
		return shouldHop || (jagexModNearby() && hopMod)
				|| (hopPeople && getPeopleCount() > hopPeopleAmount);
	}

	private boolean blacklisted(int world) {
		for (int i = 0; i < blacklistedWorlds.length; i++)
			if (blacklistedWorlds[i] == world)
				return true;
		return false;
	}

	private int getRandomWorld() {
		int world;
		do {
			world = random(305, 375);
		} while (blacklisted(world) || world == loader.client.getCurrentWorld());
		return world;
	}

	private int getInt(String a) {
		try {
			return Integer.parseInt(a);
		} catch (Exception e) {
			return -1;
		}
	}

	private int getPeopleCount() {
		int count = 0;
		List<Player> players = loader.client.getLocalPlayers();
		for (Player p : players) {
			if (inRange(p.getPosition()))
				count++;
		}
		return count;
	}

	private boolean jagexModNearby() {
		List<Player> players = loader.client.getLocalPlayers();
		for (Player p : players) {
			if (p.getName().contains("Mod") && loader.distance(p) < 20)
				return true;
		}
		return false;
	}

	public void onLogin(int a) {
		loader.log("Logged in.");
	}

	private Position[] reverse(Position[] arr) {
		Position[] ret = new Position[arr.length];
		int index = 0;
		for (int i = arr.length - 2; i >= 0; i--)
			ret[index++] = arr[i];
		ret[index++] = new Position(3080, 3421, 0);
		return ret;
	}

	private boolean checkInventoryAmount() {
		for (Food f : eatFoods) {
			if (loader.client.getInventory().getAmount(f.name) == 0)
				return false;
		}
		return true;
	}

	private String getLadderName(int i) {
		switch (i) {
		case 3:
			return "Boney ladder";
		case 2:
			return "Dripping vine";
		case 1:
		case 0:
		default:
			return "Ladder";
		}
	}

	private void walkMiniMap2(Position p) throws InterruptedException {
		final int mapX = 643, mapY = 83;
		double distance = distanceTo(p);
		double angle2 = Math.toDegrees(Math.atan2(p.getY()
				- loader.myPosition().getY(), p.getX()
				- loader.myPosition().getX())) - 90;
		double dY = distance * Math.sqrt(17);
		double rdX = -1
				* Math.sin(Math.toRadians(angle2
						- loader.client.getCameraYawAngle())) * dY;
		double rdY = Math.cos(Math.toRadians(angle2
				- loader.client.getCameraYawAngle()))
				* dY;
		RectangleDestination rd = new RectangleDestination(
				(int) (mapX + rdX - 1), (int) (mapY - rdY - 1), 3, 3);
		loader.client.moveMouseTo(rd, false, false, false);
		for (int i = 0; i < 5
				&& (Math.abs(loader.client.getMousePosition().getX()
						- (int) (mapX + rdX)) > 2 || Math.abs(loader.client
						.getMousePosition().getY() - (int) (mapY - rdY)) > 2); i++) {
			loader.sleep(random(500, 600));

		}
		if ((Math.abs(loader.client.getMousePosition().getX()
				- (int) (mapX + rdX)) <= 2 && Math.abs(loader.client
				.getMousePosition().getY() - (int) (mapY - rdY)) <= 2)) {
			loader.sleep(random(500, 600));
			loader.client.clickMouse(false);
			for (int i = 0; i < 2 && !loader.myPlayer().isMoving(); i++) {
				loader.sleep(random(500, 600));
			}
			for (int i = 0; i < 60 && loader.myPlayer().isMoving()
					&& distanceTo(p) > 6; i++) {
				loader.sleep(random(500, 600));
			}
		}
	}

	private double distanceTo(Position p) {
		Position myP = loader.myPosition();
		return Math.sqrt((p.getX() - myP.getX()) * (p.getX() - myP.getX())
				+ (p.getY() - myP.getY()) * (p.getY() - myP.getY()));
	}

	private void walkMiniMap2NoBlock(Position p) throws InterruptedException {
		final int mapX = 643, mapY = 83;
		double distance = distanceTo(p);
		double angle2 = Math.toDegrees(Math.atan2(p.getY()
				- loader.myPosition().getY(), p.getX()
				- loader.myPosition().getX())) - 90;
		double dY = distance * Math.sqrt(17);
		double rdX = -1
				* Math.sin(Math.toRadians(angle2
						- loader.client.getCameraYawAngle())) * dY;
		double rdY = Math.cos(Math.toRadians(angle2
				- loader.client.getCameraYawAngle()))
				* dY;
		System.out.println((p.getY() - loader.myPosition().getY()) + ","
				+ (p.getX() - loader.myPosition().getX()));
		System.out.println(angle2 + loader.client.getCameraYawAngle() + " "
				+ rdX + " " + rdY);
		RectangleDestination rd = new RectangleDestination(
				(int) (mapX + rdX - 1), (int) (mapY - rdY - 1), 3, 3);
		loader.client.moveMouseTo(rd, false, false, false);
		for (int i = 0; i < 5
				&& (Math.abs(loader.client.getMousePosition().getX()
						- (int) (mapX + rdX)) > 2 || Math.abs(loader.client
						.getMousePosition().getY() - (int) (mapY - rdY)) > 2); i++) {
			loader.sleep(random(500, 600));

		}
		if ((Math.abs(loader.client.getMousePosition().getX()
				- (int) (mapX + rdX)) <= 2 && Math.abs(loader.client
				.getMousePosition().getY() - (int) (mapY - rdY)) <= 2)) {
			loader.sleep(random(500, 600));
			loader.client.clickMouse(false);
			for (int i = 0; i < 2 && !loader.myPlayer().isMoving(); i++) {
				loader.sleep(random(500, 600));
			}
			for (int i = 0; i < 60 && loader.myPlayer().isMoving()
					&& distanceTo(p) > 6; i++) {
				loader.sleep(random(500, 600));
			}
		}
	}

	private void walkMiniMap2(Entity e) throws InterruptedException {
		walkMiniMap2(e.getPosition());
	}

	private void floodFill(int... floor) {
		for (int i = 0; i < freeTiles.length; i++)
			for (int j = 0; j < freeTiles.length; j++)
				freeTiles[i][j] = '?';
		int myX = loader.myX();
		floodX = myX;
		int myY = loader.myY();
		floodY = myY;
		freeTiles[freeTiles.length / 2][freeTiles.length / 2] = '0';
		List<RS2Object> objects = loader.client.getCurrentRegion().getObjects();
		for (RS2Object o : objects) {
			if (notInArray(o.getId(), floor)) {
				if (o.getY() - myY + freeTiles.length / 2 < freeTiles.length
						&& o.getY() - myY + freeTiles.length / 2 > 0
						&& o.getX() - myX + freeTiles.length / 2 < freeTiles.length
						&& o.getX() - myX + freeTiles.length / 2 > 0)
					freeTiles[o.getX() - myX + freeTiles.length / 2][o.getY()
							- myY + freeTiles.length / 2] = 'X';
			}
		}
		floodHelper(freeTiles.length / 2, freeTiles.length / 2);
	}

	private boolean notInArray(int id, int[] floors) {
		for (int i = 0; i < floors.length; i++) {
			if (id == floors[i])
				return false;
		}
		return true;
	}

	private void floodHelper(int x, int y) {
		freeTiles[x][y] = '0';
		if (x + 1 < freeTiles.length && freeTiles[x + 1][y] == '?') {
			floodHelper(x + 1, y);
		}
		if (y + 1 < freeTiles.length && freeTiles[x][y + 1] == '?') {
			floodHelper(x, y + 1);
		}
		if (x - 1 > 0 && freeTiles[x - 1][y] == '?') {
			floodHelper(x - 1, y);
		}
		if (y - 1 > 0 && freeTiles[x][y - 1] == '?') {
			floodHelper(x, y - 1);
		}
	}

	private int getPopupInterface() {
		if (loader.client.getInterface(579) != null)
			return 579;
		return -1;
	}

	private int getDialogInterface() {
		if (loader.client.getInterface(228) != null)
			return 228;
		if (loader.client.getInterface(230) != null)
			return 230;
		if (loader.client.getInterface(240) != null)
			return 240;
		if (loader.client.getInterface(241) != null)
			return 241;
		if (loader.client.getInterface(242) != null)
			return 242;
		if (loader.client.getInterface(243) != null)
			return 243;
		if (loader.client.getInterface(244) != null)
			return 244;
		return -1;
	}

	private boolean inRange(NPC n) {
		Position o = n.getPosition();
		if (o.getY() - floodY + freeTiles.length / 2 < freeTiles.length
				&& o.getY() - floodY + freeTiles.length / 2 > 0
				&& o.getX() - floodX + freeTiles.length / 2 < freeTiles.length
				&& o.getX() - floodX + freeTiles.length / 2 > 0)
			return freeTiles[o.getX() - floodX + freeTiles.length / 2][o.getY()
					- floodY + freeTiles.length / 2] == '0';
		else
			return false;
	}

	private boolean inRange(Position o) {
		if (o.getY() - floodY + freeTiles.length / 2 < freeTiles.length
				&& o.getY() - floodY + freeTiles.length / 2 > 0
				&& o.getX() - floodX + freeTiles.length / 2 < freeTiles.length
				&& o.getX() - floodX + freeTiles.length / 2 > 0)
			return freeTiles[o.getX() - floodX + freeTiles.length / 2][o.getY()
					- floodY + freeTiles.length / 2] == '0';
		else
			return false;
	}

	private void setRange(Position o) {
		if (o.getY() - floodY + freeTiles.length / 2 < freeTiles.length
				&& o.getY() - floodY + freeTiles.length / 2 > 0
				&& o.getX() - floodX + freeTiles.length / 2 < freeTiles.length
				&& o.getX() - floodX + freeTiles.length / 2 > 0)
			freeTiles[o.getX() - floodX + freeTiles.length / 2][o.getY()
					- floodY + freeTiles.length / 2] = '0';
	}

	private NPC closestAttackableNPC(ArrayList<AttackNPC> list) {
		double dist = 2000000000;
		List<NPC> allList = loader.client.getLocalNPCs();
		NPC ret = null;
		for (AttackNPC npc : list) {
			for (NPC n : allList) {
				if (n != null
						&& n.getHealth() > 0
						&& ((n.getName().equals(npc.name)
								&& n.getLevel() == npc.level
								&& !n.isUnderAttack() && n.getAnimation() == -1 && inRange(n)) || n
									.isFacing(loader.myPlayer()))) {
					if (n.isFacing(loader.myPlayer()) && n.getAnimation() != -1)
						return n;
					if (loader.distance(n.getPosition()) < dist) {
						dist = loader.distance(n.getPosition());
						ret = n;
					}
				}
			}
		}
		return ret;
	}

	private class MonitorThread implements Runnable {
		public void run() {
			while (!scriptStopped) {
				if (loader.myPlayer().getAnimation() != -1) {
					lastAnim = System.currentTimeMillis();
				}
				if (lastPos != null) {
					if (loader.myX() != lastPos.getX()
							|| loader.myY() != lastPos.getY()) {
						lastChange = System.currentTimeMillis();
						lastPos = loader.myPosition();
					}
				} else {
					lastPos = loader.myPosition();
					lastChange = System.currentTimeMillis();
				}
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void onMessage(String s) {
		scriptMessage = s;
		messageStamp = System.currentTimeMillis();
		if (s.contains("reach")) {
			firstDoor = false;
			try {
				RS2Object door = loader
						.closestObjectForName(getDoorName(currentFloor));
				for (int i = 0; i < 10 && door == null; i++) {
					door = closestObjectTo(getDoorName(currentFloor),
							floors[currentFloor].tilearr[pathToWalk.get(walkI)]);
					sleep(random(1000, 1100));
				}
				if (door != null) {
					for (int i = 0; i < 10 && !door.interact("Open"); i++)
						sleep(random(400, 500));
					boolean question = false;
					boolean passedDoor = false;
					for (int i = 0; i < 20; i++) {
						if (!(loader.client.getInterface(240) == null
								&& loader.client.getInterface(241) == null
								&& loader.client.getInterface(242) == null
								&& loader.client.getInterface(243) == null && loader.client
									.getInterface(244) == null)) {
							question = true;
							break;
						}
						if (loader.myPlayer().getAnimation() == 4282) {
							passedDoor = true;
							break;
						}
						sleep(random(500, 600));
					}
					if (!question && !passedDoor)
						return;
					else if (!question && passedDoor) {
						passedDoor = false;
						for (int i = 0; i < 10; i++) {
							if (loader.myPlayer().getAnimation() != -1) {
								passedDoor = true;
								break;
							}
							sleep(random(500, 600));
						}
						if (passedDoor) {
							sleep(random(2000, 3000));
							return;
							// log(walkI + "");
						}
					} else if (question) {
						lastChange = System.currentTimeMillis();
						sleep(random(1000, 1100));
						solveSecurityQuestion();
						passedDoor = false;
						for (int i = 0; i < 100; i++) {
							if (loader.myPlayer().getAnimation() != -1) {
								passedDoor = true;
								break;
							}
							sleep(random(100, 110));
						}
						if (passedDoor) {
							sleep(random(2000, 3000));
							return;
							// log(walkI + "");
						}
						// JOptionPane.showMessageDialog(null,
						// "Question done, new walkI=" + walkI);
					}
				}
			} catch (Exception e) {
			}
		}
	}

	private double distanceBetween(Position p, Position p1) {
		return Math.sqrt((p.getX() - p1.getX()) * (p.getX() - p1.getX())
				+ (p.getY() - p1.getY()) * (p.getY() - p1.getY()));
	}

	private void clickContinue(RS2Interface inte) throws InterruptedException {
		String oldmessage = inte.getChild(2).getMessage();
		for (RS2InterfaceChild c : inte.getChildren()) {
			if (c.getMessage().contains("here to cont")) {
				for (int i = 0; i < 10 && !c.interact(); i++) {
					sleep(random(500, 600));
				}
			}
		}
		for (int i = 0; i < 10; i++) {
			int id = getDialogInterface();
			if (id != -1) {
				if (!(oldmessage.equals(loader.client.getInterface(id)
						.getChild(2).getMessage())))
					break;
			} else {
				break;
			}
			sleep(random(500, 600));
		}
	}

	private boolean solveSecurityQuestion() {
		int id = getDialogInterface();
		if (id == -1)
			return false;
		if (id == 244) {
			RS2Interface inte = loader.client.getInterface(244);
			try {
				for (int i = 0; i < 10
						&& !loader.client.getInterface(244).getChild(6)
								.interact(); i++) {
					sleep(random(500, 600));
				}
				for (int i = 0; i < 10; i++) {
					id = getDialogInterface();
					if (id == 242)
						break;
					sleep(random(500, 600));
				}
				if (id != 242)
					return false;
				inte = loader.client.getInterface(242);
				return inte.getChild(4).interact();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			RS2Interface inte = loader.client.getInterface(id);
			try {
				String message = "";
				for (RS2InterfaceChild c : inte.getChildren()) {
					message += c.getMessage() + ' ';
				}
				clickContinue(inte);
				id = getDialogInterface();
				if (id == -1)
					return false;
				if (!answers
						.containsKey(message.substring(message.indexOf(':') + 2))) {
					log("Question unknown: "
							+ message.substring(message.indexOf(':') + 2));
					return false;
				}
				System.out.println(message.substring(message.indexOf(':') + 2));
				for (int i = 0; i < 10
						&& !loader.client
								.getInterface(id)
								.getChild(
										answers.get(message.substring(message
												.indexOf(':') + 2))).interact(); i++) {
					sleep(random(500, 600));
				}
				sleep(random(1000, 1100));
				id = getDialogInterface();
				for (int i = 0; i < 10; i++) {
					id = getDialogInterface();
					if (id != -1)
						break;
					sleep(random(500, 600));
				}
				// log(id + "");
				if (id != -1) {
					clickContinue(loader.client.getInterface(id));
				}
				return true;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	public void onExit() {
		scriptStopped = true;
		log("Thank you for using CMHStronghold!");
		log("Total EXP Gained: "
				+ (gainedAttackEXP + gainedDefenceEXP + gainedStrengthEXP
						+ gainedRangeEXP + gainedMagicEXP + gainedHitpointsEXP + gainedPrayerEXP));
		log("Total Levels Gained: "
				+ (gainedAttackLevel + gainedDefenceLevel + gainedStrengthLevel
						+ gainedRangeLevel + gainedMagicLevel
						+ gainedHitpointLevel + gainedPrayerLevel));
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getX() >= 447 && e.getX() <= 512 && e.getY() <= 363
				&& e.getY() >= 350) {
			hide = !hide;
		}
	}

	private String pad(int i) {
		String ret = i + "";
		if (ret.length() == 1) {
			return "0" + ret;
		}
		return ret;
	}

	public void onPaint(Graphics g) {
		if (!canStart)
			return;
		if (hide) {
			g.setColor(Color.BLUE);
			g.fillRect(447, 350, 65, 13);
			return;
		}
		g.setColor(new Color(0, 1, 0, 0.25f));
		if ((state == State.WALKTOROOM || state == State.FIGHT) && showFlood) {
			int d = -freeTiles.length / 2;
			for (int i = 0; i < freeTiles.length; i++) {
				for (int j = 0; j < freeTiles.length; j++) {
					if (freeTiles[i][j] != '0')
						continue;
					Position p = new Position(floodX + d + i, floodY + d + j, 0);
					if (!p.isVisible(loader.getBot()))
						continue;
					int[] polyX = new int[4];
					int[] polyY = new int[4];
					Point[] points = p.getVertices(loader.getBot());
					for (int x = 0; x < 4; x++) {
						polyX[x] = (int) points[x].getX();
						polyY[x] = (int) points[x].getY();
					}
					g.fillPolygon(polyX, polyY, 4);
				}
			}
		}
		current = System.currentTimeMillis() - start;
		hours = (int) (current / 3600000);
		current %= 3600000;
		minutes = (int) (current / 60000);
		current %= 60000;
		seconds = (int) (current / 1000);

		gainedAttackEXP = skills.getExperience(Skill.ATTACK) - startAttackEXP;
		gainedStrengthEXP = skills.getExperience(Skill.STRENGTH)
				- startStrengthEXP;
		gainedDefenceEXP = skills.getExperience(Skill.DEFENCE)
				- startDefenceEXP;
		gainedRangeEXP = skills.getExperience(Skill.RANGED) - startRangeEXP;
		gainedMagicEXP = skills.getExperience(Skill.MAGIC) - startMagicEXP;
		gainedHitpointsEXP = skills.getExperience(Skill.HITPOINTS)
				- startHitpointsEXP;
		gainedPrayerEXP = skills.getExperience(Skill.PRAYER) - startPrayerEXP;

		if (gainedAttackEXP != 0) {
			hoursToLevel = (int) (experienceToNextLevel(Skill.ATTACK) / (attackPerHour + 0.0));
			minutesToLevel = (int) (((experienceToNextLevel(Skill.ATTACK) / (attackPerHour + 0.0)) - hoursToLevel) * 60);
			secondsToLevel = (int) (((((experienceToNextLevel(Skill.ATTACK) / (attackPerHour + 0.0)) - hoursToLevel) * 60) - minutesToLevel) * 60);

		} else if (gainedDefenceEXP != 0) {
			hoursToLevel = (int) (experienceToNextLevel(Skill.DEFENCE) / (defencePerHour + 0.0));
			minutesToLevel = (int) (((experienceToNextLevel(Skill.DEFENCE) / (defencePerHour + 0.0)) - hoursToLevel) * 60);
			secondsToLevel = (int) (((((experienceToNextLevel(Skill.DEFENCE) / (defencePerHour + 0.0)) - hoursToLevel) * 60) - minutesToLevel) * 60);

		} else if (gainedStrengthEXP != 0) {
			hoursToLevel = (int) (experienceToNextLevel(Skill.STRENGTH) / (strengthPerHour + 0.0));
			minutesToLevel = (int) (((experienceToNextLevel(Skill.STRENGTH) / (strengthPerHour + 0.0)) - hoursToLevel) * 60);
			secondsToLevel = (int) (((((experienceToNextLevel(Skill.STRENGTH) / (strengthPerHour + 0.0)) - hoursToLevel) * 60) - minutesToLevel) * 60);

		} else if (gainedRangeEXP != 0) {
			hoursToLevel = (int) (experienceToNextLevel(Skill.RANGED) / (rangePerHour + 0.0));
			minutesToLevel = (int) (((experienceToNextLevel(Skill.RANGED) / (rangePerHour + 0.0)) - hoursToLevel) * 60);
			secondsToLevel = (int) (((((experienceToNextLevel(Skill.RANGED) / (rangePerHour + 0.0)) - hoursToLevel) * 60) - minutesToLevel) * 60);

		} else if (gainedMagicEXP != 0) {
			hoursToLevel = (int) (experienceToNextLevel(Skill.MAGIC) / (magicPerHour + 0.0));
			minutesToLevel = (int) (((experienceToNextLevel(Skill.MAGIC) / (magicPerHour + 0.0)) - hoursToLevel) * 60);
			secondsToLevel = (int) (((((experienceToNextLevel(Skill.MAGIC) / (magicPerHour + 0.0)) - hoursToLevel) * 60) - minutesToLevel) * 60);

		}

		attackPerHour = (int) ((gainedAttackEXP) * 3600000D / (System
				.currentTimeMillis() - start));
		strengthPerHour = (int) ((gainedStrengthEXP) * 3600000D / (System
				.currentTimeMillis() - start));
		defencePerHour = (int) ((gainedDefenceEXP) * 3600000D / (System
				.currentTimeMillis() - start));
		rangePerHour = (int) ((gainedRangeEXP) * 3600000D / (System
				.currentTimeMillis() - start));
		magicPerHour = (int) ((gainedMagicEXP) * 3600000D / (System
				.currentTimeMillis() - start));
		prayerPerHour = (int) ((gainedPrayerEXP) * 3600000D / (System
				.currentTimeMillis() - start));
		hitpointsPerHour = (int) ((gainedHitpointsEXP) * 3600000D / (System
				.currentTimeMillis() - start));

		gainedAttackLevel = skills.getLevel(Skill.ATTACK) - startAttackLevel;
		gainedStrengthLevel = skills.getLevel(Skill.STRENGTH)
				- startStrengthLevel;
		gainedDefenceLevel = skills.getLevel(Skill.DEFENCE) - startDefenceLevel;
		gainedRangeLevel = skills.getLevel(Skill.RANGED) - startRangeLevel;
		gainedMagicLevel = skills.getLevel(Skill.MAGIC) - startMagicLevel;
		gainedHitpointLevel = skills.getLevel(Skill.HITPOINTS)
				- startHitpointsLevel;
		gainedPrayerLevel = skills.getLevel(Skill.PRAYER) - startPrayerLevel;

		g.setColor(java.awt.Color.RED);
		g.setFont(new Font("Arial", Font.BOLD, 12));
		g.drawImage(paintImages[Math.max(0, currentFloor)], tabX + offsetX,
				tabY + offsetY, null);
		g.drawString((antiban ? "ANTIBAN" : state.toString()), 325 + tabX
				+ offsetX, 58 + tabY + offsetY);
		g.drawString(pad(hours) + ":" + pad(minutes) + ":" + pad(seconds), 163
				+ tabX + offsetX, 26 + tabY + offsetY);
		g.drawString(
				(attackPerHour + defencePerHour + strengthPerHour
						+ rangePerHour + magicPerHour + prayerPerHour + hitpointsPerHour)
						+ "", 160 + tabX + offsetX, 58 + tabY + offsetY);
		g.drawString(
				""
						+ (gainedAttackEXP + gainedDefenceEXP
								+ gainedStrengthEXP + gainedRangeEXP
								+ gainedMagicEXP + gainedHitpointsEXP + gainedPrayerEXP),
				160 + tabX + offsetX, 88 + tabY + offsetY);
		g.drawString(pad(hoursToLevel) + ":" + pad(minutesToLevel) + ":"
				+ pad(secondsToLevel), 325 + tabX + offsetX, 90 + tabY
				+ offsetY);
		if (gainedAttackEXP != 0) {
			g.drawString(skills.getLevel(Skill.ATTACK) + "", 367 + tabX
					+ offsetX, 26 + tabY + offsetY);
		} else if (gainedDefenceEXP != 0) {
			g.drawString(skills.getLevel(Skill.DEFENCE) + "", 367 + tabX
					+ offsetX, 26 + tabY + offsetY);
		} else if (gainedStrengthEXP != 0) {
			g.drawString(skills.getLevel(Skill.STRENGTH) + "", 367 + tabX
					+ offsetX, 26 + tabY + offsetY);
		} else if (gainedRangeEXP != 0) {
			g.drawString(skills.getLevel(Skill.RANGED) + "", 367 + tabX
					+ offsetX, 26 + tabY + offsetY);
		} else if (gainedMagicEXP != 0) {
			g.drawString(skills.getLevel(Skill.MAGIC) + "", 367 + tabX
					+ offsetX, 26 + tabY + offsetY);
		}
		g.setFont(new Font("Arial", Font.BOLD, 8));
		g.drawString(currentVersion, 490 + tabX + offsetX, 14 + tabY + offsetY);
		/*
		 * switch (paintI) { case 0: g.drawImage(paintImages[paintI], tabX +
		 * offsetX, tabY + offsetY, null); g.drawString("Time Run: " + hours +
		 * ":" + minutes + ":" + seconds, 15 + offsetX, 439 + offsetY);
		 * g.drawString("Total EXP Gained: " + (gainedAttackEXP +
		 * gainedDefenceEXP + gainedStrengthEXP + gainedRangeEXP +
		 * gainedMagicEXP + gainedHitpointsEXP + gainedPrayerEXP), 15 + offsetX,
		 * 459 + offsetY); g.drawString( "Total Levels Gained: " +
		 * (gainedAttackLevel + gainedDefenceLevel + gainedStrengthLevel +
		 * gainedRangeLevel + gainedMagicLevel + gainedHitpointLevel +
		 * gainedPrayerLevel), 15 + offsetX, 479 + offsetY);
		 * g.drawString("State: " + (antiban ? "ANTIBAN" : state.toString()), 15
		 * + offsetX, 499 + offsetY); break; case 1:
		 * g.drawImage(paintImages[paintI], tabX + offsetX, tabY + offsetY,
		 * null); if (gainedAttackEXP != 0) { g.drawString("Gained Attack EXP: "
		 * + gainedAttackEXP, 15 + offsetX, 439 + offsetY); g.drawString(
		 * "Current Attack Level: " + skills.getLevel(Skill.ATTACK), 235 +
		 * offsetX, 439 + offsetY); g.drawString("Attack EXP Per Hour: " +
		 * attackPerHour, 15 + offsetX, 459 + offsetY);
		 * 
		 * g.drawString("Time to Next Level: " + hoursToLevel + ":" +
		 * minutesToLevel + ":" + secondsToLevel, 15 + offsetX, 479 + offsetY);
		 * double perc = percentToNextLevel(Skill.ATTACK); g.setColor(new
		 * Color(0, 1, 0, 0.75f)); g.fillRect(15 + offsetX, 490 + offsetY, (int)
		 * (500 * perc / 100), 17); g.setColor(new Color(1, 1, 1, 0.75f));
		 * g.fillRect(15 + offsetX, 490 + offsetY, 490, 8); g.setColor(new
		 * Color(.5f, .5f, .5f, 0.75f)); g.fillRect(15 + offsetX, 499 + offsetY,
		 * 490, 8); g.setColor(Color.RED); g.drawString(((int) perc) + "% : " +
		 * experienceToNextLevel(Skill.ATTACK) + " TNL", 200, 462); } else if
		 * (gainedDefenceEXP != 0) { g.drawString("Gained Defence EXP: " +
		 * gainedDefenceEXP, 15 + offsetX, 439 + offsetY); g.drawString(
		 * "Current Defence Level: " + skills.getLevel(Skill.DEFENCE), 235 +
		 * offsetX, 439 + offsetY); g.drawString("Defence EXP Per Hour: " +
		 * defencePerHour, 15 + offsetX, 459 + offsetY);
		 * 
		 * g.drawString("Time to Next Level: " + hoursToLevel + ":" +
		 * minutesToLevel + ":" + secondsToLevel, 15 + offsetX, 479 + offsetY);
		 * double perc = percentToNextLevel(Skill.DEFENCE); g.setColor(new
		 * Color(0, 1, 0, 0.75f)); g.fillRect(15 + offsetX, 490 + offsetY, (int)
		 * (500 * perc / 100), 17); g.setColor(new Color(1, 1, 1, 0.75f));
		 * g.fillRect(15 + offsetX, 490 + offsetY, 490, 8); g.setColor(new
		 * Color(.5f, .5f, .5f, 0.75f)); g.fillRect(15 + offsetX, 499 + offsetY,
		 * 490, 8); g.setColor(Color.RED); g.drawString(((int) perc) + "% : " +
		 * experienceToNextLevel(Skill.DEFENCE) + " TNL", 200, 462); } else if
		 * (gainedStrengthEXP != 0) { g.drawString("Gained Strength EXP: " +
		 * gainedStrengthEXP, 15 + offsetX, 439 + offsetY); g.drawString(
		 * "Current Strength Level: " + skills.getLevel(Skill.STRENGTH), 235 +
		 * offsetX, 439 + offsetY); g.drawString("Strength EXP Per Hour: " +
		 * strengthPerHour, 15 + offsetX, 459 + offsetY);
		 * 
		 * g.drawString("Time to Next Level: " + hoursToLevel + ":" +
		 * minutesToLevel + ":" + secondsToLevel, 15 + offsetX, 479 + offsetY);
		 * double perc = percentToNextLevel(Skill.STRENGTH); g.setColor(new
		 * Color(0, 1, 0, 0.75f)); g.fillRect(15 + offsetX, 490 + offsetY, (int)
		 * (500 * perc / 100), 17); g.setColor(new Color(1, 1, 1, 0.75f));
		 * g.fillRect(15 + offsetX, 490 + offsetY, 490, 8); g.setColor(new
		 * Color(.5f, .5f, .5f, 0.75f)); g.fillRect(15 + offsetX, 499 + offsetY,
		 * 490, 8); g.setColor(Color.RED); g.drawString(((int) perc) + "% : " +
		 * experienceToNextLevel(Skill.STRENGTH) + " TNL", 200, 462); } else if
		 * (gainedRangeEXP != 0) { g.drawString("Gained Range EXP: " +
		 * gainedRangeEXP, 15 + offsetX, 439 + offsetY); g.drawString(
		 * "Current Range Level: " + skills.getLevel(Skill.RANGED), 235 +
		 * offsetX, 439 + offsetY); g.drawString("Range EXP Per Hour: " +
		 * rangePerHour, 15 + offsetX, 459 + offsetY);
		 * 
		 * g.drawString("Time to Next Level: " + hoursToLevel + ":" +
		 * minutesToLevel + ":" + secondsToLevel, 15 + offsetX, 479 + offsetY);
		 * double perc = percentToNextLevel(Skill.RANGED); g.setColor(new
		 * Color(0, 1, 0, 0.75f)); g.fillRect(15 + offsetX, 490 + offsetY, (int)
		 * (500 * perc / 100), 17); g.setColor(new Color(1, 1, 1, 0.75f));
		 * g.fillRect(15 + offsetX, 490 + offsetY, 490, 8); g.setColor(new
		 * Color(.5f, .5f, .5f, 0.75f)); g.fillRect(15 + offsetX, 499 + offsetY,
		 * 490, 8); g.setColor(Color.RED); g.drawString(((int) perc) + "% : " +
		 * experienceToNextLevel(Skill.RANGED) + " TNL", 200, 462); } else if
		 * (gainedMagicEXP != 0) { g.drawString("Gained Magic EXP: " +
		 * gainedMagicEXP, 15 + offsetX, 439 + offsetY); g.drawString(
		 * "Current Magic Level: " + skills.getLevel(Skill.MAGIC), 235 +
		 * offsetX, 439 + offsetY); g.drawString("Magic EXP Per Hour: " +
		 * magicPerHour, 15 + offsetX, 459 + offsetY);
		 * 
		 * g.drawString("Time to Next Level: " + hoursToLevel + ":" +
		 * minutesToLevel + ":" + secondsToLevel, 15 + offsetX, 479 + offsetY);
		 * double perc = percentToNextLevel(Skill.MAGIC); g.setColor(new
		 * Color(0, 1, 0, 0.75f)); g.fillRect(15 + offsetX, 490 + offsetY, (int)
		 * (500 * perc / 100), 17); g.setColor(new Color(1, 1, 1, 0.75f));
		 * g.fillRect(15 + offsetX, 490 + offsetY, 490, 8); g.setColor(new
		 * Color(.5f, .5f, .5f, 0.75f)); g.fillRect(15 + offsetX, 499 + offsetY,
		 * 490, 8); g.setColor(Color.RED); g.drawString(((int) perc) + "% : " +
		 * experienceToNextLevel(Skill.MAGIC) + " TNL", 200, 462); } else {
		 * g.drawString("This will autodetect soon", 15 + offsetX, 439 +
		 * offsetY); } break; case 2: g.drawImage(paintImages[2], -20, 205,
		 * null); g.setColor(Color.RED); g.drawString("CMHStronghold Version " +
		 * currentVersion, 12, 400);
		 * g.drawString("Script Developed by Ericthecmh", 12, 415);
		 * g.drawString("Main Website: http://cmhscripts.com", 12, 430); break;
		 * case 3: g.drawImage(paintImages[3], -20, 205, null);
		 * g.setColor(Color.RED); g.drawString("My Email: cmhscripts@mit.edu",
		 * 12, 400); g.drawString(
		 * "or leave feedback at http://cmhscripts.com/contact.html", 12, 415);
		 * g.drawString(
		 * "or on the OSBot Forum Thread under Local->Combat&Slayer->CMHStronghold"
		 * , 12, 430); g.drawString("Any and all feedback is welcome.", 12,
		 * 445); break; }
		 */
	}

	// Custom methods

	private Position randomize(Position p) {
		return new Position(p.getX() + random(1, 2 * CONST + 1) - (CONST + 1),
				p.getY() + random(1, 2 * CONST + 1) - (CONST + 1), 0);
	}

	private GroundItem getClosestGroundItemForName(String s) {
		int num = -1;
		try {
			num = Integer.parseInt(s);
		} catch (Exception e) {

		}
		GroundItem item = null;
		List<GroundItem> items = loader.client.getCurrentRegion().getItems();
		int dist = 20000000;
		for (GroundItem o : items) {
			if ((num == -1 ? (o.getName().equals(s)) : (o.getId() == num))) {
				if (item == null) {
					item = o;
					dist = loader.distance(loader.myX(), loader.myY(),
							item.getX(), item.getY());
				} else {
					if (loader.distance(loader.myX(), loader.myY(), o.getX(),
							o.getY()) < dist) {
						dist = loader.distance(loader.myX(), loader.myY(),
								o.getX(), o.getY());
						item = o;
					}
				}
			}
		}
		return item;
	}

	private RS2Object closestObjectTo(int id, Position p) {
		List<RS2Object> objects = loader.client.getCurrentRegion().getObjects();
		RS2Object object = null;
		int dist = 20000000;
		for (RS2Object o : objects) {
			if (o.getId() == id) {
				if (object == null) {
					object = o;
					dist = loader.distance(p.getX(), p.getY(), object.getX(),
							object.getY());
				} else {
					if (loader.distance(p.getX(), p.getY(), o.getX(), o.getY()) < dist) {
						dist = loader.distance(p.getX(), p.getY(), o.getX(),
								o.getY());
						object = o;
					}
				}
			}
		}
		return object;
	}

	private String getDoorName(int i) {
		switch (i) {
		case 0:
			return "Gate of War";
		case 1:
			return "Rickety door";
		case 2:
			return "Oozing barrier";
		case 3:
			return "Portal of Death";
		default:
			return "";
		}
	}

	private RS2Object closestObjectTo(int[] id, Position p) {
		List<RS2Object> objects = loader.client.getCurrentRegion().getObjects();
		RS2Object object = null;
		int dist = 20000000;
		for (RS2Object o : objects) {
			if (o.getId() == id[0] || o.getId() == id[1]) {
				if (object == null) {
					object = o;
					dist = loader.distance(p.getX(), p.getY(), object.getX(),
							object.getY());
				} else {
					if (loader.distance(p.getX(), p.getY(), o.getX(), o.getY()) < dist) {
						dist = loader.distance(p.getX(), p.getY(), o.getX(),
								o.getY());
						object = o;
					}
				}
			}
		}
		return object;
	}

	private RS2Object closestObjectTo(String name, Position p) {
		List<RS2Object> objects = loader.client.getCurrentRegion().getObjects();
		RS2Object object = null;
		int dist = 20000000;
		for (RS2Object o : objects) {
			if (o.getName().equals(name)) {
				if (object == null) {
					object = o;
					dist = loader.distance(p.getX(), p.getY(), object.getX(),
							object.getY());
				} else {
					if (loader.distance(p.getX(), p.getY(), o.getX(), o.getY()) < dist) {
						dist = loader.distance(p.getX(), p.getY(), o.getX(),
								o.getY());
						object = o;
					}
				}
			}
		}
		return object;
	}

	private RS2Object nthClosestObjectByName(String name, int n) {
		List<RS2Object> objects = loader.client.getCurrentRegion().getObjects();
		RS2Object object = null;
		int i = 0;
		for (RS2Object o : objects) {
			if (o.getName().equals(name)) {
				object = o;
				i++;
				if (i == n)
					return object;
			}
		}
		return null;
	}

	private void setRunning2(boolean on) {
		try {
			for (int i = 0; i < 10 && !loader.openTab(Tab.SETTINGS); i++) {
				sleep(random(500, 600));
			}

			RS2Interface inte = loader.client.getInterface(261);
			if (inte != null) {
				RS2InterfaceChild child = inte.getChild(36);
				if (child != null) {
					child.hover();
					sleep(random(1000, 1100));
					child.interact();
				}
			}
		} catch (Exception e) {

		}
		return;
	}

	public void Dijkstra(int start, int floor) {
		int NUM_VERTICES = floors[floor].tilearr.length;
		path = new ArrayList<ArrayList<Integer>>();
		for (int x = 0; x < NUM_VERTICES; x++) {
			path.add(new ArrayList<Integer>());
			path.get(x).add(start);
		}
		best = new int[NUM_VERTICES];
		boolean[] visited = new boolean[NUM_VERTICES];
		for (int x = 0; x < NUM_VERTICES; x++)
			best[x] = 2000000000;
		best[start] = 0;
		while (true) {

			int smallestl = 2000000000;
			int smallestx = -1;
			for (int x = 0; x < NUM_VERTICES; x++) {
				if (!visited[x]) {
					if (best[x] != 2000000000 && best[x] < smallestl) {
						smallestl = best[x];
						smallestx = x;
					}
				}
			}
			if (smallestx == -1)
				break;
			visited[smallestx] = true;
			for (int x = 0; x < NUM_VERTICES; x++) {
				if (edges[smallestx][x][floor] != 0) {
					int temp = (edges[smallestx][x][floor] == -100 ? 4
							: edges[smallestx][x][floor]) + best[smallestx];
					if (temp < best[x]) {
						best[x] = temp;
						path.set(x, (ArrayList<Integer>) path.get(smallestx)
								.clone());
						path.get(x).add(x);
					}
				}
			}
		}
	}

	private double percentToNextLevel(Skill skill) {
		int level = loader.client.getSkills().getLevel(skill);
		double total = 0;
		total = xpForLevels[level] - xpForLevels[level - 1];
		return 100 * (1 - ((experienceToNextLevel(skill) + 0.0) / total));
	}

	/***************** MADE BY ABIBOT *******************/
	int[] xpForLevels = new int[] { 0, 83, 174, 276, 388, 512, 650, 801, 969,
			1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470,
			5018, 5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363,
			14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648,
			37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014,
			91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040,
			203254, 224466, 247886, 273742, 302288, 333804, 368599, 407015,
			449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257,
			992895, 1096278, 1210421, 1336443, 1475581, 1629200, 1798808,
			1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792,
			3972294, 4385776, 4842295, 5346332, 5902831, 6517253, 7195629,
			7944614, 8771558, 9684577, 10692629, 11805606, 13034431 };

	private int experienceToNextLevel(Skill skill) {
		int xp = loader.client.getSkills().getExperience(skill);
		for (int i = 0; i < 99; i++) {
			if (xp < xpForLevels[i]) {
				return (xpForLevels[i] - xp);
			}
		}
		return (200000000 - xp);
	}

	/***************** END MADE BY ABIBOT *******************/

	private int distanceBetween(int t1, int t2, int floor) {
		return (int) (Math
				.sqrt((floors[floor].tilearr[t1].getX() - floors[floor].tilearr[t2]
						.getX())
						* (floors[floor].tilearr[t1].getX() - floors[floor].tilearr[t2]
								.getX())
						+ (floors[floor].tilearr[t1].getY() - floors[floor].tilearr[t2]
								.getY())
						* (floors[floor].tilearr[t1].getY() - floors[floor].tilearr[t2]
								.getY())));

	}

	// Custom Classes

	private class Area {
		private double minX, minY;
		private double maxX, maxY;

		private Area(double d, double e, double f, double g) {
			if (d > f) {
				minX = f;
				maxX = d;
			} else {
				maxX = f;
				minX = d;
			}
			if (e > g) {
				minY = g;
				maxY = e;
			} else {
				maxY = g;
				minY = e;
			}
			System.out.println("Max X: " + maxX + " Min X: " + minX
					+ " Min Y: " + minY + " Max Y: " + maxY);
		}

		public boolean currentlyInArea() {
			return isInArea(loader.myPosition());
		}

		public boolean isInArea(Position p) {
			return (p.getX() >= minX && p.getX() <= maxX && p.getY() >= minY && p
					.getY() <= maxY);
		}

	}

	private class Loot {
		public String name;
		public int number;
		public boolean combat, eat, stackable;

		public Loot(String n, int num, boolean c, boolean e, boolean s) {
			name = n.substring(0, 1).toUpperCase() + n.substring(1);
			number = num;
			combat = c;
			eat = e;
			stackable = s;
		}
	}

	private class Action {
		public String skill;
		public int level;
		public String action;
		public String details;
		public boolean triggered;

		public Action(String s, int l, String a, String d) {
			skill = s;
			level = l;
			action = a;
			details = d;
		}
	}

	private class Break {
		public long startTime;
		public long duration;

		public Break(int minutes, int length) {
			startTime = System.currentTimeMillis() + minutes * 60 * 1000;
			duration = length * 60 * 1000;
		}
	}

	private class Food {
		public String name;
		public int amount, eat;
		public int realeat;

		public Food(String name, int withdraw, int eatAt) {
			this.name = name.substring(0, 1).toUpperCase() + name.substring(1);
			amount = withdraw;
			eat = eatAt;
			realeat = eatAt;
		}
	}

	private class Potion {
		public String name;
		public int amount, eat;
		public int realeat;
		public Skill skill;

		public Potion(String name, int withdraw, int eatAt) {
			this.name = name.substring(0, 1).toUpperCase() + name.substring(1);
			amount = withdraw;
			eat = eatAt;
			realeat = eatAt;
			if (name.contains("ttac"))
				skill = Skill.ATTACK;
			else if (name.contains("ength"))
				skill = Skill.STRENGTH;
			else if (name.contains("ence"))
				skill = Skill.DEFENCE;
			else if (name.contains("ang"))
				skill = Skill.RANGED;
			else if (name.contains("ag"))
				skill = Skill.MAGIC;
		}
	}

	private class AttackNPC {
		public AttackNPC(String name, int level) {
			this.name = name;
			this.level = level;
		}

		public String name;
		public int level;
	}

	private class Floor {
		public Position[] tilearr;
	}

	private class CalculatePairs implements Runnable {

		public boolean done = false;

		public void run() {

			edges = new int[150][150][4];
			floors = new Floor[4];

			// Floor 1
			floors[0] = new Floor();
			floors[0].tilearr = new Position[] { new Position(1859, 5239, 0),
					new Position(1859, 5238, 0), new Position(1859, 5236, 0),
					new Position(1859, 5234, 0), new Position(1860, 5227, 0),
					new Position(1861, 5221, 0), new Position(1861, 5213, 0),
					new Position(1860, 5212, 0), new Position(1860, 5210, 0),
					new Position(1860, 5209, 0), new Position(1860, 5204, 0),
					new Position(1861, 5199, 0), new Position(1861, 5198, 0),
					new Position(1861, 5196, 0), new Position(1861, 5195, 0),
					new Position(1864, 5226, 0), new Position(1865, 5226, 0),
					new Position(1867, 5226, 0), new Position(1868, 5226, 0),
					new Position(1872, 5230, 0), new Position(1871, 5237, 0),
					new Position(1875, 5239, 0), new Position(1876, 5239, 0),
					new Position(1878, 5240, 0), new Position(1879, 5239, 0),
					new Position(1883, 5244, 0), new Position(1884, 5244, 0),
					new Position(1886, 5244, 0), new Position(1887, 5243, 0),
					new Position(1893, 5243, 0), new Position(1899, 5242, 0),
					new Position(1903, 5243, 0), new Position(1904, 5242, 0),
					new Position(1907, 5242, 0), new Position(1908, 5242, 0),
					new Position(1912, 5239, 0), new Position(1909, 5236, 0),
					new Position(1904, 5234, 0), new Position(1904, 5233, 0),
					new Position(1904, 5231, 0), new Position(1904, 5230, 0),
					new Position(1894, 5236, 0), new Position(1889, 5236, 0),
					new Position(1888, 5236, 0), new Position(1886, 5236, 0),
					new Position(1885, 5235, 0), new Position(1879, 5233, 0),
					new Position(1879, 5226, 0), new Position(1879, 5225, 0),
					new Position(1879, 5223, 0), new Position(1879, 5222, 0),
					new Position(1876, 5216, 0), new Position(1871, 5217, 0),
					new Position(1869, 5217, 0), new Position(1867, 5218, 0),
					new Position(1866, 5217, 0), new Position(1875, 5208, 0),
					new Position(1875, 5207, 0), new Position(1875, 5205, 0),
					new Position(1875, 5204, 0), new Position(1876, 5195, 0),
					new Position(1877, 5194, 0), new Position(1877, 5192, 0),
					new Position(1877, 5191, 0), new Position(1872, 5189, 0),
					new Position(1865, 5189, 0), new Position(1878, 5188, 0),
					new Position(1879, 5188, 0), new Position(1881, 5188, 0),
					new Position(1882, 5188, 0), new Position(1889, 5191, 0),
					new Position(1895, 5194, 0), new Position(1899, 5199, 0),
					new Position(1903, 5204, 0), new Position(1904, 5204, 0),
					new Position(1906, 5204, 0), new Position(1907, 5204, 0),
					new Position(1911, 5206, 0), new Position(1911, 5208, 0),
					new Position(1911, 5209, 0), new Position(1911, 5210, 0),
					new Position(1900, 5208, 0), new Position(1897, 5212, 0),
					new Position(1896, 5213, 0), new Position(1894, 5212, 0),
					new Position(1893, 5212, 0), new Position(1889, 5219, 0),
					new Position(1891, 5225, 0), new Position(1895, 5232, 0),
					new Position(1889, 5212, 0), new Position(1889, 5211, 0),
					new Position(1889, 5208, 0), new Position(1889, 5208, 0),
					new Position(1883, 5202, 0) };
			// log("Starting floor 0");
			Scanner scan = new Scanner(floor1cfg);
			while (scan.hasNext()) {
				int node1 = scan.nextInt();
				int node2 = scan.nextInt();
				String s = scan.nextLine().trim();
				if (s.charAt(0) == 'D') {
					edges[node1][node2][0] = distanceBetween(node1, node2, 0);
					edges[node2][node1][0] = distanceBetween(node1, node2, 0);
				} else {
					edges[node1][node2][0] = Integer.parseInt(s);
					edges[node2][node1][0] = Integer.parseInt(s);
				}
			}
			scan.close();
			// log("Done with floor 0");

			// Floor 2
			floors[1] = new Floor();
			floors[1].tilearr = new Position[] { new Position(2042, 5242, 0),
					new Position(2040, 5244, 0), new Position(2039, 5244, 0),
					new Position(2037, 5244, 0), new Position(2036, 5244, 0),
					new Position(2032, 5239, 0), new Position(2029, 5234, 0),
					new Position(2027, 5238, 0), new Position(2027, 5239, 0),
					new Position(2027, 5241, 0), new Position(2027, 5242, 0),
					new Position(2020, 5243, 0), new Position(2020, 5242, 0),
					new Position(2020, 5240, 0), new Position(2020, 5239, 0),
					new Position(2014, 5239, 0), new Position(2014, 5240, 0),
					new Position(2014, 5242, 0), new Position(2014, 5243, 0),
					new Position(2007, 5244, 0), new Position(2005, 5238, 0),
					new Position(2005, 5237, 0), new Position(2005, 5235, 0),
					new Position(2005, 5234, 0), new Position(2006, 5227, 0),
					new Position(2004, 5221, 0), new Position(2000, 5216, 0),
					new Position(1999, 5216, 0), new Position(1997, 5216, 0),
					new Position(1996, 5216, 0), new Position(1993, 5523, 0),
					new Position(1990, 5229, 0), new Position(1991, 5237, 0),
					new Position(1995, 5242, 0), new Position(2001, 5244, 0),
					new Position(1994, 5210, 0), new Position(1990, 5205, 0),
					new Position(1988, 5200, 0), new Position(1994, 5197, 0),
					new Position(1994, 5196, 0), new Position(1994, 5194, 0),
					new Position(1994, 5193, 0), new Position(1991, 5189, 0),
					new Position(1999, 5190, 0), new Position(2005, 5191, 0),
					new Position(2005, 5192, 0), new Position(2005, 5194, 0),
					new Position(2005, 5195, 0), new Position(2005, 5202, 0),
					new Position(2007, 5208, 0), new Position(2001, 5211, 0),
					new Position(2042, 5188, 0), new Position(2045, 5194, 0),
					new Position(2045, 5195, 0), new Position(2045, 5197, 0),
					new Position(2045, 5198, 0), new Position(2041, 5200, 0),
					new Position(2036, 5200, 0), new Position(2036, 5201, 0),
					new Position(2036, 5203, 0), new Position(2036, 5204, 0),
					new Position(2037, 5209, 0), new Position(2040, 5214, 0),
					new Position(2044, 5217, 0), new Position(2043, 5223, 0),
					new Position(2042, 5223, 0), new Position(2040, 5223, 0),
					new Position(2039, 5223, 0), new Position(2032, 5224, 0),
					new Position(2032, 5225, 0), new Position(2032, 5227, 0),
					new Position(2032, 5228, 0), new Position(2041, 5229, 0),
					new Position(2040, 5234, 0), new Position(2045, 5236, 0),
					new Position(2045, 5237, 0), new Position(2045, 5239, 0),
					new Position(2045, 5240, 0), new Position(2012, 5231, 0),
					new Position(2015, 5227, 0), new Position(2016, 5227, 0),
					new Position(2018, 5227, 0), new Position(2019, 5227, 0),
					new Position(2023, 5221, 0) };

			// log("Starting floor 1");
			scan = new Scanner(floor2cfg);
			while (scan.hasNext()) {
				int node1 = scan.nextInt();
				int node2 = scan.nextInt();
				String s = scan.nextLine().trim();
				if (s.charAt(0) == 'D') {
					edges[node1][node2][1] = distanceBetween(node1, node2, 1);
					edges[node2][node1][1] = distanceBetween(node1, node2, 1);
				} else {
					edges[node1][node2][1] = Integer.parseInt(s);
					edges[node2][node1][1] = Integer.parseInt(s);
				}
			}
			// log("Done with floor 1");
			// Floor 3
			floors[2] = new Floor();
			floors[2].tilearr = new Position[] { new Position(2132, 5256, 0),
					new Position(2132, 5257, 0), new Position(2132, 5259, 0),
					new Position(2132, 5260, 0), new Position(2137, 5263, 0),
					new Position(2138, 5263, 0), new Position(2140, 5263, 0),
					new Position(2141, 5263, 0), new Position(2146, 5262, 0),
					new Position(2147, 5255, 0), new Position(2153, 5264, 0),
					new Position(2154, 5264, 0), new Position(2156, 5264, 0),
					new Position(2157, 5264, 0), new Position(2163, 5267, 0),
					new Position(2166, 5262, 0), new Position(2166, 5261, 0),
					new Position(2166, 5259, 0), new Position(2166, 5258, 0),
					new Position(2168, 5252, 0), new Position(2167, 5272, 0),
					new Position(2168, 5272, 0), new Position(2170, 5272, 0),
					new Position(2171, 5272, 0), new Position(2171, 5279, 0),
					new Position(2170, 5286, 0), new Position(2168, 5293, 0),
					new Position(2168, 5294, 0), new Position(2168, 5296, 0),
					new Position(2168, 5297, 0), new Position(2167, 5304, 0),
					new Position(2162, 5301, 0), new Position(2160, 5296, 0),
					new Position(2162, 5290, 0), new Position(2162, 5289, 0),
					new Position(2162, 5287, 0), new Position(2162, 5286, 0),
					new Position(2163, 5278, 0), new Position(2163, 5277, 0),
					new Position(2163, 5275, 0), new Position(2163, 5274, 0),
					new Position(2153, 5297, 0), new Position(2148, 5298, 0),
					new Position(2148, 5299, 0), new Position(2148, 5301, 0),
					new Position(2148, 5302, 0), new Position(2153, 5307, 0),
					new Position(2141, 5295, 0), new Position(2140, 5295, 0),
					new Position(2138, 5295, 0), new Position(2137, 5295, 0),
					new Position(2134, 5289, 0), new Position(2131, 5292, 0),
					new Position(2131, 5293, 0), new Position(2131, 5295, 0),
					new Position(2131, 5296, 0), new Position(2132, 5282, 0),
					new Position(2132, 5281, 0), new Position(2132, 5279, 0),
					new Position(2132, 5278, 0), new Position(2129, 5270, 0),
					new Position(2152, 5268, 0), new Position(2155, 5285, 0),
					new Position(2155, 5286, 0), new Position(2155, 5288, 0),
					new Position(2156, 5288, 0), new Position(2153, 5292, 0),
					new Position(2152, 5292, 0), new Position(2149, 5292, 0),
					new Position(2148, 5292, 0), new Position(2160, 5282, 0) };

			// log("Starting floor 2");
			scan = new Scanner(floor3cfg);
			while (scan.hasNext()) {
				int node1 = scan.nextInt();
				int node2 = scan.nextInt();
				String s = scan.nextLine().trim();
				if (s.charAt(0) == 'D') {
					edges[node1][node2][2] = distanceBetween(node1, node2, 2);
					edges[node2][node1][2] = distanceBetween(node1, node2, 2);
				} else {
					edges[node1][node2][2] = Integer.parseInt(s);
					edges[node2][node1][2] = Integer.parseInt(s);
				}
			}
			// log("Done with floor 2");

			// Floor 4
			floors[3] = new Floor();
			floors[3].tilearr = new Position[] { new Position(2361, 5213, 0),
					new Position(2356, 5218, 0), new Position(2356, 5219, 0),
					new Position(2356, 5221, 0), new Position(2356, 5222, 0),
					new Position(2350, 5228, 0), new Position(2357, 5230, 0),
					new Position(2364, 5228, 0), new Position(2365, 5222, 0),
					new Position(2365, 5221, 0), new Position(2365, 5219, 0),
					new Position(2365, 5218, 0), new Position(2359, 5232, 0),
					new Position(2359, 5234, 0), new Position(2359, 5235, 0),
					new Position(2361, 5242, 0), new Position(2356, 5245, 0),
					new Position(2355, 5245, 0), new Position(2353, 5245, 0),
					new Position(2352, 5245, 0), new Position(2345, 5245, 0),
					new Position(2340, 5241, 0), new Position(2345, 5237, 0),
					new Position(2345, 5232, 0), new Position(2340, 5237, 0),
					new Position(2335, 5237, 0), new Position(2333, 5237, 0),
					new Position(2332, 5237, 0), new Position(2330, 5244, 0),
					new Position(2323, 5243, 0), new Position(2323, 5242, 0),
					new Position(2323, 5240, 0), new Position(2323, 5239, 0),
					new Position(2321, 5232, 0), new Position(2321, 5224, 0),
					new Position(2362, 5207, 0), new Position(2362, 5206, 0),
					new Position(2362, 5204, 0), new Position(2362, 5203, 0),
					new Position(2363, 5194, 0), new Position(2362, 5194, 0),
					new Position(2360, 5194, 0), new Position(2359, 5194, 0),
					new Position(2354, 5193, 0), new Position(2347, 5188, 0),
					new Position(2346, 5188, 0), new Position(2344, 5188, 0),
					new Position(2343, 5188, 0), new Position(2336, 5193, 0),
					new Position(2335, 5193, 0), new Position(2333, 5193, 0),
					new Position(2332, 5193, 0), new Position(2327, 5198, 0),
					new Position(2320, 5204, 0), new Position(2312, 5204, 0),
					new Position(2311, 5204, 0), new Position(2309, 5204, 0),
					new Position(2308, 5204, 0), new Position(2306, 5212, 0),
					new Position(2308, 5219, 0), new Position(2312, 5214, 0),
					new Position(2319, 5212, 0) };
			// log("Starting floor 3");
			scan = new Scanner(floor4cfg);
			while (scan.hasNext()) {
				int node1 = scan.nextInt();
				int node2 = scan.nextInt();
				String s = scan.nextLine().trim();
				if (s.charAt(0) == 'D') {
					edges[node1][node2][3] = distanceBetween(node1, node2, 3);
					edges[node2][node1][3] = distanceBetween(node1, node2, 3);
				} else {
					edges[node1][node2][3] = Integer.parseInt(s);
					edges[node2][node1][3] = Integer.parseInt(s);
				}
			}
			// log("Done with floor 3");
			done = true;
		}
	}

	class FloorSelection extends javax.swing.JPanel {

		/**
		 * Creates new form FloorSelection
		 */

		FloorPanel panel;

		public FloorSelection() {
			initComponents();
			panel = (FloorPanel) jPanel1;
			new Thread(new UpdateThread()).start();
		}

		class UpdateThread implements Runnable {

			public void run() {
				while (true) {
					if (scriptStopped)
						break;
					if (System.currentTimeMillis() - panel.updateTime() < 1000) {
						locationLabel.setText("Room: " + panel.getRoom()
								+ " Floor: " + (panel.getFloor() + 1));
					}
					try {
						Thread.sleep(100);
					} catch (InterruptedException ex) {
					}
				}
			}
		}

		/**
		 * This method is called from within the constructor to initialize the
		 * form. WARNING: Do NOT modify this code. The content of this method is
		 * always regenerated by the Form Editor.
		 */
		@SuppressWarnings("unchecked")
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		private void initComponents() {

			jLabel1 = new javax.swing.JLabel();
			jLabel2 = new javax.swing.JLabel();
			floorMenu6 = new javax.swing.JComboBox();
			jPanel1 = new FloorPanel();
			locationLabel = new javax.swing.JLabel();

			setPreferredSize(new java.awt.Dimension(279, 367));

			jLabel1.setText("Select room:");

			jLabel2.setText("Floor:");

			floorMenu6
					.setModel(new javax.swing.DefaultComboBoxModel(
							new String[] { "Floor 1", "Floor 2", "Floor 3",
									"Floor 4" }));
			floorMenu6.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					floorMenu6ActionPerformed(evt);
				}
			});

			jPanel1.setMinimumSize(new java.awt.Dimension(255, 255));
			jPanel1.setPreferredSize(new java.awt.Dimension(255, 255));

			javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
					jPanel1);
			jPanel1.setLayout(jPanel1Layout);
			jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(
					javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 255,
					Short.MAX_VALUE));
			jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(
					javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 255,
					Short.MAX_VALUE));

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
			this.setLayout(layout);
			layout.setHorizontalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.LEADING)
													.addGroup(
															layout.createSequentialGroup()
																	.addContainerGap()
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING)
																					.addGroup(
																							layout.createSequentialGroup()
																									.addComponent(
																											jLabel1)
																									.addPreferredGap(
																											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																									.addComponent(
																											locationLabel)
																									.addGap(0,
																											0,
																											Short.MAX_VALUE))
																					.addGroup(
																							layout.createSequentialGroup()
																									.addComponent(
																											jLabel2)
																									.addPreferredGap(
																											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																									.addComponent(
																											floorMenu6,
																											0,
																											javax.swing.GroupLayout.DEFAULT_SIZE,
																											Short.MAX_VALUE))))
													.addGroup(
															javax.swing.GroupLayout.Alignment.TRAILING,
															layout.createSequentialGroup()
																	.addComponent(
																			jPanel1,
																			javax.swing.GroupLayout.PREFERRED_SIZE,
																			javax.swing.GroupLayout.DEFAULT_SIZE,
																			javax.swing.GroupLayout.PREFERRED_SIZE)
																	.addGap(0,
																			0,
																			Short.MAX_VALUE)))
									.addContainerGap()));
			layout.setVerticalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addContainerGap()
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel1)
													.addComponent(locationLabel))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel2)
													.addComponent(
															floorMenu6,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(
											jPanel1,
											javax.swing.GroupLayout.PREFERRED_SIZE,
											javax.swing.GroupLayout.DEFAULT_SIZE,
											javax.swing.GroupLayout.PREFERRED_SIZE)
									.addContainerGap(
											javax.swing.GroupLayout.DEFAULT_SIZE,
											Short.MAX_VALUE)));
		}// </editor-fold>

		private void floorMenu6ActionPerformed(java.awt.event.ActionEvent evt) {
			panel.setFloor(floorMenu6.getSelectedIndex());
			panel.repaint();
		}

		// Variables declaration - do not modify
		private javax.swing.JComboBox floorMenu;
		private javax.swing.JComboBox floorMenu1;
		private javax.swing.JComboBox floorMenu2;
		private javax.swing.JComboBox floorMenu3;
		private javax.swing.JComboBox floorMenu4;
		private javax.swing.JComboBox floorMenu5;
		private javax.swing.JComboBox floorMenu6;
		private javax.swing.JLabel jLabel1;
		private javax.swing.JLabel jLabel2;
		private javax.swing.JPanel jPanel1;
		private javax.swing.JLabel locationLabel;
		// End of variables declaration
	}

	class CMHStrongholdGUI extends javax.swing.JFrame {

		private FloorPanel panel;
		private int floor, room;
		public boolean ready;
		public boolean isPrem;
		public DefaultListModel model1, model2;
		public DefaultTableModel foodTableModel, potionTableModel,
				lootTableModel, actionTableModel;
		private Object lock = new Object();
		private FloorSelection frame;
		private String settingsVersion;
		private ComboBoxModel profileBoxModel;

		public CMHStrongholdGUI(String username, boolean premium, String version) {
			initComponents();
			settingsVersion = version;
			isPrem = premium;
			premiumLabel.setText("PREMIUM: " + premium);
			panel = (FloorPanel) jPanel3;
			new Thread(new UpdateThread()).start();
			model1 = new DefaultListModel();
			model2 = new DefaultListModel();
			panel.setModels(model1, model2);
			allList.setModel(model1);
			attackList.setModel(model2);
			allList.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			attackList
					.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			foodTableModel = (DefaultTableModel) foodTable.getModel();
			foodTable
					.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			foodTable.putClientProperty("terminateEditOnFocusLost",
					Boolean.TRUE);
			potionTableModel = (DefaultTableModel) potionTable.getModel();
			potionTable
					.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			potionTable.putClientProperty("terminateEditOnFocusLost",
					Boolean.TRUE);
			lootTableModel = (DefaultTableModel) lootTable.getModel();
			lootTable
					.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			lootTable.putClientProperty("terminateEditOnFocusLost",
					Boolean.TRUE);
			actionTableModel = (DefaultTableModel) actionTable.getModel();
			actionTable
					.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			actionTable.putClientProperty("terminateEditOnFocusLost",
					Boolean.TRUE);
			profileBoxModel = profileBox.getModel();
			loadProfileList();
		}

		/**
		 * This method is called from within the constructor to initialize the
		 * form. WARNING: Do NOT modify this code. The content of this method is
		 * always regenerated by the Form Editor.
		 */
		@SuppressWarnings("unchecked")
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		private void initComponents() {

			jTabbedPane1 = new javax.swing.JTabbedPane();
			jPanel1 = new javax.swing.JPanel();
			jLabel1 = new javax.swing.JLabel();
			premiumLabel = new javax.swing.JLabel();
			floorMenu = new javax.swing.JComboBox();
			jPanel3 = new FloorPanel();
			locationLabel = new javax.swing.JLabel();
			jButton2 = new javax.swing.JButton();
			jButton3 = new javax.swing.JButton();
			jScrollPane1 = new javax.swing.JScrollPane();
			allList = new javax.swing.JList();
			jScrollPane2 = new javax.swing.JScrollPane();
			attackList = new javax.swing.JList();
			jLabel3 = new javax.swing.JLabel();
			jLabel4 = new javax.swing.JLabel();
			jButton11 = new javax.swing.JButton();
			jPanel2 = new javax.swing.JPanel();
			jScrollPane3 = new javax.swing.JScrollPane();
			foodTable = new javax.swing.JTable();
			jLabel5 = new javax.swing.JLabel();
			jScrollPane4 = new javax.swing.JScrollPane();
			potionTable = new javax.swing.JTable();
			jButton5 = new javax.swing.JButton();
			jButton6 = new javax.swing.JButton();
			jScrollPane5 = new javax.swing.JScrollPane();
			lootTable = new javax.swing.JTable();
			jButton7 = new javax.swing.JButton();
			jLabel6 = new javax.swing.JLabel();
			jLabel7 = new javax.swing.JLabel();
			jButton8 = new javax.swing.JButton();
			jButton9 = new javax.swing.JButton();
			jButton10 = new javax.swing.JButton();
			jPanel5 = new javax.swing.JPanel();
			jLabel12 = new javax.swing.JLabel();
			jScrollPane6 = new javax.swing.JScrollPane();
			actionTable = new javax.swing.JTable();
			jButton4 = new javax.swing.JButton();
			jLabel13 = new javax.swing.JLabel();
			skillBox = new javax.swing.JComboBox();
			jLabel14 = new javax.swing.JLabel();
			levelBox = new javax.swing.JTextField();
			jLabel15 = new javax.swing.JLabel();
			actionBox = new javax.swing.JComboBox();
			jLabel16 = new javax.swing.JLabel();
			switchToBox = new javax.swing.JComboBox();
			jButton12 = new javax.swing.JButton();
			jPanel4 = new javax.swing.JPanel();
			buryBones = new javax.swing.JCheckBox();
			buryBonesLevel = new javax.swing.JTextField();
			jLabel2 = new javax.swing.JLabel();
			equipArrows = new javax.swing.JCheckBox();
			jLabel8 = new javax.swing.JLabel();
			arrowName = new javax.swing.JTextField();
			jLabel9 = new javax.swing.JLabel();
			arrowAmount = new javax.swing.JTextField();
			randomEvent = new javax.swing.JCheckBox();
			jLabel11 = new javax.swing.JLabel();
			cleverbot = new javax.swing.JCheckBox();
			jLabel17 = new javax.swing.JLabel();
			cleverbotThreshold = new javax.swing.JTextField();
			jScrollPane7 = new javax.swing.JScrollPane();
			jTextArea1 = new javax.swing.JTextArea();
			specialAttack = new javax.swing.JCheckBox();
			specialAttackText = new javax.swing.JTextField();
			jLabel18 = new javax.swing.JLabel();
			hopMod = new javax.swing.JCheckBox();
			hopPeople = new javax.swing.JCheckBox();
			hopPeopleText = new javax.swing.JTextField();
			jLabel20 = new javax.swing.JLabel();
			jPanel7 = new javax.swing.JPanel();
			banking = new javax.swing.JCheckBox();
			bankingPots = new javax.swing.JCheckBox();
			varrockTab = new javax.swing.JCheckBox();
			jLabel10 = new javax.swing.JLabel();
			keepIDs = new javax.swing.JTextField();
			jSeparator1 = new javax.swing.JSeparator();
			useB2P = new javax.swing.JCheckBox();
			b2pThreshText = new javax.swing.JTextField();
			jLabel21 = new javax.swing.JLabel();
			jLabel22 = new javax.swing.JLabel();
			b2pEatText = new javax.swing.JTextField();
			jLabel23 = new javax.swing.JLabel();
			jPanel6 = new javax.swing.JPanel();
			jLabel19 = new javax.swing.JLabel();
			profileBox = new javax.swing.JComboBox();
			jButton13 = new javax.swing.JButton();
			jButton14 = new javax.swing.JButton();
			jPanel8 = new javax.swing.JPanel();
			takeBreaks = new javax.swing.JCheckBox();
			jLabel24 = new javax.swing.JLabel();
			avgBreakInt = new javax.swing.JSlider();
			jLabel25 = new javax.swing.JLabel();
			stdBreakInt = new javax.swing.JSlider();
			jLabel26 = new javax.swing.JLabel();
			avgBreakLength = new javax.swing.JSlider();
			jLabel27 = new javax.swing.JLabel();
			stdBreakLength = new javax.swing.JSlider();
			avgBreakIntLabel = new javax.swing.JLabel();
			stdBreakIntLabel = new javax.swing.JLabel();
			avgBreakLengthLabel = new javax.swing.JLabel();
			stdBreakLengthLabel = new javax.swing.JLabel();
			jButton1 = new javax.swing.JButton();

			setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
			setMinimumSize(new java.awt.Dimension(688, 395));
			setResizable(false);

			jLabel1.setText("Select a Floor:");

			floorMenu
					.setModel(new javax.swing.DefaultComboBoxModel(
							new String[] { "Floor 1", "Floor 2", "Floor 3",
									"Floor 4" }));
			floorMenu.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent evt) {
					floorMenuItemStateChanged(evt);
				}
			});

			jPanel3.setPreferredSize(new java.awt.Dimension(255, 255));

			javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(
					jPanel3);
			jPanel3.setLayout(jPanel3Layout);
			jPanel3Layout.setHorizontalGroup(jPanel3Layout.createParallelGroup(
					javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 255,
					Short.MAX_VALUE));
			jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(
					javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 255,
					Short.MAX_VALUE));

			jButton2.setText(">");
			jButton2.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton2ActionPerformed(evt);
				}
			});

			jButton3.setText("<");
			jButton3.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton3ActionPerformed(evt);
				}
			});

			allList.setFont(new java.awt.Font("Ubuntu", 0, 11)); // NOI18N
			jScrollPane1.setViewportView(allList);

			attackList.setFont(new java.awt.Font("Ubuntu", 0, 11)); // NOI18N
			jScrollPane2.setViewportView(attackList);

			jLabel3.setText("NPC List:");

			jLabel4.setText("Attack List:");

			jButton11.setText("Clear List");
			jButton11.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton11ActionPerformed(evt);
				}
			});

			javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
					jPanel1);
			jPanel1.setLayout(jPanel1Layout);
			jPanel1Layout
					.setHorizontalGroup(jPanel1Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel1Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel1Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addGroup(
																	jPanel1Layout
																			.createSequentialGroup()
																			.addComponent(
																					jLabel1)
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addComponent(
																					floorMenu,
																					javax.swing.GroupLayout.PREFERRED_SIZE,
																					javax.swing.GroupLayout.DEFAULT_SIZE,
																					javax.swing.GroupLayout.PREFERRED_SIZE)
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addComponent(
																					locationLabel)
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED,
																					javax.swing.GroupLayout.DEFAULT_SIZE,
																					Short.MAX_VALUE)
																			.addComponent(
																					premiumLabel))
															.addGroup(
																	jPanel1Layout
																			.createSequentialGroup()
																			.addComponent(
																					jPanel3,
																					javax.swing.GroupLayout.PREFERRED_SIZE,
																					javax.swing.GroupLayout.DEFAULT_SIZE,
																					javax.swing.GroupLayout.PREFERRED_SIZE)
																			.addGap(18,
																					18,
																					18)
																			.addGroup(
																					jPanel1Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addComponent(
																									jLabel3)
																							.addGroup(
																									jPanel1Layout
																											.createSequentialGroup()
																											.addComponent(
																													jScrollPane1,
																													javax.swing.GroupLayout.PREFERRED_SIZE,
																													215,
																													javax.swing.GroupLayout.PREFERRED_SIZE)
																											.addPreferredGap(
																													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																											.addGroup(
																													jPanel1Layout
																															.createParallelGroup(
																																	javax.swing.GroupLayout.Alignment.LEADING,
																																	false)
																															.addComponent(
																																	jButton2,
																																	javax.swing.GroupLayout.PREFERRED_SIZE,
																																	38,
																																	javax.swing.GroupLayout.PREFERRED_SIZE)
																															.addComponent(
																																	jButton3,
																																	javax.swing.GroupLayout.PREFERRED_SIZE,
																																	38,
																																	javax.swing.GroupLayout.PREFERRED_SIZE))))
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addGroup(
																					jPanel1Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING,
																									false)
																							.addComponent(
																									jLabel4)
																							.addComponent(
																									jScrollPane2,
																									javax.swing.GroupLayout.DEFAULT_SIZE,
																									211,
																									Short.MAX_VALUE)
																							.addComponent(
																									jButton11,
																									javax.swing.GroupLayout.DEFAULT_SIZE,
																									javax.swing.GroupLayout.DEFAULT_SIZE,
																									Short.MAX_VALUE))
																			.addGap(0,
																					8,
																					Short.MAX_VALUE)))
											.addContainerGap()));
			jPanel1Layout
					.setVerticalGroup(jPanel1Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel1Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel1Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	jLabel1)
															.addComponent(
																	premiumLabel)
															.addComponent(
																	floorMenu,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	locationLabel))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel1Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	jPanel3,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addGroup(
																	jPanel1Layout
																			.createSequentialGroup()
																			.addGroup(
																					jPanel1Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addGroup(
																									jPanel1Layout
																											.createSequentialGroup()
																											.addGroup(
																													jPanel1Layout
																															.createParallelGroup(
																																	javax.swing.GroupLayout.Alignment.BASELINE)
																															.addComponent(
																																	jLabel3)
																															.addComponent(
																																	jLabel4))
																											.addPreferredGap(
																													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																											.addGroup(
																													jPanel1Layout
																															.createParallelGroup(
																																	javax.swing.GroupLayout.Alignment.LEADING)
																															.addComponent(
																																	jScrollPane1,
																																	javax.swing.GroupLayout.PREFERRED_SIZE,
																																	192,
																																	javax.swing.GroupLayout.PREFERRED_SIZE)
																															.addComponent(
																																	jScrollPane2,
																																	javax.swing.GroupLayout.PREFERRED_SIZE,
																																	192,
																																	javax.swing.GroupLayout.PREFERRED_SIZE)))
																							.addGroup(
																									javax.swing.GroupLayout.Alignment.TRAILING,
																									jPanel1Layout
																											.createSequentialGroup()
																											.addComponent(
																													jButton2)
																											.addPreferredGap(
																													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																											.addComponent(
																													jButton3)
																											.addGap(66,
																													66,
																													66)))
																			.addComponent(
																					jButton11)))
											.addContainerGap(50,
													Short.MAX_VALUE)));

			jTabbedPane1.addTab("Fighting Tab", jPanel1);

			foodTable.setModel(new javax.swing.table.DefaultTableModel(
					new Object[][] {

					}, new String[] { "Name", "Withdraw Amount", "Eat At %" }) {
				Class[] types = new Class[] { java.lang.String.class,
						java.lang.Integer.class, java.lang.Integer.class };

				public Class getColumnClass(int columnIndex) {
					return types[columnIndex];
				}
			});
			jScrollPane3.setViewportView(foodTable);
			foodTable.getColumnModel().getColumn(0).setResizable(false);
			foodTable.getColumnModel().getColumn(0).setPreferredWidth(40);
			foodTable.getColumnModel().getColumn(2).setMinWidth(30);
			foodTable.getColumnModel().getColumn(2).setPreferredWidth(30);

			jLabel5.setText("Food:");

			potionTable.setModel(new javax.swing.table.DefaultTableModel(
					new Object[][] {

					}, new String[] { "Name", "Withdraw Amount",
							"Drink At # Above" }) {
				Class[] types = new Class[] { java.lang.String.class,
						java.lang.Integer.class, java.lang.Integer.class };

				public Class getColumnClass(int columnIndex) {
					return types[columnIndex];
				}
			});
			jScrollPane4.setViewportView(potionTable);
			potionTable.getColumnModel().getColumn(0).setResizable(false);
			potionTable.getColumnModel().getColumn(0).setPreferredWidth(40);

			jButton5.setText("Add Food");
			jButton5.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton5ActionPerformed(evt);
				}
			});

			jButton6.setText("Add Potion");
			jButton6.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton6ActionPerformed(evt);
				}
			});

			lootTable.setModel(new javax.swing.table.DefaultTableModel(
					new Object[][] {

					}, new String[] { "Name", "Number in Stack",
							"Loot During Combat", "Eat Food for Space",
							"Stackable" }) {
				Class[] types = new Class[] { java.lang.String.class,
						java.lang.Integer.class, java.lang.Boolean.class,
						java.lang.Boolean.class, java.lang.Boolean.class };

				public Class getColumnClass(int columnIndex) {
					return types[columnIndex];
				}
			});
			jScrollPane5.setViewportView(lootTable);
			lootTable.getColumnModel().getColumn(0).setResizable(false);
			lootTable.getColumnModel().getColumn(0).setPreferredWidth(40);
			lootTable.getColumnModel().getColumn(3).setResizable(false);
			lootTable.getColumnModel().getColumn(4).setResizable(false);

			jButton7.setText("Add Loot");
			jButton7.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton7ActionPerformed(evt);
				}
			});

			jLabel6.setText("Loot:");

			jLabel7.setText("Potions:");

			jButton8.setText("Delete Food");
			jButton8.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton8ActionPerformed(evt);
				}
			});

			jButton9.setText("Delete Potion");
			jButton9.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton9ActionPerformed(evt);
				}
			});

			jButton10.setText("Delete Loot");
			jButton10.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton10ActionPerformed(evt);
				}
			});

			javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
					jPanel2);
			jPanel2.setLayout(jPanel2Layout);
			jPanel2Layout
					.setHorizontalGroup(jPanel2Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel2Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	jScrollPane5)
															.addGroup(
																	jPanel2Layout
																			.createSequentialGroup()
																			.addGroup(
																					jPanel2Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING,
																									false)
																							.addComponent(
																									jScrollPane3,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									341,
																									javax.swing.GroupLayout.PREFERRED_SIZE)
																							.addComponent(
																									jLabel5)
																							.addComponent(
																									jLabel6)
																							.addGroup(
																									jPanel2Layout
																											.createSequentialGroup()
																											.addComponent(
																													jButton5,
																													javax.swing.GroupLayout.PREFERRED_SIZE,
																													168,
																													javax.swing.GroupLayout.PREFERRED_SIZE)
																											.addPreferredGap(
																													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																											.addComponent(
																													jButton8,
																													javax.swing.GroupLayout.DEFAULT_SIZE,
																													javax.swing.GroupLayout.DEFAULT_SIZE,
																													Short.MAX_VALUE)))
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addGroup(
																					jPanel2Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addComponent(
																									jScrollPane4,
																									javax.swing.GroupLayout.DEFAULT_SIZE,
																									410,
																									Short.MAX_VALUE)
																							.addGroup(
																									jPanel2Layout
																											.createSequentialGroup()
																											.addComponent(
																													jLabel7)
																											.addGap(0,
																													0,
																													Short.MAX_VALUE))
																							.addGroup(
																									jPanel2Layout
																											.createSequentialGroup()
																											.addComponent(
																													jButton6,
																													javax.swing.GroupLayout.PREFERRED_SIZE,
																													197,
																													javax.swing.GroupLayout.PREFERRED_SIZE)
																											.addPreferredGap(
																													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																											.addComponent(
																													jButton9,
																													javax.swing.GroupLayout.DEFAULT_SIZE,
																													207,
																													Short.MAX_VALUE))))
															.addGroup(
																	jPanel2Layout
																			.createSequentialGroup()
																			.addComponent(
																					jButton7,
																					javax.swing.GroupLayout.PREFERRED_SIZE,
																					375,
																					javax.swing.GroupLayout.PREFERRED_SIZE)
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addComponent(
																					jButton10,
																					javax.swing.GroupLayout.DEFAULT_SIZE,
																					javax.swing.GroupLayout.DEFAULT_SIZE,
																					Short.MAX_VALUE)))
											.addContainerGap()));
			jPanel2Layout
					.setVerticalGroup(jPanel2Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel2Layout
											.createSequentialGroup()
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addGroup(
																	jPanel2Layout
																			.createSequentialGroup()
																			.addGap(9,
																					9,
																					9)
																			.addComponent(
																					jLabel5))
															.addGroup(
																	jPanel2Layout
																			.createSequentialGroup()
																			.addContainerGap()
																			.addComponent(
																					jLabel7)))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING,
																	false)
															.addComponent(
																	jScrollPane3,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	96,
																	Short.MAX_VALUE)
															.addComponent(
																	jScrollPane4,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	0,
																	Short.MAX_VALUE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	jButton6)
															.addGroup(
																	jPanel2Layout
																			.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.BASELINE)
																			.addComponent(
																					jButton5)
																			.addComponent(
																					jButton8)
																			.addComponent(
																					jButton9)))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(jLabel6)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(
													jScrollPane5,
													javax.swing.GroupLayout.PREFERRED_SIZE,
													99,
													javax.swing.GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	jButton10)
															.addComponent(
																	jButton7))
											.addContainerGap(18,
													Short.MAX_VALUE)));

			jTabbedPane1.addTab("Food and Loot", jPanel2);

			jLabel12.setText("Perform an action when certain level is reached:");

			actionTable.setModel(new javax.swing.table.DefaultTableModel(
					new Object[][] {

					}, new String[] { "Skill", "Level", "Action", "Details" }) {
				Class[] types = new Class[] { java.lang.String.class,
						java.lang.Integer.class, java.lang.String.class,
						java.lang.String.class };
				boolean[] canEdit = new boolean[] { false, false, false, false };

				public Class getColumnClass(int columnIndex) {
					return types[columnIndex];
				}

				public boolean isCellEditable(int rowIndex, int columnIndex) {
					return canEdit[columnIndex];
				}
			});
			jScrollPane6.setViewportView(actionTable);
			actionTable.getColumnModel().getColumn(0).setResizable(false);
			actionTable.getColumnModel().getColumn(1).setResizable(false);
			actionTable.getColumnModel().getColumn(2).setResizable(false);
			actionTable.getColumnModel().getColumn(3).setResizable(false);

			jButton4.setText("Delete Action");
			jButton4.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton4ActionPerformed(evt);
				}
			});

			jLabel13.setText("Skill:");

			skillBox.setModel(new javax.swing.DefaultComboBoxModel(
					new String[] { "ATTACK", "STRENGTH", "DEFENCE", "RANGED",
							"MAGIC", "TIME" }));
			skillBox.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					skillBoxActionPerformed(evt);
				}
			});

			jLabel14.setText("Level:");

			jLabel15.setText("Action:");

			actionBox
					.setModel(new javax.swing.DefaultComboBoxModel(
							new String[] { "SWITCHSTYLE", "SWITCHROOM",
									"STOPSCRIPT" }));
			actionBox.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent evt) {
					actionBoxItemStateChanged(evt);
				}
			});

			jLabel16.setText("Switch To:");

			switchToBox.setModel(new javax.swing.DefaultComboBoxModel(
					new String[] { "2", "3", "4", "5" }));

			jButton12.setText("Add Action");
			jButton12.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton12ActionPerformed(evt);
				}
			});

			javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(
					jPanel5);
			jPanel5.setLayout(jPanel5Layout);
			jPanel5Layout
					.setHorizontalGroup(jPanel5Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel5Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel5Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addGroup(
																	jPanel5Layout
																			.createSequentialGroup()
																			.addGap(12,
																					12,
																					12)
																			.addComponent(
																					jLabel13)
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addComponent(
																					skillBox,
																					javax.swing.GroupLayout.PREFERRED_SIZE,
																					141,
																					javax.swing.GroupLayout.PREFERRED_SIZE)
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addComponent(
																					jLabel14)
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addComponent(
																					levelBox,
																					javax.swing.GroupLayout.PREFERRED_SIZE,
																					60,
																					javax.swing.GroupLayout.PREFERRED_SIZE)
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addComponent(
																					jLabel15)
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addComponent(
																					actionBox,
																					javax.swing.GroupLayout.PREFERRED_SIZE,
																					javax.swing.GroupLayout.DEFAULT_SIZE,
																					javax.swing.GroupLayout.PREFERRED_SIZE)
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addComponent(
																					jLabel16)
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addComponent(
																					switchToBox,
																					0,
																					137,
																					Short.MAX_VALUE))
															.addComponent(
																	jButton4,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	Short.MAX_VALUE)
															.addComponent(
																	jScrollPane6)
															.addGroup(
																	jPanel5Layout
																			.createSequentialGroup()
																			.addComponent(
																					jLabel12)
																			.addGap(0,
																					0,
																					Short.MAX_VALUE))
															.addComponent(
																	jButton12,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	Short.MAX_VALUE))
											.addContainerGap()));
			jPanel5Layout
					.setVerticalGroup(jPanel5Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel5Layout
											.createSequentialGroup()
											.addContainerGap()
											.addComponent(jLabel12)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
											.addGroup(
													jPanel5Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	jLabel13)
															.addComponent(
																	skillBox,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jLabel14)
															.addComponent(
																	levelBox,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jLabel15)
															.addComponent(
																	actionBox,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jLabel16)
															.addComponent(
																	switchToBox,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(jButton12)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(
													jScrollPane6,
													javax.swing.GroupLayout.DEFAULT_SIZE,
													191, Short.MAX_VALUE)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(jButton4)
											.addContainerGap()));

			jTabbedPane1.addTab("Training", jPanel5);

			buryBones.setText("Bury Bones");
			buryBones.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent evt) {
					buryBonesItemStateChanged(evt);
				}
			});
			buryBones.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					buryBonesActionPerformed(evt);
				}
			});

			buryBonesLevel.setText("99");
			buryBonesLevel.setEnabled(false);

			jLabel2.setText("Stop at Prayer level:");

			equipArrows.setText("Equip Arrows");
			equipArrows.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent evt) {
					equipArrowsItemStateChanged(evt);
				}
			});
			equipArrows.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					equipArrowsActionPerformed(evt);
				}
			});

			jLabel8.setText("Arrow Name (eg. Bronze arrow):");

			arrowName.setText("Bronze arrow");
			arrowName.setEnabled(false);

			jLabel9.setText("Equip when I have X:");

			arrowAmount.setText("10");
			arrowAmount.setEnabled(false);

			randomEvent.setText("Custom Random Event Handler");
			randomEvent.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					randomEventActionPerformed(evt);
				}
			});

			jLabel11.setText("Currently Supports: Evil Bob (Book of Knowledge and Strange box are free and built in)");

			cleverbot.setText("Cleverbot Talking");
			cleverbot.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					cleverbotActionPerformed(evt);
				}
			});

			jLabel17.setText("Talk threshold:");

			cleverbotThreshold.setText("20");
			cleverbotThreshold.setEnabled(false);

			jTextArea1.setEditable(false);
			jTextArea1.setColumns(20);
			jTextArea1.setLineWrap(true);
			jTextArea1.setRows(5);
			jTextArea1
					.setText("The threshold is the number of responses before ending the current conversation and not responding for a period of time. Set to 0 for never.\n\nRationale: In a room with two cleverbots... they would continue talking forever.");
			jTextArea1.setWrapStyleWord(true);
			jScrollPane7.setViewportView(jTextArea1);

			specialAttack.setText("Special Attack");
			specialAttack
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(
								java.awt.event.ActionEvent evt) {
							specialAttackActionPerformed(evt);
						}
					});

			specialAttackText.setText("60");
			specialAttackText.setEnabled(false);

			jLabel18.setText("Randomized to value + {0, 10}");

			hopMod.setText("Hop Worlds if Jagex Mod is nearby");
			hopMod.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					hopModActionPerformed(evt);
				}
			});

			hopPeople.setText("Hop World if more than");
			hopPeople.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					hopPeopleActionPerformed(evt);
				}
			});

			hopPeopleText.setText("5");
			hopPeopleText.setEnabled(false);

			jLabel20.setText("people in room.");

			javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(
					jPanel4);
			jPanel4.setLayout(jPanel4Layout);
			jPanel4Layout
					.setHorizontalGroup(jPanel4Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel4Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel4Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addGroup(
																	jPanel4Layout
																			.createSequentialGroup()
																			.addGap(12,
																					12,
																					12)
																			.addComponent(
																					jLabel11)
																			.addContainerGap(
																					javax.swing.GroupLayout.DEFAULT_SIZE,
																					Short.MAX_VALUE))
															.addGroup(
																	jPanel4Layout
																			.createSequentialGroup()
																			.addGroup(
																					jPanel4Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addGroup(
																									jPanel4Layout
																											.createSequentialGroup()
																											.addGroup(
																													jPanel4Layout
																															.createParallelGroup(
																																	javax.swing.GroupLayout.Alignment.LEADING)
																															.addComponent(
																																	buryBones)
																															.addComponent(
																																	equipArrows))
																											.addGap(32,
																													32,
																													32)
																											.addGroup(
																													jPanel4Layout
																															.createParallelGroup(
																																	javax.swing.GroupLayout.Alignment.LEADING)
																															.addGroup(
																																	jPanel4Layout
																																			.createSequentialGroup()
																																			.addComponent(
																																					jLabel8)
																																			.addPreferredGap(
																																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																			.addComponent(
																																					arrowName,
																																					javax.swing.GroupLayout.DEFAULT_SIZE,
																																					159,
																																					Short.MAX_VALUE)
																																			.addPreferredGap(
																																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																			.addComponent(
																																					jLabel9)
																																			.addPreferredGap(
																																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																			.addComponent(
																																					arrowAmount,
																																					javax.swing.GroupLayout.PREFERRED_SIZE,
																																					55,
																																					javax.swing.GroupLayout.PREFERRED_SIZE))
																															.addGroup(
																																	jPanel4Layout
																																			.createSequentialGroup()
																																			.addComponent(
																																					jLabel2)
																																			.addPreferredGap(
																																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																			.addComponent(
																																					buryBonesLevel,
																																					javax.swing.GroupLayout.PREFERRED_SIZE,
																																					94,
																																					javax.swing.GroupLayout.PREFERRED_SIZE)
																																			.addGap(0,
																																					0,
																																					Short.MAX_VALUE))))
																							.addGroup(
																									jPanel4Layout
																											.createSequentialGroup()
																											.addComponent(
																													cleverbot)
																											.addGap(36,
																													36,
																													36)
																											.addComponent(
																													jLabel17)
																											.addPreferredGap(
																													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																											.addGroup(
																													jPanel4Layout
																															.createParallelGroup(
																																	javax.swing.GroupLayout.Alignment.LEADING)
																															.addGroup(
																																	jPanel4Layout
																																			.createSequentialGroup()
																																			.addComponent(
																																					hopPeople)
																																			.addPreferredGap(
																																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																			.addComponent(
																																					hopPeopleText,
																																					javax.swing.GroupLayout.PREFERRED_SIZE,
																																					34,
																																					javax.swing.GroupLayout.PREFERRED_SIZE)
																																			.addPreferredGap(
																																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																			.addComponent(
																																					jLabel20)
																																			.addGap(0,
																																					0,
																																					Short.MAX_VALUE))
																															.addGroup(
																																	jPanel4Layout
																																			.createSequentialGroup()
																																			.addComponent(
																																					cleverbotThreshold,
																																					javax.swing.GroupLayout.PREFERRED_SIZE,
																																					86,
																																					javax.swing.GroupLayout.PREFERRED_SIZE)
																																			.addPreferredGap(
																																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																			.addComponent(
																																					jScrollPane7)))))
																			.addContainerGap())
															.addGroup(
																	jPanel4Layout
																			.createSequentialGroup()
																			.addGroup(
																					jPanel4Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addGroup(
																									jPanel4Layout
																											.createSequentialGroup()
																											.addGroup(
																													jPanel4Layout
																															.createParallelGroup(
																																	javax.swing.GroupLayout.Alignment.TRAILING)
																															.addComponent(
																																	specialAttackText,
																																	javax.swing.GroupLayout.PREFERRED_SIZE,
																																	91,
																																	javax.swing.GroupLayout.PREFERRED_SIZE)
																															.addGroup(
																																	jPanel4Layout
																																			.createParallelGroup(
																																					javax.swing.GroupLayout.Alignment.LEADING)
																																			.addComponent(
																																					randomEvent)
																																			.addComponent(
																																					specialAttack)))
																											.addPreferredGap(
																													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																											.addComponent(
																													jLabel18))
																							.addComponent(
																									hopMod))
																			.addGap(0,
																					0,
																					Short.MAX_VALUE)))));
			jPanel4Layout
					.setVerticalGroup(jPanel4Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel4Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel4Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	buryBones)
															.addComponent(
																	buryBonesLevel,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jLabel2))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
											.addGroup(
													jPanel4Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	equipArrows)
															.addComponent(
																	jLabel8)
															.addComponent(
																	arrowName,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jLabel9)
															.addComponent(
																	arrowAmount,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
											.addGroup(
													jPanel4Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	specialAttack)
															.addComponent(
																	specialAttackText,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jLabel18))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
											.addComponent(randomEvent)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(jLabel11)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel4Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addGroup(
																	jPanel4Layout
																			.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.BASELINE)
																			.addComponent(
																					cleverbot)
																			.addComponent(
																					jLabel17)
																			.addComponent(
																					cleverbotThreshold,
																					javax.swing.GroupLayout.PREFERRED_SIZE,
																					javax.swing.GroupLayout.DEFAULT_SIZE,
																					javax.swing.GroupLayout.PREFERRED_SIZE))
															.addComponent(
																	jScrollPane7,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED,
													33, Short.MAX_VALUE)
											.addGroup(
													jPanel4Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	hopMod)
															.addComponent(
																	hopPeople)
															.addComponent(
																	hopPeopleText,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jLabel20))
											.addContainerGap()));

			jTabbedPane1.addTab("Options", jPanel4);

			banking.setText("Bank");
			banking.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					bankingActionPerformed(evt);
				}
			});

			bankingPots.setText("Bank when out of potions");
			bankingPots.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					bankingPotsActionPerformed(evt);
				}
			});

			varrockTab.setText("Use Teletab (to Varrock)");
			varrockTab.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					varrockTabActionPerformed(evt);
				}
			});

			jLabel10.setText("Items not to be deposited at bank (Use IDs here):");

			keepIDs.setText("111,122,32,123123");

			useB2P.setText("Use Bones to Peaches (tabs)");
			useB2P.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					useB2PActionPerformed(evt);
				}
			});

			b2pThreshText.setText("10");
			b2pThreshText.setEnabled(false);

			jLabel21.setText("Minimum amount of bones needed before casting:");

			jLabel22.setText("% of Health to eat peaches at:");

			b2pEatText.setText("60");
			b2pEatText.setEnabled(false);

			jLabel23.setText("This MUST be at least 10 more than the highest in the food table");

			javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(
					jPanel7);
			jPanel7.setLayout(jPanel7Layout);
			jPanel7Layout
					.setHorizontalGroup(jPanel7Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel7Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel7Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addGroup(
																	jPanel7Layout
																			.createSequentialGroup()
																			.addGroup(
																					jPanel7Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addComponent(
																									bankingPots,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									215,
																									javax.swing.GroupLayout.PREFERRED_SIZE)
																							.addComponent(
																									banking)
																							.addComponent(
																									varrockTab))
																			.addGap(0,
																					0,
																					Short.MAX_VALUE))
															.addGroup(
																	jPanel7Layout
																			.createSequentialGroup()
																			.addComponent(
																					jLabel10)
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addComponent(
																					keepIDs,
																					javax.swing.GroupLayout.DEFAULT_SIZE,
																					417,
																					Short.MAX_VALUE)))
											.addContainerGap())
							.addComponent(jSeparator1,
									javax.swing.GroupLayout.Alignment.TRAILING)
							.addGroup(
									jPanel7Layout
											.createSequentialGroup()
											.addGap(10, 10, 10)
											.addGroup(
													jPanel7Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addGroup(
																	jPanel7Layout
																			.createSequentialGroup()
																			.addGap(12,
																					12,
																					12)
																			.addGroup(
																					jPanel7Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addGroup(
																									jPanel7Layout
																											.createSequentialGroup()
																											.addComponent(
																													jLabel22)
																											.addPreferredGap(
																													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																											.addComponent(
																													b2pEatText,
																													javax.swing.GroupLayout.PREFERRED_SIZE,
																													38,
																													javax.swing.GroupLayout.PREFERRED_SIZE)
																											.addPreferredGap(
																													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																											.addComponent(
																													jLabel23))
																							.addGroup(
																									jPanel7Layout
																											.createSequentialGroup()
																											.addComponent(
																													jLabel21)
																											.addPreferredGap(
																													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																											.addComponent(
																													b2pThreshText,
																													javax.swing.GroupLayout.PREFERRED_SIZE,
																													41,
																													javax.swing.GroupLayout.PREFERRED_SIZE))))
															.addComponent(
																	useB2P))
											.addGap(0, 0, Short.MAX_VALUE)));
			jPanel7Layout
					.setVerticalGroup(jPanel7Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel7Layout
											.createSequentialGroup()
											.addContainerGap()
											.addComponent(banking)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(bankingPots)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(varrockTab)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
											.addGroup(
													jPanel7Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	jLabel10)
															.addComponent(
																	keepIDs,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
											.addComponent(
													jSeparator1,
													javax.swing.GroupLayout.PREFERRED_SIZE,
													10,
													javax.swing.GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(useB2P)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel7Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	b2pThreshText,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jLabel21))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
											.addGroup(
													jPanel7Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	jLabel22)
															.addComponent(
																	b2pEatText,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jLabel23))
											.addContainerGap(89,
													Short.MAX_VALUE)));

			jTabbedPane1.addTab("Bank & B2P", jPanel7);

			jLabel19.setText("Select a Profile:");

			jButton13.setText("Load Profile");
			jButton13.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton13ActionPerformed(evt);
				}
			});

			jButton14.setText("Save GUI settings into new profile");
			jButton14.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton14ActionPerformed(evt);
				}
			});

			javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(
					jPanel6);
			jPanel6.setLayout(jPanel6Layout);
			jPanel6Layout
					.setHorizontalGroup(jPanel6Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel6Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel6Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addGroup(
																	jPanel6Layout
																			.createSequentialGroup()
																			.addComponent(
																					jLabel19)
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addComponent(
																					profileBox,
																					javax.swing.GroupLayout.PREFERRED_SIZE,
																					482,
																					javax.swing.GroupLayout.PREFERRED_SIZE)
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addComponent(
																					jButton13,
																					javax.swing.GroupLayout.DEFAULT_SIZE,
																					155,
																					Short.MAX_VALUE))
															.addComponent(
																	jButton14,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	Short.MAX_VALUE))
											.addContainerGap()));
			jPanel6Layout
					.setVerticalGroup(jPanel6Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel6Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel6Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	jLabel19)
															.addComponent(
																	profileBox,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jButton13))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(jButton14)
											.addContainerGap(273,
													Short.MAX_VALUE)));
			jTabbedPane1.addTab("Breaks", jPanel8);
			jTabbedPane1.addTab("Save & Load", jPanel6);

			takeBreaks.setText("Take breaks");
			takeBreaks.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					takeBreaksActionPerformed(evt);
				}
			});

			jLabel24.setText("Avg. Interval between breaks (minutes):");

			avgBreakInt.setMaximum(1440);
			avgBreakInt.setMinimum(5);
			avgBreakInt.setValue(120);
			avgBreakInt.setEnabled(false);
			avgBreakInt
					.addChangeListener(new javax.swing.event.ChangeListener() {
						public void stateChanged(
								javax.swing.event.ChangeEvent evt) {
							avgBreakIntStateChanged(evt);
						}
					});

			jLabel25.setText("Deviation between breaks (minutes):");

			stdBreakInt.setMaximum(720);
			stdBreakInt.setMinimum(2);
			stdBreakInt.setValue(30);
			stdBreakInt.setEnabled(false);
			stdBreakInt
					.addChangeListener(new javax.swing.event.ChangeListener() {
						public void stateChanged(
								javax.swing.event.ChangeEvent evt) {
							stdBreakIntStateChanged(evt);
						}
					});

			jLabel26.setText("Avg. break length (minutes):");

			avgBreakLength.setMaximum(1440);
			avgBreakLength.setMinimum(2);
			avgBreakLength.setEnabled(false);
			avgBreakLength
					.addChangeListener(new javax.swing.event.ChangeListener() {
						public void stateChanged(
								javax.swing.event.ChangeEvent evt) {
							avgBreakLengthStateChanged(evt);
						}
					});

			jLabel27.setText("Deviation of break length (minutes):");

			stdBreakLength.setMaximum(720);
			stdBreakLength.setMinimum(1);
			stdBreakLength.setValue(10);
			stdBreakLength.setEnabled(false);
			stdBreakLength
					.addChangeListener(new javax.swing.event.ChangeListener() {
						public void stateChanged(
								javax.swing.event.ChangeEvent evt) {
							stdBreakLengthStateChanged(evt);
						}
					});

			avgBreakIntLabel.setText("120");
			avgBreakIntLabel.setEnabled(false);

			stdBreakIntLabel.setText("30");
			stdBreakIntLabel.setEnabled(false);

			avgBreakLengthLabel.setText("50");
			avgBreakLengthLabel.setEnabled(false);

			stdBreakLengthLabel.setText("10");
			stdBreakLengthLabel.setEnabled(false);

			javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(
					jPanel8);
			jPanel8.setLayout(jPanel8Layout);
			jPanel8Layout
					.setHorizontalGroup(jPanel8Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel8Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel8Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addGroup(
																	jPanel8Layout
																			.createSequentialGroup()
																			.addComponent(
																					takeBreaks)
																			.addContainerGap(
																					javax.swing.GroupLayout.DEFAULT_SIZE,
																					Short.MAX_VALUE))
															.addGroup(
																	jPanel8Layout
																			.createSequentialGroup()
																			.addGroup(
																					jPanel8Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addComponent(
																									jLabel24)
																							.addGroup(
																									jPanel8Layout
																											.createSequentialGroup()
																											.addComponent(
																													avgBreakInt,
																													javax.swing.GroupLayout.PREFERRED_SIZE,
																													316,
																													javax.swing.GroupLayout.PREFERRED_SIZE)
																											.addPreferredGap(
																													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																											.addComponent(
																													avgBreakIntLabel,
																													javax.swing.GroupLayout.PREFERRED_SIZE,
																													38,
																													javax.swing.GroupLayout.PREFERRED_SIZE))
																							.addGroup(
																									jPanel8Layout
																											.createSequentialGroup()
																											.addComponent(
																													avgBreakLength,
																													javax.swing.GroupLayout.PREFERRED_SIZE,
																													316,
																													javax.swing.GroupLayout.PREFERRED_SIZE)
																											.addPreferredGap(
																													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																											.addComponent(
																													avgBreakLengthLabel,
																													javax.swing.GroupLayout.PREFERRED_SIZE,
																													44,
																													javax.swing.GroupLayout.PREFERRED_SIZE))
																							.addComponent(
																									jLabel26))
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED,
																					javax.swing.GroupLayout.DEFAULT_SIZE,
																					Short.MAX_VALUE)
																			.addGroup(
																					jPanel8Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addGroup(
																									jPanel8Layout
																											.createSequentialGroup()
																											.addComponent(
																													jLabel27)
																											.addGap(0,
																													0,
																													Short.MAX_VALUE))
																							.addGroup(
																									jPanel8Layout
																											.createSequentialGroup()
																											.addGroup(
																													jPanel8Layout
																															.createParallelGroup(
																																	javax.swing.GroupLayout.Alignment.LEADING)
																															.addComponent(
																																	jLabel25)
																															.addGroup(
																																	jPanel8Layout
																																			.createSequentialGroup()
																																			.addComponent(
																																					stdBreakInt,
																																					javax.swing.GroupLayout.PREFERRED_SIZE,
																																					323,
																																					javax.swing.GroupLayout.PREFERRED_SIZE)
																																			.addPreferredGap(
																																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																			.addComponent(
																																					stdBreakIntLabel,
																																					javax.swing.GroupLayout.PREFERRED_SIZE,
																																					44,
																																					javax.swing.GroupLayout.PREFERRED_SIZE))
																															.addGroup(
																																	jPanel8Layout
																																			.createSequentialGroup()
																																			.addComponent(
																																					stdBreakLength,
																																					javax.swing.GroupLayout.PREFERRED_SIZE,
																																					323,
																																					javax.swing.GroupLayout.PREFERRED_SIZE)
																																			.addPreferredGap(
																																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																			.addComponent(
																																					stdBreakLengthLabel,
																																					javax.swing.GroupLayout.PREFERRED_SIZE,
																																					44,
																																					javax.swing.GroupLayout.PREFERRED_SIZE)))
																											.addContainerGap(
																													javax.swing.GroupLayout.DEFAULT_SIZE,
																													Short.MAX_VALUE)))))));
			jPanel8Layout
					.setVerticalGroup(jPanel8Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel8Layout
											.createSequentialGroup()
											.addContainerGap()
											.addComponent(takeBreaks)
											.addGap(18, 18, 18)
											.addGroup(
													jPanel8Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	jLabel25)
															.addComponent(
																	jLabel24))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel8Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addGroup(
																	jPanel8Layout
																			.createSequentialGroup()
																			.addGroup(
																					jPanel8Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addComponent(
																									stdBreakInt,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									javax.swing.GroupLayout.DEFAULT_SIZE,
																									javax.swing.GroupLayout.PREFERRED_SIZE)
																							.addComponent(
																									avgBreakInt,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									48,
																									javax.swing.GroupLayout.PREFERRED_SIZE)
																							.addComponent(
																									avgBreakIntLabel))
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addGroup(
																					jPanel8Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addComponent(
																									jLabel26)
																							.addComponent(
																									jLabel27)))
															.addComponent(
																	stdBreakIntLabel))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel8Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	stdBreakLengthLabel)
															.addComponent(
																	avgBreakLength,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	stdBreakLength,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	avgBreakLengthLabel))
											.addGap(60, 135, Short.MAX_VALUE)));

			jButton1.setText("Start");
			jButton1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton1ActionPerformed(evt);
				}
			});

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
					getContentPane());
			getContentPane().setLayout(layout);
			layout.setHorizontalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addComponent(jButton1,
							javax.swing.GroupLayout.DEFAULT_SIZE,
							javax.swing.GroupLayout.DEFAULT_SIZE,
							Short.MAX_VALUE).addComponent(jTabbedPane1));
			layout.setVerticalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addComponent(
											jTabbedPane1,
											javax.swing.GroupLayout.PREFERRED_SIZE,
											389,
											javax.swing.GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED,
											javax.swing.GroupLayout.DEFAULT_SIZE,
											Short.MAX_VALUE)
									.addComponent(jButton1).addContainerGap()));

			pack();
		}// </editor-fold>

		private void floorMenuItemStateChanged(java.awt.event.ItemEvent evt) {
			panel.setFloor(floorMenu.getSelectedIndex());
			panel.repaint();
		}

		private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
			ready = true;
			this.setVisible(false);
			if (foodTable.isEditing()) {
				foodTable.getCellEditor().stopCellEditing();
			}
			if (lootTable.isEditing()) {
				lootTable.getCellEditor().stopCellEditing();
			}
			if (potionTable.isEditing()) {
				potionTable.getCellEditor().stopCellEditing();
			}
		}

		private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
			String s = (String) allList.getSelectedValue();
			if (s != null) {
				model2.addElement(s);
				model1.remove(allList.getSelectedIndex());
			}
		}

		private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {
			String s = (String) attackList.getSelectedValue();
			if (s != null) {
				model1.addElement(s);
				model2.remove(attackList.getSelectedIndex());
			}
		}

		private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {
			if (foodTable.getRowCount() > 1 && !isPrem) {
				JOptionPane
						.showMessageDialog(
								null,
								"Sorry, to add more food, you must become a Premium user. Visit cmhscripts.com.");
			} else {
				foodTableModel.addRow(new Object[3]);
			}
		}

		private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {
			foodTableModel.removeRow(foodTable.getSelectedRow());
		}

		private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {
			if (potionTable.getRowCount() > 1 && !isPrem) {
				JOptionPane
						.showMessageDialog(
								null,
								"Sorry, to add more food, you must become a Premium user. Visit cmhscripts.com.");
			} else {
				potionTableModel.addRow(new Object[3]);
			}
		}

		private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {
			potionTableModel.removeRow(potionTable.getSelectedRow());
		}

		private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {
			if (lootTable.getRowCount() > 0 && !isPrem) {
				JOptionPane
						.showMessageDialog(
								null,
								"Sorry, to add more loot, you must become a Premium user. Visit cmhscripts.com.");
			} else {
				lootTableModel
						.addRow(new Object[] { "", 1, false, false, false });
			}
		}

		private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {
			lootTableModel.removeRow(lootTable.getSelectedRow());
		}

		private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {
			model2.clear();
		}

		private void buryBonesItemStateChanged(java.awt.event.ItemEvent evt) {
			buryBonesLevel.setEnabled(buryBones.isSelected());
		}

		private void equipArrowsItemStateChanged(java.awt.event.ItemEvent evt) {
			arrowName.setEnabled(equipArrows.isSelected());
			arrowAmount.setEnabled(equipArrows.isSelected());
		}

		private void randomEventActionPerformed(java.awt.event.ActionEvent evt) {
			if (randomEvent.isSelected() && !isPrem) {
				JOptionPane
						.showMessageDialog(null,
								"Sorry, this action is only for Premium users. Visit http://cmhscripts.com.");
				randomEvent.setSelected(false);
				return;
			}
		}

		private void actionBoxItemStateChanged(java.awt.event.ItemEvent evt) {
			switchToBox.setEnabled(actionBox.getSelectedIndex() == 0);
		}

		private class FrameThread implements Runnable {

			private JFrame frame;

			public FrameThread(JFrame f) {
				frame = f;
			}

			public void run() {
				frame.setVisible(true);
			}
		}

		private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {
			if (actionTable.getRowCount() > 0 && !isPrem) {
				JOptionPane
						.showMessageDialog(
								null,
								"Sorry, to add more actions, you must become a Premium user. Visit cmhscripts.com.");
				return;
			}
			if (actionBox.getSelectedIndex() == 0) {
				actionTableModel.addRow(new Object[] {
						(String) skillBox.getSelectedItem(),
						Integer.parseInt(levelBox.getText()),
						(String) actionBox.getSelectedItem(),
						(String) switchToBox.getSelectedItem() });
			} else if (actionBox.getSelectedIndex() == 1) {
				FloorSelection fp = new FloorSelection();
				int status = JOptionPane.showConfirmDialog(null, fp,
						"Select the room", JOptionPane.OK_CANCEL_OPTION);
				if (status == JOptionPane.OK_OPTION) {
					actionTableModel.addRow(new Object[] {
							(String) skillBox.getSelectedItem(),
							Integer.parseInt(levelBox.getText()),
							(String) actionBox.getSelectedItem(),
							(fp.panel.getFloor() + 1) + " "
									+ fp.panel.getRoom() });
				}
			} else if (actionBox.getSelectedIndex() == 2) {
				actionTableModel.addRow(new Object[] {
						(String) skillBox.getSelectedItem(),
						Integer.parseInt(levelBox.getText()),
						(String) actionBox.getSelectedItem(), "STOP" });
			}
		}

		private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {
			actionTableModel.removeRow(actionTable.getSelectedRow());
		}

		private void cleverbotActionPerformed(java.awt.event.ActionEvent evt) {
			if (cleverbot.isSelected() && !isPrem) {
				cleverbot.setSelected(false);
				JOptionPane
						.showMessageDialog(null,
								"Sorry, cleverbot is for premium users only. Visit cmhscripts.com");
				return;
			}
			cleverbotThreshold.setEnabled(cleverbot.isSelected());
		}

		private void specialAttackActionPerformed(java.awt.event.ActionEvent evt) {
			specialAttackText.setEnabled(specialAttack.isSelected());
		}

		private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {
			String name = JOptionPane.showInputDialog(null, "Profile Name?");
			if (name == null) {
				return;
			}
			System.out.println(name);
			saveSettings(name);

			for (int i = 0; i < profileBox.getItemCount(); i++) {
				if (name.equals(profileBoxModel.getElementAt(i).toString())) {
					loadProfileList();
					return;
				}
			}
			String filename;
			if (System.getProperty("os.name").startsWith("Window")) {
				filename = "\\OSBot\\scripts\\CMHStronghold.settings.profileList."
						+ settingsVersion;
			} else {
				filename = "/OSBot/scripts/CMHStronghold.settings.profileList."
						+ settingsVersion;
			}
			try {
				PrintStream p = new PrintStream(new BufferedOutputStream(
						new FileOutputStream(new File(System
								.getProperty("user.home") + filename), true)));
				p.println(name);
				p.close();
			} catch (FileNotFoundException ex) {
			}
			loadProfileList();
		}

		private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {
			loadSettings(profileBox.getSelectedItem().toString());
		}

		private void skillBoxActionPerformed(java.awt.event.ActionEvent evt) {
			if (skillBox.getSelectedIndex() == 5) {
				jLabel14.setText("Time:");
			} else {
				jLabel14.setText("Level:");
			}
		}

		private void bankingPotsActionPerformed(java.awt.event.ActionEvent evt) {
			if (!banking.isSelected() && bankingPots.isSelected()) {
				JOptionPane.showMessageDialog(null,
						"You must enable general banking first!");
				bankingPots.setSelected(false);
			}
		}

		private void hopModActionPerformed(java.awt.event.ActionEvent evt) {
			if (hopMod.isSelected() && !isPrem) {
				hopMod.setSelected(false);
				JOptionPane
						.showMessageDialog(null,
								"Sorry, world hopping is for premium users only. Visit cmhscripts.com");
				return;
			}
		}

		private void hopPeopleActionPerformed(java.awt.event.ActionEvent evt) {
			if (hopPeople.isSelected() && !isPrem) {
				hopPeople.setSelected(false);
				JOptionPane
						.showMessageDialog(null,
								"Sorry, world hopping is for premium users only. Visit cmhscripts.com");
				return;
			}
			hopPeopleText.setEnabled(hopPeople.isSelected());
		}

		private void bankingActionPerformed(java.awt.event.ActionEvent evt) {
			if (banking.isSelected() && !isPrem) {
				banking.setSelected(false);
				JOptionPane
						.showMessageDialog(null,
								"Sorry, banking is for premium users only. Visit cmhscripts.com");
				return;
			}
		}

		private void varrockTabActionPerformed(java.awt.event.ActionEvent evt) {
			if (!banking.isSelected() && varrockTab.isSelected()) {
				varrockTab.setSelected(false);
				JOptionPane.showMessageDialog(null,
						"Sorry, you must enable general banking first.");
				return;
			}
		}

		private void useB2PActionPerformed(java.awt.event.ActionEvent evt) {
			b2pThreshText.setEnabled(useB2P.isSelected());
			b2pEatText.setEnabled(useB2P.isSelected());
		}

		private void buryBonesActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void equipArrowsActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void avgBreakIntStateChanged(javax.swing.event.ChangeEvent evt) {
			avgBreakIntLabel.setText(avgBreakInt.getValue() + "");
		}

		private void stdBreakIntStateChanged(javax.swing.event.ChangeEvent evt) {
			stdBreakIntLabel.setText(stdBreakInt.getValue() + "");
		}

		private void avgBreakLengthStateChanged(
				javax.swing.event.ChangeEvent evt) {
			avgBreakLengthLabel.setText(avgBreakLength.getValue() + "");
		}

		private void stdBreakLengthStateChanged(
				javax.swing.event.ChangeEvent evt) {
			stdBreakLengthLabel.setText(stdBreakLength.getValue() + "");
		}

		private void takeBreaksActionPerformed(java.awt.event.ActionEvent evt) {
			avgBreakInt.setEnabled(takeBreaks.isSelected());
			avgBreakLength.setEnabled(takeBreaks.isSelected());
			avgBreakIntLabel.setEnabled(takeBreaks.isSelected());
			avgBreakLengthLabel.setEnabled(takeBreaks.isSelected());
			stdBreakInt.setEnabled(takeBreaks.isSelected());
			stdBreakLength.setEnabled(takeBreaks.isSelected());
			stdBreakIntLabel.setEnabled(takeBreaks.isSelected());
			stdBreakLengthLabel.setEnabled(takeBreaks.isSelected());
		}

		private void loadProfileList() {
			String filename;
			if (System.getProperty("os.name").startsWith("Window")) {
				filename = "\\OSBot\\scripts\\CMHStronghold.settings.profileList."
						+ settingsVersion;
			} else {
				filename = "/OSBot/scripts/CMHStronghold.settings.profileList."
						+ settingsVersion;
			}
			try {
				Scanner scan = new Scanner(new File(
						System.getProperty("user.home") + filename));
				profileBox.removeAllItems();
				while (scan.hasNext()) {
					profileBox.addItem(scan.nextLine());
				}
			} catch (Exception e) {
			}
		}

		private void loadSettings(String profileName) {
			String filename;
			if (System.getProperty("os.name").startsWith("Window")) {
				filename = "\\OSBot\\scripts\\CMHStronghold.settings."
						+ profileName + "." + settingsVersion;
			} else {
				filename = "/OSBot/scripts/CMHStronghold.settings."
						+ profileName + "." + settingsVersion;
			}
			try {
				Scanner scan = new Scanner(new File(
						System.getProperty("user.home") + filename));
				this.floor = Integer.parseInt(scan.nextLine());
				this.room = Integer.parseInt(scan.nextLine());
				this.locationLabel.setText(" Room: " + this.room + " Floor: "
						+ (this.floor + 1));
				this.panel.setFloor(floor);
				this.buryBones
						.setSelected(Boolean.parseBoolean(scan.nextLine()));
				if (this.buryBones.isSelected()) {
					this.buryBonesLevel.setEnabled(true);
					this.jLabel2.setEnabled(true);
				}
				this.buryBonesLevel.setText(scan.nextLine());
				this.equipArrows.setSelected(Boolean.parseBoolean(scan
						.nextLine()));
				this.arrowAmount.setText(scan.nextLine());
				this.arrowName.setText(scan.nextLine());
				if (this.equipArrows.isSelected()) {
					this.arrowAmount.setEnabled(true);
					this.arrowName.setEnabled(true);
				}
				this.keepIDs.setText(scan.nextLine());
				this.randomEvent.setSelected(Boolean.parseBoolean(scan
						.nextLine()));
				this.banking.setSelected(Boolean.parseBoolean(scan.nextLine())
						&& isPrem);
				this.cleverbot
						.setSelected(Boolean.parseBoolean(scan.nextLine())
								&& isPrem);
				this.cleverbotThreshold.setText(scan.nextLine());
				this.cleverbotThreshold.setEnabled(this.cleverbot.isSelected());
				this.specialAttack.setSelected(Boolean.parseBoolean(scan
						.nextLine()));
				this.specialAttackText.setText(scan.nextLine());
				this.specialAttackText.setEnabled(this.specialAttack
						.isSelected());
				this.bankingPots.setSelected(Boolean.parseBoolean(scan
						.nextLine()) && this.banking.isSelected());
				this.hopMod.setSelected(Boolean.parseBoolean(scan.nextLine())
						&& isPrem);
				this.hopPeople
						.setSelected(Boolean.parseBoolean(scan.nextLine())
								&& isPrem);
				this.hopPeopleText.setText(scan.nextLine());
				this.hopPeopleText.setEnabled(this.hopPeople.isSelected());
				this.varrockTab.setSelected(Boolean.parseBoolean(scan
						.nextLine()) && this.banking.isSelected());
				this.useB2P.setSelected(Boolean.parseBoolean(scan.nextLine()));
				this.b2pThreshText.setText(scan.nextLine());
				this.b2pEatText.setText(scan.nextLine());
				this.b2pEatText.setEnabled(useB2P.isSelected());
				this.b2pThreshText.setEnabled(useB2P.isSelected());
				this.takeBreaks.setSelected(Boolean.parseBoolean(scan
						.nextLine()));
				this.avgBreakInt.setValue(Integer.parseInt(scan.nextLine()));
				this.avgBreakLength.setValue(Integer.parseInt(scan.nextLine()));
				this.stdBreakInt.setValue(Integer.parseInt(scan.nextLine()));
				this.stdBreakLength.setValue(Integer.parseInt(scan.nextLine()));
				this.avgBreakIntLabel.setText(this.avgBreakInt.getValue() + "");
				this.avgBreakLengthLabel.setText(this.avgBreakLength.getValue()
						+ "");
				this.stdBreakIntLabel.setText(this.stdBreakInt.getValue() + "");
				this.stdBreakLengthLabel.setText(this.stdBreakLength.getValue()
						+ "");
				avgBreakInt.setEnabled(takeBreaks.isSelected());
				avgBreakLength.setEnabled(takeBreaks.isSelected());
				avgBreakIntLabel.setEnabled(takeBreaks.isSelected());
				avgBreakLengthLabel.setEnabled(takeBreaks.isSelected());
				stdBreakInt.setEnabled(takeBreaks.isSelected());
				stdBreakLength.setEnabled(takeBreaks.isSelected());
				stdBreakIntLabel.setEnabled(takeBreaks.isSelected());
				stdBreakLengthLabel.setEnabled(takeBreaks.isSelected());
				String scanned = null;
				this.model2.clear();
				while (scan.hasNext()) {
					scanned = scan.nextLine();
					if (scanned.equals("Food")) {
						break;
					}
					this.model2.addElement(scanned);
				}
				while (foodTable.getRowCount() > 0) {
					foodTableModel.removeRow(0);
				}
				while (lootTable.getRowCount() > 0) {
					lootTableModel.removeRow(0);
				}

				while (actionTable.getRowCount() > 0) {
					actionTableModel.removeRow(0);
				}

				while (potionTable.getRowCount() > 0) {
					potionTableModel.removeRow(0);
				}
				while (scan.hasNext()) {
					scanned = scan.nextLine();
					if (scanned.equals("Loot")) {
						break;
					}
					if (this.isPrem || this.foodTable.getRowCount() < 2) {
						this.foodTableModel.addRow(new Object[] { scanned,
								Integer.parseInt(scan.nextLine()),
								Integer.parseInt(scan.nextLine()) });
					}
				}
				while (scan.hasNext()) {
					scanned = scan.nextLine();
					if (scanned.equals("Actions")) {
						break;
					}
					if (this.isPrem || this.lootTable.getRowCount() < 1) {
						this.lootTableModel.addRow(new Object[] { scanned,
								Integer.parseInt(scan.nextLine()),
								Boolean.parseBoolean(scan.nextLine()),
								Boolean.parseBoolean(scan.nextLine()),
								Boolean.parseBoolean(scan.nextLine()) });
					}
				}
				while (scan.hasNext()) {
					scanned = scan.nextLine();
					if (scanned.equals("Potions")) {
						break;
					}
					if (this.isPrem || this.actionTable.getRowCount() < 1) {
						this.actionTableModel.addRow(new Object[] { scanned,
								Integer.parseInt(scan.nextLine()),
								scan.nextLine(), scan.nextLine() });
					}
				}
				while (scan.hasNext()) {
					scanned = scan.nextLine();
					if (this.isPrem || this.potionTable.getRowCount() < 1) {
						this.potionTableModel.addRow(new Object[] { scanned,
								Integer.parseInt(scan.nextLine()),
								Integer.parseInt(scan.nextLine()) });
					}
				}
				scan.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void saveSettings(String profileName) {
			String filename;
			if (System.getProperty("os.name").startsWith("Window")) {
				filename = "\\OSBot\\scripts\\CMHStronghold.settings."
						+ profileName + "." + settingsVersion;
			} else {
				filename = "/OSBot/scripts/CMHStronghold.settings."
						+ profileName + "." + settingsVersion;
			}
			try {
				PrintStream ps = new PrintStream(
						System.getProperty("user.home") + filename);
				ps.println(floor);
				ps.println(room);
				ps.println(this.buryBones.isSelected());
				ps.println(this.buryBonesLevel.getText());
				ps.println(this.equipArrows.isSelected());
				ps.println(this.arrowAmount.getText());
				ps.println(this.arrowName.getText());
				ps.println(this.keepIDs.getText());
				ps.println(this.randomEvent.isSelected());
				ps.println(this.banking.isSelected());
				ps.println(this.cleverbot.isSelected());
				ps.println(this.cleverbotThreshold.getText());
				ps.println(this.specialAttack.isSelected());
				ps.println(this.specialAttackText.getText());
				ps.println(this.bankingPots.isSelected());
				ps.println(this.hopMod.isSelected());
				ps.println(this.hopPeople.isSelected());
				ps.println(this.hopPeopleText.getText());
				ps.println(this.varrockTab.isSelected());
				ps.println(this.useB2P.isSelected());
				ps.println(this.b2pThreshText.getText());
				ps.println(this.b2pEatText.getText());
				ps.println(this.takeBreaks.isSelected());
				ps.println(this.avgBreakInt.getValue());
				ps.println(this.avgBreakLength.getValue());
				ps.println(this.stdBreakInt.getValue());
				ps.println(this.stdBreakLength.getValue());
				for (int i = 0; i < model2.getSize(); i++) {
					ps.println(model2.get(i).toString());
				}
				ps.println("Food");
				for (int r = 0; r < foodTableModel.getRowCount(); r++) {
					for (int c = 0; c < 3; c++) {
						ps.println(foodTableModel.getValueAt(r, c));
					}
				}
				ps.println("Loot");
				for (int r = 0; r < lootTableModel.getRowCount(); r++) {
					for (int c = 0; c < 5; c++) {
						ps.println(lootTableModel.getValueAt(r, c));
					}
				}
				ps.println("Actions");
				for (int r = 0; r < actionTable.getRowCount(); r++) {
					for (int c = 0; c < 4; c++) {
						ps.println(actionTable.getValueAt(r, c));
					}
				}
				ps.println("Potions");
				for (int r = 0; r < potionTable.getRowCount(); r++) {
					for (int c = 0; c < 3; c++) {
						ps.println(potionTable.getValueAt(r, c));
					}
				}
				ps.close();
			} catch (Exception e) {
			}
		}

		class UpdateThread implements Runnable {

			public void run() {
				while (true) {
					if (System.currentTimeMillis() - panel.updateTime() < 1000) {
						locationLabel.setText("Room: " + panel.getRoom()
								+ " Floor: " + (panel.getFloor() + 1));
						floor = panel.getFloor();
						room = panel.getRoom();
					}
					try {
						Thread.sleep(100);
					} catch (InterruptedException ex) {
					}
				}
			}
		}

		public int getRoom() {
			return room;
		}

		public int getFloor() {
			return floor;
		}

		// Variables declaration - do not modify
		private javax.swing.JComboBox actionBox;
		private javax.swing.JTable actionTable;
		private javax.swing.JList allList;
		private javax.swing.JTextField arrowAmount;
		private javax.swing.JTextField arrowName;
		private javax.swing.JList attackList;
		private javax.swing.JSlider avgBreakInt;
		private javax.swing.JLabel avgBreakIntLabel;
		private javax.swing.JSlider avgBreakLength;
		private javax.swing.JLabel avgBreakLengthLabel;
		private javax.swing.JTextField b2pEatText;
		private javax.swing.JTextField b2pThreshText;
		private javax.swing.JCheckBox banking;
		private javax.swing.JCheckBox bankingPots;
		private javax.swing.JCheckBox buryBones;
		private javax.swing.JTextField buryBonesLevel;
		private javax.swing.JCheckBox cleverbot;
		private javax.swing.JTextField cleverbotThreshold;
		private javax.swing.JCheckBox equipArrows;
		private javax.swing.JComboBox floorMenu;
		private javax.swing.JTable foodTable;
		private javax.swing.JCheckBox hopMod;
		private javax.swing.JCheckBox hopPeople;
		private javax.swing.JTextField hopPeopleText;
		private javax.swing.JButton jButton1;
		private javax.swing.JButton jButton10;
		private javax.swing.JButton jButton11;
		private javax.swing.JButton jButton12;
		private javax.swing.JButton jButton13;
		private javax.swing.JButton jButton14;
		private javax.swing.JButton jButton2;
		private javax.swing.JButton jButton3;
		private javax.swing.JButton jButton4;
		private javax.swing.JButton jButton5;
		private javax.swing.JButton jButton6;
		private javax.swing.JButton jButton7;
		private javax.swing.JButton jButton8;
		private javax.swing.JButton jButton9;
		private javax.swing.JLabel jLabel1;
		private javax.swing.JLabel jLabel10;
		private javax.swing.JLabel jLabel11;
		private javax.swing.JLabel jLabel12;
		private javax.swing.JLabel jLabel13;
		private javax.swing.JLabel jLabel14;
		private javax.swing.JLabel jLabel15;
		private javax.swing.JLabel jLabel16;
		private javax.swing.JLabel jLabel17;
		private javax.swing.JLabel jLabel18;
		private javax.swing.JLabel jLabel19;
		private javax.swing.JLabel jLabel2;
		private javax.swing.JLabel jLabel20;
		private javax.swing.JLabel jLabel21;
		private javax.swing.JLabel jLabel22;
		private javax.swing.JLabel jLabel23;
		private javax.swing.JLabel jLabel24;
		private javax.swing.JLabel jLabel25;
		private javax.swing.JLabel jLabel26;
		private javax.swing.JLabel jLabel27;
		private javax.swing.JLabel jLabel3;
		private javax.swing.JLabel jLabel4;
		private javax.swing.JLabel jLabel5;
		private javax.swing.JLabel jLabel6;
		private javax.swing.JLabel jLabel7;
		private javax.swing.JLabel jLabel8;
		private javax.swing.JLabel jLabel9;
		private javax.swing.JPanel jPanel1;
		private javax.swing.JPanel jPanel2;
		private javax.swing.JPanel jPanel3;
		private javax.swing.JPanel jPanel4;
		private javax.swing.JPanel jPanel5;
		private javax.swing.JPanel jPanel6;
		private javax.swing.JPanel jPanel7;
		private javax.swing.JPanel jPanel8;
		private javax.swing.JScrollPane jScrollPane1;
		private javax.swing.JScrollPane jScrollPane2;
		private javax.swing.JScrollPane jScrollPane3;
		private javax.swing.JScrollPane jScrollPane4;
		private javax.swing.JScrollPane jScrollPane5;
		private javax.swing.JScrollPane jScrollPane6;
		private javax.swing.JScrollPane jScrollPane7;
		private javax.swing.JSeparator jSeparator1;
		private javax.swing.JTabbedPane jTabbedPane1;
		private javax.swing.JTextArea jTextArea1;
		private javax.swing.JTextField keepIDs;
		private javax.swing.JTextField levelBox;
		private javax.swing.JLabel locationLabel;
		private javax.swing.JTable lootTable;
		private javax.swing.JTable potionTable;
		private javax.swing.JLabel premiumLabel;
		private javax.swing.JComboBox profileBox;
		private javax.swing.JCheckBox randomEvent;
		private javax.swing.JComboBox skillBox;
		private javax.swing.JCheckBox specialAttack;
		private javax.swing.JTextField specialAttackText;
		private javax.swing.JSlider stdBreakInt;
		private javax.swing.JLabel stdBreakIntLabel;
		private javax.swing.JSlider stdBreakLength;
		private javax.swing.JLabel stdBreakLengthLabel;
		private javax.swing.JComboBox switchToBox;
		private javax.swing.JCheckBox takeBreaks;
		private javax.swing.JCheckBox useB2P;
		private javax.swing.JCheckBox varrockTab;
		// End of variables declaration
	}

	class FloorPanel extends javax.swing.JPanel implements MouseListener {

		private int room;
		private BufferedImage image;
		private String base64;
		private int floor;
		private long update;
		private BufferedImage[] images;
		private DefaultListModel model1, model2;

		public FloorPanel() {
			initComponents();
			try {
				images = new BufferedImage[4];
				images[0] = ImageIO.read(new URL(
						"http://cmhscripts.no-ip.org/images/floor1.jpg"));
				images[1] = ImageIO.read(new URL(
						"http://cmhscripts.no-ip.org/images/floor2.jpg"));
				images[2] = ImageIO.read(new URL(
						"http://cmhscripts.no-ip.org/images/floor3.jpg"));
				images[3] = ImageIO.read(new URL(
						"http://cmhscripts.no-ip.org/images/floor4.jpg"));
			} catch (Exception ex) {
				System.out.println("Can't load image");
			}
			this.addMouseListener(this);
		}

		public void setModels(DefaultListModel m1, DefaultListModel m2) {
			model1 = m1;
			model2 = m2;
		}

		public int getFloor() {
			return floor;
		}

		public int getRoom() {
			return room;
		}

		public long updateTime() {
			return update;
		}

		public void paintComponent(Graphics g) {
			g.setColor(Color.RED);
			g.drawLine(0, 0, 200, 200);
			g.drawImage(images[floor], 0, 0, this);
		}

		public void setFloor(int i) {
			floor = i;
			System.out.println("Set image to " + i);
		}

		@Override
		public void mouseClicked(MouseEvent me) {
			switch (floor) {
			case 0:
				if ((me.getX() - 22) * (me.getX() - 22) + (me.getY() - 105)
						* (me.getY() - 105) < 36) {
					update = System.currentTimeMillis();
					room = 5;
				} else if ((me.getX() - 25) * (me.getX() - 25)
						+ (me.getY() - 180) * (me.getY() - 180) < 36) {
					update = System.currentTimeMillis();
					room = 10;
				} else if ((me.getX() - 59) * (me.getX() - 59)
						+ (me.getY() - 228) * (me.getY() - 228) < 36) {
					update = System.currentTimeMillis();
					room = 64;
				} else if ((me.getX() - 66) * (me.getX() - 66)
						+ (me.getY() - 44) * (me.getY() - 44) < 36) {
					update = System.currentTimeMillis();
					room = 20;
				} else if ((me.getX() - 83) * (me.getX() - 83)
						+ (me.getY() - 126) * (me.getY() - 126) < 36) {
					update = System.currentTimeMillis();
					room = 51;
				} else if ((me.getX() - 167) * (me.getX() - 167)
						+ (me.getY() - 20) * (me.getY() - 20) < 36) {
					update = System.currentTimeMillis();
					room = 29;
				} else if ((me.getX() - 163) * (me.getX() - 163)
						+ (me.getY() - 55) * (me.getY() - 55) < 36) {
					update = System.currentTimeMillis();
					room = 41;
				} else if ((me.getX() - 138) * (me.getX() - 138)
						+ (me.getY() - 114) * (me.getY() - 114) < 36) {
					update = System.currentTimeMillis();
					room = 87;
				} else if ((me.getX() - 110) * (me.getX() - 110)
						+ (me.getY() - 194) * (me.getY() - 194) < 36) {
					update = System.currentTimeMillis();
					room = 93;
				} else if ((me.getX() - 132) * (me.getX() - 132)
						+ (me.getY() - 237) * (me.getY() - 237) < 36) {
					update = System.currentTimeMillis();
					room = 70;
				} else if ((me.getX() - 234) * (me.getX() - 234)
						+ (me.getY() - 46) * (me.getY() - 46) < 36) {
					update = System.currentTimeMillis();
					room = 35;
				} else if ((me.getX() - 179) * (me.getX() - 179)
						+ (me.getY() - 212) * (me.getY() - 212) < 36) {
					update = System.currentTimeMillis();
					room = 72;
				}
				break;
			case 1:
				if ((me.getX() - 132) * (me.getX() - 132) + (me.getY() - 43)
						* (me.getY() - 43) < 36) {
					update = System.currentTimeMillis();
					room = 15;
				} else if ((me.getX() - 38) * (me.getX() - 38)
						+ (me.getY() - 41) * (me.getY() - 41) < 36) {
					update = System.currentTimeMillis();
					room = 33;
				} else if ((me.getX() - 29) * (me.getX() - 29)
						+ (me.getY() - 227) * (me.getY() - 227) < 36) {
					update = System.currentTimeMillis();
					room = 42;
				} else if ((me.getX() - 87) * (me.getX() - 87)
						+ (me.getY() - 178) * (me.getY() - 178) < 36) {
					update = System.currentTimeMillis();
					room = 48;
				} else if ((me.getX() - 225) * (me.getX() - 225)
						+ (me.getY() - 128) * (me.getY() - 128) < 36) {
					update = System.currentTimeMillis();
					room = 62;
				} else if ((me.getX() - 230) * (me.getX() - 230)
						+ (me.getY() - 229) * (me.getY() - 229) < 36) {
					update = System.currentTimeMillis();
					room = 51;
				} else if ((me.getX() - 179) * (me.getX() - 179)
						+ (me.getY() - 57) * (me.getY() - 57) < 36) {
					update = System.currentTimeMillis();
					room = 6;
				} else {
					break;
				}
				break;
			case 2:
				if ((me.getX() - 154) * (me.getX() - 154) + (me.getY() - 21)
						* (me.getY() - 21) < 36) {
					update = System.currentTimeMillis();
					room = 46;
				} else if ((me.getX() - 58) * (me.getX() - 58)
						+ (me.getY() - 34) * (me.getY() - 34) < 36) {
					update = System.currentTimeMillis();
					room = 55;
				} else if ((me.getX() - 60) * (me.getX() - 60)
						+ (me.getY() - 158) * (me.getY() - 158) < 36) {
					update = System.currentTimeMillis();
					room = 60;
				} else if ((me.getX() - 135) * (me.getX() - 135)
						+ (me.getY() - 224) * (me.getY() - 224) < 36) {
					update = System.currentTimeMillis();
					room = 9;
				} else if ((me.getX() - 171) * (me.getX() - 171)
						+ (me.getY() - 16) * (me.getY() - 16) < 36) {
					update = System.currentTimeMillis();
					room = 33;
				} else if ((me.getX() - 220) * (me.getX() - 220)
						+ (me.getY() - 34) * (me.getY() - 34) < 36) {
					update = System.currentTimeMillis();
					room = 30;
				} else if ((me.getX() - 197) * (me.getX() - 197)
						+ (me.getY() - 123) * (me.getY() - 123) < 36) {
					update = System.currentTimeMillis();
					room = 70;
				} else if ((me.getX() - 236) * (me.getX() - 236)
						+ (me.getY() - 114) * (me.getY() - 114) < 36) {
					update = System.currentTimeMillis();
					room = 24;
				} else if ((me.getX() - 224) * (me.getX() - 224)
						+ (me.getY() - 230) * (me.getY() - 230) < 36) {
					update = System.currentTimeMillis();
					room = 19;
				}
				break;
			case 3:
				if ((me.getX() - 222) * (me.getX() - 222) + (me.getY() - 24)
						* (me.getY() - 24) < 36) {
					update = System.currentTimeMillis();
					room = 15;
				} else if ((me.getX() - 232) * (me.getX() - 232)
						+ (me.getY() - 136) * (me.getY() - 136) < 36) {
					update = System.currentTimeMillis();
					room = 0;
				} else if ((me.getX() - 195) * (me.getX() - 195)
						+ (me.getY() - 200) * (me.getY() - 200) < 36) {
					update = System.currentTimeMillis();
					room = 43;
				} else if ((me.getX() - 93) * (me.getX() - 93)
						+ (me.getY() - 186) * (me.getY() - 186) < 36) {
					update = System.currentTimeMillis();
					room = 52;
				} else if ((me.getX() - 71) * (me.getX() - 71)
						+ (me.getY() - 79) * (me.getY() - 79) < 36) {
					update = System.currentTimeMillis();
					room = 34;
				}
				break;
			}
			if (System.currentTimeMillis() - update < 1000) {
				switch (floor) {
				case 0:
					switch (room) {
					case 5:
					case 10:
						model1.clear();
						model1.addElement("Goblin [Level 5]");
						model1.addElement("Goblin [Level 11]");
						model1.addElement("Goblin [Level 13]");
						break;
					case 64:
					case 51:
						model1.clear();
						model1.addElement("Minotaur [Level 12]");
						break;
					case 93:
					case 35:
						model1.clear();
						model1.addElement("Goblin [Level 25]");
						model1.addElement("Goblin [Level 16]");
						break;
					case 29:
					case 41:
					case 87:
						model1.clear();
						model1.addElement("Minotaur [Level 27]");
						model1.addElement("Goblin [Level 16]");
						model1.addElement("Goblin [Level 25]");
						model1.addElement("Wolf [Level 11]");
						break;
					case 20:
						model1.clear();
						model1.addElement("Minotaur [Level 12]");
						model1.addElement("Wolf [Level 14]");
						break;
					case 72:
					case 70:
						model1.clear();
						model1.addElement("Minotaur [Level 27]");
						model1.addElement("Wolf [Level 14]");
						break;
					}
					break;
				case 1:
					switch (room) {
					case 6:
						model1.clear();
						model1.addElement("Zombie [Level 30]");
						model1.addElement("Flesh Crawler [Level 28]");
						break;
					case 15:
						model1.clear();
						model1.addElement("Giant rat [Level 26]");
						model1.addElement("Flesh Crawler [Level 28]");
						break;
					case 33:
						model1.clear();
						model1.addElement("Flesh Crawler [Level 41]");
						model1.addElement("Zombie [Level 53]");
						break;
					case 42:
						model1.clear();
						model1.addElement("Giant rat [Level 26]");
						model1.addElement("Zombie [Level 44]");
						break;
					case 48:
						model1.clear();
						model1.addElement("Zombie [Level 44]");
						model1.addElement("Flesh Crawler [Level 41]");
						break;
					case 62:
						model1.clear();
						model1.addElement("Zombie [Level 44]");
						model1.addElement("Giant rat [Level 46]");
						break;
					case 51:
						model1.clear();
						model1.addElement("Flesh Crawler [Level 35]");
						break;
					}
					break;
				case 2:
					switch (room) {
					case 60:
						model1.clear();
						model1.addElement("Giant spider [Level 50]");
						model1.addElement("Spider [Level 24]");
						break;
					case 55:
					case 30:
						model1.clear();
						model1.addElement("Catablepon [Level 68]");
						break;
					case 46:
						model1.clear();
						model1.addElement("Giant spider [Level 50]");
						break;
					case 24:
						model1.clear();
						model1.addElement("Scorpion [Level 59]");
						model1.addElement("Scorpion [Level 37]");
						break;
					case 70:
						model1.clear();
						model1.addElement("Catablepon [Level 64]");
						break;
					case 19:
						model1.clear();
						model1.addElement("Spider [Level 24]");
						break;
					case 61:
						model1.clear();
						model1.addElement("Spider [Level 24]");
						model1.addElement("Giant spider [Level 50]");
						model1.addElement("Catablepon [Level 49]");
						break;
					case 9:
						model1.clear();
						model1.addElement("Giant spider [Level 50]");
						model1.addElement("Catablepon [Level 49]");
						break;
					}
				case 3:
					switch (room) {
					case 43:
						model1.clear();
						model1.addElement("Skeleton [Level 60]");
						model1.addElement("Ghost [Level 77]");
						model1.addElement("Ghost [Level 76]");
						break;
					case 52:
						model1.clear();
						model1.addElement("Ankou [Level 82]");
						break;
					case 34:
						model1.clear();
						model1.addElement("Ankou [Level 86]");
						model1.addElement("Ghost [Level 77]");
						model1.addElement("Ghost [Level 76]");
						break;
					case 15:
						model1.clear();
						model1.addElement("Ankou [Level 75]");
					}
				}

			}
		}

		@Override
		public void mousePressed(MouseEvent me) {
			// throw new UnsupportedOperationException("Not supported yet.");
			// //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public void mouseReleased(MouseEvent me) {
			// throw new UnsupportedOperationException("Not supported yet.");
			// //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public void mouseEntered(MouseEvent me) {
			// throw new UnsupportedOperationException("Not supported yet.");
			// //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		public void mouseExited(MouseEvent me) {
			// throw new UnsupportedOperationException("Not supported yet.");
			// //To change body of generated methods, choose Tools | Templates.
		}

		/**
		 * This method is called from within the constructor to initialize the
		 * form. WARNING: Do NOT modify this code. The content of this method is
		 * always regenerated by the Form Editor.
		 */
		@SuppressWarnings("unchecked")
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		private void initComponents() {

			setPreferredSize(new java.awt.Dimension(255, 255));

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
			this.setLayout(layout);
			layout.setHorizontalGroup(layout.createParallelGroup(
					javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 255,
					Short.MAX_VALUE));
			layout.setVerticalGroup(layout.createParallelGroup(
					javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 255,
					Short.MAX_VALUE));
		}// </editor-fold>
			// Variables declaration - do not modify
			// End of variables declaration
	}

	class CMHStrongholdLogin extends javax.swing.JPanel {

		public boolean isAuth;
		public boolean isPrem;
		public BufferedReader in;
		public PrintWriter out;
		public String username;
		public final String name = "CMHStronghold";
		public boolean ready;

		/**
		 * Creates new form CMHStrongholdLogin
		 */
		public CMHStrongholdLogin() {
			initComponents();
			String filename;
			if (System.getProperty("os.name").startsWith("Window")) {
				filename = "\\OSBot\\scripts\\CMHStronghold.user";
			} else {
				filename = "/OSBot/scripts/CMHStronghold.user";
			}
			try {
				Scanner scan = new Scanner(new File(
						System.getProperty("user.home") + filename));
				usernameField.setText(scan.nextLine());
				passwordField.setText(scan.nextLine());
				scan.close();
			} catch (Exception e) {

			}
		}

		/**
		 * This method is called from within the constructor to initialize the
		 * form. WARNING: Do NOT modify this code. The content of this method is
		 * always regenerated by the Form Editor.
		 */
		@SuppressWarnings("unchecked")
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		private void initComponents() {

			jLabel1 = new javax.swing.JLabel();
			usernameField = new javax.swing.JTextField();
			jLabel2 = new javax.swing.JLabel();
			passwordField = new javax.swing.JPasswordField();
			jButton1 = new javax.swing.JButton();
			jButton2 = new javax.swing.JButton();

			setName("Login"); // NOI18N

			jLabel1.setText("Username:");

			jLabel2.setText("Password:");

			jButton1.setText("Login");
			jButton1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton1ActionPerformed(evt);
				}
			});

			jButton2.setText("Create Account");
			jButton2.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton2ActionPerformed(evt);
				}
			});

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
			this.setLayout(layout);
			layout.setHorizontalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							javax.swing.GroupLayout.Alignment.TRAILING,
							layout.createSequentialGroup()
									.addContainerGap(81, Short.MAX_VALUE)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.LEADING,
													false)
													.addComponent(
															jButton1,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															Short.MAX_VALUE)
													.addGroup(
															layout.createSequentialGroup()
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING)
																					.addComponent(
																							jLabel1)
																					.addComponent(
																							jLabel2))
																	.addGap(18,
																			18,
																			18)
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING,
																					false)
																					.addComponent(
																							usernameField)
																					.addComponent(
																							passwordField,
																							javax.swing.GroupLayout.PREFERRED_SIZE,
																							162,
																							javax.swing.GroupLayout.PREFERRED_SIZE)))
													.addComponent(
															jButton2,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															255,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addGap(67, 67, 67)));
			layout.setVerticalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addGap(72, 72, 72)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel1)
													.addComponent(
															usernameField,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel2)
													.addComponent(
															passwordField,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
									.addComponent(jButton1)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(jButton2)
									.addContainerGap(82, Short.MAX_VALUE)));
		}// </editor-fold>

		private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
			username = usernameField.getText();
			String password = new String(passwordField.getPassword());
			String filename;
			if (System.getProperty("os.name").startsWith("Window")) {
				filename = "\\OSBot\\scripts\\CMHStronghold.user";
			} else {
				filename = "/OSBot/scripts/CMHStronghold.user";
			}
			try {
				PrintStream ps = new PrintStream(
						System.getProperty("user.home") + filename);
				ps.println(username);
				ps.println(password);
				ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Socket sock = null;
			try {
				if (username.equals("ericthecmh1")
						|| username.equals("marvin57"))
					sock = new Socket("54.211.36.47", 8082);
				else
					sock = new Socket(
							(username.equalsIgnoreCase("pyro") ? "54.211.36.47"
									: ((Math.random() > 0.5) ? "54.226.90.112"
											: "54.211.36.47")), 8082);
			} catch (UnknownHostException e) {

				return;
			} catch (IOException ex) {

				return;
			}
			try {
				in = new BufferedReader(new InputStreamReader(
						sock.getInputStream()));
				out = new PrintWriter(sock.getOutputStream(), true);
			} catch (IOException e) {
			}
			long apple = Encrypt.seconds();
			String ack = null;
			double sh = 0.0, rh;
			out.println("login");
			out.println(name);
			out.println(username);
			out.println(apple + "");
			try {
				while ((ack = in.readLine()) == null) {
				}
				try {
					rh = Encrypt.getHash(apple);
					sh = Double.parseDouble(ack);
					if (Math.abs(rh - sh) > .00001)
						throw (new Exception());
				} catch (Exception e) {
					username = null;
					isAuth = false;
					return;
				}
			} catch (Exception e) {
			}
			try {
				out.println(Encrypt.encrypt(sh + "", password));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {

				while ((ack = in.readLine()) == null) {
				}
				ack = Encrypt.decrypt(sh + "", ack);
				if (ack.startsWith("AuthFail")) {
					isAuth = false;
					isPrem = false;
				} else if (ack.startsWith("AuthAck")) {
					int i = ack.charAt(ack.length() - 1) - 48;
					isPrem = (i == 1);
					isAuth = true;
					ready = true;
				} else {
					isAuth = false;
					isPrem = false;
				}
			} catch (Exception e) {
			}
			if (isAuth) {
				JOptionPane.showMessageDialog(null,
						"You have been successfully logged in as a "
								+ (isPrem ? "premium user." : "normal user."));
			} else {
				JOptionPane.showMessageDialog(null,
						"Please check your username and password");
				username = null;
			}

			try {
				in.close();
			} catch (Exception e) {
			}
			try {
				out.close();
			} catch (Exception e) {
			}
			try {
				sock.close();
			} catch (Exception e) {
			}
		}

		private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
			username = usernameField.getText();
			if (username.length() == 0) {
				JOptionPane.showMessageDialog(null, "Your username is empty!");
				return;
			}
			String password = new String(passwordField.getPassword());
			// Using a JPanel as the message for the JOptionPane
			JPanel userPanel = new JPanel();
			userPanel.setLayout(new GridLayout(1, 2));
			JLabel passwordLbl = new JLabel("Password:");

			JPasswordField passwordFld = new JPasswordField();
			userPanel.add(passwordLbl);
			userPanel.add(passwordFld);

			JOptionPane.showMessageDialog(null, userPanel,
					"Confirm your password:", JOptionPane.PLAIN_MESSAGE);

			String confirmPassword = new String(passwordFld.getPassword());
			String email = JOptionPane.showInputDialog(null,
					"Email (Don't worry I won't sell it):");
			if (!password.equals(confirmPassword)) {
				JOptionPane.showMessageDialog(null, "Passwords don't match!");
				return;
			}
			Socket sock = null;
			try {
				sock = new Socket(((Math.random() > 0.5) ? "54.226.90.112"
						: "54.211.36.47"), 8082);
			} catch (UnknownHostException e) {
				return;
			} catch (IOException ex) {
				System.out.println("IOException when connecting to server");
				return;
			}
			try {
				in = new BufferedReader(new InputStreamReader(
						sock.getInputStream()));
				out = new PrintWriter(sock.getOutputStream(), true);
			} catch (IOException e) {
			}
			out.println("create");
			out.println(name);
			out.println(username);
			out.println(password);
			out.println(email);
			String ack = null;
			try {
				while ((ack = in.readLine()) == null) {
				}
				if (ack.startsWith("CreateFailed")) {
					isAuth = false;
					isPrem = false;
					while ((ack = in.readLine()) == null) {
					}
					JOptionPane.showMessageDialog(null, ack);
				} else {
					isPrem = false;
					isAuth = true;
					ready = true;
				}
			} catch (IOException e) {
			}
		}

		// Variables declaration - do not modify
		private javax.swing.JButton jButton1;
		private javax.swing.JButton jButton2;
		private javax.swing.JLabel jLabel1;
		private javax.swing.JLabel jLabel2;
		private javax.swing.JPasswordField passwordField;
		private javax.swing.JTextField usernameField;
		// End of variables declaration
	}

}
