import java.awt.Graphics;
import java.awt.event.MouseEvent;

import org.osbot.script.Script;
import org.osbot.script.ScriptManifest;

@ScriptManifest(name = "CMHStronghold", author = "Ericthecmh", version = 0.1D, info = "Fights in the Stronghold of Security. Version 1.8.7")
public class LoaderStronghold extends Script {
	CMHStronghold stronghold = new CMHStronghold(this);

	public void onStart() {
		stronghold.onStart();
	}

	public int onLoop() {
		return stronghold.onLoop();
	}

	public void onExit() {
		stronghold.onExit();
	}

	public void onPaint(Graphics g) {
		stronghold.onPaint(g);
	}

	public void mouseClicked(MouseEvent e) {
		stronghold.mouseClicked(e);
	}

	public void onMessage(String s) {
		stronghold.onMessage(s);
	}

	public void onPlayerMessage(String s, String message) {
		stronghold.onPlayerMessage(s, message);
	}
}
