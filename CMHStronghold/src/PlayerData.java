import org.osbot.script.rs2.skill.Skill;
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Objects;
 
/**
 * A class which holds Hiscore data for a specific player.
 * Create a new instance by invoking the static factory method {@link PlayerData#lookup(String)}
 * @see SkillData
 * @author Sam
 */
public final class PlayerData {
 
    private static final String PATH_FORMAT = "http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player=%s";
    private static final int SKILL_NUMBER = 24;
 
    private final String name;
    private final int[] rankArray;
    private final int[] levelArray;
    private final int[] xpArray;
 
    private PlayerData(String name, int[] rankArray, int[] levelArray, int[] xpArray) {
        this.name = name;
        this.rankArray = rankArray;
        this.levelArray = levelArray;
        this.xpArray = xpArray;
    }
 
    /**
     * @return The player's name.
     */
    public String name() {
        return name;
    }
 
    /**
     * Gets the overall hi-score data for the player.
     * This returns an anomalous {@link SkillData} instance with a {@code null}
     * skill.
     * @return a {@link SkillData} instance that represents the overall rank,
     * level and xp for the player.
     */
    public SkillData getOverall() {
        return new SkillData(null, rankArray[0], levelArray[0], xpArray[0]);
    }
 
    /**
     * Gets the {@link SkillData} associated with the input argument {@link Skill}
     * @param skill the skill to get hi-score data for
     * @return a SkillData instance that contains hi-score information for the
     * argument Skill
     */
    public SkillData getSkillData(Skill skill) {
        return new SkillData(skill, rankArray[skill.getId() + 1], levelArray[skill.getId() + 1], xpArray[skill.getId() + 1]);
    }
 
    /**
     * Looks up a player on the OSRS website to find the hi-score information for that player.
     * @param name The name of the player
     * @return A new PlayerData instance containing information about the skills of the input player
     * @throws IOException if something goes wrong during lookup
     */
    public static PlayerData lookup(String name) throws IOException {
        try {
            final URL url = new URL(String.format(PATH_FORMAT, name));
            final int[] rankArray = new int[SKILL_NUMBER];
            final int[] levelArray = new int[SKILL_NUMBER];
            final int[] xpArray = new int[SKILL_NUMBER];
            try (final BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
                String read;
                for (int i = 0; (read = reader.readLine()) != null && i < SKILL_NUMBER; i++) {
                    String[] data = read.split(",");
                    rankArray[i] = Integer.parseInt(data[0].replaceAll("[^0-9]", ""));
                    levelArray[i] = Integer.parseInt(data[1].replaceAll("[^0-9]", ""));
                    xpArray[i] = Integer.parseInt(data[2].replaceAll("[^0-9]", ""));
                }
                return new PlayerData(name, rankArray, levelArray, xpArray);
            }
        } catch (MalformedURLException e) {
            throw new AssertionError("Won't happen");
        }
    }
 
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final PlayerData that = (PlayerData) o;
        return Objects.equals(name, ((PlayerData) o).name())
                && Arrays.equals(levelArray, that.levelArray)
                && Arrays.equals(rankArray, that.rankArray)
                && Arrays.equals(xpArray, that.xpArray);
 
    }
 
    @Override
    public int hashCode() {
        int result = rankArray != null ? Arrays.hashCode(rankArray) : 0;
        result = 31 * result + (levelArray != null ? Arrays.hashCode(levelArray) : 0);
        result = 31 * result + (xpArray != null ? Arrays.hashCode(xpArray) : 0);
        return result;
    }
 
}