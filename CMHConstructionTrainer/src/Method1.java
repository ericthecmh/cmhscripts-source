import java.awt.Point;

import org.osbot.script.Script;


public class Method1 extends Method{

	public Method1(Script loader){
		super(loader);
		actions.add(new ActionBuildSimple(1,15,true,"Chair space",new Point(50,83),loader,"Chair",new int[]{960,2,23,0,1539,2,2,1,2347,1,1,0,8794,1,1,0,8013,0,1,0,8007,0,0,0},396,"Please build a WORKSHOP to continue"));
		actions.add(new ActionBuildSimple(16,31,true,"Heraldry space",new Point(48,116),loader,"Helmet pluming stand",new int[]{8778,2,24,0,2347,1,1,0,8794,1,1,0,8013,0,0,0,8007,0,0,0},394,"Please build a OAK WORKBENCH to continue"));
		actions.add(new ActionWorkbench(32,37,false,new Point(295,136),new Point(160,287),new Point(53,213),loader,new int[]{8778,6,24,0,2347,1,1,0,8794,1,1,0,8013,0,1,0,8007,0,0,0},397,396,"",8552));
		actions.add(new ActionWorkbench(38,45,true,new Point(295,136),new Point(160,287),new Point(53,275),loader,new int[]{8780,4,24,0,2347,1,1,0,8794,1,1,0,8013,0,1,0,8007,0,0,0},397,396,"Please build a STEEL FRAMED WORKBENCH to continue",8554));
		actions.add(new ActionWorkbench(46,51,false,new Point(295,136),new Point(160,287),new Point(269,80),loader,new int[]{8780,6,12,0,8790,4,8,0,2347,1,1,0,8794,1,1,0,8013,0,1,0,8007,0,0,0},397,396,"",8556));
		actions.add(new ActionWorkbench(52,98,true,new Point(295,136),new Point(160,287), new Point(269,80),loader,new int[]{8783,6,12,0,8790,4,8,0,2347,1,1,0,8794,1,1,0,8013,0,1,0,8007,0,0,0},397,396,"Congratulations! You are at level 99 construction",8558));
	}
}
