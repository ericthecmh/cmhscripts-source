import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.imageio.ImageIO;

import org.osbot.script.Script;
import org.osbot.script.ScriptManifest;
import org.osbot.script.mouse.MouseDestination;
import org.osbot.script.mouse.RectangleDestination;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.model.RS2Object;
import org.osbot.script.rs2.randoms.RandomBehaviourHook;
import org.osbot.script.rs2.randoms.RandomManager;
import org.osbot.script.rs2.skill.Skill;
import org.osbot.script.rs2.ui.RS2Interface;

@ScriptManifest(name = "CMHConstruction", author = "Ericthecmh", version = 1.0D, info = "Trains Construction using several preset methods")
public class CMHConstructionTrainer extends Script {

	private boolean canStart;
	private int method;
	private int banking;
	private Method methodClass;
	public State state;
	private int walkI;
	private int pathI;
	private boolean bankFailsafe;
	private BufferedImage[] paintImages = new BufferedImage[4];
	private long startTime;
	private int paintI;
	private int startExp;
	private final int offsetX = -3, offsetY = -20;
	private final String currentVersion = "1.3.0";
	public long lastServerUpdate;
	public long lastServerExpUpdate;
	private int[] list2;
	private int toggleRun;

	private final Position varrockCenter = new Position(3212, 3428, 0);
	private final Position[] pathToBank = new Position[] {
			new Position(3208, 3429, 0), new Position(3201, 3429, 0),
			new Position(3194, 3431, 0), new Position(3185, 3435, 0) };

	public enum State {
		EXECUTE, TOBANK, TELEPORT_GLORY, WALKVARROCKBANK, BANKVARROCK_EDGE, TELEPORTHOUSE, EXIT_PORTAL, ENTER_PORTAL, DROP_ITEMS;
	}

	public void onStart() {
		log("Loading paint");
		new Thread(new Runnable() {
			public void run() {
				try {
					paintImages[0] = ImageIO.read(new URL(
							"http://cmhscripts.no-ip.org/osbot/paint/main.png"));
					paintImages[1] = ImageIO.read(new URL(
							"http://cmhscripts.no-ip.org/osbot/paint/stats.png"));
					paintImages[2] = ImageIO.read(new URL(
							"http://cmhscripts.no-ip.org/osbot/paint/script.png"));
					paintImages[3] = ImageIO.read(new URL(
							"http://cmhscripts.no-ip.org/osbot/paint/feedback.png"));
				} catch (Exception e) {
				}
			}
		}).start();
		try {
			for (int i = 0; i < 10; i++)
				sleep(random(400, 500));
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		CMHConstructionGUI gui = new CMHConstructionGUI();
		gui.setResizable(false);
		gui.setVisible(true);
		while (gui.isVisible() && !gui.ready) {
			try {
				sleep(300);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		gui.setVisible(false);
		method = gui.getTrainingMethod();
		banking = gui.getBankingMethod();
		canStart = true;
		if (closestObjectForName("Bank booth") != null)
			state = State.BANKVARROCK_EDGE;
		else
			state = State.EXECUTE;
		switch (method) {
		case 0:
			methodClass = new Method1(this);
			break;
		case 1:
			methodClass = new Method2(this);
			break;
		case 2:
			methodClass = new Method3(this);
			break;
		case 3:
			methodClass = new Method4(this);
			break;
		case 4:
			methodClass = new Method5(this);
			break;
		default:
			methodClass = null;
			canStart = false;
		}
		toggleRun = random(45,55);
		startExp = client.getSkills().getExperience(Skill.CONSTRUCTION);
		lastServerExpUpdate = startExp;
		lastServerUpdate = System.currentTimeMillis();
		startTime = System.currentTimeMillis();
	}

	public int onLoop() {
		randomManager.registerHook(new RandomBehaviourHook(
				RandomManager.SECURITY_BOOK) {
			public boolean shouldActivate() {
				return false;
			}
		});
		if (canStart) {
			if(client.getRunEnergy() > toggleRun && !isRunning()){
				try {
					setRunning(true);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				toggleRun = random(45,55);
			}
			switch (state) {
			case EXECUTE:
				if (methodClass.shouldBank) {
					methodClass.shouldBank = false;
					if (banking == 0)
						state = State.TOBANK;
					else if (banking == 1)
						state = State.TELEPORT_GLORY;
					break;
				}
				if (methodClass.execute())
					try {
						stop();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				break;
			case TOBANK:
				if (banking == 0) {
					try {
						if (client.getInventory().contains("Varrock teleport")) {
							client.getInventory().interactWithName(
									"Varrock teleport", "Break");
							for (int i = 0; i < 20
									&& distance(varrockCenter) > 20; i++) {
								sleep(random(500, 600));
							}
							if (distance(varrockCenter) < 20) {
								walkI = 0;
								pathI = 2;
								state = State.WALKVARROCKBANK;
							}
							break;
						}
					} catch (Exception e) {
					}
				} else if (banking == 1) {
					state = State.TELEPORT_GLORY;
					break;
				}
				break;
			case WALKVARROCKBANK:
				if (walkI == 0) {
					try {
						walkMiniMap2(randomize(pathToBank[walkI++]));
						break;
					} catch (InterruptedException e) {
					}
				}
				if (walkI == pathToBank.length) {
					walkI = 0;
					state = State.BANKVARROCK_EDGE;
					break;
				}
				try {
					if (realDistance(pathToBank[walkI - 1]) <= 6) {
						walkMiniMap2(randomize(pathToBank[walkI++]));
					} else
						walkMiniMap2(randomize(pathToBank[walkI - 1]));

				} catch (InterruptedException e) {

				}
				break;
			case BANKVARROCK_EDGE:
				try {
					client.rotateCameraPitch(90);
				} catch (InterruptedException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				RS2Object booth = closestObjectForName("Bank booth");
				if (booth == null)
					break;
				if (!client.getBank().isOpen()) {
					try {
						for (int i = 0; i < 10 && !booth.interact("Bank"); i++) {
							sleep(random(300, 400));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					for (int i = 0; i < 10 && !client.getBank().isOpen(); i++) {
						try {
							sleep(random(300, 400));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				int[] objects = methodClass.getCurrentAction().getObjectIDs();
				int[] list = new int[objects.length / 4];
				int listi = 0;
				for (int i = 0; i < objects.length; i += 4)
					list[listi++] = objects[i];
				try {
					client.getBank().depositAllExcept(list);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (banking == 0) {
					try {
						if (client.getBank().contains(8007)
								&& !client.getInventory().contains(8007)
								&& !(method == 4 && client.getSkills()
										.getLevel(Skill.CONSTRUCTION) >= 40))
							client.getBank().withdraw1(8007);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (client.getBank().contains(8013)
						&& !client.getInventory().contains(8007)) {
					try {
						client.getBank().withdraw1(8013);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				for (int i = 0; i < objects.length; i += 4) {
					if (client.getBank().getAmount(objects[i])
							+ client.getInventory().getAmount(objects[i]) < objects[i + 1]) {
						log("You don't have enough required materials.");
						try {
							stop();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (client.getBank().contains(objects[i])
							&& client.getInventory().getAmount(objects[i]) < objects[i + 2]) {
						if (objects[i + 3] == 1) {
							try {
								client.getBank().withdrawAll(objects[i]);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							try {
								client.getBank().withdrawX(objects[i],
										objects[i + 2]);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
				for (int i = 0; i < objects.length; i += 4) {
					if (client.getInventory().getAmount(objects[i]) < objects[i + 1]) {
						bankFailsafe = true;
						break;
					}
				}
				try {
					for (int i = 0; i < 10 && !client.getBank().close(); i++) {
						sleep(random(300, 400));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				state = State.TELEPORTHOUSE;
				break;
			case TELEPORTHOUSE:
				while (client.getBank().isOpen())
					try {
						client.getBank().close();
						sleep(random(2000, 2100));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if (client.getInventory().contains("Teleport to house")) {
					try {
						client.getInventory().interactWithName(
								"Teleport to house", "Break");
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					for (int i = 0; i < 10
							&& !isValid(client.getInterface(399)); i++) {
						try {
							sleep(random(500, 600));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					for (int i = 0; i < 100
							&& isValid(client.getInterface(399)); i++) {
						try {
							sleep(random(500, 600));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					try {
						sleep(random(1000, 2000));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (distance(closestObjectForName("Portal")) < 10)
						state = State.EXIT_PORTAL;
				}
				break;
			case EXIT_PORTAL:
				RS2Object portal = closestObjectForName("Portal");
				try {
					for (int i = 0; i < 10 && !portal.interact("Enter"); i++) {
						sleep(random(300, 400));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Position oldPos = myPlayer().getPosition();
				for (int i = 0; i < 10 && distanceTo(oldPos) < 20; i++) {
					try {
						sleep(random(1000, 1100));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				state = State.ENTER_PORTAL;
				break;
			case ENTER_PORTAL:
				portal = closestObjectForName("Portal");
				try {
					for (int i = 0; i < 10 && !portal.interact("Enter"); i++) {
						sleep(random(300, 400));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int i = 0; i < 10
						&& (client.getInterface(232) == null
								|| !client.getInterface(232).isValid() || !client
								.getInterface(232).isVisible()); i++) {
					try {
						sleep(random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (!(client.getInterface(232) == null
						|| !client.getInterface(232).isValid() || !client
						.getInterface(232).isVisible())) {
					try {
						for (int i = 0; i < 10
								&& !client.getInterface(232).getChild(2)
										.interact(); i++)
							sleep(random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				oldPos = myPlayer().getPosition();
				for (int i = 0; i < 10 && distanceTo(oldPos) < 20; i++) {
					try {
						sleep(random(1000, 1100));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				for (int i = 0; i < 10
						&& !(client.getInterface(399) == null
								|| !client.getInterface(399).isValid() || !client
								.getInterface(399).isVisible()); i++)
					try {
						sleep(random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if (distance(oldPos) > 20)
					state = State.EXECUTE;
				break;
			case TELEPORT_GLORY:
				try {
					if (Math.abs(client.getCameraPitchAngle() - 40) > 5)
						client.rotateCameraPitch(40);
					if (client.getCameraYawAngle() > 5
							&& client.getCameraYawAngle() < 355)
						client.rotateCameraToAngle(0);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				RS2Object glory = closestObjectForName("Amulet of Glory");
				if (glory != null) {
					if (!glory.isVisible() || distance(glory) > 5) {
						try {
							this.walkMiniMap(glory.getPosition());
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					for (int i = 0; i < 10 && myPlayer().isMoving(); i++)
						try {
							sleep(random(400, 500));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					try {
						MouseDestination md = glory.getMouseDestination();
						double x = md.getBoundingBox().getCenterX();
						double y = md.getBoundingBox().getCenterY();
						RectangleDestination rd = new RectangleDestination(
								(int) x - 2, (int) y - 2, 4, 4);
						for (int i = 0; i < 10
								&& !client.moveMouseTo(rd, false, true, false); i++) {
							sleep(random(500, 600));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					for (int i = 0; i < 3
							&& (client.getInterface(234) == null
									|| !client.getInterface(234).isValid() || !client
									.getInterface(234).isVisible()); i++) {
						if (myPlayer().isMoving())
							i--;
						try {
							sleep(random(500, 600));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (!(client.getInterface(234) == null
							|| !client.getInterface(234).isValid() || !client
							.getInterface(234).isVisible())) {
						try {
							for (int i = 0; i < 10
									&& !client.getInterface(234).getChild(1)
											.interact(); i++) {
								sleep(random(500, 600));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						for (int i = 0; i < 10
								&& closestObjectForName("Bank booth") == null
								|| distance(closestObjectForName("Bank booth")) > 20; i++) {
							try {
								sleep(random(1000, 1100));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						if (closestObjectForName("Bank booth") != null
								&& distance(closestObjectForName("Bank booth")) < 20)
							state = State.BANKVARROCK_EDGE;
					}
				}
				break;
			case DROP_ITEMS:
				portal = closestObjectForName("Portal");
				try {
					for (int i = 0; i < 10 && !portal.interact("Enter"); i++) {
						sleep(random(300, 400));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				oldPos = myPlayer().getPosition();
				for (int i = 0; i < 10 && distanceTo(oldPos) < 20; i++) {
					try {
						sleep(random(1000, 1100));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				list2 = new int[methodClass.getCurrentAction().getObjectIDs().length / 4];
				for (int i = 0; i < list2.length; i++) {
					list2[i] = methodClass.getCurrentAction().getObjectIDs()[i * 4];
				}
				try {
					sleep(random(1000, 1200));
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					new Thread(new Runnable() {
						public void run() {
							try {
								client.getInventory().dropAllExcept(list2);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}).start();
					sleep(random(4000, 5000));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				state = State.ENTER_PORTAL;
				break;
			}
		}
		return random(300, 400);
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getX() >= 9 && e.getX() <= 75 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 0;
		} else if (e.getX() >= 75 && e.getX() <= 152 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 1;
		} else if (e.getX() >= 152 && e.getX() <= 220 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 2;
		} else if (e.getX() >= 220 && e.getX() <= 296 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 3;
		}
		if (distance(e.getX(), e.getY(), 506, 358) <= 14) {
			hide = !hide;
		}
	}

	private boolean hide = false;
	private boolean antiban = false;
	private int gainedExp;

	private String pad(int i) {
		String ret = i + "";
		if (ret.length() == 1) {
			return "0" + ret;
		}
		return ret;
	}

	private double percentToNextLevel(Skill skill) {
		int level = client.getSkills().getLevel(skill);
		double total = 0.0;
		total = xpForLevels[level] - xpForLevels[level - 1];
		return 1 - ((experienceToNextLevel(skill) + 0.0) / total);
	}

	/***************** MADE BY ABIBOT *******************/
	int[] xpForLevels = new int[] { 0, 83, 174, 276, 388, 512, 650, 801, 969,
			1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470,
			5018, 5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363,
			14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648,
			37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014,
			91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040,
			203254, 224466, 247886, 273742, 302288, 333804, 368599, 407015,
			449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257,
			992895, 1096278, 1210421, 1336443, 1475581, 1629200, 1798808,
			1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792,
			3972294, 4385776, 4842295, 5346332, 5902831, 6517253, 7195629,
			7944614, 8771558, 9684577, 10692629, 11805606, 13034431 };

	private int experienceToNextLevel(Skill skill) {
		int xp = client.getSkills().getExperience(skill);
		for (int i = 0; i < 99; i++) {
			if (xp < xpForLevels[i]) {
				return (xpForLevels[i] - xp);
			}
		}
		return (200000000 - xp);
	}

	public void onPaint(Graphics g) {
		try {
			if (canStart) {
				g.setColor(new Color(0, 1, 0, 0.25f));
				if (hide) {
					g.setColor(Color.BLUE);
					g.fillOval(492, 346, 28, 28);
					return;
				}
				switch (paintI) {
				case 0:
					g.drawImage(paintImages[paintI], -20, 205, null);
					g.setColor(Color.RED);
					String status = state.toString();
					if (antiban)
						status = "ANTIBAN";
					g.drawString("Current State: " + status, 12, 400);
					g.drawString("Logged in as: " + myPlayer().getName() + ".",
							12, 415);
					double hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
					int minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
					int seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
					g.drawString("Runtime: " + "Running for: "
							+ pad((int) (hours)) + ":" + pad(minutes) + ":"
							+ pad(seconds), 12, 430);
					gainedExp = client.getSkills().getExperience(
							Skill.CONSTRUCTION)
							- startExp;
					g.drawString("Rate: " + (int) (gainedExp / hours)
							+ " EXP/HR", 12, 445);
					// g.drawString("Mined " + (mined) + " Ores", 12, 460);
					break;
				case 1:
					g.drawImage(paintImages[1], -20, 205, null);
					g.setColor(Color.RED);
					hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
					minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
					seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
					gainedExp = client.getSkills().getExperience(
							Skill.CONSTRUCTION)
							- startExp;
					g.drawString("Current Construction Level: "
							+ client.getSkills().getLevel(Skill.CONSTRUCTION),
							12, 400);
					g.drawString("Experience Gained: " + gainedExp, 12, 415);
					g.drawString("Experience to Level: "
							+ experienceToNextLevel(Skill.CONSTRUCTION), 12,
							430);
					double r = ((gainedExp) / (seconds + minutes * 60 + hours * 3600.0));
					if (r == 0)
						r = .00000001;
					r *= 2;
					int TTL = (int) (experienceToNextLevel(Skill.CONSTRUCTION) / r);
					hours = (TTL) / 3600;
					minutes = (int) (((TTL) / 60.0) % 60);
					seconds = (int) ((TTL) % 60);
					g.drawString("Time To Level: " + pad((int) hours) + ":"
							+ pad(minutes) + ":" + pad(seconds), 12, 445);
					double perc = percentToNextLevel(Skill.CONSTRUCTION);
					g.setColor(new Color(0, 1, 0, 0.75f));
					g.fillRect(15 + offsetX, 475 + offsetY, (int) (500 * perc),
							17);
					g.setColor(new Color(1, 1, 1, 0.75f));
					g.fillRect(15 + offsetX, 475 + offsetY, 490, 8);
					g.setColor(new Color(.5f, .5f, .5f, 0.75f));
					g.fillRect(15 + offsetX, 484 + offsetY, 490, 8);
					g.setColor(Color.RED);
					g.drawString((int) (perc * 100) + "% : "
							+ experienceToNextLevel(Skill.CONSTRUCTION)
							+ " TNL", 200, 489 + offsetY);
					break;
				case 2:
					g.drawImage(paintImages[2], -20, 205, null);
					g.setColor(Color.RED);
					g.drawString("CMHConstructionTrainer Version "
							+ currentVersion, 12, 400);
					g.drawString("Script Developed by Ericthecmh", 12, 415);
					break;
				case 3:
					g.drawImage(paintImages[3], -20, 205, null);
					g.setColor(Color.RED);
					g.drawString("My Email: info@cmhscripts.com", 12, 400);
					g.drawString(
							"or leave feedback on the OSBot Forum Thread under SDN->Construction->CMHConstruction",
							12, 430);
					g.drawString("Any and all feedback is welcome.", 12, 445);
					break;
				}
			}
		} catch (Exception e) {
		}
	}

	private boolean isValid(RS2Interface i) {
		if (i == null)
			return false;
		if (!i.isValid())
			return false;
		return (i.isVisible());
	}

	private Position randomize(Position p) {
		return new Position(p.getX() + random(1, 2 * 2 + 1) - (2 + 1), p.getY()
				+ random(1, 2 * 2 + 1) - (2 + 1), 0);
	}

	private void walkMiniMap2(Position p) throws InterruptedException {
		final int mapX = 643, mapY = 83;
		double distance = distanceTo(p);
		double angle2 = Math.toDegrees(Math.atan2(p.getY()
				- myPosition().getY(), p.getX() - myPosition().getX())) - 90;
		double dY = distance * Math.sqrt(17);
		double rdX = -1
				* Math.sin(Math.toRadians(angle2 - client.getCameraYawAngle()))
				* dY;
		double rdY = Math.cos(Math.toRadians(angle2
				- client.getCameraYawAngle()))
				* dY;
		RectangleDestination rd = new RectangleDestination(
				(int) (mapX + rdX - 1), (int) (mapY - rdY - 1), 3, 3);
		client.moveMouseTo(rd, false, false, false);
		for (int i = 0; i < 5
				&& (Math.abs(client.getMousePosition().getX()
						- (int) (mapX + rdX)) > 2 || Math.abs(client
						.getMousePosition().getY() - (int) (mapY - rdY)) > 2); i++) {
			sleep(random(500, 600));

		}
		if ((Math.abs(client.getMousePosition().getX() - (int) (mapX + rdX)) <= 2 && Math
				.abs(client.getMousePosition().getY() - (int) (mapY - rdY)) <= 2)) {
			sleep(random(500, 600));
			client.clickMouse(false);
			for (int i = 0; i < 2 && !myPlayer().isMoving(); i++) {
				sleep(random(500, 600));
			}
			for (int i = 0; i < 60 && myPlayer().isMoving()
					&& distanceTo(p) > 6; i++) {
				sleep(random(500, 600));
			}
		}
	}

	private double distanceTo(Position p) {
		Position myP = myPosition();
		return Math.sqrt((p.getX() - myP.getX()) * (p.getX() - myP.getX())
				+ (p.getY() - myP.getY()) * (p.getY() - myP.getY()));
	}

}
