import java.awt.Point;

import org.osbot.script.Script;


public class Method5 extends Method{
	public Method5(Script loader){
		super(loader);
		actions.add(new ActionBuildSimple(1,15,true,"Chair space",new Point(50,83),loader,"Chair",new int[]{960,2,23,0,1539,2,0,1,2347,1,1,0,8794,1,1,0,8013,0,1,0,8007,0,0,0},396,"Please build a WORKSHOP to continue"));
		actions.add(new ActionBuildSimple(16,32,false,"Heraldry space",new Point(48,116),loader,"Helmet pluming stand",new int[]{8778,2,24,0,2347,1,1,0,8794,1,0,0,8013,1,1,0,8007,0,0,0},394,"Please build a KITCHEN to continue"));
		actions.add(new ActionBuildSimple(33,39,true,"Larder space",new Point(52,190),loader,"Larder",new int[]{8778,8,24,0,2347,1,1,0,8794,1,1,0,8013,0,1,0,8007,0,0,0},394,"Please hire a butler to continue"));
		actions.add(new ActionBuildSimpleButler(40,73,true,"Larder space",new Point(52,190),loader,"Larder",new int[]{8778,8,24,0,2347,1,1,0,8794,1,1,0,8013,0,1,0,995,5000,5000,1},394,"Please build a DUNGEON to continue",2));
		actions.add(new ActionBuildSimpleButler(74,98,true,"Door space",new Point(52,190),loader,"Oak door",new int[]{8778,10,24,0,2347,1,1,0,8794,1,1,0,8013,0,1,0,995,5000,5000,1},394,"Congratulation, you are now at level 99 construction",2));
	}
}
