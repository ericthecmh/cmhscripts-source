import java.awt.Point;

import javax.swing.JOptionPane;

import org.osbot.script.Script;
import org.osbot.script.mouse.RectangleDestination;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.model.Item;
import org.osbot.script.rs2.model.RS2Object;

public class ActionWorkbench implements ActionInterface {

	private int lowerRange;
	private int upperRange;
	private boolean shouldStop;
	private Point mousePosition0;
	private Point mousePosition1;
	private Point mousePosition2;
	private Script loader;
	public final int[] objectIDs;
	private int popupID1;
	private int popupID2;
	private String message;
	private int producedID;

	public ActionWorkbench(int l, int u, boolean s, Point mp0, Point mp,
			Point mp2, Script loader, int[] objects, int popup1, int popup2,
			String message, int pid) {
		lowerRange = l;
		upperRange = u;
		shouldStop = s;
		mousePosition0 = mp0;
		mousePosition1 = mp;
		mousePosition2 = mp2;
		this.loader = loader;
		objectIDs = objects;
		popupID1 = popup1;
		popupID2 = popup2;
		this.message = message;
		producedID = pid;
	}

	public boolean shouldExecute(int level) {
		return (level >= lowerRange && level <= upperRange);
	}

	public int[] getObjectIDs() {
		return objectIDs;
	}

	public boolean shouldBank() {
		for (int i = 0; i < objectIDs.length; i += 4) {
			// loader.log(objectIDs[i] + ":"
			// + loader.client.getInventory().getAmount(objectIDs[i])
			// + ":" + objectIDs[i + 1]);
			if (loader.client.getInventory().getAmount(objectIDs[i]) < objectIDs[i + 1])
				return true;
		}
		return false;
	}

	public boolean onLevelUp(int level) {
		if (level > upperRange && shouldStop) {
			JOptionPane.showMessageDialog(null, message);
			return true;
		}
		return false;
	}

	public boolean execute() {
		for (Item item : loader.client.getInventory().getItems()) {
			if (item == null)
				continue;
			boolean isInList = false;
			for (int i = 0; i < objectIDs.length; i += 4) {
				if (item.getId() == objectIDs[i]
						|| item.getName().contains("table"))
					isInList = true;
			}
			if (!isInList) {
				((CMHConstructionTrainer) loader).state = ((CMHConstructionTrainer) loader).state.DROP_ITEMS;
				return false;
			}
		}
		RS2Object openSpace = loader.closestObjectForName("Workbench");
		try {
			for (int i = 0; i < 10 && !openSpace.interact("Work-at"); i++) {
				loader.sleep(loader.random(300, 400));
			}
		} catch (InterruptedException e) {
			return false;
		}
		for (int i = 0; i < 10
				&& (loader.client.getInterface(popupID1) == null
						|| !loader.client.getInterface(popupID1).isVisible() || !loader.client
						.getInterface(popupID1).isValid()); i++) {
			if (loader.myPlayer().isMoving())
				i--;
			try {
				loader.sleep(loader.random(300, 400));
			} catch (InterruptedException e) {
			}
		}
		if ((loader.client.getInterface(popupID1) == null
				|| !loader.client.getInterface(popupID1).isVisible() || !loader.client
				.getInterface(popupID1).isValid()))
			return false;

		// Scroll bar
		for (int i = 0; i < 10
				&& loader.client.getInterface(397).getChild(115).getPosition()
						.getY() > 300; i++) {
			try {
				loader.client.moveMouse(
						new RectangleDestination(
								(int) mousePosition0.getX() - 4,
								(int) mousePosition0.getY() - 4, 8, 8), false);
			} catch (InterruptedException e) {
			}
			try {
				loader.sleep(loader.random(400, 500));
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			try {
				for (int j = 0; j < loader.random(3, 5); j++) {
					loader.client.clickMouse(false);
					loader.sleep(loader.random(70, 100));
				}
			} catch (InterruptedException e) {
			}
			try {
				loader.sleep(loader.random(500, 1500));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		try {
			loader.client.moveMouse(
					new RectangleDestination((int) mousePosition1.getX() - 110,
							(int) mousePosition1.getY() - 8, 200, 16), false);
		} catch (InterruptedException e) {
		}
		try {
			loader.sleep(loader.random(400, 500));
		} catch (InterruptedException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		for (int i = 0; i < 10; i++) {
			try {
				loader.client.clickMouse(false);
			} catch (InterruptedException e) {
			}
			try {
				loader.sleep(loader.random(500, 1500));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if ((loader.client.getInterface(popupID1) == null
					|| !loader.client.getInterface(popupID1).isVisible() || !loader.client
					.getInterface(popupID1).isValid()))
				break;
		}

		// SECOND

		for (int i = 0; i < 10
				&& (loader.client.getInterface(popupID2) == null
						|| !loader.client.getInterface(popupID2).isVisible() || !loader.client
						.getInterface(popupID2).isValid()); i++) {
			if (loader.myPlayer().isMoving())
				i--;
			try {
				loader.sleep(loader.random(300, 400));
			} catch (InterruptedException e) {
			}
		}
		if ((loader.client.getInterface(popupID2) == null
				|| !loader.client.getInterface(popupID2).isVisible() || !loader.client
				.getInterface(popupID2).isValid())) {
			try {
				loader.walkMiniMap(new Position(loader.myPosition().getX()
						+ loader.random(1, 3), loader.myPosition().getY()
						+ loader.random(1, 3), 0));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
		try {
			loader.client.moveMouse(
					new RectangleDestination((int) mousePosition2.getX() - 8,
							(int) mousePosition2.getY() - 8, 16, 16), false);
		} catch (InterruptedException e) {
		}
		try {
			loader.sleep(loader.random(400, 500));
		} catch (InterruptedException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		long oldCount = loader.client.getInventory().getAmount(producedID);
		for (int i = 0; i < 3; i++) {
			try {
				loader.client.clickMouse(false);
			} catch (InterruptedException e) {
			}
			try {
				loader.sleep(loader.random(500, 1500));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if ((loader.client.getInterface(popupID2) == null
					|| !loader.client.getInterface(popupID2).isVisible() || !loader.client
					.getInterface(popupID2).isValid()))
				break;
		}
		// loader.log("Waiting for finish " + producedID);
		// loader.log(oldCount + "");
		for (int i = 0; i < 50; i++) {
			if (loader.client.getInventory().getAmount(producedID) != oldCount)
				break;
			try {
				loader.sleep(loader.random(500, 600));
			} catch (InterruptedException e) {
				return false;
			}
		}
		if (loader.client.getInventory().getAmount(producedID) == oldCount) {
			try {
				loader.walkMiniMap(new Position(loader.myPosition().getX()
						+ loader.random(1, 3), loader.myPosition().getY()
						+ loader.random(1, 3), 0));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// loader.log("Finished");
		try {
			loader.sleep(loader.random(400, 700));
		} catch (InterruptedException e) {
			return false;
		}
		return false;
	}

	private double distanceBetween(Point a, Point b) {
		return Math.sqrt((a.getX() - b.getX()) * (a.getX() - b.getX())
				+ (a.getY() - b.getY()) * (a.getY() - b.getY()));
	}
}
