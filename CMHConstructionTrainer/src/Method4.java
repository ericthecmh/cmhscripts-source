import java.awt.Point;

import org.osbot.script.Script;


public class Method4 extends Method{
	public Method4(Script loader){
		super(loader);
		actions.add(new ActionBuildSimple(1,15,true,"Chair space",new Point(50,83),loader,"Chair",new int[]{960,2,23,0,1539,2,0,1,2347,1,1,0,8794,1,1,0,8013,0,1,0,8007,0,0,0},396,"Please build a WORKSHOP to continue"));
		actions.add(new ActionBuildSimple(16,32,false,"Heraldry space",new Point(48,116),loader,"Helmet pluming stand",new int[]{8778,2,24,0,2347,1,1,0,8794,1,1,0,8013,0,1,0,8007,0,0,0},394,"Please build a KITCHEN to continue"));
		actions.add(new ActionBuildSimple(33,73,true,"Larder space",new Point(52,190),loader,"Larder",new int[]{8778,8,24,0,2347,1,1,0,8794,1,1,0,8013,0,1,0,8007,0,0,0},394,"Please build a DUNGEON to continue"));
		actions.add(new ActionBuildSimple(74,98,true,"Door space",new Point(52,190),loader,"Oak door",new int[]{8778,10,20,0,2347,1,1,0,8794,1,1,0,8013,0,1,0,8007,0,0,0},394,"Congratulation, you are now at level 99 construction"));
	}
}
