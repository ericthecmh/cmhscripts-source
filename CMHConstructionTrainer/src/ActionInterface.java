
public interface ActionInterface {
	abstract boolean shouldBank();
	abstract boolean execute();
	abstract boolean shouldExecute(int level);
	abstract boolean onLevelUp(int level);
	abstract int[] getObjectIDs();
}
