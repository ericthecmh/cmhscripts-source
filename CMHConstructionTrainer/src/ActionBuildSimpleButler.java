import java.awt.Point;

import javax.swing.JOptionPane;

import org.osbot.script.Script;
import org.osbot.script.mouse.RectangleDestination;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.model.Item;
import org.osbot.script.rs2.model.RS2Object;
import org.osbot.script.rs2.ui.RS2Interface;

public class ActionBuildSimpleButler implements ActionInterface {

	private int lowerRange;
	private int upperRange;
	private boolean shouldStop;
	private String beforeName;
	private Point mousePosition;
	private Script loader;
	private String afterName;
	public final int[] objectIDs;
	private int popupID;
	private String message;
	private int requiredPlanks;
	private int childID;

	public ActionBuildSimpleButler(int l, int u, boolean s, String b, Point mp,
			Script loader, String a, int[] objects, int popup, String message,
			int cid) {
		lowerRange = l;
		upperRange = u;
		shouldStop = s;
		beforeName = b;
		mousePosition = mp;
		this.loader = loader;
		objectIDs = objects;
		popupID = popup;
		this.message = message;
		afterName = a;
		childID = cid;
	}

	public boolean shouldExecute(int level) {
		return (level >= lowerRange && level <= upperRange);
	}

	public int[] getObjectIDs() {
		return objectIDs;
	}

	private boolean shouldBank;

	public boolean shouldBank2() {
		if (isValid(loader.client.getInterface(241))) {
			return !loader.client.getInterface(241).getChild(2).getMessage()
					.contains("goods");
		}
		return true;
	}

	public boolean shouldBank() {
		for (int i = 0; i < objectIDs.length; i += 4) {
			// loader.log(objectIDs[i] + ":" +
			// loader.client.getInventory().getAmount(objectIDs[i]) + ":" +
			// objectIDs[i+1]);
			if (loader.client.getInventory().getAmount(objectIDs[i]) < objectIDs[i + 1]) {
				shouldBank = true;
				return false;
			}
		}
		shouldBank = false;
		return false;
	}

	public boolean onLevelUp(int level) {
		if (level > upperRange && shouldStop) {
			JOptionPane.showMessageDialog(null, message);
			return true;
		}
		return false;
	}

	private boolean isValid(RS2Interface i) {
		if (i == null)
			return false;
		if (!i.isValid())
			return false;
		return (i.isVisible());
	}

	private boolean butlerEnRoute;

	private boolean doButlerBank() {
		try {
			RS2Object bell = loader.closestObjectForName("Rope bell-pull");
			if (bell == null)
				return false;
			try {
				for (int i = 0; i < 10 && !bell.interact("Ring"); i++) {
					loader.sleep(loader.random(500, 600));
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				loader.sleep(loader.random(1000,1100));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			for (int i = 0; i < 30 && !isValid(loader.client.getInterface(241)); i++) {
				if (loader.myPlayer().isMoving())
					i--;
				if (isValid(loader.client.getInterface(242))) {
					try {
						for (int j = 0; j < 10
								&& !loader.client.getInterface(242).getChild(4)
										.interact(); j++) {
							loader.sleep(loader.random(500, 600));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					for (int j = 0; j < 10
							&& !isValid(loader.client.getInterface(230)); j++) {
						if (isValid(loader.client.getInterface(228))) {
							butlerEnRoute = true;
							return true;
						}
						try {
							loader.sleep(loader.random(500, 600));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (isValid(loader.client.getInterface(230))) {
						try {
							for (int j = 0; j < 10
									&& loader.client.getInterface(230)
											.getChild(1).interact(); j++)
								loader.sleep(loader.random(500, 600));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return false;
					} else {
						return false;
					}
				}
				try {
					loader.sleep(loader.random(500, 600));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (isValid(loader.client.getInterface(241))) {
				try {
					for (int i = 0; i < 10
							&& !loader.client.getInterface(241).getChild(3)
									.interact(); i++)
						loader.sleep(loader.random(500, 600));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int i = 0; i < 10
						&& !isValid(loader.client.getInterface(234)); i++) {
					if (!shouldBank2()) {
						butlerEnRoute = false;
						shouldBank = false;
						return true;
					}
					try {
						loader.sleep(loader.random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (isValid(loader.client.getInterface(234))) {
					try {
						for (int i = 0; i < 10
								&& !loader.client.getInterface(234).getChild(2)
										.interact(); i++)
							loader.sleep(loader.random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					for (int i = 0; i < 10
							&& !isValid(loader.client.getInterface(228)); i++) {
						try {
							loader.sleep(loader.random(500, 600));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (isValid(loader.client.getInterface(228))) {
						try {
							for (int i = 0; i < 10
									&& !loader.client.getInterface(228)
											.getChild(2).interact(); i++)
								loader.sleep(loader.random(500, 600));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						for (int i = 0; i < 10
								&& !isValid(loader.client.getInterface(64)); i++) {
							try {
								loader.sleep(loader.random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						if (isValid(loader.client.getInterface(64))) {
							try {
								for (int i = 0; i < 10
										&& !loader.client.getInterface(64)
												.getChild(3).interact(); i++)
									loader.sleep(loader.random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							for (int i = 0; i < 10
									&& !isValid(loader.client.getInterface(242)); i++) {
								try {
									loader.sleep(loader.random(500, 600));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							if (isValid(loader.client.getInterface(242))) {
								try {
									for (int i = 0; i < 10
											&& !loader.client.getInterface(242)
													.getChild(4).interact(); i++)
										loader.sleep(loader.random(500, 600));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								for (int i = 0; i < 10
										&& !isValid(loader.client
												.getInterface(234)); i++) {
									try {
										loader.sleep(loader.random(500, 600));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								if (isValid(loader.client.getInterface(234))) {
									try {
										for (int i = 0; i < 10
												&& !loader.client
														.getInterface(234)
														.getChild(childID)
														.interact(); i++)
											loader.sleep(loader
													.random(500, 600));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									for (int i = 0; i < 10
											&& isValid(loader.client
													.getInterface(234)); i++)
										try {
											loader.sleep(loader
													.random(500, 600));
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									try {
										loader.client.typeString("" + 20);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									for (int i = 0; i < 10
											&& !isValid(loader.client
													.getInterface(241)); i++) {
										if (isValid(loader.client
												.getInterface(242))) {
											loader.log("You have run out of oak planks! Terminating script");
											try {
												loader.stop();
											} catch (InterruptedException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										}
										try {
											loader.sleep(loader
													.random(500, 600));
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
									if (isValid(loader.client.getInterface(241))) {
										try {
											for (int i = 0; i < 10
													&& !loader.client
															.getInterface(241)
															.getChild(3)
															.interact(); i++)
												loader.sleep(loader.random(500,
														600));
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										try {
											loader.sleep(loader.random(3000));
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										butlerEnRoute = true;
									} else {
										return false;
									}
								} else {
									return false;
								}
							} else {
								return false;
							}
						} else {
							return false;
						}
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (NullPointerException e) {
			return false;
		}
		return true;
	}

	public boolean execute() {
		for (Item item : loader.client.getInventory().getItems()) {
			if (item == null)
				continue;
			boolean isInList = false;
			for (int i = 0; i < objectIDs.length; i += 4) {
				if (item.getId() == objectIDs[i])
					isInList = true;
			}
			if (!isInList) {
				((CMHConstructionTrainer) loader).state = ((CMHConstructionTrainer) loader).state.DROP_ITEMS;
				return false;
			}
		}
		if (shouldBank) {
			doButlerBank();
			return false;
		}
		if (!butlerEnRoute) {
			doButlerBank();
			return false;
		}
		RS2Object openSpace = loader.closestObjectForName(beforeName);
		if (openSpace == null) {
			RS2Object builtObject = loader.closestObjectForName(afterName);
			if (builtObject == null) {
				// loader.log("NULL");
				return false;
			}
			try {
				for (int i = 0; i < 10 && !builtObject.interact("Remove"); i++) {
					loader.sleep(loader.random(300, 400));
				}
			} catch (InterruptedException e) {
				return false;
			}
			for (int i = 0; i < 10
					&& (loader.client.getInterface(228) == null
							|| !loader.client.getInterface(228).isValid() || !loader.client
							.getInterface(228).isVisible()); i++)
				try {
					loader.sleep(loader.random(400, 500));
				} catch (InterruptedException e) {
					return false;
				}
			if (!(loader.client.getInterface(228) == null
					|| !loader.client.getInterface(228).isValid() || !loader.client
					.getInterface(228).isVisible()))
				try {
					for (int i = 0; i < 10
							&& !loader.client.getInterface(228).getChild(1)
									.interact(); i++)
						loader.sleep(loader.random(300, 400));
				} catch (InterruptedException e) {
					return false;
				}
			try {
				loader.sleep(loader.random(1500, 2000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			openSpace = loader.closestObjectForName(beforeName);
			if (openSpace == null) {
				loader.log("There are no open spaces for " + beforeName);
				return true;
			}
		}
		try {
			for (int i = 0; i < 10 && !openSpace.interact("Build"); i++) {
				loader.sleep(loader.random(300, 400));
			}
		} catch (InterruptedException e) {
			return false;
		}
		for (int i = 0; i < 10
				&& (loader.client.getInterface(popupID) == null
						|| !loader.client.getInterface(popupID).isVisible() || !loader.client
						.getInterface(popupID).isValid()); i++) {
			if (loader.myPlayer().isMoving())
				i--;
			try {
				loader.sleep(loader.random(300, 400));
			} catch (InterruptedException e) {
			}
		}
		if ((loader.client.getInterface(popupID) == null
				|| !loader.client.getInterface(popupID).isVisible() || !loader.client
				.getInterface(popupID).isValid()))
			return false;
		for (int i = 0; i < 5
				&& distanceBetween(loader.client.getMousePosition(),
						mousePosition) > 10; i++) {
			try {
				loader.client.moveMouse(
						new RectangleDestination(
								(int) mousePosition.getX() - 7,
								(int) mousePosition.getY() - 7, 14, 14), false);
			} catch (InterruptedException e) {
			}
		}
		try {
			loader.sleep(loader.random(400, 500));
		} catch (InterruptedException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		long oldCount = loader.client.getInventory().getAmount(objectIDs[0]);
		for (int i = 0; i < 3; i++) {
			try {
				loader.client.clickMouse(false);
			} catch (InterruptedException e) {
			}
			try {
				loader.sleep(loader.random(500, 1500));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if ((loader.client.getInterface(popupID) == null
					|| !loader.client.getInterface(popupID).isVisible() || !loader.client
					.getInterface(popupID).isValid()))
				break;
		}
		// loader.log("Waiting for finish " + objectIDs[0]);
		// loader.log(oldCount+"");
		for (int i = 0; i < 30; i++) {
			if (loader.client.getInventory().getAmount(objectIDs[0]) != oldCount)
				break;
			try {
				loader.sleep(loader.random(500, 600));
			} catch (InterruptedException e) {
				return false;
			}
		}
		if (loader.client.getInventory().getAmount(objectIDs[0]) == oldCount) {
			try {
				loader.walkMiniMap(new Position(loader.myPosition().getX()
						+ loader.random(1, 3), loader.myPosition().getY()
						+ loader.random(1, 3), 0));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// loader.log("Finished");
		try {
			loader.sleep(loader.random(400, 700));
		} catch (InterruptedException e) {
			return false;
		}
		RS2Object builtObject = loader.closestObjectForName(afterName);
		if (builtObject == null) {
			return false;
		}
		try {
			for (int i = 0; i < 10 && !builtObject.interact("Remove"); i++) {
				loader.sleep(loader.random(300, 400));
			}
		} catch (InterruptedException e) {
			return false;
		}
		for (int i = 0; i < 10
				&& (loader.client.getInterface(228) == null
						|| !loader.client.getInterface(228).isValid() || !loader.client
						.getInterface(228).isVisible()); i++)
			try {
				loader.sleep(loader.random(400, 500));
			} catch (InterruptedException e) {
				return false;
			}
		if (!(loader.client.getInterface(228) == null
				|| !loader.client.getInterface(228).isValid() || !loader.client
				.getInterface(228).isVisible()))
			try {
				for (int i = 0; i < 10
						&& !loader.client.getInterface(228).getChild(1)
								.interact(); i++)
					loader.sleep(loader.random(300, 400));
			} catch (InterruptedException e) {
				return false;
			}
		for (int i = 0; i < 10 && loader.myPlayer().getAnimation() == -1; i++) {
			try {
				loader.sleep(loader.random(500, 600));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		for (int i = 0; i < 10 && loader.myPlayer().getAnimation() != -1; i++) {
			try {
				loader.sleep(loader.random(500, 600));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	private double distanceBetween(Point a, Point b) {
		return Math.sqrt((a.getX() - b.getX()) * (a.getX() - b.getX())
				+ (a.getY() - b.getY()) * (a.getY() - b.getY()));
	}
}
