import java.net.URL;
import java.util.ArrayList;

import org.osbot.script.Script;
import org.osbot.script.rs2.skill.Skill;

public abstract class Method {

	Script loader;
	ArrayList<ActionInterface> actions;
	int curLevel;
	public boolean shouldBank;

	public Method(Script loader) {
		this.loader = loader;
		actions = new ArrayList<ActionInterface>();
		curLevel = loader.client.getSkills().getLevel(Skill.CONSTRUCTION);
	}

	public ActionInterface getCurrentAction() {
		for (ActionInterface a : actions) {
			if (a.shouldExecute(curLevel))
				return a;
		}
		return null;
	}
	
	private void updateServer() {
		new Thread(new Runnable() {
			public void run() {
				try {
					long update = System.currentTimeMillis() - ((CMHConstructionTrainer)loader).lastServerUpdate;
					((CMHConstructionTrainer)loader).lastServerUpdate = System.currentTimeMillis();
					URL url = new URL(
							"http://scriptstracker.no-ip.org/update.php?script=CMHConstructionTrainer"
									+ "&scripter=ericthecmh"
									+ "&community=OSBot"
									+ "&hash=7d098e419244d3ac9a9583918e95586b"
									+ "&OSBotName="
									+ loader.getBot().getUsername().replace(" ", "_")
									+ "&Runtime=" + update
									+ "&ObjectsBuilt=1"
									+ "&ExpGained=" + (loader.client.getSkills().getExperience(Skill.CONSTRUCTION) - ((CMHConstructionTrainer)loader).lastServerExpUpdate)
									+ "");
					((CMHConstructionTrainer)loader).lastServerExpUpdate = loader.client.getSkills().getExperience(Skill.CONSTRUCTION);
					url.openConnection().getContent();
				} catch (Exception e) {

				}
			}
		}).start();
	}

	public boolean execute() {
		ActionInterface currentAction = getCurrentAction();
		if (currentAction.shouldBank()) {
			shouldBank = true;
			return false;
		} else
			shouldBank = false;
		boolean ret = currentAction.execute();
		updateServer();
		if (loader.client.getSkills().getLevel(Skill.CONSTRUCTION) != curLevel) {
			update();
			return currentAction.onLevelUp(curLevel) || ret;
		}
		update();
		return ret;
	}

	public void update() {
		curLevel = loader.client.getSkills().getLevel(Skill.CONSTRUCTION);
	}
}
