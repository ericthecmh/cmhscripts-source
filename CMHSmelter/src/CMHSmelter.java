import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import org.osbot.script.Script;
import org.osbot.script.mouse.MouseDestination;
import org.osbot.script.mouse.RectangleDestination;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.model.Entity;
import org.osbot.script.rs2.model.RS2Object;
import org.osbot.script.rs2.skill.Skill;
import org.osbot.script.rs2.ui.Bank;
import org.osbot.script.rs2.ui.RS2Interface;
import org.osbot.script.rs2.ui.RS2InterfaceChild;
import org.osbot.script.rs2.ui.Tab;

public class CMHSmelter extends Script {
	private Script loader;
	private final String currentVersion = "2.6.0";
	private final String settingsVersion = "1.7.5";
	private boolean canStart = false;
	private State state;
	private int walkI = 0;
	private int pathIndex;
	private int bars = 0;
	private long startTime;
	private final int CONST = 1;
	private int startExp = 0;
	private int gainedExp = 0;
	private boolean scriptStopped;
	private boolean antiban = false;
	private CMHSmelterLogin gui;
	private CMHSmelterGUI newgui;
	private BufferedImage[] paintImages = new BufferedImage[4];
	private boolean doneLoading;
	private int paintI = 0;
	private final int offsetX = -3, offsetY = -20;
	private boolean hide;
	private boolean updated;
	private String username;
	private boolean isPrem;
	private Bank b;
	private long lastAnim;
	private int location;
	private int bar;
	private double randomThresh;
	private int cameraPitch;
	private int cameraYaw;
	private int oldCount;
	private int turnOnRunAt;
	private boolean prepToBank;
	private long lastRuntimeUpdate;
	private int lastExpUpdate, lastBarsUpdate;
	private boolean useForging;

	public CMHSmelter(Script l) {
		loader = l;
	}

	/*********************************** GENERAL ***********************************/
	private final int[][] oreIDs = new int[][] { new int[] { 436, 438 },
			{ 440 }, { 440, 453 }, { 447, 453 }, { 449, 453 }, { 451, 453 },
			{ 442 }, { 444 } };
	private final int[] barIDs = new int[] { 2349, 2351, 2353, 2359, 2361,
			2363, 2355, 2357 };
	private final int[] recAmount = new int[] { 1, 0, 2, 4, 6, 8, 0, 0, 0 };
	private final String[] barNames = new String[] { "Bronze", "Iron", "Steel",
			"Mithril", "Adamant", "Rune", "Silver", "Gold" };
	private final int[] cid = new int[] { 13, 21, 29, 37, 41, 45, 25, 33 };
	private final RectangleDestination[] rect = new RectangleDestination[] {
			new RectangleDestination(29, 409, 40, 30),
			new RectangleDestination(134, 409, 40, 30),
			new RectangleDestination(239, 409, 40, 30),
			new RectangleDestination(342, 409, 40, 30),
			new RectangleDestination(395, 409, 40, 30),
			new RectangleDestination(446, 409, 40, 30),
			new RectangleDestination(186, 409, 40, 30),
			new RectangleDestination(290, 409, 40, 30) };
	private final RectangleDestination[] rect2 = new RectangleDestination[] {
			new RectangleDestination(29, 473, 40, 8),
			new RectangleDestination(134, 473, 40, 8),
			new RectangleDestination(239, 473, 40, 8),
			new RectangleDestination(342, 473, 40, 8),
			new RectangleDestination(395, 473, 40, 8),
			new RectangleDestination(446, 473, 40, 8),
			new RectangleDestination(186, 473, 40, 8),
			new RectangleDestination(290, 473, 40, 8) };
	private final int ringOfForgingID = 2568;

	/*********************************** ALKHARID ***********************************/
	private final int alkharidFurnaceID = 24009; // UPDATE
	private final Position[] pathToAlkharidFurnace = new Position[] {
			new Position(3276, 3174, 0), new Position(3279, 3180, 0),
			new Position(3275, 3186, 0) };
	private final Area alkharidFurnaceArea = new Area(3279, 3184, 3272, 3188);
	private final Area alkharidBankArea = new Area(3269, 3173, 3272, 3161);
	private final Position alkharidDoorTile = new Position(3279, 3185, 0);
	private int alkharidDoorID = 13705; // UPDATE

	private class Area {
		private double minX, minY;
		private double maxX, maxY;

		private Area(double d, double e, double f, double g) {
			if (d > f) {
				minX = f;
				maxX = d;
			} else {
				maxX = f;
				minX = d;
			}
			if (e > g) {
				minY = g;
				maxY = e;
			} else {
				maxY = g;
				minY = e;
			}
			System.out.println("Max X: " + maxX + " Min X: " + minX
					+ " Min Y: " + minY + " Max Y: " + maxY);
		}

		public boolean currentlyInArea() {
			return isInArea(loader.myPosition());
		}

		public boolean isInArea(Position p) {
			return (p.getX() >= minX && p.getX() <= maxX && p.getY() >= minY && p
					.getY() <= maxY);
		}

	}

	private enum State {
		WALK_TO_FURNACE, SMELT, BANK, WALKTOBANK;
	}

	public void onStart() {
		log("Loading paint");

		new Thread(new Runnable() {
			public void run() {
				try {
					paintImages[0] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/main.png"));
					paintImages[1] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/stats.png"));
					paintImages[2] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/script.png"));
					paintImages[3] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/feedback.png"));
					doneLoading = true;
				} catch (Exception e) {
				}
			}
		}).start();
		try {
			for (int i = 0; i < 10; i++)
				loader.sleep(random(400, 500));
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		gui = new CMHSmelterLogin();
		gui.isAuth = true;
		gui.isPrem = true;
		username = loader.myPlayer().getName();
		isPrem = gui.isPrem;
		newgui = new CMHSmelterGUI();
		loadSettings(newgui);
		newgui.setVisible(true);
		while (!newgui.ready && newgui.isVisible()) {
			try {
				loader.sleep(random(300, 400));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		newgui.setVisible(false);
		saveSettings(newgui);
		location = newgui.locationMenu.getSelectedIndex();
		bar = newgui.barsMenu.getSelectedIndex();
		if (location == 0) {
			if (alkharidFurnaceArea.currentlyInArea()) {
				state = State.SMELT;
				prepToBank = false;
			} else if (alkharidBankArea.currentlyInArea())
				state = State.BANK;
			else {
				loader.log("You must start in the bank or the furnace room.");
				return;
			}
		}
		startExp = loader.client.getSkills().getExperience(Skill.SMITHING);
		randomThresh = Math.random() * 0.15 + .5;
		cameraPitch = random(50, 65);
		cameraYaw = loader.client.getCameraYawAngle();
		turnOnRunAt = random(35, 65);
		oldCount = (int) loader.client.getInventory().getAmount(barIDs[bar]);
		b = loader.client.getBank();
		useForging = newgui.forging.isSelected();
		lastAnim = System.currentTimeMillis();
		startTime = System.currentTimeMillis();
		lastRuntimeUpdate = startTime;
		canStart = true;
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getX() >= 9 && e.getX() <= 75 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 0;
		} else if (e.getX() >= 75 && e.getX() <= 152 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 1;
		} else if (e.getX() >= 152 && e.getX() <= 220 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 2;
		} else if (e.getX() >= 220 && e.getX() <= 296 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 3;
		}
		if (loader.distance(e.getX(), e.getY(), 506, 358) <= 14) {
			hide = !hide;
		}
	}

	public void onExit() {
		log("Thank you for using CMHSmelter. You have smelted " + bars
				+ " bars.");
		log("and gained " + gainedExp + " EXP.");
		scriptStopped = true;
	}

	private RS2Object closestObjectTo(int name, Position p) {
		List<RS2Object> objects = loader.client.getCurrentRegion().getObjects();
		RS2Object object = null;
		int dist = 20000000;
		for (RS2Object o : objects) {
			if (o.getId() == name) {
				if (object == null) {
					object = o;
					dist = loader.distance(p.getX(), p.getY(), object.getX(),
							object.getY());
				} else {
					if (loader.distance(p.getX(), p.getY(), o.getX(), o.getY()) < dist) {
						dist = loader.distance(p.getX(), p.getY(), o.getX(),
								o.getY());
						object = o;
					}
				}
			}
		}
		return object;
	}

	private void walkMiniMap2(Position p) throws InterruptedException {
		final int mapX = 643, mapY = 83;
		double distance = distanceTo(p);
		double angle2 = Math.toDegrees(Math.atan2(p.getY()
				- loader.myPosition().getY(), p.getX()
				- loader.myPosition().getX())) - 90;
		double dY = distance * Math.sqrt(17);
		double rdX = -1
				* Math.sin(Math.toRadians(angle2
						- loader.client.getCameraYawAngle())) * dY;
		double rdY = Math.cos(Math.toRadians(angle2
				- loader.client.getCameraYawAngle()))
				* dY;
		RectangleDestination rd = new RectangleDestination(
				(int) (mapX + rdX - 1), (int) (mapY - rdY - 1), 3, 3);
		loader.client.moveMouseTo(rd, false, false, false);
		for (int i = 0; i < 5
				&& (Math.abs(loader.client.getMousePosition().getX()
						- (int) (mapX + rdX)) > 2 || Math.abs(loader.client
						.getMousePosition().getY() - (int) (mapY - rdY)) > 2); i++) {
			loader.sleep(random(500, 600));

		}
		if ((Math.abs(loader.client.getMousePosition().getX()
				- (int) (mapX + rdX)) <= 2 && Math.abs(loader.client
				.getMousePosition().getY() - (int) (mapY - rdY)) <= 2)) {
			loader.sleep(random(500, 600));
			loader.client.clickMouse(false);
			for (int i = 0; i < 2 && !loader.myPlayer().isMoving(); i++) {
				loader.sleep(random(500, 600));
			}
			for (int i = 0; i < 60 && loader.myPlayer().isMoving()
					&& distanceTo(p) > 6; i++) {
				loader.sleep(random(500, 600));
			}
		}
	}

	private double distanceTo(Position p) {
		Position myP = loader.myPosition();
		return Math.sqrt((p.getX() - myP.getX()) * (p.getX() - myP.getX())
				+ (p.getY() - myP.getY()) * (p.getY() - myP.getY()));
	}

	private void walkMiniMap2NoBlock(Position p) throws InterruptedException {
		final int mapX = 643, mapY = 83;
		double distance = distanceTo(p);
		double angle2 = Math.toDegrees(Math.atan2(p.getY()
				- loader.myPosition().getY(), p.getX()
				- loader.myPosition().getX())) - 90;
		double dY = distance * Math.sqrt(17);
		double rdX = -1
				* Math.sin(Math.toRadians(angle2
						- loader.client.getCameraYawAngle())) * dY;
		double rdY = Math.cos(Math.toRadians(angle2
				- loader.client.getCameraYawAngle()))
				* dY;
		System.out.println((p.getY() - loader.myPosition().getY()) + ","
				+ (p.getX() - loader.myPosition().getX()));
		System.out.println(angle2 + loader.client.getCameraYawAngle() + " "
				+ rdX + " " + rdY);
		RectangleDestination rd = new RectangleDestination(
				(int) (mapX + rdX - 1), (int) (mapY - rdY - 1), 3, 3);
		loader.client.moveMouseTo(rd, false, false, false);
		for (int i = 0; i < 5
				&& (Math.abs(loader.client.getMousePosition().getX()
						- (int) (mapX + rdX)) > 2 || Math.abs(loader.client
						.getMousePosition().getY() - (int) (mapY - rdY)) > 2); i++) {
			loader.sleep(random(500, 600));

		}
		if ((Math.abs(loader.client.getMousePosition().getX()
				- (int) (mapX + rdX)) <= 2 && Math.abs(loader.client
				.getMousePosition().getY() - (int) (mapY - rdY)) <= 2)) {
			loader.sleep(random(500, 600));
			loader.client.clickMouse(false);
			for (int i = 0; i < 2 && !loader.myPlayer().isMoving(); i++) {
				loader.sleep(random(500, 600));
			}
			for (int i = 0; i < 60 && loader.myPlayer().isMoving()
					&& distanceTo(p) > 6; i++) {
				loader.sleep(random(500, 600));
			}
		}
	}

	private void walkMiniMap2(Entity e) throws InterruptedException {
		walkMiniMap2(e.getPosition());
	}

	public int onLoop() {
		try {
			if (canStart) {
				RS2Interface inte = loader.client.getInterface(499);
				if (inte != null) {
					RS2InterfaceChild c = inte.getChild(24);
					if (c != null) {
						try {
							c.hover();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							c.interact();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				try {
					if (loader.myPlayer().getAnimation() != -1) {
						lastAnim = System.currentTimeMillis();
					}
					try {
						if ((int) (loader.client.getInventory()
								.getAmount(barIDs[bar])) != oldCount) {
							if ((int) (loader.client.getInventory()
									.getAmount(barIDs[bar])) > oldCount) {
								bars += ((int) (loader.client.getInventory()
										.getAmount(barIDs[bar])) - oldCount);
							}
							oldCount = (int) (loader.client.getInventory()
									.getAmount(barIDs[bar]));
						}
					} catch (Exception e) {
					}
					try {
						if (!loader.isRunning()
								&& loader.client.getRunEnergy() > turnOnRunAt
								&& !loader.client.getBank().isOpen()) {
							try {
								setRunning2(true);
								turnOnRunAt = random(35, 65);
								loader.sleep(random(500, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} catch (Exception e) {
					}
					switch (state) {
					case BANK:
						try {
							if (doneBanking()) {
								walkI = 0;
								state = State.WALK_TO_FURNACE;
								break;
							}
							for (int i = 0; i < 10 && !b.isOpen(); i++) {
								RS2Object booth = closestObjectForName("Bank booth");
								try {
									loader.sleep(random(300, 400));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								if (booth == null)
									break;
								try {
									for (int j = 0; j < 10
											&& !booth.interact("Bank"); j++)
										loader.sleep(random(500, 600));
									loader.sleep(random(500, 700));
									for (int j = 0; j < 10 && !b.isOpen(); j++) {
										loader.sleep(random(200, 300));
										if (loader.myPlayer().isMoving())
											j--;
									}
								} catch (Exception e) {
								}
							}
							try {
								depositBank();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							try {
								try {
									if (!hasEnough()
											&& loader.client.getBank().isOpen()) {
										loader.sleep(random(2000, 2500));
										if (!hasEnough()
												&& loader.client.getBank()
														.isOpen()) {
											loader.client.getBank().close();
											loader.sleep(random(1000, 1100));
											loader.log("Out of ores");
											loader.stop();
										}
									}
									withdrawBank();
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} catch (Exception e) {
							}
							while (loader.client.getInventory().contains(
									ringOfForgingID)) {
								if (b.isOpen()) {
									for (int i = 0; i < 10 && !b.close(); i++)
										loader.sleep(random(500, 600));
									loader.sleep(random(1000, 1200));
								}
								loader.client.getInventory().interactWithId(
										ringOfForgingID, "Wear");
								loader.sleep(random(500, 600));
							}
						} catch (Exception e) {
						}
						break;
					case WALK_TO_FURNACE:
						if (location == 0) {
							walkAlkharidFurnace();
						}
						break;
					case SMELT:
						if (location == 0) {
							doneLoading = false;
							Thread sThread = new Thread(new Runnable() {
								public void run() {
									try {
										smeltAlkharid();
									} catch (Exception e) {

									} finally {
										doneLoading = true;
									}
								}
							});
							sThread.start();
							try {
								while (!doneLoading) {
									if (!alkharidFurnaceArea.currentlyInArea()) {
										loader.walkMiniMap(pathToAlkharidFurnace[pathToAlkharidFurnace.length - 1]);
										for (int i = 0; i < 2
												&& !loader.myPlayer()
														.isMoving(); i++) {
											loader.sleep(random(500, 600));
										}
										for (int i = 0; i < 20
												&& loader.myPlayer().isMoving(); i++) {
											loader.sleep(random(500, 600));
										}
									}
									loader.sleep(random(500, 600));
								}
							} catch (Exception e) {

							} finally {
								sThread.interrupt();
								sThread.stop();
								doneLoading = true;
							}
						}
						break;
					case WALKTOBANK:
						if (location == 0) {
							walkAlkharidBank();
						}
						break;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
		}
		return random(500, 600);
	}

	private void setRunning2(boolean on) {
		try {
			for (int i = 0; i < 10 && !loader.openTab(Tab.SETTINGS); i++) {
				sleep(random(500, 600));
			}

			RS2Interface inte = loader.client.getInterface(261);
			if (inte != null) {
				RS2InterfaceChild child = inte.getChild(53);
				if (child != null) {
					child.hover();
					sleep(random(1000, 1100));
					child.interact();
					for (int i = 0; i < 3 && !loader.isRunning(); i++)
						sleep(random(350, 500));
				}
			}
		} catch (Exception e) {

		}
		return;
	}

	private RS2Object closestObjectForName(String name) {
		double dist = 2000000000;
		RS2Object ret = null;
		for (RS2Object o : loader.client.getCurrentRegion().getObjects()) {
			if (o.getName().equals(name) && distanceTo(o) < dist
					&& o.getModel().getVerticeCount() > 0) {
				dist = distanceTo(o);
				ret = o;
			}
		}
		return ret;
	}

	private double distanceTo(Entity e) {
		return Math.sqrt((loader.myX() - e.getX()) * (loader.myX() - e.getX())
				+ (loader.myY() - e.getY()) * (loader.myY() - e.getY()));
	}

	private void depositBank() throws InterruptedException {
		for (int x = 0; x < 10
				&& (!loader.client.getInventory().isEmpty()); x++) {
				b.depositAll();
			loader.sleep(random(900, 1100));
		}
	}

	private void withdrawBank() throws InterruptedException {
		if (useForging
				&& loader.equipmentTab.getSlotForName("Ring of forging") == null) {
			for (int i = 0; i < 10 && !b.withdraw1(ringOfForgingID); i++)
				loader.sleep(random(500, 600));
			return;
		}
		if (bar == 0) {
			if (loader.client.getInventory().getAmount(oreIDs[bar][0]) < 14)
				for (int i = 0; i < 10
						&& !b.withdrawX(oreIDs[bar][0],
								(int) (14 - loader.client.getInventory()
										.getAmount(oreIDs[bar][0]))); i++)
					loader.sleep(random(500, 600));
			for (int i = 0; i < 10
					&& !loader.client.getInventory().contains(oreIDs[bar][0]); i++)
				loader.sleep(random(500, 600));
			if (loader.client.getInventory().getAmount(oreIDs[bar][1]) < 14)
				for (int i = 0; i < 10 && !b.withdrawX(oreIDs[bar][1], 14); i++)
					loader.sleep(random(500, 600));
			for (int i = 0; i < 10
					&& !loader.client.getInventory().contains(oreIDs[bar][1]); i++)
				loader.sleep(random(500, 600));
		} else if (bar == 1 || bar == 6 || bar == 7) {
			if (loader.client.getInventory().getAmount(oreIDs[bar][0]) < 28)
				for (int i = 0; i < 10 && !b.withdrawAll(oreIDs[bar][0]); i++)
					loader.sleep(random(500, 600));
			for (int i = 0; i < 10
					&& !loader.client.getInventory().contains(oreIDs[bar][0]); i++)
				loader.sleep(random(500, 600));
		} else if (bar == 2) {
			if (loader.client.getInventory().getAmount(oreIDs[bar][0]) < 9)
				for (int i = 0; i < 10
						&& !b.withdrawX(oreIDs[bar][0],
								(int) (9 - loader.client.getInventory()
										.getAmount(oreIDs[bar][0]))); i++)
					loader.sleep(random(500, 600));
			for (int i = 0; i < 10
					&& !loader.client.getInventory().contains(oreIDs[bar][0]); i++)
				loader.sleep(random(500, 600));
			if (loader.client.getInventory().getAmount(oreIDs[bar][1]) < 18)
				for (int i = 0; i < 10 && !b.withdrawAll(oreIDs[bar][1]); i++)
					loader.sleep(random(500, 600));
			for (int i = 0; i < 10
					&& loader.client.getInventory().getAmount(oreIDs[bar][1]) < 5; i++)
				loader.sleep(random(500, 600));
		} else if (bar == 3) {
			if (loader.client.getInventory().getAmount(oreIDs[bar][0]) < 5)
				for (int i = 0; i < 10
						&& !b.withdrawX(oreIDs[bar][0],
								(int) (5 - loader.client.getInventory()
										.getAmount(oreIDs[bar][0]))); i++)
					loader.sleep(random(500, 600));
			for (int i = 0; i < 10
					&& !loader.client.getInventory().contains(oreIDs[bar][0]); i++)
				loader.sleep(random(500, 600));
			if (loader.client.getInventory().getAmount(oreIDs[bar][1]) < 20)
				for (int i = 0; i < 10 && !b.withdrawAll(oreIDs[bar][1]); i++)
					loader.sleep(random(500, 600));
			for (int i = 0; i < 10
					&& loader.client.getInventory().getAmount(oreIDs[bar][1]) < 5; i++)
				loader.sleep(random(500, 600));
		} else if (bar == 4) {
			if (loader.client.getInventory().getAmount(oreIDs[bar][0]) < 4)
				for (int i = 0; i < 10
						&& !b.withdrawX(oreIDs[bar][0],
								(int) (4 - loader.client.getInventory()
										.getAmount(oreIDs[bar][0]))); i++)
					loader.sleep(random(500, 600));
			for (int i = 0; i < 10
					&& !loader.client.getInventory().contains(oreIDs[bar][0]); i++)
				loader.sleep(random(500, 600));
			if (loader.client.getInventory().getAmount(oreIDs[bar][1]) < 24)
				for (int i = 0; i < 10 && !b.withdrawAll(oreIDs[bar][1]); i++)
					loader.sleep(random(500, 600));
			for (int i = 0; i < 10
					&& loader.client.getInventory().getAmount(oreIDs[bar][1]) < 5; i++)
				loader.sleep(random(500, 600));
		} else if (bar == 5) {
			if (loader.client.getInventory().getAmount(oreIDs[bar][0]) < 3)
				for (int i = 0; i < 10
						&& !b.withdrawX(oreIDs[bar][0],
								(int) (3 - loader.client.getInventory()
										.getAmount(oreIDs[bar][0]))); i++)
					loader.sleep(random(500, 600));
			for (int i = 0; i < 10
					&& !loader.client.getInventory().contains(oreIDs[bar][0]); i++)
				loader.sleep(random(500, 600));
			if (loader.client.getInventory().getAmount(oreIDs[bar][1]) < 24)
				for (int i = 0; i < 10 && !b.withdrawAll(oreIDs[bar][1]); i++)
					loader.sleep(random(500, 600));
			for (int i = 0; i < 10
					&& loader.client.getInventory().getAmount(oreIDs[bar][1]) < 5; i++)
				loader.sleep(random(500, 600));
		}
	}

	private boolean doneBanking() {
		if (bar == 0) {
			return loader.client.getInventory().getAmount(oreIDs[bar][0]) == 14
					&& loader.client.getInventory().getAmount(oreIDs[bar][1]) == 14;
		} else if (bar == 1 || bar == 6 || bar == 7) {
			return loader.client.getInventory().getAmount(oreIDs[bar][0]) == 28;
		} else if (bar == 2) {
			return loader.client.getInventory().getAmount(oreIDs[bar][0]) == 9
					&& loader.client.getInventory().getAmount(oreIDs[bar][1]) >= 18;
		} else if (bar == 3) {
			return loader.client.getInventory().getAmount(oreIDs[bar][0]) == 5
					&& loader.client.getInventory().getAmount(oreIDs[bar][1]) >= 20;
		} else if (bar == 4) {
			return loader.client.getInventory().getAmount(oreIDs[bar][0]) == 4
					&& loader.client.getInventory().getAmount(oreIDs[bar][1]) >= 24;
		} else if (bar == 5) {
			return loader.client.getInventory().getAmount(oreIDs[bar][0]) == 3
					&& loader.client.getInventory().getAmount(oreIDs[bar][1]) >= 24;
		}
		return false;
	}

	private boolean hasEnough() {
		if (bar == 0) {
			return loader.client.getBank().getAmount(oreIDs[bar][1]) >= 14
					&& loader.client.getBank().getAmount(oreIDs[bar][1]) >= 14;
		} else if (bar == 1 || bar == 6 || bar == 7) {
			return loader.client.getBank().getAmount(oreIDs[bar][0]) >= 28;
		} else if (bar == 2) {
			return loader.client.getBank().getAmount(oreIDs[bar][0]) >= 9
					&& loader.client.getBank().getAmount(oreIDs[bar][1]) >= 18;
		} else if (bar == 3) {
			return loader.client.getBank().getAmount(oreIDs[bar][0]) >= 5
					&& loader.client.getBank().getAmount(oreIDs[bar][1]) >= 20;
		} else if (bar == 4) {
			return loader.client.getBank().getAmount(oreIDs[bar][0]) >= 4
					&& loader.client.getBank().getAmount(oreIDs[bar][1]) >= 24;
		} else if (bar == 5) {
			return loader.client.getBank().getAmount(oreIDs[bar][0]) >= 3
					&& loader.client.getBank().getAmount(oreIDs[bar][1]) >= 24;
		}
		return false;
	}

	private void walkAlkharidFurnace() {
		if (walkI == 0) {
			try {
				loader.walkMiniMap(randomize(pathToAlkharidFurnace[walkI++]));
				return;
			} catch (InterruptedException e) {
			}
		}
		RS2Object door = closestObjectTo(alkharidDoorID, alkharidDoorTile);
		if (door != null && distanceBetween(alkharidDoorTile, door) < 2.0) {
			if (door.isVisible()) {
				try {
					for (int i = 0; i < 10 && !door.interact("Open"); i++) {
						loader.sleep(random(500, 600));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					loader.walk(door);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			walkI = pathToAlkharidFurnace.length;
			return;
		}
		if (walkI == pathToAlkharidFurnace.length) {
			walkI = 0;
			prepToBank = false;
			state = State.SMELT;
			return;
		}
		try {
			if (loader.realDistance(pathToAlkharidFurnace[walkI - 1]) <= 6) {
				loader.walkMiniMap(randomize(pathToAlkharidFurnace[walkI++]));
			} else
				loader.walkMiniMap(randomize(pathToAlkharidFurnace[walkI - 1]));
		} catch (InterruptedException e) {

		}
	}

	private double distanceBetween(Position p1, Entity p2) {
		return Math.sqrt((p1.getX() - p2.getX()) * (p1.getX() - p2.getX())
				+ (p1.getY() - p2.getY()) * (p1.getY() - p2.getY()));
	}

	private double distanceBetween(Entity p1, Entity p2) {
		return Math.sqrt((p1.getX() - p2.getX()) * (p1.getX() - p2.getX())
				+ (p1.getY() - p2.getY()) * (p1.getY() - p2.getY()));
	}

	private void smeltAlkharid() throws InterruptedException {
		if (!prepToBank) {
			if (Math.abs(loader.client.getCameraPitchAngle() - cameraPitch) > 5) {
				new Thread(new Runnable() {
					public void run() {
						try {
							loader.client.rotateCameraPitch(cameraPitch);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch
							// block
							e.printStackTrace();
						}
					}
				}).start();
			}
			// if (Math.abs(loader.client.getCameraYawAngle() - cameraYaw) > 5)
			// {
			// new Thread(new Runnable() {
			// public void run() {
			// try {
			// loader.client.rotateCameraToAngle(cameraYaw);
			// } catch (InterruptedException e) {
			// // TODO Auto-generated catch
			// // block
			// e.printStackTrace();
			// }
			// }
			// }).start();
			// }
		}
		if (loader.client.getInventory().getAmount(oreIDs[bar][0]) <= 2
				&& !prepToBank) {
			prepToBank = true;
			new Thread(new Runnable() {
				public void run() {
					try {
						loader.client.rotateCameraToAngle(random(125, 160));
						loader.client.rotateCameraPitch(0);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}).start();
		}
		if (loader.client.getInventory().getAmount(oreIDs[bar][0]) == 0
				|| (oreIDs[bar].length == 2 && loader.client.getInventory()
						.getAmount(oreIDs[bar][1]) < recAmount[bar])) {
			updateServer();
			state = State.WALKTOBANK;
			return;
		} else {
			if (System.currentTimeMillis() - lastAnim > 5000) {
				RS2Object furnace = loader.closestObject(alkharidFurnaceID);
				if (furnace != null) {
					if (furnace.interact("Smelt")) {
						loader.sleep(random(500, 600));
						try {
							loader.client.moveMouseTo(rect[bar], false, false,
									false);
							for (int i = 0; i < 10
									&& (loader.client.getInterface(311) == null || loader.client
											.getInterface(311).getChild(
													cid[bar]) == null); i++)
								loader.sleep(random(500, 600));
							loader.sleep(random(700, 900));
						} catch (Exception e) {
						}
						if (loader.client.getInterface(311) != null
								&& loader.client.getInterface(311).getChild(
										cid[bar]) != null) {
							loader.client.clickMouse(true);
							try {
								loader.client.moveMouseTo(rect2[bar], false,
										false, false);
								sleep(random(500, 600));
								loader.client.clickMouse(false);
								sleep(random(500, 600));
								loader.type(random(100, 9999) + "");
							} catch (Exception e) {
							}
							loader.sleep(random(1000, 2000));
						}
					}
				}
			} else if (!prepToBank) {
				if (Math.random() < 0.1) {
					Thread antibanThread = new Thread(new Runnable() {
						public void run() {
							doAntiban();
						}

						private void doAntiban() {
							antiban = true;
							try {
								int r = random(9);
								Rectangle k = new Rectangle(1 + random(515),
										1 + random(330), 10, 10);
								switch (r) {
								case 2:
									loader.client.moveMouseTo(
											new RectangleDestination(k), false,
											false, false);
									loader.sleep(random(3000, 5000));
									break;
								case 3:
									loader.openTab(Tab.SKILLS);
									loader.sleep(random(300, 400));
									try {
										loader.client.getInterface(320)
												.getChild(128).hover();
									} catch (NullPointerException e) {
										loader.sleep(1000);
										return;
									}
									for (int j = 0; j < 10
											&& loader.myPlayer().getAnimation() != -1; j++) {
										loader.sleep(random(250, 350));
									}
									if (true) {
										loader.openTab(Tab.INVENTORY);
										randomThresh = Math.random() * 0.15 + .5;
									}
									break;
								case 4:
								case 5:
								case 9:
									cameraYaw = random(1, 359);
									loader.client
											.rotateCameraToAngle(cameraYaw);
									break;
								case 7:
								case 8:
									cameraPitch = random(50, 65);
									loader.client
											.rotateCameraPitch(cameraPitch);
								}
							} catch (InterruptedException e) {
							}
							antiban = false;
						}
					});
					antibanThread.start();
					for (int i = 0; i < 10 && antiban; i++) {
						loader.sleep(random(800, 900));
					}
					loader.sleep(random(1500, 2000));
					if (antiban) {
						antiban = false;
						/*
						 * try { antibanThread.interrupt();
						 * antibanThread.stop(); } catch (Exception e) { }
						 */
					}
				}
			}
		}
	}

	private void updateServer() {
		new Thread(new Runnable() {
			public void run() {
				try {
					int gainedTotal = gainedExp;
					int newbars = (bars) - lastBarsUpdate;
					URL url = new URL(
							"http://cmhscripts.no-ip.org/update.php?script=CMHSmelter&Username="
									+ loader.myPlayer().getName()
									+ "&Runtime="
									+ (System.currentTimeMillis() - lastRuntimeUpdate)
									+ "&ExpGained="
									+ (gainedTotal - lastExpUpdate)
									+ "&Smelted=" + newbars + "&OSBotName="
									+ loader.getBot().getUsername());
					lastRuntimeUpdate = System.currentTimeMillis();
					lastExpUpdate = gainedTotal;
					lastBarsUpdate = bars;
					url.openConnection().getContent();
				} catch (Exception e) {

				}
			}
		}).start();
	}

	private void type(int a) {
		String str = a + "";
		for (int i = 0; i < str.length(); i++) {
			try {
				loader.bot.getKeyboard().pressKey(str.charAt(i));
				loader.sleep(random(200, 400));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void walkAlkharidBank() {
		if (alkharidBankArea.currentlyInArea()) {
			for (int i = 0; i < 10 && loader.myPlayer().isMoving(); i++) {
				try {
					sleep(random(500, 600));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			for (int i = 0; i < 5 && !b.isOpen(); i++) {
				try {
					sleep(random(500, 600));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			state = State.BANK;
			return;
		}
		if (!loader.myPlayer().isMoving()) {
			RS2Object door = closestObjectTo(alkharidDoorID, alkharidDoorTile);
			if (door != null && distanceBetween(alkharidDoorTile, door) < 2.0) {
				if (door.isVisible()) {
					try {
						for (int i = 0; i < 10 && !door.interact("Open"); i++) {
							loader.sleep(random(500, 600));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					try {
						loader.walk(door);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					new Thread(new Runnable() {
						public void run() {
							try {
								loader.client.rotateCameraPitch(90);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							// try {
							// loader.client.rotateCameraToAngle(175 + random(
							// 0, 10));
							// } catch (InterruptedException e) {
							// // TODO Auto-generated catch block
							// e.printStackTrace();
							// }
						}
					}).start();
					try {
						loader.sleep(random(2000, 3000));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				walkI = pathToAlkharidFurnace.length;
				return;
			}
			RS2Object bank = closestObjectForName("Bank booth");
			if (!prepToBank) {
				new Thread(new Runnable() {
					public void run() {
						try {
							loader.client.rotateCameraToAngle(random(125, 160));
							loader.client.rotateCameraPitch(0);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}).start();
				prepToBank = false;
				for (int i = 0; i < 10 && !bank.isVisible(); i++) {
					try {
						loader.sleep(random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			try {
				bank.interact("Bank");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for (int i = 0; i < 10 && !loader.myPlayer().isMoving(); i++)
				try {
					sleep(random(300, 400));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return;
	}

	private Position randomize(Position p) {
		return new Position(p.getX() + random(1, 2 * CONST + 1) - (CONST + 1),
				p.getY() + random(1, 2 * CONST + 1) - (CONST + 1), 0);
	}

	private void loadSettings(CMHSmelterGUI gui) {
		String filename;
		if (System.getProperty("os.name").startsWith("Window")) {
			filename = "\\OSBot\\scripts\\CMHSmelter.settings"
					+ settingsVersion;
		} else {
			filename = "/OSBot/scripts/CMHSmelter.settings" + settingsVersion;
		}
		try {
			Scanner scan = new Scanner(new File(System.getProperty("user.home")
					+ filename));
			gui.locationMenu
					.setSelectedIndex(Integer.parseInt(scan.nextLine()));
			gui.barsMenu.setSelectedIndex(Integer.parseInt(scan.nextLine()));
			gui.forging.setSelected(Boolean.parseBoolean(scan.nextLine()));
			scan.close();
		} catch (Exception e) {

		}
	}

	private void saveSettings(CMHSmelterGUI gui) {
		String filename;
		if (System.getProperty("os.name").startsWith("Window")) {
			filename = "\\OSBot\\scripts\\CMHSmelter.settings"
					+ settingsVersion;
		} else {
			filename = "/OSBot/scripts/CMHSmelter.settings" + settingsVersion;
		}
		try {
			PrintStream ps = new PrintStream(System.getProperty("user.home")
					+ filename);
			ps.println(gui.locationMenu.getSelectedIndex());
			ps.println(gui.barsMenu.getSelectedIndex());
			ps.println(gui.forging.isSelected());
			ps.close();
		} catch (Exception e) {

		}
	}

	public void onPaint(Graphics g) {
		if (canStart) {
			if (hide) {
				g.setColor(Color.BLUE);
				g.fillOval(492, 346, 28, 28);
				return;
			}
			switch (paintI) {
			case 0:
				g.drawImage(paintImages[paintI], -20, 205, null);
				g.setColor(Color.RED);
				String status = state.toString();
				if (antiban)
					status = "ANTIBAN";
				g.drawString("Current State: " + status, 12, 400);
				g.drawString("Logged in as: " + loader.myPlayer().getName()
						+ ".", 12, 415);
				double hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
				int minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
				int seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
				g.drawString("Runtime: " + "Running for: " + pad((int) (hours))
						+ ":" + pad(minutes) + ":" + pad(seconds), 12, 430);

				gainedExp = loader.client.getSkills().getExperience(
						Skill.SMITHING)
						- startExp;
				g.drawString("Rate: " + (int) (bars / hours) + " "
						+ barNames[bar] + " bars/HR", 12, 445);
				g.drawString("Smelted " + (bars) + " bars", 12, 460);
				break;
			case 1:
				g.drawImage(paintImages[1], -20, 205, null);
				g.setColor(Color.RED);
				hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
				minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
				seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
				gainedExp = loader.client.getSkills().getExperience(
						Skill.SMITHING)
						- startExp;
				g.drawString("Current Smithing Level: "
						+ loader.client.getSkills().getLevel(Skill.SMITHING),
						12, 400);
				g.drawString("Experience Gained: " + gainedExp, 12, 415);
				g.drawString("Experience to Level: "
						+ experienceToNextLevel(Skill.SMITHING), 12, 430);
				double r = ((gainedExp) / (seconds + minutes * 60 + hours * 3600.0));
				if (r == 0)
					r = .00000001;
				r *= 2;
				int TTL = (int) (experienceToNextLevel(Skill.SMITHING) / r);
				hours = (TTL) / 3600;
				minutes = (int) (((TTL) / 60.0) % 60);
				seconds = (int) ((TTL) % 60);
				g.drawString("Time To Level: " + pad((int) hours) + ":"
						+ pad(minutes) + ":" + pad(seconds), 12, 445);
				double perc = percentToNextLevel(Skill.SMITHING);
				g.setColor(new Color(0, 1, 0, 0.75f));
				g.fillRect(15 + offsetX, 475 + offsetY, (int) (500 * perc), 17);
				g.setColor(new Color(1, 1, 1, 0.75f));
				g.fillRect(15 + offsetX, 475 + offsetY, 490, 8);
				g.setColor(new Color(.5f, .5f, .5f, 0.75f));
				g.fillRect(15 + offsetX, 484 + offsetY, 490, 8);
				g.setColor(Color.RED);
				g.drawString((int) (perc * 100) + "% : "
						+ experienceToNextLevel(Skill.SMITHING) + " TNL", 200,
						489 + offsetY);
				break;
			case 2:
				g.drawImage(paintImages[2], -20, 205, null);
				g.setColor(Color.RED);
				g.drawString("CMHSmelter Version " + currentVersion, 12, 400);
				g.drawString("Script Developed by Ericthecmh", 12, 415);
				break;
			case 3:
				g.drawImage(paintImages[3], -20, 205, null);
				g.setColor(Color.RED);
				g.drawString("My Email: info@cmhscripts.com", 12, 400);
				g.drawString(
						"or leave feedback on the OSBot Forum Thread under SDN->Mining&Smithing->CMHSmelter",
						12, 430);
				g.drawString("Any and all feedback is welcome.", 12, 445);
				break;
			}
			// g.drawString((antiban ? "ANTIBAN" : state.toString()), 100, 100);
			// gainedExp =
			// loader.client.getSkills().getExperience(Skill.SMITHING)
			// - startExp;
			// g.drawString("Experience Gained: " + gainedExp, 100, 180);
			// mined = gainedExp / 5;
			// double hours = (System.currentTimeMillis() - startTime) / 1000.0
			// / 3600;
			// g.drawString("" + (int) (mined / hours) + " ESS/HR", 100, 120);
			// g.drawString("" + (mined) + " Mined ess", 100, 140);
			// int minutes = (int) (((System.currentTimeMillis() - startTime) /
			// 1000.0 / 60.0) % 60);
			// int seconds = (int) (((System.currentTimeMillis() - startTime) /
			// 1000.0) % 60);
			// g.drawString("Running for: " + (int) (hours) + ":" + minutes +
			// ":"
			// + seconds, 100, 160);
			// g.drawString("Current Mining Level: "
			// + loader.client.getSkills().getLevel(Skill.SMITHING), 100,
			// 200);
		}
	}

	private String pad(int i) {
		String ret = i + "";
		if (ret.length() == 1) {
			return "0" + ret;
		}
		return ret;
	}

	private double percentToNextLevel(Skill skill) {
		int level = loader.client.getSkills().getLevel(skill);
		double total = 0.0;
		total = xpForLevels[level] - xpForLevels[level - 1];
		return 1 - ((experienceToNextLevel(skill) + 0.0) / total);
	}

	/***************** MADE BY ABIBOT *******************/
	int[] xpForLevels = new int[] { 0, 83, 174, 276, 388, 512, 650, 801, 969,
			1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470,
			5018, 5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363,
			14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648,
			37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014,
			91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040,
			203254, 224466, 247886, 273742, 302288, 333804, 368599, 407015,
			449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257,
			992895, 1096278, 1210421, 1336443, 1475581, 1629200, 1798808,
			1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792,
			3972294, 4385776, 4842295, 5346332, 5902831, 6517253, 7195629,
			7944614, 8771558, 9684577, 10692629, 11805606, 13034431 };

	private int experienceToNextLevel(Skill skill) {
		int xp = loader.client.getSkills().getExperience(skill);
		for (int i = 0; i < 99; i++) {
			if (xp < xpForLevels[i]) {
				return (xpForLevels[i] - xp);
			}
		}
		return (200000000 - xp);
	}

	class CMHSmelterLogin extends javax.swing.JPanel {

		public boolean isAuth;
		public boolean isPrem;
		public BufferedReader in;
		public PrintWriter out;
		public String username;
		public final String name = "CMHSmelter";
		public boolean ready;

		/**
		 * Creates new form CMHStrongholdLogin
		 */
		public CMHSmelterLogin() {
			initComponents();
			String filename;
			if (System.getProperty("os.name").startsWith("Window")) {
				filename = "\\OSBot\\scripts\\CMHSmelter.user";
			} else {
				filename = "/OSBot/scripts/CMHSmelter.user";
			}
			try {
				Scanner scan = new Scanner(new File(
						System.getProperty("user.home") + filename));
				usernameField.setText(scan.nextLine());
				passwordField.setText(scan.nextLine());
				scan.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		/**
		 * This method is called from within the constructor to initialize the
		 * form. WARNING: Do NOT modify this code. The content of this method is
		 * always regenerated by the Form Editor.
		 */
		@SuppressWarnings("unchecked")
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		private void initComponents() {

			jLabel1 = new javax.swing.JLabel();
			usernameField = new javax.swing.JTextField();
			jLabel2 = new javax.swing.JLabel();
			passwordField = new javax.swing.JPasswordField();
			jButton1 = new javax.swing.JButton();
			jButton2 = new javax.swing.JButton();

			setName("Login"); // NOI18N

			jLabel1.setText("Username:");

			jLabel2.setText("Password:");

			jButton1.setText("Login");
			jButton1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton1ActionPerformed(evt);
				}
			});

			jButton2.setText("Create Account");
			jButton2.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton2ActionPerformed(evt);
				}
			});

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
			this.setLayout(layout);
			layout.setHorizontalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							javax.swing.GroupLayout.Alignment.TRAILING,
							layout.createSequentialGroup()
									.addContainerGap(81, Short.MAX_VALUE)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.LEADING,
													false)
													.addComponent(
															jButton1,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															Short.MAX_VALUE)
													.addGroup(
															layout.createSequentialGroup()
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING)
																					.addComponent(
																							jLabel1)
																					.addComponent(
																							jLabel2))
																	.addGap(18,
																			18,
																			18)
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING,
																					false)
																					.addComponent(
																							usernameField)
																					.addComponent(
																							passwordField,
																							javax.swing.GroupLayout.PREFERRED_SIZE,
																							162,
																							javax.swing.GroupLayout.PREFERRED_SIZE)))
													.addComponent(
															jButton2,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															255,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addGap(67, 67, 67)));
			layout.setVerticalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addGap(72, 72, 72)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel1)
													.addComponent(
															usernameField,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel2)
													.addComponent(
															passwordField,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
									.addComponent(jButton1)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(jButton2)
									.addContainerGap(82, Short.MAX_VALUE)));
		}// </editor-fold>

		private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {

		}

		private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
			username = usernameField.getText();
			if (username.length() == 0) {
				JOptionPane.showMessageDialog(null, "Your username is empty!");
				return;
			}
			String password = new String(passwordField.getPassword());
			// Using a JPanel as the message for the JOptionPane
			JPanel userPanel = new JPanel();
			userPanel.setLayout(new GridLayout(1, 2));
			JLabel passwordLbl = new JLabel("Password:");

			JPasswordField passwordFld = new JPasswordField();
			userPanel.add(passwordLbl);
			userPanel.add(passwordFld);

			JOptionPane.showMessageDialog(null, userPanel,
					"Confirm your password:", JOptionPane.PLAIN_MESSAGE);

			String confirmPassword = new String(passwordFld.getPassword());
			String email = JOptionPane.showInputDialog(null,
					"Email (Don't worry I won't sell it):");
			if (!password.equals(confirmPassword)) {
				JOptionPane.showMessageDialog(null, "Passwords don't match!");
				return;
			}
			Socket sock = null;
			try {
				if (username.equals("ericthecmh1")
						|| username.equals("marvin57")) {
					sock = new Socket("54.211.36.47", 8086);
				} else {
					sock = new Socket(((Math.random() > 0.5) ? "54.226.90.112"
							: "54.211.36.47"), 8086);
				}
			} catch (UnknownHostException e) {

				return;
			} catch (IOException ex) {

				return;
			}
			try {
				in = new BufferedReader(new InputStreamReader(
						sock.getInputStream()));
				out = new PrintWriter(sock.getOutputStream(), true);
			} catch (IOException e) {
			}
			out.println("create");
			out.println(name);
			out.println(username);
			out.println(password);
			out.println(email);
			String ack = null;
			try {
				while ((ack = in.readLine()) == null) {
				}
				if (ack.startsWith("CreateFailed")) {
					isAuth = false;
					isPrem = false;
					while ((ack = in.readLine()) == null) {
					}
					JOptionPane.showMessageDialog(null, ack);
				} else {
					isPrem = false;
					isAuth = false;
					// ready = false;
					// username = null;
					JOptionPane.showMessageDialog(null,
							"Your account has been created.");
				}
			} catch (IOException e) {
			}
		}

		// Variables declaration - do not modify
		private javax.swing.JButton jButton1;
		private javax.swing.JButton jButton2;
		private javax.swing.JLabel jLabel1;
		private javax.swing.JLabel jLabel2;
		private javax.swing.JPasswordField passwordField;
		private javax.swing.JTextField usernameField;
		// End of variables declaration
	}

	class CMHSmelterGUI extends javax.swing.JFrame {

		/**
		 * Creates new form CMHSmelterGUI
		 */

		public boolean ready;

		public CMHSmelterGUI() {
			initComponents();
		}

		/**
		 * This method is called from within the constructor to initialize the
		 * form. WARNING: Do NOT modify this code. The content of this method is
		 * always regenerated by the Form Editor.
		 */
		@SuppressWarnings("unchecked")
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		private void initComponents() {

			jLabel1 = new javax.swing.JLabel();
			locationMenu = new javax.swing.JComboBox();
			jLabel2 = new javax.swing.JLabel();
			barsMenu = new javax.swing.JComboBox();
			jButton1 = new javax.swing.JButton();
			forging = new javax.swing.JCheckBox();

			setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

			jLabel1.setText("Location:");

			locationMenu.setModel(new javax.swing.DefaultComboBoxModel(
					new String[] { "Al Kharid" }));

			jLabel2.setText("Bars:");

			barsMenu.setModel(new javax.swing.DefaultComboBoxModel(
					new String[] { "Bronze", "Iron", "Steel", "Mithril",
							"Adamant", "Rune", "Silver", "Gold" }));

			jButton1.setText("Start");
			jButton1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton1ActionPerformed(evt);
				}
			});

			forging.setText("Use ring of forging");

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
					getContentPane());
			getContentPane().setLayout(layout);
			layout.setHorizontalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addContainerGap()
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.LEADING)
													.addComponent(
															jButton1,
															javax.swing.GroupLayout.Alignment.TRAILING,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															Short.MAX_VALUE)
													.addGroup(
															javax.swing.GroupLayout.Alignment.TRAILING,
															layout.createSequentialGroup()
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING)
																					.addComponent(
																							jLabel1)
																					.addComponent(
																							jLabel2))
																	.addPreferredGap(
																			javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING)
																					.addComponent(
																							locationMenu,
																							0,
																							javax.swing.GroupLayout.DEFAULT_SIZE,
																							Short.MAX_VALUE)
																					.addComponent(
																							barsMenu,
																							0,
																							300,
																							Short.MAX_VALUE)))
													.addGroup(
															layout.createSequentialGroup()
																	.addComponent(
																			forging)
																	.addGap(0,
																			0,
																			Short.MAX_VALUE)))
									.addContainerGap()));
			layout.setVerticalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addContainerGap()
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel1)
													.addComponent(
															locationMenu,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel2)
													.addComponent(
															barsMenu,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(forging)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(jButton1)
									.addContainerGap(
											javax.swing.GroupLayout.DEFAULT_SIZE,
											Short.MAX_VALUE)));

			pack();
		}// </editor-fold>

		private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
			ready = true;
		}

		// Variables declaration - do not modify
		private javax.swing.JComboBox barsMenu;
		private javax.swing.JCheckBox forging;
		private javax.swing.JButton jButton1;
		private javax.swing.JLabel jLabel1;
		private javax.swing.JLabel jLabel2;
		private javax.swing.JComboBox locationMenu;
		// End of variables declaration
	}

}
