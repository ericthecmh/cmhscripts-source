import java.util.ArrayList;

public class CMHCleverbot {
	ChatterBotSession session;
	ArrayList<String> responses;
	private long timestamp = 0;
	int waittime;
	
	public CMHCleverbot(ArrayList<String> responses, int waittime) {
		this.responses = responses;
		this.waittime = waittime;
		while (session == null) {
			try {
				ChatterBotFactory factory = new ChatterBotFactory();
				ChatterBot cleverbot = factory.create(ChatterBotType.CLEVERBOT);
				session = cleverbot.createSession();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void recreateSession() {
		session = null;
		while (session == null) {
			try {
				ChatterBotFactory factory = new ChatterBotFactory();
				ChatterBot cleverbot = factory.create(ChatterBotType.CLEVERBOT);
				session = cleverbot.createSession();
			} catch (Exception e) {

			}
		}
		timestamp = System.currentTimeMillis();
	}

	public void getResponse(final String s) {
		if (System.currentTimeMillis() - timestamp > waittime) {
			new Thread(new Runnable() {
				public void run() {
					try {
						responses.add(session.think(s));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}).start();
		}
	}
}
