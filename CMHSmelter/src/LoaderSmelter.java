import java.awt.Graphics;
import java.awt.event.MouseEvent;

import org.osbot.script.Script;
import org.osbot.script.ScriptManifest;

@ScriptManifest(name = "CMHSmelter", author = "Ericthecmh", version = 1.4D, info = "Makes bars. Currently supports location: Al Kharid. Vesion 1.8.7")
public class LoaderSmelter extends Script {
	CMHSmelter smelter = new CMHSmelter(this);

	public void onStart() {
		smelter.onStart();
	}

	public int onLoop() {
		return smelter.onLoop();
	}

	public void onExit() {
		smelter.onExit();
	}

	public void onPaint(Graphics g) {
		smelter.onPaint(g);
	}

	public void mouseClicked(MouseEvent e) {
		smelter.mouseClicked(e);
	}
}
