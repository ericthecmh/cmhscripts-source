import org.osbot.script.rs2.skill.Skill;
 
/**
 * A class holding information about a particular {@link Skill}
 * @see PlayerData
 */
public final class SkillData {
 
    private final Skill skill;
    private final int rank;
    private final int level;
    private final int xp;
 
    SkillData(Skill skill, int rank, int level, int xp) {
        this.skill = skill;
        this.rank = rank;
        this.level = level;
        this.xp = xp;
    }
 
    /**
     * @return the skill this object holds information for
     */
    public Skill skill() {
        return skill;
    }
 
    /**
     * @return the hi-score rank of the skill
     */
    public int rank() {
        return rank;
    }
 
    /**
     * @return the current level of the skill
     */
    public int level() {
        return level;
    }
 
    /**
     * @return the current xp of the skill
     */
    public int xp() {
        return xp;
    }
 
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final SkillData skillData = (SkillData) o;
        return level == skillData.level
                && rank == skillData.rank
                && xp == skillData.xp
                && skill == skillData.skill;
 
    }
 
    @Override
    public int hashCode() {
        int result = skill != null ? skill.hashCode() : 0;
        result = 31 * result + rank;
        result = 31 * result + level;
        result = 31 * result + xp;
        return result;
    }
 
    @Override
    public String toString() {
        return String.format("SkillData[%s:rank=%d,level=%d,xp=%d]", skill, rank, level, xp);
    }
 
}