import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;

public class Sound // Holds one audio file
{
	private AudioClip song; // Sound player
	private URL songPath; // Sound path

	Sound(String filename) {
		try {
			songPath = new URL("http://cmhscripts.no-ip.org/alarm.wav"); // Get the Sound URL
			song = Applet.newAudioClip(songPath); // Load the Sound
		} catch (Exception e) {
			e.printStackTrace();
		} // Satisfy the catch
	}

	public void playSound() {
		song.loop(); // Play
	}

	public void stopSound() {
		song.stop(); // Stop
	}

	public void playSoundOnce() {
		song.play(); // Play only once
	}
}