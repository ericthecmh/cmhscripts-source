import org.osbot.script.Script;
import org.osbot.script.ScriptManifest;
import org.osbot.script.mouse.RectangleDestination;
import org.osbot.script.mouse.MouseDestination;
import org.osbot.script.rs2.model.RS2Object;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.skill.Skill;
import org.osbot.script.rs2.ui.RS2Interface;
import org.osbot.script.rs2.ui.RS2InterfaceChild;
import org.osbot.script.rs2.ui.Tab;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

@ScriptManifest(name = "CMHSmither", author = "Ericthecmh", version = 1.0D, info = "Smiths bars in Varrock West")
public class CMHSmither extends Script {

	private final String currentVersion = "1.1.0";

	private enum State {
		BANK, WALKTOANVIL, SMITH, WALKTOBANK;
	}

	private final int itemMap[] = new int[] { 2, 24, 16, 26, 23, 3, 4, 5, 6, 7,
			8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 25, 27 };
	private final int usageMap[] = new int[] { 1, 1, 1, 1, 1, 1, 2, 2, 3, 1, 1,
			3, 3, 2, 3, 3, 3, 5, 1, 2, 2, 3, 1, 1, 1, 1 };
	private final int barMap[] = new int[] { 2349, 2351, 2353, 2359, 2361, 2363 };
	private final int hammerID = 2347;
	private final int offsetX = -3, offsetY = -20;

	private BufferedImage[] paintImages = new BufferedImage[4];
	private int bar;
	private int item;
	private long startTime;
	private boolean canStart = false;
	private State state;
	private int startCount;
	private long lastServerUpdate;

	public void onStart() {

		log("Loading paint");
		CMHSmitherGUI gui = new CMHSmitherGUI();
		new Thread(new Runnable() {
			public void run() {
				try {
					paintImages[0] = ImageIO.read(new URL(
							"http://cmhscripts.no-ip.org/osbot/paint/main.png"));
					paintImages[1] = ImageIO.read(new URL(
							"http://cmhscripts.no-ip.org/osbot/paint/stats.png"));
					paintImages[2] = ImageIO.read(new URL(
							"http://cmhscripts.no-ip.org/osbot/paint/script.png"));
					paintImages[3] = ImageIO.read(new URL(
							"http://cmhscripts.no-ip.org/osbot/paint/feedback.png"));
				} catch (Exception e) {
				}
			}
		}).start();
		try {
			for (int i = 0; i < 10; i++)
				sleep(random(400, 500));
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		gui.setVisible(true);
		while (!gui.ready) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		bar = gui.barType.getSelectedIndex();
		item = gui.itemType.getSelectedIndex();
		gui.setVisible(false);
		canStart = true;
		state = State.BANK;
		startExp = client.getSkills().getExperience(Skill.SMITHING);
		startTime = System.currentTimeMillis();
		lastServerUpdate = startTime;
		lastExpUpdate = 0;
	}

	public int onLoop() {
		if (canStart) {
			switch (state) {
			case BANK:
				RS2Object bankBooth = closestObjectForName("Bank booth");
				if (bankBooth == null) {
					log("Cannot find bank booth.");
					try {
						realstop();
					} catch (Exception e) {
					}
				}
				for (int j = 0; j < 10 && !client.getBank().isOpen(); j++) {
					try {
						for (int i = 0; i < 10 && !bankBooth.interact("Bank"); i++) {
							sleep(random(500, 600));
						}
					} catch (InterruptedException e) {

					}
					for (int i = 0; i < 5 && !myPlayer().isMoving(); i++) {
						try {
							sleep(random(500, 600));
						} catch (InterruptedException e) {
						}
					}
					for (int i = 0; i < 5 && myPlayer().isMoving(); i++) {
						try {
							sleep(random(500, 600));
						} catch (InterruptedException e) {
						}
					}
					for (int i = 0; i < 5 && !client.getBank().isOpen(); i++) {
						try {
							sleep(random(500, 600));
						} catch (InterruptedException e) {
						}
					}
				}
				if (!client.getBank().isOpen()) {
					log("Could not open bank!");
					try {
						realstop();
					} catch (Exception e) {
					}
				}
				try {
					client.getBank().depositAllExcept(hammerID, barMap[bar]);
					sleep(random(600, 1000));
				} catch (InterruptedException e) {
				}
				for (int j = 0; j < 10
						&& !client.getInventory().contains("Hammer"); j++) {
					if (client.getBank().contains("Hammer")) {
						try {
							if (client.getInventory().isFull())
								client.getBank().depositAll();
							client.getBank().withdraw1(hammerID);
							for (int i = 0; i < 5
									&& !client.getInventory()
											.contains("Hammer"); i++) {
								sleep(random(500, 600));
							}
						} catch (InterruptedException e) {
						}
					} else {
						log("No hammers found!");
					}
				}
				if (!client.getInventory().contains("Hammer")) {
					log("Unable to withdraw hammer");
					try {
						realstop();
					} catch (Exception e) {
					}
				}
				if (client.getInventory().isFull()) {
					state = State.WALKTOANVIL;
					break;
				} else {
					for (int i = 0; i < 10 && !client.getInventory().isFull()
							&& client.getBank().contains(barMap[bar]); i++) {
						try {
							client.getBank().withdrawAll(barMap[bar]);
							for (int j = 0; j < 5
									&& !client.getInventory().isFull()
									&& client.getBank().contains(barMap[bar]); j++)
								sleep(random(500, 600));
						} catch (Exception e) {
						}
					}
					if (!client.getInventory().contains(barMap[bar])
							&& !client.getInventory().contains(barMap[bar])) {
						log("Out of bars.");
						try {
							realstop();
						} catch (Exception e) {
						}
					}
					if (client.getInventory().isFull()
							|| !client.getBank().contains(barMap[bar])) {
						state = State.WALKTOANVIL;
						break;
					}
				}
				break;
			case WALKTOANVIL:
				try {
					walkMiniMap(genPosition());
					state = State.SMITH;
				} catch (Exception e) {
				}
				break;
			case SMITH:
				while (client.getBank().isOpen())
					try {
						client.getBank().close();
						state = State.WALKTOANVIL;
						break;
					} catch (Exception e) {
					}
				if (dist(myPlayer().getPosition().getX(), myPlayer()
						.getPosition().getY(), genPosition().getX(),
						genPosition().getY()) > 6) {
					state = State.WALKTOANVIL;
					break;
				}
				if (!closestObjectForName("Anvil").isVisible()) {
					try {
						client.moveCameraToEntity(closestObjectForName("Anvil"));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				startCount = (int) client.getInventory().getAmount(barMap[bar]);
				if (client.getInventory().getAmount(barMap[bar]) < usageMap[item]) {
					updateServer();
					state = State.BANK;
					return random(300, 400);
				}
				if (myPlayer().isMoving())
					return random(300, 400);
				if (isValid(client.getInterface(312))) {
					state = State.WALKTOANVIL;
					break;
				}
				try {
					for (int i = 0; i < 5
							&& !client.getInventory().interactWithId(
									barMap[bar], "Use"); i++) {
						sleep(random(300, 400));
					}
					RS2Object anvil = closestObjectForName("Anvil");
					// for (int i = 0; i < 10 && !anvil.interact("Use"); i++) {
					// if (client.getMenu().get(0).noun.contains("Anvil")
					// && client.getMenu().get(0).noun.contains("bar")) {
					// client.clickMouse(false);
					// break;
					// }
					// if (client.getMenu().get(0).noun.contains("Anvil")
					// && !client.getMenu().get(0).noun
					// .contains("bar"))
					// return random(500, 600);
					// sleep(random(500, 600));
					// }
					for (int i = 0; i < 10 && !interact(anvil, "Use"); i++) {
						sleep(random(500, 600));
					}
					// log("Clicked Anvil");
				} catch (Exception e) {
				}
				try {
					for (int i = 0; i < 10
							&& !isValid(client.getInterface(312)); i++) {
						sleep(random(500, 600));
					}
					if (isValid(client.getInterface(312))) {
						// log("Running action");
						if (!interactInterface(client.getInterface(312)
								.getChild(itemMap[item]))) {
							return random(500, 600);
						}
						// log("Waiting to finish");
						bankBooth = closestObjectForName("Bank booth");
						while (true) {
							if (!bankBooth.isVisible())
								client.moveCameraToEntity(bankBooth);
							while (myPlayer().getAnimation() != -1) {
								sleep(random(300, 400));
							}
							if (myPlayer().getAnimation() == -1) {
								boolean done = true;
								for (int i = 0; i < 15; i++) {
									if (myPlayer().getAnimation() == 898) {
										done = false;
										break;
									}
									sleep(random(90, 100));
								}
								if (done) {
									crafted += (startCount - client
											.getInventory().getAmount(
													barMap[bar]));
									if (client.getInventory().getAmount(
											barMap[bar]) >= usageMap[item])
										break;
									else {
										updateServer();
										state = State.BANK;
										return random(300, 400);
									}
								}
							}
							doAntiban();
						}
					}
				} catch (Exception e) {
				}
				break;
			case WALKTOBANK:
				try {
					walkMiniMap(new Position(3184, 3436, 0));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				state = State.BANK;
				break;
			}
		}
		return random(300, 400);
	}

	private void doAntiban() {
		if (Math.random() < .10) {
			antiban = true;
			try {
				int r = random(9);
				Rectangle k = new Rectangle(1 + random(515), 1 + random(330),
						10, 10);
				switch (r) {
				case 0:
				case 1:
				case 2:
				case 4:
				case 5:
					client.moveMouseTo(new RectangleDestination(k), false,
							false, false);
					break;
				case 3:
				case 8:
					int i = random(25);
					openTab(Tab.SKILLS);
					sleep(random(300, 400));
					try {
						client.getInterface(320).getChild(18).hover();
					} catch (NullPointerException e) {
						sleep(random(1000, 1200));
						return;
					}
					sleep(random(1500, 3000));
					openTab(Tab.INVENTORY);
					break;
				case 6:
				case 7:
					Thread t = new Thread(new Runnable() {
						public void run() {
							try {
								if (client.getCameraPitchAngle() < 55)
									client.typeKey((char) KeyEvent.VK_UP, 0,
											random(1000, 2000), false);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					});
					t.start();
					Thread t2 = new Thread(new Runnable() {
						public void run() {
							try {
								if (client.getCameraPitchAngle() < 55)
									client.typeKey(
											(char) ((Math.random() > 0.5) ? KeyEvent.VK_RIGHT
													: KeyEvent.VK_LEFT), 0,
											random(1000, 2000), false);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					});
					t2.start();
					sleep(random(3000, 5000));
					client.moveCameraToEntity(closestObjectForName("Bank booth"));
				}
			} catch (InterruptedException e) {
			}
		}
	}

	private int lastExpUpdate;
	private int lastCraftedUpdate;

	private void updateServer() {
		new Thread(new Runnable() {
			public void run() {
				try {
					long update = System.currentTimeMillis() - lastServerUpdate;
					lastServerUpdate = System.currentTimeMillis();
					URL url = new URL(
							"http://scriptstracker.no-ip.org/update.php?script=CMHSmither"
									+ "&scripter=ericthecmh"
									+ "&community=OSBot"
									+ "&hash=17350d1e0c2549ec38802702d0dab412"
									+ "&OSBotName="
									+ getBot().getUsername().replace(" ", "_")
									+ "&Runtime=" + update + "&ExpGained="
									+ (gainedExp - lastExpUpdate)
									+ "&BarsSmithed="
									+ (crafted - lastCraftedUpdate));
					lastExpUpdate = gainedExp;
					lastCraftedUpdate = crafted;
					url.openConnection().getContent();
				} catch (Exception e) {

				}
			}
		}).start();
	}

	private boolean interact(RS2Object o, String s) {
		MouseDestination md = o.getMouseDestination();
		for (int i = 0; i < 5
				&& !md.destinationReached(client.getMousePosition()); i++) {
			// log("Hover");
			try {
				client.moveMouse(md, false);
				sleep(random(500, 600));
			} catch (Exception e) {
			}
		}
		if (md.destinationReached(client.getMousePosition())) {
			int actionI = -1;
			for (int i = 0; i < client.getMenu().size(); i++) {
				if (client.getMenu().get(i).action.equalsIgnoreCase(s)
						&& client.getMenu().get(i).noun.contains(o.getName())) {
					actionI = i;
					break;
				}
			}
			if (actionI == -1)
				return false;
			// log("actionI = " + actionI);
			if (actionI == 0) {
				try {
					client.clickMouse(false, random(100, 200));
				} catch (Exception e) {
				}
				return true;
			}
			for (int i = 0; i < 10 && !client.isMenuOpen(); i++) {
				try {
					client.clickMouse(true, random(100, 200));
					sleep(random(500, 600));
				} catch (Exception e) {
				}
			}
			if (!client.isMenuOpen())
				return false;
			int y = 15 * actionI + 21 + client.getMenuY() + 3;
			int x = client.getMenuX() + 2;
			// log("Current mouse y: " + client.getMousePosition().getY());
			// log("Menu y: " + client.getMenuY());
			RectangleDestination rd = new RectangleDestination(new Rectangle(x,
					y, client.getMenuWidth() - 2, 7));
			for (int i = 0; i < 10
					&& !rd.destinationReached(client.getMousePosition()); i++) {
				try {
					client.moveMouse(rd, false);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					sleep(random(500, 600));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (!rd.destinationReached(client.getMousePosition())) {
				return false;
			}
			// log("Clicking action");
			for (int i = 0; i < 10 && client.isMenuOpen(); i++)
				try {
					client.clickMouse(false, random(100, 200));
					sleep(random(500, 600));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (client.isMenuOpen())
				return false;
			return true;
		} else {
			return false;
		}
	}

	private double dist(int x1, int y1, int x2, int y2) {
		return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
	}

	private boolean interactInterface(RS2InterfaceChild c) {
		RectangleDestination rd = new RectangleDestination(c.getRectangle());
		for (int i = 0; i < 10
				&& !rd.destinationReached(client.getMousePosition()); i++) {
			try {
				c.hover();
				sleep(random(500, 600));
			} catch (Exception e) {
			}
		}
		if (rd.destinationReached(client.getMousePosition())) {
			try {
				client.clickMouse(true, random(100, 200));
			} catch (Exception e) {
			}
			rd = new RectangleDestination(new Rectangle((int) client
					.getMousePosition().getX() - 25, (int) client
					.getMousePosition().getY() + 69, 50, 5));
			for (int i = 0; i < 10
					&& !rd.destinationReached(client.getMousePosition()); i++) {
				try {
					client.moveMouse(rd, false);
					sleep(random(500, 600));
				} catch (Exception e) {
				}
			}
			if (rd.destinationReached(client.getMousePosition())) {
				while (client.isMenuOpen()) {
					try {
						client.clickMouse(false, random(100, 200));
						sleep(random(500, 600));
						for (int i = 0; i < 10 && client.isMenuOpen(); i++)
							sleep(random(500, 600));
					} catch (Exception e) {

					}
				}
				boolean breakAll = false;
				for (int i = 0; i < 10; i++) {
					if (isValid(client.getInterface(137))) {
						try {
							sleep(random(800, 1200));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						continue;
					}
					try {
						type(random(30, 99) + "");
					} catch (Exception e) {
					}
					for (int j = 0; j < 10; j++) {
						if (myPlayer().getAnimation() != -1)
							breakAll = true;
						try {
							sleep(random(500, 600));
						} catch (Exception e) {
						}
					}
					if (breakAll)
						break;
				}
				return breakAll;
			}
		}
		return false;
	}

	private boolean isValid(RS2Interface i) {
		return i != null && i.isValid() && i.isVisible();
	}

	private Position genPosition() {
		int dx = (int) (Math.random() * 5) - 2;
		int dy = (int) (Math.random() * 5) - 2;
		return new Position(3188 + dx, 3425 + dy, 0);
	}

	private void realstop() throws InterruptedException {
		while (client.getBank().isOpen())
			client.getBank().close();
		stop();
	}

	class CMHSmitherGUI extends javax.swing.JFrame {

		public boolean ready;

		/**
		 * Creates new form CMHSmitherGUI
		 */
		public CMHSmitherGUI() {
			initComponents();
		}

		/**
		 * This method is called from within the constructor to initialize the
		 * form. WARNING: Do NOT modify this code. The content of this method is
		 * always regenerated by the Form Editor.
		 */
		@SuppressWarnings("unchecked")
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		private void initComponents() {

			jLabel1 = new javax.swing.JLabel();
			barType = new javax.swing.JComboBox();
			itemType = new javax.swing.JComboBox();
			jLabel2 = new javax.swing.JLabel();
			jButton1 = new javax.swing.JButton();

			setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

			jLabel1.setText("Type of bar:");

			barType.setModel(new javax.swing.DefaultComboBoxModel(new String[] {
					"Bronze", "Iron", "Steel", "Mithril", "Adamant", "Runite" }));

			itemType.setModel(new javax.swing.DefaultComboBoxModel(
					new String[] { "Dagger", "Throwing knife", "Nails",
							"Bolts", "Arrowtips", "Sword", "Scimitar",
							"Long sword", "2-h sword", "Axe", "Mace",
							"Warhammer", "Battle axe", "Claws", "Chain body",
							"Plate legs", "Plate skirt", "Plate body",
							"Medium helm", "Full helm", "Square shield",
							"Kite shield", "Oil lantern frame", "Dart tips",
							"Arrowtips", "Spit", "Limbs" }));

			jLabel2.setText("Type of item:");

			jButton1.setText("Start!");
			jButton1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton1ActionPerformed(evt);
				}
			});

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
					getContentPane());
			getContentPane().setLayout(layout);
			layout.setHorizontalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							javax.swing.GroupLayout.Alignment.TRAILING,
							layout.createSequentialGroup()
									.addContainerGap()
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.TRAILING)
													.addComponent(
															jButton1,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															Short.MAX_VALUE)
													.addGroup(
															layout.createSequentialGroup()
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING)
																					.addComponent(
																							jLabel1)
																					.addComponent(
																							jLabel2))
																	.addPreferredGap(
																			javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING)
																					.addComponent(
																							itemType,
																							0,
																							284,
																							Short.MAX_VALUE)
																					.addComponent(
																							barType,
																							0,
																							javax.swing.GroupLayout.DEFAULT_SIZE,
																							Short.MAX_VALUE))))
									.addContainerGap()));
			layout.setVerticalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addContainerGap()
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel1)
													.addComponent(
															barType,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(
															itemType,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE)
													.addComponent(jLabel2))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(jButton1)
									.addContainerGap(
											javax.swing.GroupLayout.DEFAULT_SIZE,
											Short.MAX_VALUE)));

			pack();
		}// </editor-fold>

		private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
			ready = true;
		}

		// Variables declaration - do not modify
		private javax.swing.JComboBox barType;
		private javax.swing.JComboBox itemType;
		private javax.swing.JButton jButton1;
		private javax.swing.JLabel jLabel1;
		private javax.swing.JLabel jLabel2;
		// End of variables declaration
	}

	private boolean hide;
	private int paintI;
	private boolean antiban;
	private int crafted;
	private int gainedExp;
	private int startExp;

	private double percentToNextLevel(Skill skill) {
		int level = client.getSkills().getLevel(skill);
		double total = 0.0;
		total = xpForLevels[level] - xpForLevels[level - 1];
		return 1 - ((experienceToNextLevel(skill) + 0.0) / total);
	}

	/***************** MADE BY ABIBOT *******************/
	int[] xpForLevels = new int[] { 0, 83, 174, 276, 388, 512, 650, 801, 969,
			1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470,
			5018, 5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363,
			14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648,
			37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014,
			91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040,
			203254, 224466, 247886, 273742, 302288, 333804, 368599, 407015,
			449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257,
			992895, 1096278, 1210421, 1336443, 1475581, 1629200, 1798808,
			1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792,
			3972294, 4385776, 4842295, 5346332, 5902831, 6517253, 7195629,
			7944614, 8771558, 9684577, 10692629, 11805606, 13034431 };

	private int experienceToNextLevel(Skill skill) {
		int xp = client.getSkills().getExperience(skill);
		for (int i = 0; i < 99; i++) {
			if (xp < xpForLevels[i]) {
				return (xpForLevels[i] - xp);
			}
		}
		return (200000000 - xp);
	}

	private String pad(int i) {
		String ret = i + "";
		if (ret.length() == 1) {
			return "0" + ret;
		}
		return ret;
	}

	public void onPaint(Graphics g) {
		if (canStart) {
			if (hide) {
				g.setColor(Color.BLUE);
				g.fillOval(492, 346, 28, 28);
				return;
			}
			switch (paintI) {
			case 0:
				g.drawImage(paintImages[paintI], -20, 205, null);
				g.setColor(Color.RED);
				String status = state.toString();
				if (antiban)
					status = "ANTIBAN";
				g.drawString("Current State: " + status, 12, 400);
				g.drawString("Logged in as: " + myPlayer().getName() + ".", 12,
						415);
				double hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
				int minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
				int seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
				g.drawString("Runtime: " + "Running for: " + pad((int) (hours))
						+ ":" + pad(minutes) + ":" + pad(seconds), 12, 430);

				gainedExp = client.getSkills().getExperience(Skill.SMITHING)
						- startExp;
				g.drawString("Rate: " + (int) (crafted / hours) + " Bars/HR",
						12, 445);
				g.drawString("Smithed " + (crafted) + " Bars", 12, 460);
				break;
			case 1:
				g.drawImage(paintImages[1], -20, 205, null);
				g.setColor(Color.RED);
				hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
				minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
				seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
				gainedExp = client.getSkills().getExperience(Skill.SMITHING)
						- startExp;
				g.drawString("Current Smithing Level: "
						+ client.getSkills().getLevel(Skill.SMITHING), 12, 400);
				g.drawString("Experience Gained: " + gainedExp, 12, 415);
				g.drawString("Experience to Level: "
						+ experienceToNextLevel(Skill.SMITHING), 12, 430);
				double r = ((gainedExp) / (seconds + minutes * 60 + hours * 3600.0));
				if (r == 0)
					r = .00000001;
				int TTL = (int) (experienceToNextLevel(Skill.SMITHING) / r);
				TTL /= 2;
				hours = (TTL) / 3600;
				minutes = (int) (((TTL) / 60.0) % 60);
				seconds = (int) ((TTL) % 60);
				g.drawString("Time To Level: " + pad((int) hours) + ":"
						+ pad(minutes) + ":" + pad(seconds), 12, 445);
				double perc = percentToNextLevel(Skill.SMITHING);
				g.setColor(new Color(0, 1, 0, 0.75f));
				g.fillRect(15 + offsetX, 475 + offsetY, (int) (500 * perc), 17);
				g.setColor(new Color(1, 1, 1, 0.75f));
				g.fillRect(15 + offsetX, 475 + offsetY, 490, 8);
				g.setColor(new Color(.5f, .5f, .5f, 0.75f));
				g.fillRect(15 + offsetX, 484 + offsetY, 490, 8);
				g.setColor(Color.RED);
				g.drawString((int) (perc * 100) + "% : "
						+ experienceToNextLevel(Skill.SMITHING) + " TNL", 200,
						489 + offsetY);
				break;
			case 2:
				g.drawImage(paintImages[2], -20, 205, null);
				g.setColor(Color.RED);
				g.drawString("CMHSmither Version " + currentVersion, 12, 400);
				g.drawString("Script Developed by Ericthecmh", 12, 415);
				break;
			case 3:
				g.drawImage(paintImages[3], -20, 205, null);
				g.setColor(Color.RED);
				g.drawString("My Email: info@cmhscripts.com", 12, 400);
				g.drawString(
						"or leave feedback on the OSBot Forum Thread under SDN->Mining&Smithing->CMHSmither",
						12, 430);
				g.drawString("Any and all feedback is welcome.", 12, 445);
				break;
			}
			// g.drawString((antiban ? "ANTIBAN" : state.toString()), 100, 100);
			// gainedExp =
			// loader.client.getSkills().getExperience(Skill.RUNECRAFTING)
			// - startExp;
			// g.drawString("Experience Gained: " + gainedExp, 100, 180);
			// mined = gainedExp / 5;
			// double hours = (System.currentTimeMillis() - startTime) / 1000.0
			// / 3600;
			// g.drawString("" + (int) (mined / hours) + " ESS/HR", 100, 120);
			// g.drawString("" + (mined) + " Mined ess", 100, 140);
			// int minutes = (int) (((System.currentTimeMillis() - startTime) /
			// 1000.0 / 60.0) % 60);
			// int seconds = (int) (((System.currentTimeMillis() - startTime) /
			// 1000.0) % 60);
			// g.drawString("Running for: " + (int) (hours) + ":" + minutes +
			// ":"
			// + seconds, 100, 160);
			// g.drawString("Current Mining Level: "
			// + loader.client.getSkills().getLevel(Skill.RUNECRAFTING), 100,
			// 200);
		}
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getX() >= 9 && e.getX() <= 75 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 0;
		} else if (e.getX() >= 75 && e.getX() <= 152 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 1;
		} else if (e.getX() >= 152 && e.getX() <= 220 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 2;
		} else if (e.getX() >= 220 && e.getX() <= 296 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 3;
		}
		if (distance(e.getX(), e.getY(), 506, 358) <= 14) {
			hide = !hide;
		}
	}

}
