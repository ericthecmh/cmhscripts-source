import java.awt.Graphics;
import java.awt.event.MouseEvent;

import org.osbot.script.Script;
import org.osbot.script.ScriptManifest;
import org.osbot.script.rs2.model.RS2Object;

@ScriptManifest(name = "CMHPouches", author = "Ericthecmh", version = 1.0D, info = "Gets pouches in the Abyss")
public class LoaderPouches extends Script {

	CMHPouches pouches = new CMHPouches(this);

	public void onStart() {
		pouches.onStart();
	}

	public int onLoop() {
		try {
			return pouches.onLoop();
		} catch (Exception e) {
		}
		return random(300, 400);
	}

	public void onExit() {
		pouches.onExit();
	}

}
