import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

// UAU
import org.osbot.script.Script;
import org.osbot.script.ScriptManifest;
import org.osbot.script.mouse.MouseDestination;
import org.osbot.script.mouse.RectangleDestination;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.model.RS2Object;
import org.osbot.script.rs2.skill.Skill;
import org.osbot.script.rs2.ui.EquipmentSlot;
import org.osbot.script.rs2.ui.RS2Interface;
import org.osbot.script.rs2.ui.RS2InterfaceChild;
import org.osbot.script.rs2.ui.Tab;

@ScriptManifest(name = "CMHFireCrafter", author = "Ericthecmh", version = 1.0D, info = "Crafts Fire runes!")
public class CMHFireCrafter extends Script {

	private final String currentVersion = "1.3.1";

	private final int essIDs[] = new int[] { 1436, 7936 };
	private final int duelingID = 2552;
	private final int talismanID = 1442;
	private final Position duelArenaPosition = new Position(3314, 3235, 0);
	private final int closedGateID = 3197;
	private final int openGateID = 4140;
	private final Position altarPosition = new Position(3313, 3253, 0);
	private final Position cwPosition = new Position(2439, 3088, 0);
	private final Position pathToAltar[][] = new Position[][] {
			new Position[] { new Position(3306, 3240, 0),
					new Position(3307, 3249, 0), new Position(3312, 3256, 0) },
			new Position[] { new Position(3306, 3240, 0),
					new Position(3307, 3249, 0), new Position(3312, 3256, 0) } };
	private final int CONST = 1;
	private final int offsetX = -3, offsetY = -20;

	private BufferedImage[] paintImages = new BufferedImage[4];

	private boolean hide;
	private int essID;
	private boolean useTalisman;
	private boolean useTiara;
	private boolean canStart;
	private long startTime;
	private int walkI;
	private int gainedExp;
	private int startExp;
	private int paintI;
	private int crafted;
	private int pathI;
	private int toggleRun;
	private RS2Object altar;

	private State state;

	private enum State {
		BANK, TELEPORT_ALKHARID, OPEN_GATE, WALK_TO_ALTAR, ENTER_ALTAR, CRAFT, TELEPORT_CW;
	}

	public void onStart() {
		log("Loading paint");
		CMHFireCrafterGUI gui = new CMHFireCrafterGUI();
		new Thread(new Runnable() {
			public void run() {
				try {
					paintImages[0] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/main.png"));
					paintImages[1] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/stats.png"));
					paintImages[2] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/script.png"));
					paintImages[3] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/feedback.png"));
				} catch (Exception e) {
				}
			}
		}).start();
		try {
			for (int i = 0; i < 10; i++)
				sleep(random(400, 500));
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		gui.setVisible(true);
		while (!gui.ready && gui.isVisible()) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (!gui.ready)
			return;
		gui.setVisible(false);
		if (gui.regEss.isSelected())
			essID = essIDs[0];
		else if (gui.pureEss.isSelected())
			essID = essIDs[1];

		if (gui.talisman.isSelected())
			useTalisman = true;
		else if (gui.tiara.isSelected())
			useTiara = true;

		state = State.BANK;
		if (closestObjectForName("Cactus") != null)
			state = State.OPEN_GATE;
		if (closestObjectForName("Mysterious ruins") != null
				&& distance(closestObjectForName("Mysterious ruins")) < 10) {
			state = State.ENTER_ALTAR;
		}
		canStart = true;
		startExp = client.getSkills().getExperience(Skill.RUNECRAFTING);
		toggleRun = random(30, 50);
		startTime = System.currentTimeMillis();
		lastServerUpdate = startTime;
	}

	public int onLoop() {
		if (client.getRunEnergy() > toggleRun && !isRunning()) {
			setRunning2(true);
			toggleRun = random(30, 50);
		}
		if (canStart) {
			switch (state) {
			case BANK:
				if (getRingSlot() != null
						&& (!useTiara || getTiaraSlot() != null)
						&& (!useTalisman || client.getInventory().contains(
								talismanID))
						&& client.getInventory().contains(essID)) {
					state = State.TELEPORT_ALKHARID;
					log("TELEPORT ALKHARID");
					break;
				}
				boolean booth = false;
				RS2Object bankBooth = closestObjectForName("Bank chest");
				if (bankBooth == null) {
					booth = true;
					bankBooth = closestObjectForName("Bank booth");
				}
				if (bankBooth != null) {
					try {
						if (!client.getBank().isOpen())
							bankBooth.interact(booth ? "Bank" : "Use");
						sleep(random(1000, 1100));
						new Thread(new Runnable() {
							public void run() {
								Thread t = new Thread(new Runnable() {
									public void run() {
										try {
											if (client.getCameraPitchAngle() < 55)
												client.typeKey(
														(char) KeyEvent.VK_UP,
														0, random(2600, 3400),
														false);
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
								});
								t.start();
								if (state != State.BANK) {
									t.interrupt();
									t.stop();
								}
							}
						}).start();
						if (client.getBank().isOpen()) {
							if (getRingSlot() == null) {
								if (useTalisman)
									client.getBank().depositAllExcept(
											talismanID);
								else
									client.getBank().depositAll();
								boolean gotRing = false;
								int id;
								for (id = 2552; id <= 2564; id += 2) {
									if (client.getBank().contains(id)) {
										gotRing = true;
										for (int i = 0; i < 10
												&& !client.getBank().withdraw1(
														id); i++)
											sleep(random(500, 600));
										break;
									}
								}
								if (client.getInventory().contains(id)) {
									gotRing = true;
									for (int i = 0; i < 10
											&& !client.getBank().close(); i++) {
										sleep(random(500, 600));
									}
									for (int i = 0; i < 10
											&& !client.getInventory()
													.interactWithId(id, "Wear"); i++) {
										sleep(random(500, 600));
									}
									break;
								}
							}
							if (getTiaraSlot() == null && useTiara) {
								client.getBank().depositAll();
								boolean gotTiara = false;
								int id;
								for (id = 5537; id <= 5537; id += 2) {
									if (client.getBank().contains(id)) {
										gotTiara = true;
										for (int i = 0; i < 10
												&& !client.getBank().withdraw1(
														id); i++)
											sleep(random(500, 600));
										break;
									}
								}
								if (client.getInventory().contains(id)) {
									gotTiara = true;
									for (int i = 0; i < 10
											&& !client.getBank().close(); i++) {
										sleep(random(500, 600));
									}
									for (int i = 0; i < 10
											&& !client.getInventory()
													.interactWithId(id, "Wear"); i++) {
										sleep(random(500, 600));
									}
									break;
								}
							}
							if (useTalisman
									&& !client.getInventory().contains(
											talismanID)) {
								client.getBank().depositAll();
								boolean gotTalisman = false;
								int id;
								for (id = talismanID; id <= talismanID; id += 2) {
									if (client.getBank().contains(id)) {
										gotTalisman = true;
										for (int i = 0; i < 10
												&& !client.getBank().withdraw1(
														id); i++)
											sleep(random(500, 600));
										break;
									}
								}
								if (client.getInventory().contains(id)) {
									gotTalisman = true;
								}
							}
							log("AOEU");
							if (getRingSlot() != null
									&& (!useTiara || getTiaraSlot() != null)
									&& (!useTalisman || client.getInventory()
											.contains(talismanID))) {
								if (useTalisman)
									client.getBank().depositAllExcept(
											talismanID);
								else
									client.getBank().depositAll();
								client.getBank().withdrawAll(essID);
							}
						}
					} catch (Exception e) {

					}
				}
				break;
			case TELEPORT_ALKHARID:
				while (client.getBank().isOpen()) {
					try {
						client.getBank().close();
						sleep(random(1000, 1100));
					} catch (InterruptedException e) {

					}
				}
				if (Math.random() < 0.05) {
					new Thread(new Runnable() {
						public void run() {
							Thread t = new Thread(new Runnable() {
								public void run() {
									try {
										if (client.getCameraPitchAngle() < 55)
											client.typeKey(
													(char) ((Math.random() < 0.5) ? KeyEvent.VK_LEFT
															: KeyEvent.VK_RIGHT),
													0, random(600, 2000), false);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							});
							t.start();
							if (state != State.TELEPORT_ALKHARID) {
								t.interrupt();
								t.stop();
							}
						}
					}).start();
				}
				try {
					for (int i = 0; i < 10 && !equipmentTab.open(); i++) {
						sleep(random(500, 600));
					}
				} catch (Exception e) {
				}
				if (isValid(client.getInterface(387))) {
					try {
						for (int i = 0; i < 10
								&& !client.getInterface(387).getChild(22)
										.interact("Operate"); i++)
							sleep(random(500, 600));
						for (int i = 0; i < 5
								&& !isValid(client.getInterface(230)); i++) {
							sleep(random(500, 600));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (isValid(client.getInterface(230))) {
						try {
							for (int i = 0; i < 10
									&& !client.getInterface(230).getChild(1)
											.interact(); i++) {
								sleep(random(500, 600));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						for (int i = 0; i < 15
								&& distance(duelArenaPosition) > 20; i++) {
							try {
								sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						if (distance(duelArenaPosition) < 20) {
							state = State.OPEN_GATE;
							try {
								sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
				break;
			case OPEN_GATE:
				RS2Object closedGate = closestObject(closedGateID);
				RS2Object openGate = closestObject(openGateID);
				if (closedGate != null && openGate == null) {
					try {
						closedGate.interact("Open");
						sleep(random(1000, 1100));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (closedGate == null && openGate != null) {
					state = State.WALK_TO_ALTAR;
					pathI = random(0, 1);
				}
				break;
			case WALK_TO_ALTAR:
				closedGate = closestObject(closedGateID);
				openGate = closestObject(openGateID);
				if (closedGate != null
						&& openGate == null
						&& !this.canReach(pathToAltar[pathI][pathToAltar[pathI].length - 1])) {
					try {
						closedGate.interact("Open");
						sleep(random(1000, 1100));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (closedGate == null && openGate != null) {
				}
				if (walkI == 0) {
					try {
						walkMiniMap(randomize(pathToAltar[pathI][walkI++]));
						break;
					} catch (InterruptedException e) {
					}
				}
				if (walkI == pathToAltar.length) {
					walkI = 0;
					state = State.ENTER_ALTAR;
					break;
				}
				try {
					if (realDistance(pathToAltar[pathI][walkI - 1]) <= ((client
							.getConfig(173) == 1) ? 6 : 5)) {
						walkMiniMap(randomize(pathToAltar[pathI][walkI++]));
					} else
						walkMiniMap(randomize(pathToAltar[pathI][walkI - 1]));
				} catch (InterruptedException e) {

				}
				break;
			case ENTER_ALTAR:
				altar = closestObjectForName("Mysterious ruins");
				if (altar == null)
					break;
				new Thread(new Runnable() {
					public void run() {
						try {
							client.moveCameraToEntity(altar);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}).start();
				Position oldPosition = myPlayer().getPosition();
				if (useTiara) {
					try {
						for (int i = 0; i < 10 && !altar.interact("Enter"); i++) {
							sleep(random(500, 600));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (useTalisman) {
					try {
						for (int i = 0; i < 5
								&& !client.getInventory().interactWithId(
										talismanID, "Use"); i++) {
							sleep(random(300, 400));
						}
						for (int i = 0; i < 10 && !interact(altar, "Use"); i++) {
							try {
								sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				for (int i = 0; i < 20 && distance(oldPosition) < 20; i++) {
					try {
						sleep(random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (distance(oldPosition) > 20)
					state = State.CRAFT;
				break;
			case CRAFT:
				if (closestObjectForName("Mysterious ruins") != null) {
					try {
						sleep(random(1000, 1100));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (closestObjectForName("Mysterious ruins") != null) {
						state = State.ENTER_ALTAR;
					}
				}
				altar = closestObjectForName("Altar");
				if (altar != null) {
					try {
						for (int i = 0; i < 10 && !altar.interact("Craft-rune"); i++) {
							sleep(random(500, 600));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (Math.random() < 0.1) {
						new Thread(new Runnable() {
							public void run() {
								Thread t = new Thread(new Runnable() {
									public void run() {
										try {
											if (client.getCameraPitchAngle() < 55)
												client.typeKey(
														(char) ((Math.random() < 0.5) ? KeyEvent.VK_LEFT
																: KeyEvent.VK_RIGHT),
														0, random(600, 2000),
														false);
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
								});
								t.start();
								if (state != State.CRAFT) {
									t.interrupt();
									t.stop();
								}
							}
						}).start();
					}
					for (int i = 0; i < 10
							&& client.getInventory().contains(essID); i++) {
						try {
							sleep(random(500, 600));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (!client.getInventory().contains(essID)) {
						try {
							sleep(random(800, 1200));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						crafted += client.getInventory().getAmount("Fire rune");
						state = State.TELEPORT_CW;
					}
				}
				break;
			case TELEPORT_CW:
				if (Math.random() < 0.05) {
					new Thread(new Runnable() {
						public void run() {
							Thread t = new Thread(new Runnable() {
								public void run() {
									try {
										if (client.getCameraPitchAngle() < 55)
											client.typeKey(
													(char) ((Math.random() < 0.5) ? KeyEvent.VK_LEFT
															: KeyEvent.VK_RIGHT),
													0, random(600, 2000), false);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							});
							t.start();
							if (state != State.TELEPORT_CW) {
								t.interrupt();
								t.stop();
							}
						}
					}).start();
				}
				while (client.getBank().isOpen()) {
					try {
						client.getBank().close();
						sleep(random(1000, 1100));
					} catch (InterruptedException e) {

					}
				}
				try {
					for (int i = 0; i < 10 && !equipmentTab.open(); i++) {
						sleep(random(500, 600));
					}
				} catch (Exception e) {
				}
				if (isValid(client.getInterface(387))) {
					try {
						for (int i = 0; i < 10
								&& !client.getInterface(387).getChild(22)
										.interact("Operate"); i++)
							sleep(random(500, 600));
						for (int i = 0; i < 5
								&& !isValid(client.getInterface(230)); i++) {
							sleep(random(500, 600));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (isValid(client.getInterface(230))) {
						try {
							for (int i = 0; i < 10
									&& !client.getInterface(230).getChild(2)
											.interact(); i++) {
								sleep(random(500, 600));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						for (int i = 0; i < 15 && distance(cwPosition) > 20; i++) {
							try {
								sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						if (distance(cwPosition) < 20) {
							state = State.BANK;
							try {
								sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
				break;
			}
		}
		return random(300, 400);
	}

	private double percentToNextLevel(Skill skill) {
		int level = client.getSkills().getLevel(skill);
		double total = 0.0;
		total = xpForLevels[level] - xpForLevels[level - 1];
		return 1 - ((experienceToNextLevel(skill) + 0.0) / total);
	}

	/***************** MADE BY ABIBOT *******************/
	int[] xpForLevels = new int[] { 0, 83, 174, 276, 388, 512, 650, 801, 969,
			1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470,
			5018, 5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363,
			14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648,
			37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014,
			91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040,
			203254, 224466, 247886, 273742, 302288, 333804, 368599, 407015,
			449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257,
			992895, 1096278, 1210421, 1336443, 1475581, 1629200, 1798808,
			1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792,
			3972294, 4385776, 4842295, 5346332, 5902831, 6517253, 7195629,
			7944614, 8771558, 9684577, 10692629, 11805606, 13034431 };

	private int experienceToNextLevel(Skill skill) {
		int xp = client.getSkills().getExperience(skill);
		for (int i = 0; i < 99; i++) {
			if (xp < xpForLevels[i]) {
				return (xpForLevels[i] - xp);
			}
		}
		return (200000000 - xp);
	}

	/***************** END MADE BY ABIBOT *******************/

	private Position randomize(Position p) {
		return new Position(p.getX() + random(1, 2 * CONST + 1) - (CONST + 1),
				p.getY() + random(1, 2 * CONST + 1) - (CONST + 1), 0);
	}

	private boolean interact(RS2Object o, String s) {
		MouseDestination md = o.getMouseDestination();
		for (int i = 0; i < 5
				&& !md.destinationReached(client.getMousePosition()); i++) {
			// log("Hover");
			try {
				client.moveMouse(md, false);
				sleep(random(500, 600));
			} catch (Exception e) {
			}
		}
		if (md.destinationReached(client.getMousePosition())) {
			int actionI = -1;
			for (int i = 0; i < client.getMenu().size(); i++) {
				if (client.getMenu().get(i).action.equalsIgnoreCase(s)
						&& client.getMenu().get(i).noun.contains(o.getName())) {
					actionI = i;
					break;
				}
			}
			if (actionI == -1)
				return false;
			// log("actionI = " + actionI);
			if (actionI == 0) {
				try {
					client.clickMouse(false, random(100, 200));
				} catch (Exception e) {
				}
				return true;
			}
			for (int i = 0; i < 10 && !client.isMenuOpen(); i++) {
				try {
					client.clickMouse(true, random(100, 200));
					sleep(random(500, 600));
				} catch (Exception e) {
				}
			}
			if (!client.isMenuOpen())
				return false;
			int y = 15 * actionI + 21 + client.getMenuY() + 3;
			int x = client.getMenuX() + 2;
			// log("Current mouse y: " + client.getMousePosition().getY());
			// log("Menu y: " + client.getMenuY());
			RectangleDestination rd = new RectangleDestination(new Rectangle(x,
					y, client.getMenuWidth() - 2, 7));
			for (int i = 0; i < 10
					&& !rd.destinationReached(client.getMousePosition()); i++) {
				try {
					client.moveMouse(rd, false);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					sleep(random(500, 600));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (!rd.destinationReached(client.getMousePosition())) {
				return false;
			}
			// log("Clicking action");
			for (int i = 0; i < 10 && client.isMenuOpen(); i++)
				try {
					client.clickMouse(false, random(100, 200));
					sleep(random(500, 600));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (client.isMenuOpen())
				return false;
			return true;
		} else {
			return false;
		}
	}

	private long lastServerUpdate;
	private int lastExpUpdate;
	private int lastCraftedUpdate;

	private void updateServer() {
		new Thread(new Runnable() {
			public void run() {
				try {
					long update = System.currentTimeMillis() - lastServerUpdate;
					lastServerUpdate = System.currentTimeMillis();
					URL url = new URL(
							"http://scriptstracker.no-ip.org/update.php?script=CMHSmither"
									+ "&scripter=ericthecmh"
									+ "&community=OSBot"
									+ "&hash=17350d1e0c2549ec38802702d0dab412"
									+ "&OSBotName="
									+ getBot().getUsername().replace(" ", "_")
									+ "&Runtime=" + update + "&ExpGained="
									+ (gainedExp - lastExpUpdate)
									+ "&BarsSmithed="
									+ (crafted - lastCraftedUpdate));
					lastExpUpdate = gainedExp;
					lastCraftedUpdate = crafted;
					url.openConnection().getContent();
				} catch (Exception e) {

				}
			}
		}).start();
	}

	private boolean isValid(RS2Interface i) {
		return i != null && i.isValid() && i.isVisible();
	}

	private EquipmentSlot getTiaraSlot() {
		return equipmentTab.getSlotForNameThatContains("Fire tiara");
	}

	private EquipmentSlot getRingSlot() {
		EquipmentSlot ring = null;
		for (int i = 2; i <= 8; i++) {
			ring = equipmentTab.getSlotForNameThatContains("Ring of dueling("
					+ i + ")");
			if (ring != null)
				break;
		}
		return ring;
	}

	private void setRunning2(boolean on) {
		try {
			for (int i = 0; i < 10 && !openTab(Tab.SETTINGS); i++) {
				sleep(random(500, 600));
			}

			RS2Interface inte = client.getInterface(261);
			if (inte != null) {
				RS2InterfaceChild child = inte.getChild(53);
				if (child != null) {
					child.hover();
					sleep(random(1000, 1100));
					child.interact();
					for (int i = 0; i < 3 && !isRunning(); i++)
						sleep(random(350, 500));
				}
			}
		} catch (Exception e) {

		}
		return;
	}

	public void onPaint(Graphics g) {
		if (canStart) {
			if (hide) {
				g.setColor(Color.BLUE);
				g.fillOval(492, 346, 28, 28);
				return;
			}
			switch (paintI) {
			case 0:
				g.drawImage(paintImages[paintI], -20, 205, null);
				g.setColor(Color.RED);
				String status = state.toString();
				g.drawString("Current State: " + status, 12, 400);
				g.drawString("Logged in as: " + myPlayer().getName() + ".", 12,
						415);
				double hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
				int minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
				int seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
				g.drawString("Runtime: " + "Running for: " + pad((int) (hours))
						+ ":" + pad(minutes) + ":" + pad(seconds), 12, 430);

				gainedExp = client.getSkills()
						.getExperience(Skill.RUNECRAFTING) - startExp;
				g.drawString("Rate: " + (int) (crafted / hours)
						+ " Runes/HR || " + (int) (gainedExp / hours)
						+ " EXP/HR", 12, 445);
				g.drawString("Crafted " + (crafted) + " Runes", 12, 460);
				break;
			case 1:
				g.drawImage(paintImages[1], -20, 205, null);
				g.setColor(Color.RED);
				hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
				minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
				seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
				gainedExp = client.getSkills()
						.getExperience(Skill.RUNECRAFTING) - startExp;
				g.drawString("Current Runecrafting Level: "
						+ client.getSkills().getLevel(Skill.RUNECRAFTING), 12,
						400);
				g.drawString("Experience Gained: " + gainedExp, 12, 415);
				g.drawString("Experience to Level: "
						+ experienceToNextLevel(Skill.RUNECRAFTING), 12, 430);
				double r = ((gainedExp) / (seconds + minutes * 60 + hours * 3600.0));
				if (r == 0)
					r = .00000001;
				int TTL = (int) (experienceToNextLevel(Skill.RUNECRAFTING) / r);
				TTL /= 2;
				hours = (TTL) / 3600;
				minutes = (int) (((TTL) / 60.0) % 60);
				seconds = (int) ((TTL) % 60);
				g.drawString("Time To Level: " + pad((int) hours) + ":"
						+ pad(minutes) + ":" + pad(seconds), 12, 445);
				double perc = percentToNextLevel(Skill.RUNECRAFTING);
				g.setColor(new Color(0, 1, 0, 0.75f));
				g.fillRect(15 + offsetX, 475 + offsetY, (int) (500 * perc), 17);
				g.setColor(new Color(1, 1, 1, 0.75f));
				g.fillRect(15 + offsetX, 475 + offsetY, 490, 8);
				g.setColor(new Color(.5f, .5f, .5f, 0.75f));
				g.fillRect(15 + offsetX, 484 + offsetY, 490, 8);
				g.setColor(Color.RED);
				g.drawString((int) (perc * 100) + "% : "
						+ experienceToNextLevel(Skill.RUNECRAFTING) + " TNL",
						200, 489 + offsetY);
				break;
			case 2:
				g.drawImage(paintImages[2], -20, 205, null);
				g.setColor(Color.RED);
				g.drawString("CMHFireCrafter Version " + currentVersion, 12,
						400);
				g.drawString("Script Developed by Ericthecmh", 12, 415);
				break;
			case 3:
				g.drawImage(paintImages[3], -20, 205, null);
				g.setColor(Color.RED);
				g.drawString("My Email: info@cmhscripts.com", 12, 400);
				g.drawString(
						"or leave feedback on the OSBot Forum Thread under SDN->Runecrafting->CMHFireCrafter",
						12, 430);
				g.drawString("Any and all feedback is welcome.", 12, 445);
				break;
			}
			// g.drawString((antiban ? "ANTIBAN" : state.toString()), 100, 100);
			// gainedExp =
			// loader.client.getSkills().getExperience(Skill.RUNECRAFTING)
			// - startExp;
			// g.drawString("Experience Gained: " + gainedExp, 100, 180);
			// mined = gainedExp / 5;
			// double hours = (System.currentTimeMillis() - startTime) / 1000.0
			// / 3600;
			// g.drawString("" + (int) (mined / hours) + " ESS/HR", 100, 120);
			// g.drawString("" + (mined) + " Mined ess", 100, 140);
			// int minutes = (int) (((System.currentTimeMillis() - startTime) /
			// 1000.0 / 60.0) % 60);
			// int seconds = (int) (((System.currentTimeMillis() - startTime) /
			// 1000.0) % 60);
			// g.drawString("Running for: " + (int) (hours) + ":" + minutes +
			// ":"
			// + seconds, 100, 160);
			// g.drawString("Current Mining Level: "
			// + loader.client.getSkills().getLevel(Skill.RUNECRAFTING), 100,
			// 200);
		}
	}

	private String pad(int i) {
		String ret = i + "";
		if (ret.length() == 1) {
			return "0" + ret;
		}
		return ret;
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getX() >= 9 && e.getX() <= 75 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 0;
		} else if (e.getX() >= 75 && e.getX() <= 152 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 1;
		} else if (e.getX() >= 152 && e.getX() <= 220 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 2;
		} else if (e.getX() >= 220 && e.getX() <= 296 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 3;
		}
		if (distance(e.getX(), e.getY(), 506, 358) <= 14) {
			hide = !hide;
		}
	}

}
