import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;

import org.osbot.script.rs2.skill.Skill;

public class Main {

	static PlayerData pdata = null;

	public static void main(String[] args) {
		getBlacklist();
		while (map == null) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (map != null)
			for (int key : map.keySet()) {
				System.out.println(key + ":" + map.get(key));
			}
	}

	static HashMap<Integer, Integer> tempMap;
	static HashMap<Integer, Integer> map;

	private static void getBlacklist() {
		new Thread(new Runnable() {
			public void run() {
				try {
					tempMap = new HashMap<Integer, Integer>();
					URL url = new URL(
							"http://ericthecmh.com/osbot/getBlacklist.php");
					BufferedReader in = new BufferedReader(
							new InputStreamReader(url.openStream()));
					String inputLine;
					long lastSuccess = System.currentTimeMillis();
					while (true) {
						inputLine = in.readLine();
						while (inputLine == null) {
							inputLine = in.readLine();
							if (System.currentTimeMillis() - lastSuccess > 5000) {
								inputLine = "DONE";
								break;
							}
						}
						lastSuccess = System.currentTimeMillis();
						if (inputLine.equals("DONE")) {
							break;
						} else {
							tempMap.put(Integer.parseInt(inputLine.substring(0,
									inputLine.indexOf(' '))), Integer
									.parseInt(inputLine.substring(inputLine
											.indexOf(' ') + 1)));
						}
					}
				} catch (Exception e) {
					tempMap = null;
				}
				if(tempMap != null){
					map = tempMap;
				}
			}
		}).start();
	}

}
