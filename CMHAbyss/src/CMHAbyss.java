import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import org.osbot.script.Script;
import org.osbot.script.mouse.MouseDestination;
import org.osbot.script.mouse.RectangleDestination;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.model.Entity;
import org.osbot.script.rs2.model.Item;
import org.osbot.script.rs2.model.NPC;
import org.osbot.script.rs2.model.Player;
import org.osbot.script.rs2.model.RS2Object;
import org.osbot.script.rs2.randoms.RandomBehaviourHook;
import org.osbot.script.rs2.randoms.RandomManager;
import org.osbot.script.rs2.skill.Skill;
import org.osbot.script.rs2.skill.Skills;
import org.osbot.script.rs2.ui.Bank;
import org.osbot.script.rs2.ui.EquipmentSlot;
import org.osbot.script.rs2.ui.RS2Interface;
import org.osbot.script.rs2.ui.RS2InterfaceChild;
import org.osbot.script.rs2.ui.Tab;

public class CMHAbyss extends Script {
	private boolean debug = false;
	private boolean shouldHop = false;
	private Script loader;
	private final String currentVersion = "3.8.0";
	private final String settingsVersion = "1.7.1";
	private final boolean useNewRotate = true;
	private final double ROT = 109.0;
	private final int rolID = 2570;
	private boolean canStart = false;
	private State state;
	private int walkI = 0;
	private int pathIndex;
	private int crafted = 0;
	private long startTime;
	private final int CONST = 1;
	private boolean locked = false;
	private int startExp = 0;
	private int gainedExp = 0;
	private Thread antibanThread;
	private double antibanTrigger = 0.1;
	private boolean scriptStopped;
	private boolean antiban = false;
	private CMHAbyssLogin gui;
	private CMHAbyssGUI newgui;
	private BufferedImage[] paintImages = new BufferedImage[4];
	private boolean doneLoading;
	private int paintI = 0;
	private final int offsetX = -3, offsetY = -20;
	private boolean hide;
	private boolean updated;
	private String username;
	private boolean isPrem;
	private int[] neededItems;
	private int essID;
	private int foodID;
	private int runThresh1O, runThresh1;
	private int best;
	private int emTeleportThresh;
	private boolean getNew;
	private RS2Object bestObject;
	private boolean isFilled;
	private boolean isFilled1;
	private boolean nullWalk;
	private boolean hopWorlds = false;
	private boolean poisoned, teleblocked;
	private long lastRuntimeUpdate;
	private int lastExpUpdate;
	private boolean toggleRunInner;
	private boolean drinkPots, takeFood;
	private int runDrinkAt, takeFoodNum;
	private ArrayList<Integer> blacklistX = new ArrayList<Integer>();
	private ArrayList<Integer> blacklistY = new ArrayList<Integer>();
	Position nullWalkStartPosition;
	int nullWalkCount;
	private int direction = 0;
	private PlayerData pdata;
	private boolean useROL;
	private int[] pouchesToUse;

	private boolean mineRocks, chopTendrils, distractEyes, squeezeGaps,
			burnBoils, eatFood;
	private String craftAt, foodName;
	private int craftAtIndex;
	private int teleportMethod, hpHealed, hpEatAt;
	private int emergencyTeleportMethod;
	private boolean normalTele;
	private HashMap<String, PKerPair> map;

	/****************************** GENERAL VARIABLES ******************************/
	private final int[] pouchIDs = new int[] { 5509, 5510, 5512, 5514 };
	private final int[] degradedPouchIDs = new int[] { 5511, 5513, 5515 };
	private final int[] allPouchIDs = new int[] { 5509, 5510, 5511, 5512, 5513,
			5514, 5515 };
	private final Position[][] pathToDitch = new Position[][] { new Position[] {
			new Position(3098, 3501, 0), new Position(3103, 3508, 0),
			new Position(3104, 3516, 0), new Position(3104, 3522, 0) } };
	private final Position[][] pathToAbyss = new Position[][] { new Position[] {
			new Position(3101, 3531, 0), new Position(3100, 3541, 0),
			new Position(3101, 3550, 0), new Position(3105, 3555, 0) } };
	private final Position[][] pathFromVarrock = new Position[][] {
			new Position[] { new Position(3205, 3428, 0),
					new Position(3196, 3429, 0), new Position(3188, 3429, 0),
					new Position(3179, 3429, 0), new Position(3168, 3428, 0),
					new Position(3161, 3423, 0), new Position(3152, 3421, 0),
					new Position(3146, 3420, 0), new Position(3138, 3422, 0),
					new Position(3130, 3423, 0), new Position(3122, 3423, 0),
					new Position(3115, 3423, 0), new Position(3107, 3420, 0),
					new Position(3098, 3424, 0), new Position(3092, 3429, 0),
					new Position(3092, 3437, 0), new Position(3092, 3444, 0),
					new Position(3093, 3452, 0), new Position(3088, 3460, 0),
					new Position(3095, 3465, 0), new Position(3099, 3472, 0),
					new Position(3099, 3480, 0), new Position(3094, 3489, 0) },
			new Position[] { new Position(3205, 3428, 0),
					new Position(3196, 3429, 0), new Position(3188, 3429, 0),
					new Position(3179, 3429, 0), new Position(3168, 3428, 0),
					new Position(3161, 3423, 0), new Position(3152, 3421, 0),
					new Position(3146, 3420, 0), new Position(3138, 3422, 0),
					new Position(3130, 3423, 0), new Position(3122, 3423, 0),
					new Position(3115, 3423, 0), new Position(3107, 3420, 0),
					new Position(3098, 3424, 0), new Position(3092, 3429, 0),
					new Position(3092, 3437, 0), new Position(3092, 3444, 0),
					new Position(3093, 3452, 0), new Position(3088, 3460, 0),
					new Position(3083, 3465, 0), new Position(3081, 3474, 0),
					new Position(3083, 3483, 0), new Position(3090, 3490, 0) } };
	private final int[] blacklistedWorlds = new int[] { 308, 316, 307, 315,
			323, 324, 325, 331, 332, 337, 339, 340, 347, 348, 355, 356, 363,
			364, 371, 372 };
	private final int[] pickIDs = { 1265, 1267, 1269, 1273, 1271, 1275 };
	private final int[] hatchetIDs = { 1351, 1349, 1353, 1355, 1357, 1359 };
	private final int tinderboxID = 590;
	private final int regEssID = 1436;
	private final int pureEssID = 7936;
	private final Area lumbyArea = new Area(3210, 3230, 3230, 3210);
	private final Area bankArea = new Area(3083, 3501, 3100, 3486);
	private final Area rimmArea = new Area(2955, 3220, 2951, 3228);
	private final Area generalStoreArea = new Area(3074, 3516, 3086, 3504);
	private final int[] angle = new int[] { 180, 270, 270, 270, 270, 0, 0, 0,
			90, 90, 90, 90, 180 };

	private enum State {
		BANK, WALK_TO_DITCH, HOP_DITCH, WALK_TO_ABYSS, TELEPORT, ENTER_WALL, ENTER_RIFT, CRAFT, TELEPORT2, WALK_FROM_VARROCK, TELEPORT_HOUSE, ASCEND, WALK_LUMBY_BANK, LUMBY_BANK, ENTER_PORTAL;
	}

	public CMHAbyss(Script loader) {
		this.loader = loader;
	}

	public void onStart() {
		loader.client.setMouseSpeed(random(15, 20));
		log("Loading paint");
		final String main64, stats64, script64, feedback64;
		main64 = stats64 = script64 = feedback64 = null;
		getBlacklist();
		new Thread(new Runnable() {
			public void run() {
				try {
					paintImages[0] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/main.png"));
					paintImages[1] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/stats.png"));
					paintImages[2] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/script.png"));
					paintImages[3] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/feedback.png"));
					doneLoading = true;
				} catch (Exception e) {
				}
			}
		}).start();
		try {
			for (int i = 0; i < 10; i++)
				loader.sleep(random(400, 500));
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		gui = new CMHAbyssLogin();
		gui.isAuth = true;
		gui.isPrem = true;

		if (!gui.isAuth) {
			JOptionPane.showMessageDialog(null,
					"You must be logged in to run this script");
			return;
		}

		username = loader.myPlayer().getName();
		isPrem = gui.isPrem;
		newgui = new CMHAbyssGUI();
		loadSettings(newgui);
		newgui.setVisible(true);
		while (!newgui.ready && newgui.isVisible()) {
			try {
				loader.sleep(random(300, 400));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		newgui.setVisible(false);
		saveSettings(newgui);
		int neededItemsCount = 0;
		int neededItemsI = 0;
		try {
			emTeleportThresh = Integer.parseInt(newgui.emTeleport.getText());
			mineRocks = newgui.mineRocks.isSelected();
			chopTendrils = newgui.chopTendrils.isSelected();
			distractEyes = newgui.distractEyes.isSelected();
			burnBoils = newgui.burnBoils.isSelected();
			squeezeGaps = newgui.squeezeGaps.isSelected();
			craftAt = (String) newgui.craftAt.getSelectedItem();
			craftAtIndex = newgui.craftAt.getSelectedIndex();
			teleportMethod = newgui.teleportMethod.getSelectedIndex();
			eatFood = newgui.eatFood.isSelected();
			if (eatFood) {
				foodName = newgui.foodName.getText();
				hpHealed = newgui.unknown.isSelected() ? 1 : Integer
						.parseInt(newgui.hpHealed.getText());
				hpEatAt = Integer.parseInt(newgui.hpEatAt.getText());
			}
			essID = newgui.pureEss.isSelected() ? pureEssID : regEssID;
			if (mineRocks)
				neededItemsCount += 6;
			if (chopTendrils)
				neededItemsCount += 6;
			if (burnBoils)
				neededItemsCount++;
			neededItemsCount += pouchIDs.length + degradedPouchIDs.length;
			neededItems = new int[neededItemsCount];
			if (mineRocks) {
				for (int i = 0; i < pickIDs.length; i++)
					neededItems[neededItemsI++] = pickIDs[i];
			}
			if (chopTendrils)
				for (int i = 0; i < hatchetIDs.length; i++)
					neededItems[neededItemsI++] = hatchetIDs[i];
			if (burnBoils)
				neededItems[neededItemsI++] = tinderboxID;
			for (int i = 0; i < pouchIDs.length; i++)
				neededItems[neededItemsI++] = pouchIDs[i];
			for (int i = 0; i < degradedPouchIDs.length; i++)
				neededItems[neededItemsI++] = degradedPouchIDs[i];
			runThresh1O = Integer.parseInt(newgui.runAt.getText());
			runThresh1 = runThresh1O - 5 + random(0, 10);
			toggleRunInner = newgui.toggleRunInner.isSelected();
			drinkPots = newgui.drinkPots.isSelected();
			runDrinkAt = Integer.parseInt(newgui.energyDrinkAt.getText());
			takeFood = newgui.takeFood.isSelected();
			takeFoodNum = Integer.parseInt(newgui.takeFoodNum.getText());
		} catch (Exception e) {
			loader.log("Could not parse GUI.");
			return;
		}
		isFilled = false;
		state = State.BANK;
		if (loader.closestObjectForName("Abyssal rift") != null
				&& distanceTo(loader.closestObjectForName("Abyssal rift")
						.getPosition()) < 17.5) {
			state = State.ENTER_RIFT;
		}
		if (loader.closestObjectForName("Portal") != null
				&& loader.distance(loader.closestObjectForName("Portal")) < 10
				&& loader.closestObjectForName("Door") != null)
			state = State.TELEPORT_HOUSE;
		if (distanceTo(pathFromVarrock[0][0]) < 20) {
			state = State.WALK_FROM_VARROCK;
		}
		if (teleportMethod == 1) {
			String method = (String) JOptionPane
					.showInputDialog(
							null,
							"You're using worn glory. What kind of emergency teleport do you want?",
							"Emergency teleport", JOptionPane.PLAIN_MESSAGE,
							null, new String[] { "None", "House teleport",
									"Varrock teleport" }, "None");
			if (method.charAt(0) == 'N')
				emergencyTeleportMethod = 2;
			else if (method.charAt(0) == 'H')
				emergencyTeleportMethod = 0;
			else if (method.charAt(0) == 'V')
				emergencyTeleportMethod = 1;
			else
				emergencyTeleportMethod = 2;
			loader.log("Using emergency teleport: " + method);
		}
		if (getRingSlot() != null) {
			loader.log("Using ring of life.");
			useROL = true;
		}
		int count = 0;
		for (int id : allPouchIDs) {
			if (loader.client.getInventory().contains(id)) {
				count++;
			}
		}
		pouchesToUse = new int[count];
		count = 0;
		for (int id : allPouchIDs) {
			if (loader.client.getInventory().contains(id)) {
				pouchesToUse[count++] = id;
			}
		}
		startExp = loader.client.getSkills().getExperience(Skill.RUNECRAFTING);
		canStart = true;
		startTime = System.currentTimeMillis();
		lastRuntimeUpdate = startTime;
	}

	private boolean blacklisted(int world) {
		for (int i = 0; i < blacklistedWorlds.length; i++)
			if (blacklistedWorlds[i] == world)
				return true;
		return false;
	}

	private int getRandomWorld() {
		int world;
		do {
			world = random(305, 375);
		} while (blacklisted(world) || world == loader.client.getCurrentWorld());
		return world;
	}

	public void onLogin(int a) {
		loader.log("Logged in");
	}

	public void onMessage(String message) {
		// System.out.println(message);
		if (message.contains("poison")) {
			poisoned = true;
		}
		if (message.contains("block") && message.contains("tele"))
			teleblocked = true;
	}

	public void onPlayerMessage(String name, String message) {

	}

	public void onExit() {
		log("Thank you for using CMHAbyss. You have crafted " + crafted
				+ " runes.");
		log("and gained " + gainedExp + " EXP.");
		scriptStopped = true;
	}

	public int onLoop() {
		if (loader.closestObjectForName("Abyssal rift") != null
				&& distanceTo(loader.closestObjectForName("Abyssal rift")
						.getPosition()) < 17.5) {
			state = State.ENTER_RIFT;
		}

		loader.client.setMouseSpeed(random(15, 20));
		try {
			if (loader.client.getConfig(170) == 1) {
				toggleMouse();
			}
			if (canStart) {
				if (rimmArea.currentlyInArea()) {
					RS2Object portal = loader.closestObjectForName("Portal");
					if (portal != null) {
						Position p = loader.myPosition();
						for (int i = 0; i < 10 && !portal.interact("Enter"); i++)
							sleep(random(500, 600));
						RS2Interface inte = loader.client.getInterface(232);
						for (int i = 0; i < 10 && inte == null; i++) {
							inte = loader.client.getInterface(232);
							sleep(random(500, 600));
						}
						for (int i = 0; i < 10 && !inte.getChild(1).interact(); i++)
							sleep(random(500, 600));
						for (int i = 0; i < 10 && loader.distance(p) < 10; i++)
							sleep(random(500, 600));
						sleep(random(5000, 6000));
						state = State.TELEPORT_HOUSE;
					}
				}
				if (lumbyArea.currentlyInArea()) {
					walkI = 0;
					state = State.ASCEND;
				}
				switch (state) {
				case BANK:
					getBlacklist();
					new Thread(new Runnable() {
						public void run() {
							Thread t = new Thread(new Runnable() {
								public void run() {
									try {
										if (loader.client.getCameraPitchAngle() < 55)
											loader.client.typeKey(
													(char) KeyEvent.VK_UP, 0,
													random(2600, 3400), false);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							});
							t.start();
							if (state != State.BANK) {
								t.interrupt();
								t.stop();
							}
						}
					}).start();
					normalTele = false;
					if (loader.closestObjectForName("Abyssal rift") != null
							&& distanceTo(loader.closestObjectForName(
									"Abyssal rift").getPosition()) < 40) {
						blacklistX.clear();
						blacklistY.clear();
						getNew = true;
						state = State.ENTER_WALL;
						bestObject = null;
						doneLoading = true;
						break;
					}
					if (!bankArea.currentlyInArea()) {
						RS2Object bankBooth = loader
								.closestObjectForName("Bank booth");
						if (bankBooth != null && distanceTo(bankBooth) < 20) {
							walkMiniMap2(bankBooth);
						} else {
							break;
						}
					}
					Bank b = loader.client.getBank();
					for (int i = 0; i < 10 && !b.isOpen(); i++) {
						RS2Object booth = closestObjectForName("Bank booth",
								"Bank");

						loader.sleep(random(300, 400));
						if (booth == null)
							break;

						booth.interact("Bank");
						loader.sleep(random(500, 700));
						for (int j = 0; j < 10 && !b.isOpen(); j++) {
							loader.sleep(random(200, 300));
							if (loader.myPlayer().isMoving())
								j--;
						}
					}
					boolean doneBanking = loader.client.getSkills()
							.getCurrentLevel(Skill.HITPOINTS) >= hpEatAt;
					if (loader.client.getInventory().isFull()) {
						if ((teleportMethod == 2 || teleportMethod == 1
								&& emergencyTeleportMethod == 1)
								&& (!loader.client.getInventory()
										.contains(8007) || loader.client
										.getInventory().getAmount(8007) > 5))
							doneBanking = false;
						if ((teleportMethod == 0 || teleportMethod == 1
								&& emergencyTeleportMethod == 0)
								&& (!loader.client.getInventory()
										.contains(8013) || loader.client
										.getInventory().getAmount(8013) > 5))
							doneBanking = false;
						if (teleportMethod == 1 && getGlorySlot() == null)
							doneBanking = false;
						if (bankContainsPouches()) {
							doneBanking = false;
							getPouches();
						}
						pathIndex = random(0, 1);
						if (doneBanking) {
							state = State.WALK_TO_DITCH;
							break;
						}
					}
					// System.out.println("Depositing");
					if (useROL) {
						if (getRingSlot() == null) {
							loader.log("Getting ring of life");
							loader.client.getBank().depositAllExcept(
									allPouchIDs);
							boolean gotRing = false;
							for (int id = rolID; id <= rolID; id = rolID) {
								if (loader.client.getBank().contains(id)) {
									gotRing = true;
									for (int i = 0; i < 10
											&& !loader.client.getBank()
													.withdraw1(id); i++)
										loader.sleep(random(500, 600));
									break;
								}
							}
							if (loader.client.getInventory().contains(rolID)) {
								gotRing = true;
								for (int i = 0; i < 10
										&& !loader.client.getBank().close(); i++) {
									loader.sleep(random(500, 600));
								}
								for (int i = 0; i < 10
										&& !loader.client.getInventory()
												.interactWithId(rolID, "Wear"); i++) {
									loader.sleep(random(1000, 1200));
								}
								break;
							}
						}
					}
					if (((teleportMethod == 2 || teleportMethod == 1
							&& emergencyTeleportMethod == 1) && (!loader.client
							.getInventory().contains(8007) || loader.client
							.getInventory().getAmount(8007) > 5))
							|| ((teleportMethod == 0 || teleportMethod == 1
									&& emergencyTeleportMethod == 0) && (!loader.client
									.getInventory().contains(8013) || loader.client
									.getInventory().getAmount(8013) > 5))
							|| teleportMethod == 1) {
						int[] tempNeededItems;
						if (isFilled || isFilled1) {
							tempNeededItems = new int[neededItems.length + 2];
							for (int i = 0; i < neededItems.length; i++) {
								tempNeededItems[i] = neededItems[i];
							}
							tempNeededItems[neededItems.length] = essID;
							tempNeededItems[neededItems.length + 1] = foodID;

						} else {
							tempNeededItems = neededItems;
						}
						int[] tempNeededItems2;
						if (teleportMethod == 1 && emergencyTeleportMethod != 2) {
							tempNeededItems2 = new int[tempNeededItems.length + 1];
							for (int i = 0; i < tempNeededItems.length; i++)
								tempNeededItems2[i] = tempNeededItems[i];
							tempNeededItems2[tempNeededItems.length] = (emergencyTeleportMethod == 1 ? (loader.client
									.getInventory().getAmount(8007) <= 5 ? 8007
									: 0) : (loader.client.getInventory()
									.getAmount(8013) <= 5 ? 8013 : 0));
						} else {
							tempNeededItems2 = tempNeededItems;
						}
						for (int x = 0; x < 10
								&& !b.depositAllExcept(tempNeededItems2); x++) {
							loader.sleep(random(900, 1100));
						}
					}
					boolean shouldBreak = false;
					// System.out.println("Checking food");
					if (loader.client.getSkills().getCurrentLevel(
							Skill.HITPOINTS) < hpEatAt
							|| drinkPots
							&& loader.client.getRunEnergy() < runDrinkAt) {
						shouldBreak = true;

						if (drinkPots
								&& loader.client.getRunEnergy() < runDrinkAt) {
							boolean breakAll = false;
							int withdrawCount = 0;
							for (int post = 4; post > 0; post--) {
								for (Item i : loader.client.getBank()
										.getItems()) {
									if (i == null)
										break;
									if (i.getName().equals(
											"Super energy(" + post + ")")) {
										for (int t = 0; t < 10
												&& !loader.client.getBank()
														.withdraw1(i.getId()); t++)
											loader.sleep(random(500, 600));
										breakAll = true;
										break;
									}
								}
								if (breakAll)
									break;
							}
							if (!breakAll) {
								loader.log("Out of super energy potions.");
								for (int i = 0; i < 10 && !b.close(); i++) {
									loader.sleep(random(500, 600));
								}
								loader.sleep(random(1000, 1500));
								loader.stop();
							}
						}
						if (loader.client.getSkills().getCurrentLevel(
								Skill.HITPOINTS) < hpEatAt) {
							int numNeeded = (int) ((loader.client.getSkills()
									.getLevel(Skill.HITPOINTS) - loader.client
									.getSkills().getCurrentLevel(
											Skill.HITPOINTS)) / hpHealed) + 1;
							for (Item i : loader.client.getBank().getItems()) {
								if (i == null)
									break;
								if (i.getName().equals(foodName)) {
									if (numNeeded == 1) {
										loader.client.getBank()
												.depositAllExcept(allPouchIDs);
										for (int t = 0; t < 10
												&& !loader.client.getBank()
														.withdraw1(i.getId()); t++)
											loader.sleep(random(500, 600));
									} else if (numNeeded == 5) {
										loader.client.getBank()
												.depositAllExcept(allPouchIDs);

										for (int t = 0; t < 10
												&& !loader.client.getBank()
														.withdraw5(i.getId()); t++)
											loader.sleep(random(500, 600));
									} else if (numNeeded == 10) {
										loader.client.getBank()
												.depositAllExcept(allPouchIDs);

										for (int t = 0; t < 10
												&& !loader.client.getBank()
														.withdraw10(i.getId()); t++)
											loader.sleep(random(500, 600));
									} else {
										loader.client.getBank()
												.depositAllExcept(allPouchIDs);
										for (int t = 0; t < 10
												&& !loader.client
														.getBank()
														.withdrawX(
																i.getId(),
																Math.min(
																		numNeeded,
																		18)); t++)
											loader.sleep(random(500, 600));
									}
								}
							}
							loader.sleep(random(1000, 1100));
						}
						for (int i = 0; i < 10 && !b.close(); i++)
							loader.sleep(random(500, 600));
						if (loader.client.getInventory().contains(foodName)) {
							while (!scriptStopped
									&& (loader.client.getSkills()
											.getCurrentLevel(Skill.HITPOINTS) != loader.client
											.getSkills().getLevel(
													Skill.HITPOINTS) && loader.client
											.getInventory().contains(foodName))) {
								if (loader.client.getBank().isOpen()) {
									for (int i = 0; i < 10
											&& !loader.client.getBank().close(); i++)
										loader.sleep(random(500, 600));
								}
								int oldHealth = loader.client.getSkills()
										.getCurrentLevel(Skill.HITPOINTS);

								if (loader.client.getBank().isOpen())
									for (int i = 0; i < 10
											&& !loader.client.getBank().close(); i++)
										loader.sleep(random(500, 600));
								for (int i = 0; i < 10
										&& !loader.client.getInventory()
												.interactWithName(foodName,
														"Eat"); i++) {
									for (int j = 0; j < 10
											&& !loader.client.getBank().close(); j++)
										loader.sleep(random(500, 600));
									loader.sleep(random(500, 600));
								}
								for (int i = 0; i < 10
										&& loader.client.getSkills()
												.getCurrentLevel(
														Skill.HITPOINTS) == oldHealth; i++) {

									loader.sleep(random(500, 600));
								}
								for (int i = 0; i < 10
										&& loader.myPlayer().getAnimation() != -1; i++)
									loader.sleep(random(500, 600));
								loader.sleep(random(500, 600));
							}
						}
						for (int post = 4; post > 0; post--) {
							if (loader.client.getInventory().contains(
									"Super energy(" + post + ")")) {
								while (!scriptStopped
										&& (loader.client.getRunEnergy() < 99 && loader.client
												.getInventory().contains(
														"Super energy(" + post
																+ ")"))) {
									if (loader.client.getBank().isOpen()) {
										for (int i = 0; i < 10
												&& !loader.client.getBank()
														.close(); i++)
											loader.sleep(random(500, 600));
									}
									int oldEnergy = loader.client
											.getRunEnergy();

									if (loader.client.getBank().isOpen())
										for (int i = 0; i < 10
												&& !loader.client.getBank()
														.close(); i++)
											loader.sleep(random(500, 600));
									loader.client.getInventory()
											.interactWithName(
													"Super energy(" + post
															+ ")", "Drink");
									for (int i = 0; i < 10
											&& loader.client.getRunEnergy() == oldEnergy; i++) {
										loader.sleep(random(500, 600));
									}
									for (int i = 0; i < 10
											&& loader.myPlayer().getAnimation() != -1; i++)
										loader.sleep(random(500, 600));
									loader.sleep(random(500, 600));
								}
							}
						}
					}
					if (!shouldBreak) {
						// System.out.println("Not eating");

						if (mineRocks && !containsOneOf(pickIDs)) {
							withdrawOne(pickIDs);
						}
						if (chopTendrils && !containsOneOf(hatchetIDs)) {
							withdrawOne(hatchetIDs);
						}
						if (burnBoils && !containsOneOf(tinderboxID)) {
							// if (debug)
							// System.out.println("Withdrawing tinderbox");
							withdrawOne(tinderboxID);
						}
						if ((!mineRocks || containsOneOf(pickIDs))
								&& (!chopTendrils || containsOneOf(hatchetIDs))
								&& (!burnBoils || containsOneOf(tinderboxID))) {
							if (((teleportMethod == 2 || teleportMethod == 1
									&& emergencyTeleportMethod == 1) && !loader.client
									.getInventory().contains(8007))
									|| ((teleportMethod == 0 || teleportMethod == 1
											&& emergencyTeleportMethod == 0) && !loader.client
											.getInventory().contains(8013))
									|| (teleportMethod == 1 && getGlorySlot() == null)) {
								if ((teleportMethod == 2 || teleportMethod == 1
										&& emergencyTeleportMethod == 1)) {
									if (!bankArea.currentlyInArea())
										break;
									if (!loader.client.getBank().contains(8007)
											&& loader.client.getBank()
													.getAmount(8007) <= 0
											&& loader.client.getBank().isOpen()) {
										loader.sleep(random(1000, 1500));
										if (!loader.client.getBank().contains(
												8007)
												&& loader.client.getBank()
														.getAmount(8007) <= 0
												&& loader.client.getBank()
														.isOpen()) {
											loader.log("Out of teletabs");

											loader.sleep(random(1000, 1500));
											loader.stop();
											for (int i = 0; i < 10
													&& !b.close(); i++) {
												loader.sleep(random(500, 600));
											}
										}
									}
									for (int i = 0; i < 10
											&& !loader.client.getBank()
													.withdraw1(8007); i++)
										loader.sleep(random(500, 600));
								}
								if ((teleportMethod == 0 || teleportMethod == 1
										&& emergencyTeleportMethod == 0)) {

									if (!bankArea.currentlyInArea())
										break;
									if (!loader.client.getBank().contains(8013)
											&& loader.client.getBank()
													.getAmount(8013) <= 0
											&& loader.client.getBank().isOpen()) {
										loader.sleep(random(1000, 1500));
										if (!loader.client.getBank().contains(
												8013)
												&& loader.client.getBank()
														.getAmount(8013) <= 0
												&& loader.client.getBank()
														.isOpen()) {
											loader.log("Out of teletabs");
											for (int i = 0; i < 10
													&& !b.close(); i++) {
												loader.sleep(random(500, 600));
											}
											loader.sleep(random(1000, 1500));
											loader.stop();
										}
									}
									for (int i = 0; i < 10
											&& !loader.client.getBank()
													.withdraw1(8013); i++)
										loader.sleep(random(500, 600));
								}
								if (teleportMethod == 1) {
									// System.out.println("checking glories");
									if (getGlorySlot() == null) {
										boolean gotAmmy = false;
										for (int id = 1712; id >= 1706; id -= 2) {
											if (loader.client.getBank()
													.contains(id)) {
												gotAmmy = true;
												for (int i = 0; i < 10
														&& !loader.client
																.getBank()
																.withdraw1(id); i++)
													loader.sleep(random(500,
															600));
												for (int i = 0; i < 10
														&& !loader.client
																.getBank()
																.close(); i++) {
													loader.sleep(random(500,
															600));
												}
												for (int i = 0; i < 10
														&& !loader.client
																.getInventory()
																.interactWithId(
																		id,
																		"Wear"); i++) {
													loader.sleep(random(500,
															600));
												}
												break;
											}
										}

										if (!bankArea.currentlyInArea())
											break;
										if (!gotAmmy) {
											loader.log("Out of glories");
											for (int i = 0; i < 10
													&& !b.close(); i++) {
												loader.sleep(random(500, 600));
											}
											loader.sleep(random(1000, 1500));
											loader.stop();
										}
										break;
									}
								}
							}
							if (!bankArea.currentlyInArea())
								break;
							if (!loader.client.getBank().isOpen())
								break;
							if (loader.client.getBank().getAmount(essID) <= 0
									&& loader.client.getBank().isOpen()) {
								loader.sleep(random(1000, 1100));
								if (!loader.client.getBank().contains(essID)) {
									loader.log("Out of essence!");
									for (int i = 0; i < 10 && !b.close(); i++) {
										loader.sleep(random(500, 600));
									}
									loader.sleep(random(1000, 1500));
									loader.stop();
								}
							}
							if (loader.client.getInventory().isFull()) {
								if ((teleportMethod == 2 || teleportMethod == 1
										&& emergencyTeleportMethod == 1)
										&& (!loader.client.getInventory()
												.contains(8007) || loader.client
												.getInventory().getAmount(8007) > 5))
									break;
								if ((teleportMethod == 0 || teleportMethod == 1
										&& emergencyTeleportMethod == 0)
										&& (!loader.client.getInventory()
												.contains(8013) || loader.client
												.getInventory().getAmount(8013) > 5))
									break;
								if (teleportMethod == 1
										&& getGlorySlot() == null)
									break;
								if (loader.client.getSkills().getCurrentLevel(
										Skill.HITPOINTS) < hpEatAt)
									break;
								pathIndex = random(0, 1);
								state = State.WALK_TO_DITCH;
								break;
							}
							if (takeFood
									&& loader.client.getBank().contains(
											foodName)
									&& loader.client.getInventory().getAmount(
											foodName) == 0) {
								for (Item i : loader.client.getBank()
										.getItems())
									if (i != null
											&& i.getName().equals(foodName)) {
										foodID = i.getId();
										loader.client.getBank().withdrawX(
												i.getId(), takeFoodNum);
									}
							}
							long oldcount = loader.client.getInventory()
									.getAmount(essID);
							for (int i = 0; i < 2
									&& !loader.client.getBank().withdrawAll(
											essID); i++) {

								loader.sleep(random(500, 600));
							}
							for (int i = 0; i < 10
									&& oldcount == loader.client.getInventory()
											.getAmount(essID); i++) {
								loader.sleep(random(90, 110));
							}
							if (containsPouches() && !isFilled) {
								for (int i = 0; i < 10
										&& !loader.client.getBank().close(); i++)
									loader.sleep(random(500, 600));
								fillPouches();
								break;
							}
							for (int i = 0; i < 10
									&& !loader.client.getInventory().contains(
											essID); i++) {
								loader.sleep(random(500, 600));
							}
						}
					}

					break;
				case WALK_TO_DITCH:
					while (loader.client.getBank().isOpen()) {
						loader.client.getBank().close();
					}
					if (loader.client.getRunEnergy() > runThresh1
							&& !loader.isRunning()) {
						runThresh1 = runThresh1O - 5 + random(0, 10);
						setRunning2(true);
					}
					if (walkI == 0) {
						walkMiniMap2(randomize2(pathToDitch[0][walkI++]));
						break;
					}
					if (walkI == pathToDitch[0].length) {
						walkI = 0;
						state = State.HOP_DITCH;
						break;
					}

					if (distanceTo(pathToDitch[0][walkI - 1]) <= 8.5) {
						walkMiniMap2(randomize2(pathToDitch[0][walkI++]));
					} else
						walkMiniMap2(randomize2(pathToDitch[0][walkI - 1]));
					break;
				case HOP_DITCH:
					if (pkerNearBy()) {
						loader.log("PKER DETECTED");
						for (int i = 0; i < 100; i++)
							loader.sleep(random(100, 120));
						loader.worldHopper.hopWorld(getRandomWorld());
					}
					new Thread(new Runnable() {
						public void run() {
							Thread t = new Thread(new Runnable() {
								public void run() {
									try {
										if (loader.client.getCameraPitchAngle() < 55)
											loader.client.typeKey(
													(char) KeyEvent.VK_UP, 0,
													random(2600, 3400), false);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							});
							t.start();
							if (state != State.HOP_DITCH) {
								t.interrupt();
								t.stop();
							}
						}
					}).start();
					if (!useNewRotate) {
						if (loader.client.getCameraYawAngle() > 11
								&& loader.client.getCameraYawAngle() <= 359)
							new Thread(new Runnable() {
								public void run() {
									Thread t = new Thread(new Runnable() {
										public void run() {
											try {
												loader.client
														.rotateCameraToAngle(6);
											} catch (InterruptedException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										}
									});
									t.start();
									if (state != State.HOP_DITCH) {
										t.interrupt();
										t.stop();
									}
								}
							}).start();
					} else {
						if (Math.abs(loader.client.getCameraYawAngle() - 6) > 10)
							rotateCameraToAngle(6);
					}
					if (loader.client.getRunEnergy() > runThresh1
							&& !loader.isRunning()) {
						runThresh1 = runThresh1O - 5 + random(0, 10);
						setRunning2(true);
					}
					if (loader.myY() > 3521) {
						walkI = 0;
						pathIndex = random(Math.max(0, pathIndex), 1);
						state = State.WALK_TO_ABYSS;
						break;
					}
					if (loader.client.getInterface(382) != null) {
						for (int i = 0; i < 10
								&& !loader.client.getInterface(382)
										.getChild(18).interact(); i++)
							loader.sleep(random(500, 600));
						for (int i = 0; i < 10
								&& loader.myPlayer().getAnimation() == -1; i++) {

							loader.sleep(random(500, 600));
						}
						for (int i = 0; i < 10
								&& loader.myPlayer().getAnimation() != -1
								&& loader.myPlayer().getY() <= 3521; i++) {

							loader.sleep(random(500, 600));
						}
					}
					if (!loader.myPlayer().isMoving()) {
						RS2Object ditch = closestObjectForName(
								"Wilderness Ditch", "Cross");
						if (ditch != null) {
							if (distanceTo(ditch) > 4) {
								walkMiniMap2(ditch.getPosition());
							}

							for (int i = 0; i < 10 && !ditch.interact("Cross"); i++) {
								loader.sleep(random(500, 600));
							}
							for (int i = 0; i < 10
									&& loader.client.getInterface(382) == null
									&& loader.myPlayer().getAnimation() == -1; i++) {

								loader.sleep(random(300, 400));
							}
							if (loader.myPlayer().getAnimation() != -1) {
								for (int i = 0; i < 10
										&& loader.myPlayer().getAnimation() != -1
										&& loader.myPlayer().getY() <= 3521; i++) {

									loader.sleep(random(500, 600));
								}

								loader.sleep(random(500, 600));
							}
						}
					}
					break;
				case WALK_TO_ABYSS:
					if (loader.myPlayer().getY() > 3600
							|| loader.myPlayer().getY() < 3520
							&& loader.closestObjectForName("Abyssal rift") == null) {
						switch (teleportMethod) {
						case 0:
							state = State.TELEPORT_HOUSE;
						case 1:
							state = State.BANK;
						case 2:
							state = State.WALK_FROM_VARROCK;
							pathIndex = random(0, 1);
						}
						break;
					}

					try {
						new Thread(new Runnable() {
							public void run() {
								Thread t = new Thread(new Runnable() {
									public void run() {
										try {
											if (loader.client
													.getCameraPitchAngle() > 40)
												loader.client
														.typeKey(
																(char) KeyEvent.VK_DOWN,
																0,
																random(2600,
																		3400),
																false);
										} catch (InterruptedException e) {
											// TODO Auto-generated catch
											// block
											e.printStackTrace();
										}
									}
								});
								t.start();
								if (state != State.WALK_TO_ABYSS) {
									t.interrupt();
									t.stop();
								}
							}
						}).start();
					} catch (Exception e) {
					}
					doneLoading = false;
					Thread walkThread = new Thread(new Runnable() {
						public void run() {
							try {
								if (walkI == 0) {
									walkMiniMap2(randomize2(pathToAbyss[0][walkI++]));
									doneLoading = true;
									return;
								}
								if (walkI == pathToAbyss[0].length) {
									walkI = 0;
									state = State.TELEPORT;
									doneLoading = true;
									return;
								}

								if (distanceTo(pathToAbyss[0][walkI - 1]) <= 8.5) {
									walkMiniMap2(randomize2(pathToAbyss[0][walkI++]));
								} else
									walkMiniMap2(randomize2(pathToAbyss[0][walkI - 1]));
								doneLoading = true;
								return;
							} catch (Exception e) {
							}
						}
					});
					if (!(poisoned || loader.myPlayer().isUnderAttack()
							&& (playerFacingMe())))
						walkThread.start();
					while (!doneLoading) {
						if (!teleblocked
								&& (poisoned || loader.myPlayer()
										.isUnderAttack() && (playerFacingMe()))) {
							addPKERBlacklist(getPlayerFacingMe());
							hopWorlds = true;
							walkThread.interrupt();
							walkThread.stop();
							switch (teleportMethod) {
							case 0:
								Position old = loader.myPosition();
								while (loader.client.getInventory().contains(
										8013)) {
									loader.client.getInventory()
											.interactWithId(8013, "Break");
									if (teleblocked == true) {
										return random(500, 600);
									}
									if (loader
											.closestObjectForName("Abyssal rift") != null
											&& distanceTo(loader
													.closestObjectForName(
															"Abyssal rift")
													.getPosition()) < 50) {
										state = State.ENTER_WALL;
										return random(500, 600);
									}
									loader.sleep(random(500, 600));
								}
								for (int i = 0; i < 20 && distanceTo(old) < 50; i++) {
									loader.sleep(random(500, 600));
								}
								if (distanceTo(old) > 50) {
									state = State.TELEPORT_HOUSE;
									loader.sleep(random(3900, 4100));
								}
								break;
							case 1:
								if (emergencyTeleportMethod == 0) {
									old = loader.myPosition();
									while (loader.client.getInventory()
											.contains(8013)) {
										loader.client.getInventory()
												.interactWithId(8013, "Break");
										if (teleblocked == true) {
											return random(500, 600);
										}
										if (loader
												.closestObjectForName("Abyssal rift") != null
												&& distanceTo(loader
														.closestObjectForName(
																"Abyssal rift")
														.getPosition()) < 50) {
											state = State.ENTER_WALL;
											return random(500, 600);
										}
										loader.sleep(random(500, 600));
									}
									for (int i = 0; i < 20
											&& distanceTo(old) < 50; i++) {
										loader.sleep(random(500, 600));
									}
									if (distanceTo(old) > 50) {
										state = State.TELEPORT_HOUSE;
										loader.sleep(random(3900, 4100));
									}
									break;
								} else if (emergencyTeleportMethod == 1) {
									old = loader.myPosition();
									while (loader.client.getInventory()
											.contains(8007)) {
										loader.client.getInventory()
												.interactWithId(8007, "Break");
										if (teleblocked == true) {
											return random(500, 600);
										}
										if (loader
												.closestObjectForName("Abyssal rift") != null
												&& distanceTo(loader
														.closestObjectForName(
																"Abyssal rift")
														.getPosition()) < 50) {
											state = State.ENTER_WALL;
											return random(500, 600);
										}
										loader.sleep(random(500, 600));
									}
									for (int i = 0; i < 20
											&& distanceTo(old) < 50; i++) {
										loader.sleep(random(500, 600));
									}
									if (distanceTo(old) > 50) {
										pathIndex = random(0, 1);
										state = State.WALK_FROM_VARROCK;
									}
									break;
								} else if (1 == 1)
									break;
								EquipmentSlot glory = null;
								for (int i = 1; i <= 4; i++) {
									glory = loader.equipmentTab
											.getSlotForNameThatContains("Amulet of glory("
													+ i + ")");
									if (glory != null)
										break;
								}
								old = loader.myPosition();

								if (teleblocked == true) {
									return random(500, 600);
								}
								if (glory != null) {
									for (int i = 0; i < 10
											&& !loader.equipmentTab.open(); i++) {
										loader.sleep(random(500, 600));
									}
									for (int i = 0; i < 10
											&& !loader.client.getInterface(387)
													.getChild(14)
													.interact("Operate"); i++) {
										if (loader
												.closestObjectForName("Abyssal rift") != null
												&& distanceTo(loader
														.closestObjectForName(
																"Abyssal rift")
														.getPosition()) < 50) {
											state = State.ENTER_WALL;
											return random(500, 600);
										}
										loader.sleep(random(500, 600));
									}
								}
								for (int i = 0; i < 10
										&& loader.client.getInterface(234) == null; i++) {
									loader.sleep(random(500, 600));
								}
								if (loader.client.getInterface(234) != null) {
									for (int i = 0; i < 10
											&& !loader.client.getInterface(234)
													.getChild(1).interact(); i++) {
										if (loader
												.closestObjectForName("Abyssal rift") != null
												&& distanceTo(loader
														.closestObjectForName(
																"Abyssal rift")
														.getPosition()) < 50) {
											state = State.ENTER_WALL;
											return random(500, 600);
										}
										loader.sleep(random(500, 600));
									}
									for (int i = 0; i < 20
											&& distanceTo(old) < 10; i++) {
										loader.sleep(random(500, 600));
									}
									if (distanceTo(old) > 10) {
										isFilled = false;
										state = State.BANK;
										break;
									}
								}
								break;
							case 2:
								old = loader.myPosition();
								while (loader.client.getInventory().contains(
										8007)) {
									loader.client.getInventory()
											.interactWithId(8007, "Break");
									if (teleblocked == true) {
										return random(500, 600);
									}
									if (loader
											.closestObjectForName("Abyssal rift") != null
											&& distanceTo(loader
													.closestObjectForName(
															"Abyssal rift")
													.getPosition()) < 50) {
										state = State.ENTER_WALL;
										return random(500, 600);
									}
									loader.sleep(random(500, 600));
								}
								for (int i = 0; i < 20 && distanceTo(old) < 50; i++) {
									loader.sleep(random(500, 600));
								}
								if (distanceTo(old) > 50) {
									pathIndex = random(0, 1);
									state = State.WALK_FROM_VARROCK;
								}
							}
							if (poisoned
									&& (teleportMethod == 0 || teleportMethod == 1
											&& emergencyTeleportMethod == 0)) {
								loader.log("You've been poisoned. Logging out.");
								while (loader.client.getGameState() != 0) {
									loader.stop();
									loader.sleep(random(1000, 1200));
								}
								poisoned = false;
								loader.stop();
								break;
							}
							if (hopWorlds) {
								loader.log("Anti-PK measure: hopping worlds");
								for (int i = 0; i < 100; i++)
									loader.sleep(random(100, 120));
								hopWorlds = false;
								loader.worldHopper.hopWorld(getRandomWorld());
							}
							break;
						}
						loader.sleep(random(500, 600));
					}
					break;
				case TELEPORT:
					if (loader.myPlayer().getY() > 3600
							|| loader.myPlayer().getY() < 3520
							&& loader.closestObjectForName("Abyssal rift") == null) {
						switch (teleportMethod) {
						case 0:
							state = State.TELEPORT_HOUSE;
							break;
						case 1:
							state = State.BANK;
							break;
						case 2:
							state = State.WALK_FROM_VARROCK;
							pathIndex = random(0, 1);
							break;
						}
						break;
					}
					doneLoading = false;
					Thread tThread = new Thread(new Runnable() {
						public void run() {
							if (loader.myY() < 3521) {
								state = State.HOP_DITCH;
								doneLoading = true;
								return;
							}
							if (loader.closestObjectForName("Abyssal rift") != null) {
								blacklistX.clear();
								blacklistY.clear();
								getNew = true;
								state = State.ENTER_WALL;
								bestObject = null;
								doneLoading = true;
								return;
							}
							NPC mage = loader
									.closestNPCForName("Mage of Zamorak");
							if (mage != null) {
								Position old = loader.myPosition();

								try {
									for (int i = 0; i < 10
											&& !mage.interact("Teleport"); i++)
										loader.sleep(random(500, 600));
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								for (int i = 0; i < 10 && distanceTo(old) < 20; i++) {

									try {
										loader.sleep(random(500, 600));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								if (distanceTo(old) > 20) {
									getNew = true;
									blacklistX.clear();
									blacklistY.clear();
									state = State.ENTER_WALL;
									bestObject = null;
									new Thread(new Runnable() {
										public void run() {
											Thread t = new Thread(
													new Runnable() {
														public void run() {
															try {
																if (loader.client
																		.getCameraPitchAngle() < 55)
																	loader.client
																			.typeKey(
																					(char) KeyEvent.VK_UP,
																					0,
																					random(2600,
																							3400),
																					false);
															} catch (InterruptedException e) {
																// TODO
																// Auto-generated
																// catch block
																e.printStackTrace();
															}
														}
													});
											t.start();
											if (state != State.TELEPORT) {
												t.interrupt();
												t.stop();
											}
										}
									}).start();
									try {
										Thread.sleep(random(500, 600));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch
										// block
										e.printStackTrace();
									}
									doneLoading = true;
									return;
								} else {
									// loader.log("Distance: " +
									// distanceTo(old));
								}
							} else if (loader.myPlayer().getY() > 3600
									|| loader.myPlayer().getY() < 3520) {
								blacklistX.clear();
								blacklistY.clear();
								state = State.ENTER_WALL;
								bestObject = null;
								doneLoading = true;
								getNew = true;
							} else {
								try {
									loader.walk(new Position(3105, 3553, 0));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							doneLoading = true;
						}
					});
					if (!(poisoned || loader.myPlayer().isUnderAttack()
							&& (playerFacingMe())))
						tThread.start();
					while (!doneLoading) {
						new Thread(new Runnable() {
							public void run() {
								if (poisoned
										|| loader.myPlayer().isUnderAttack()
										&& (playerFacingMe())) {
									addPKERBlacklist(getPlayerFacingMe());
								}
							}
						});

						if (!teleblocked
								&& (poisoned || loader.myPlayer()
										.isUnderAttack() && (playerFacingMe()))) {
							addPKERBlacklist(getPlayerFacingMe());
							hopWorlds = true;
							tThread.interrupt();
							tThread.stop();
							switch (teleportMethod) {
							case 0:
								Position old = loader.myPosition();
								while (loader.client.getInventory().contains(
										8013)) {
									loader.client.getInventory()
											.interactWithId(8013, "Break");
									if (teleblocked == true) {
										return random(500, 600);
									}
									if (loader
											.closestObjectForName("Abyssal rift") != null
											&& distanceTo(loader
													.closestObjectForName(
															"Abyssal rift")
													.getPosition()) < 50) {
										state = State.ENTER_WALL;
										return random(500, 600);
									}
									loader.sleep(random(500, 600));
								}
								for (int i = 0; i < 20 && distanceTo(old) < 50; i++) {
									loader.sleep(random(500, 600));
								}
								if (distanceTo(old) > 50) {
									state = State.TELEPORT_HOUSE;
									loader.sleep(random(3900, 4100));
								}
								break;
							case 1:
								if (emergencyTeleportMethod == 0) {
									old = loader.myPosition();
									while (loader.client.getInventory()
											.contains(8013)) {
										loader.client.getInventory()
												.interactWithId(8013, "Break");
										if (teleblocked == true) {
											return random(500, 600);
										}
										if (loader
												.closestObjectForName("Abyssal rift") != null
												&& distanceTo(loader
														.closestObjectForName(
																"Abyssal rift")
														.getPosition()) < 50) {
											state = State.ENTER_WALL;
											return random(500, 600);
										}
										loader.sleep(random(500, 600));
									}
									for (int i = 0; i < 20
											&& distanceTo(old) < 50; i++) {
										loader.sleep(random(500, 600));
									}
									if (distanceTo(old) > 50) {
										state = State.TELEPORT_HOUSE;
										loader.sleep(random(3900, 4100));
									}
									break;
								} else if (emergencyTeleportMethod == 1) {
									old = loader.myPosition();
									while (loader.client.getInventory()
											.contains(8007)) {
										loader.client.getInventory()
												.interactWithId(8007, "Break");
										if (teleblocked == true) {
											return random(500, 600);
										}
										if (loader
												.closestObjectForName("Abyssal rift") != null
												&& distanceTo(loader
														.closestObjectForName(
																"Abyssal rift")
														.getPosition()) < 50) {
											state = State.ENTER_WALL;
											return random(500, 600);
										}
										loader.sleep(random(500, 600));
									}
									for (int i = 0; i < 20
											&& distanceTo(old) < 50; i++) {
										loader.sleep(random(500, 600));
									}
									if (distanceTo(old) > 50) {
										pathIndex = random(0, 1);
										state = State.WALK_FROM_VARROCK;
									}
									break;
								} else if (1 == 1)
									break;
								EquipmentSlot glory = null;
								for (int i = 1; i <= 4; i++) {
									glory = loader.equipmentTab
											.getSlotForNameThatContains("Amulet of glory("
													+ i + ")");
									if (glory != null)
										break;
								}
								old = loader.myPosition();

								if (teleblocked == true) {
									return random(500, 600);
								}
								if (glory != null) {
									for (int i = 0; i < 10
											&& !loader.equipmentTab.open(); i++) {
										loader.sleep(random(500, 600));
									}
									for (int i = 0; i < 10
											&& !loader.client.getInterface(387)
													.getChild(14)
													.interact("Operate"); i++) {
										if (loader
												.closestObjectForName("Abyssal rift") != null
												&& distanceTo(loader
														.closestObjectForName(
																"Abyssal rift")
														.getPosition()) < 50) {
											state = State.ENTER_WALL;
											return random(500, 600);
										}
										loader.sleep(random(500, 600));
									}
								}
								for (int i = 0; i < 10
										&& loader.client.getInterface(234) == null; i++) {
									loader.sleep(random(500, 600));
								}
								if (loader.client.getInterface(234) != null) {
									for (int i = 0; i < 10
											&& !loader.client.getInterface(234)
													.getChild(1).interact(); i++) {
										if (loader
												.closestObjectForName("Abyssal rift") != null
												&& distanceTo(loader
														.closestObjectForName(
																"Abyssal rift")
														.getPosition()) < 50) {
											state = State.ENTER_WALL;
											return random(500, 600);
										}
										loader.sleep(random(500, 600));
									}
									for (int i = 0; i < 20
											&& distanceTo(old) < 10; i++) {
										loader.sleep(random(500, 600));
									}
									if (distanceTo(old) > 10) {
										isFilled = false;
										state = State.BANK;
										break;
									}
								}
								break;
							case 2:
								old = loader.myPosition();
								while (loader.client.getInventory().contains(
										8007)) {
									loader.client.getInventory()
											.interactWithId(8007, "Break");
									if (teleblocked == true) {
										return random(500, 600);
									}
									if (loader
											.closestObjectForName("Abyssal rift") != null
											&& distanceTo(loader
													.closestObjectForName(
															"Abyssal rift")
													.getPosition()) < 50) {
										state = State.ENTER_WALL;
										return random(500, 600);
									}
									loader.sleep(random(500, 600));
								}
								for (int i = 0; i < 20 && distanceTo(old) < 50; i++) {
									loader.sleep(random(500, 600));
								}
								if (distanceTo(old) > 50) {
									pathIndex = random(0, 1);
									state = State.WALK_FROM_VARROCK;
								}
							}
							if (poisoned
									&& (teleportMethod == 0 || teleportMethod == 1
											&& emergencyTeleportMethod == 0)) {
								loader.log("You've been poisoned. Logging out.");
								while (loader.client.getGameState() != 0) {
									loader.stop();
									loader.sleep(random(1000, 1200));
								}
								poisoned = false;
								loader.stop();
								break;
							}
							if (hopWorlds) {
								for (int i = 0; i < 100; i++)
									loader.sleep(random(100, 120));
								loader.log("Anti-PK measure: hopping worlds");
								hopWorlds = false;
								loader.worldHopper.hopWorld(getRandomWorld());
							}
							break;
						}
						loader.sleep(random(500, 600));
					}
					break;
				case ENTER_WALL:
					loader.randomManager.registerHook(new RandomBehaviourHook(
							RandomManager.SANDWICH_LADY) {
						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}
					});

					loader.randomManager.registerHook(new RandomBehaviourHook(
							RandomManager.TALKERS) {

						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}

					});

					loader.randomManager.registerHook(new RandomBehaviourHook(
							RandomManager.SECURITY_BOOK) {

						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}

					});

					loader.randomManager.registerHook(new RandomBehaviourHook(
							RandomManager.MILES_GILES_NILES) {

						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}

					});

					loader.randomManager.registerHook(new RandomBehaviourHook(
							RandomManager.RUN_AWAY_FROM_COMBAT) {

						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}

					});

					doneLoading = false;
					Thread wthread = new Thread(new Runnable() {
						public void run() {
							try {
								if ((loader
										.closestNPCForName("Mage of Zamorak") != null || loader
										.myPlayer().getY() < 3700)
										&& loader
												.closestObjectForName("Abyssal rift") == null
										&& loader
												.closestNPCForName("Dark mage") == null) {
									state = State.TELEPORT;
									doneLoading = true;
									return;
								}
								if (loader.closestObjectForName("Abyssal rift") != null
										&& distanceTo(loader
												.closestObjectForName(
														"Abyssal rift")
												.getPosition()) < 17.5) {
									state = State.ENTER_RIFT;
									doneLoading = true;
									return;
								}
								// if (debug)
								// System.out.println("GOT HERE 1");
								// getNearestObstacle();
								if (getNew) {
									// if (debug)
									// System.out.println("Getting new");
									RS2Object tempBestObject = getBestObject();
									if (tempBestObject != null
											&& bestObject != null
											&& (tempBestObject.getX() != bestObject
													.getX() || tempBestObject
													.getY() != bestObject
													.getY())) {
										blacklistX.add(bestObject.getX());
										blacklistY.add(bestObject.getY());
									}
									bestObject = tempBestObject;
									// if (debug && bestObject == null)
									// System.out.println("NULL?????");
								}
								if (bestObject != null) {
									nullWalkStartPosition = null;
									// System.out.print("Not null. ");
									// System.out.println("ID: "
									// + bestObject.getId()
									// + " Distance: "
									// + distanceTo(bestObject)
									// + " getNew=" + getNew);
									if (distanceTo(bestObject) > 4) {

										// System.out.println("trying to walk");
										walk2(bestObject, true);
										if (getNew) {
											RS2Object localBestObject = getBestObject();
											if (localBestObject != null) {
												if (localBestObject != null
														&& bestObject != null
														&& (localBestObject
																.getX() != bestObject
																.getX() || localBestObject
																.getY() != bestObject
																.getY())) {
													blacklistX.add(bestObject
															.getX());
													blacklistY.add(bestObject
															.getY());
												}
												bestObject = localBestObject;
											}
											if (nullWalk) {
												if (distanceTo(bestObject) > 16)
													bestObject = null;
												else
													nullWalk = false;
											}
										}
										loader.sleep(random(500, 600));
									} else {
										if (loader
												.closestObjectForName("Abyssal rift") != null
												&& distanceTo(loader
														.closestObjectForName(
																"Abyssal rift")
														.getPosition()) < 17.5) {
											state = State.ENTER_RIFT;
											doneLoading = true;
											return;
										}
										if (getNew) {
											bestObject = getBestObject();
										}
										// if (debug)
										// System.out.println("interacting");
										switch (best) {
										case 0:

											if (interact2(bestObject, "Mine"))
												getNew = false;
											doneLoading = true;
											return;
										case 1:

											if (interact2(bestObject, "Chop"))
												getNew = false;
											doneLoading = true;
											return;
										case 2:

											if (interact2(bestObject,
													"Distract"))
												getNew = false;
											doneLoading = true;
											return;
										case 3:

											if (interact2(bestObject,
													"Burn-down"))
												getNew = false;
											doneLoading = true;
											return;
										case 4:

											if (interact2(bestObject,
													"Squeeze-through"))
												getNew = false;
										}
										doneLoading = true;
										return;
									}
								} else if (getNew) {
									if (nullWalkStartPosition != null
											&& nullWalkCount > 5
											&& distanceTo(nullWalkStartPosition) < 10) {
										blacklistX.clear();
										blacklistY.clear();
										nullWalkStartPosition = null;
									}
									if (nullWalkStartPosition == null) {
										nullWalkCount = 0;
										nullWalkStartPosition = loader
												.myPlayer().getPosition();
									}
									nullWalkCount++;
									nullWalk = true;
									// if (debug)
									// System.out.println("NULL");
									RS2Object rift = loader
											.closestObjectForName("Abyssal rift");
									if (rift != null) {
										// System.out.println("Null walk");
										walk2(new Position(rift.getX()
												+ (rift.getY() - loader
														.myPosition().getY()),
												rift.getY()
														- (rift.getX() - loader
																.myPosition()
																.getX()), 0),
												true);
										loader.sleep(random(500, 600));
									}
									if (loader
											.closestObjectForName("Abyssal rift") != null
											&& distanceTo(loader
													.closestObjectForName(
															"Abyssal rift")
													.getPosition()) < 17.5) {
										state = State.ENTER_RIFT;
										doneLoading = true;
										return;
									}
								}
							} catch (Exception e) {
							} finally {

								doneLoading = true;
							}

						}
					});
					wthread.start();
					while (!doneLoading) {
						if (loader.closestObjectForName("Abyssal rift") != null
								&& distanceTo(loader.closestObjectForName(
										"Abyssal rift").getPosition()) < 17.5) {
							state = State.ENTER_RIFT;
							wthread.interrupt();
							wthread.stop();
							doneLoading = true;
							break;
						}
						if (loader.client.getSkills().getCurrentLevel(
								Skill.HITPOINTS) < emTeleportThresh) {
							if (loader.client.getInventory().contains(foodName)) {
								for (int i = 0; i < 3
										&& !loader.client.getInventory()
												.interactWithName(foodName,
														"Eat"); i++)
									loader.sleep(random(500, 600));
							} else {
								loader.log("Emergency Teleport");
								state = State.TELEPORT2;
							}
							wthread.interrupt();
							wthread.stop();
							doneLoading = true;
							break;
						}
						loader.sleep(random(100, 200));
					}

					loader.randomManager
							.unregisterHook(RandomManager.SANDWICH_LADY);

					loader.randomManager.unregisterHook(RandomManager.TALKERS);

					loader.randomManager
							.unregisterHook(RandomManager.SECURITY_BOOK);

					loader.randomManager
							.unregisterHook(RandomManager.MILES_GILES_NILES);

					loader.randomManager
							.unregisterHook(RandomManager.RUN_AWAY_FROM_COMBAT);
					break;
				case ENTER_RIFT:
					if (teleblocked) {
						loader.log("You've been teleblocked. Logging out.");
						while (loader.client.getGameState() != 0) {
							loader.stop();
							loader.sleep(random(1000, 1200));
						}
						teleblocked = false;
						loader.stop();
						break;
					}
					if (loader.isRunning() && toggleRunInner) {
						setRunning2(false);
						loader.sleep(random(500, 600));
					}
					if (containsDecayedPouches()) {
						walkMiniMap2(loader.closestNPCForName("Dark mage"));
						NPC darkMage = loader.closestNPCForName("Dark mage");
						if (darkMage != null) {
							for (int i = 0; i < 10
									&& !darkMage.interact("Repairs"); i++)
								loader.sleep(random(500, 600));
							for (int i = 0; i < 2
									&& !loader.myPlayer().isMoving(); i++) {
								loader.sleep(random(500, 600));
							}
							while (loader.myPlayer().isMoving())
								loader.sleep(random(500, 600));
							for (int i = 0; i < 10 && containsDecayedPouches(); i++)
								loader.sleep(random(500, 600));
							break;
						}
					}
					RS2Object altar = closestObjectForName("Altar",
							"Craft-rune");
					if (altar != null && distanceTo(altar) < 20) {
						state = State.CRAFT;
						break;
					}

					RS2Object rift = closestObjectForName(craftAt,
							"Exit-through");
					if (rift != null) {
						if (!rift.isVisible()) {
							loader.walk(rift.getPosition());
							break;
						}

						try {
							boolean breakAll = false;
							for (int i = 0; i < 100; i++) {
								if (!useNewRotate) {
									if (Math.abs(loader.client
											.getCameraYawAngle()
											- angle[craftAtIndex]) > 5
											|| (angle[craftAtIndex] == 0 && (loader.client
													.getCameraYawAngle() > 5 || loader.client
													.getCameraYawAngle() < 355))) {
										new Thread(new Runnable() {
											public void run() {
												Thread t = new Thread(
														new Runnable() {
															public void run() {
																try {
																	loader.client
																			.rotateCameraToAngle(angle[craftAtIndex]);
																} catch (InterruptedException e) {
																	// TODO
																	// Auto-generated
																	// catch
																	// block
																	e.printStackTrace();
																}
															}
														});
												t.start();
												if (state != State.ENTER_RIFT) {
													t.interrupt();
													t.stop();
												}
											}
										}).start();
									}
								} else {
									if (Math.abs(loader.client
											.getCameraYawAngle()
											- angle[craftAtIndex]) > 10
											|| (angle[craftAtIndex] == 0 && (loader.client
													.getCameraYawAngle() > 10 || loader.client
													.getCameraYawAngle() < 350))) {
										rotateCameraToAngle(angle[craftAtIndex]);
									}
								}
								if (interact(rift, "Exit-through"))
									break;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						Position old = loader.myPosition();
						for (int i = 0; i < 10 && distanceTo(old) < 10; i++) {
							loader.sleep(random(500, 600));
						}
						if (distanceTo(old) >= 10)
							state = State.CRAFT;
					}
					break;
				case CRAFT:
					altar = closestObjectForName("Altar", "Craft-rune");
					if (altar != null) {
						for (int i = 0; i < 10
								&& !altar.interact("Craft-rune")
								&& loader.client.getInventory().getAmount(
										pureEssID)
										+ loader.client.getInventory()
												.getAmount(regEssID) != 0; i++)
							loader.sleep(random(500, 600));
						for (int i = 0; i < 5
								&& loader.myPlayer().getAnimation() == -1; i++) {
							loader.sleep(random(500, 600));
						}
						for (int i = 0; i < 10
								&& loader.myPlayer().getAnimation() != -1; i++) {
							loader.sleep(random(500, 600));
						}
						if (containsPouches() && (isFilled || isFilled1)) {
							loader.sleep(random(700, 1000));
							emptyPouches();
						}

						if (loader.client.getInventory().getAmount(pureEssID)
								+ loader.client.getInventory().getAmount(
										regEssID) == 0) {
							crafted += getRunesCount();
							// System.out.println("Calling update");
							updateServer(getRunesCount());
							normalTele = true;
							state = State.TELEPORT2;
							break;
						}
					}
					break;
				case TELEPORT2:
					if (loader.closestObjectForName("Abyssal rift") != null
							&& distanceTo(loader.closestObjectForName(
									"Abyssal rift").getPosition()) < 17.5) {
						state = State.ENTER_RIFT;
						break;
					}
					switch (teleportMethod) {
					case 0:
						Position old = loader.myPosition();
						for (int i = 0; i < 10
								&& !loader.client.getInventory()
										.interactWithId(8013, "Break"); i++) {
							if (loader.closestObjectForName("Abyssal rift") != null
									&& distanceTo(loader.closestObjectForName(
											"Abyssal rift").getPosition()) < 17.5) {
								state = State.ENTER_RIFT;
								return random(500, 600);
							}
							loader.sleep(random(500, 600));
						}
						for (int i = 0; i < 10 && distanceTo(old) < 50; i++) {
							loader.sleep(random(500, 600));
						}
						if (distanceTo(old) > 50) {
							state = State.TELEPORT_HOUSE;
							loader.sleep(random(1000, 1100));
							for (int i = 0; i < 20
									&& loader.client.getInterface(399) != null; i++) {
								loader.sleep(random(250, 300));
							}
							loader.sleep(random(250, 300));
						}
						break;
					case 1:
						if (normalTele) {
							EquipmentSlot glory = null;
							for (int i = 1; i <= 4; i++) {
								glory = loader.equipmentTab
										.getSlotForNameThatContains("Amulet of glory("
												+ i + ")");
								if (glory != null)
									break;
							}
							if (glory != null) {
								for (int i = 0; i < 10
										&& !loader.equipmentTab.open(); i++) {
									loader.sleep(random(500, 600));
								}
								for (int i = 0; i < 10
										&& !loader.client.getInterface(387)
												.getChild(14)
												.interact("Operate"); i++) {
									if (loader
											.closestObjectForName("Abyssal rift") != null
											&& distanceTo(loader
													.closestObjectForName(
															"Abyssal rift")
													.getPosition()) < 17.5) {
										state = State.ENTER_RIFT;
										return random(500, 600);
									}
									loader.sleep(random(500, 600));
								}

							}
							for (int i = 0; i < 10
									&& loader.client.getInterface(234) == null; i++) {
								loader.sleep(random(500, 600));
							}
							if (loader.client.getInterface(234) != null) {
								old = loader.myPosition();
								for (int i = 0; i < 10
										&& !loader.client.getInterface(234)
												.getChild(1).interact(); i++) {
									if (loader
											.closestObjectForName("Abyssal rift") != null
											&& distanceTo(loader
													.closestObjectForName(
															"Abyssal rift")
													.getPosition()) < 17.5) {
										state = State.ENTER_RIFT;
										return random(500, 600);
									}
									loader.sleep(random(500, 600));
								}
								for (int i = 0; i < 10 && distanceTo(old) < 10; i++) {
									if (loader
											.closestObjectForName("Abyssal rift") != null
											&& distanceTo(loader
													.closestObjectForName(
															"Abyssal rift")
													.getPosition()) < 17.5) {
										state = State.ENTER_RIFT;
										return random(500, 600);
									}
									loader.sleep(random(500, 600));
								}
								if (distanceTo(old) > 10) {
									isFilled = false;
									state = State.BANK;
								}
							}
							break;
						}
						switch (emergencyTeleportMethod) {
						case 2: // Nothing
							EquipmentSlot glory = null;
							for (int i = 1; i <= 4; i++) {
								glory = loader.equipmentTab
										.getSlotForNameThatContains("Amulet of glory("
												+ i + ")");
								if (glory != null)
									break;
							}
							if (glory != null) {
								for (int i = 0; i < 10
										&& !loader.equipmentTab.open(); i++) {
									loader.sleep(random(500, 600));
								}
								for (int i = 0; i < 10
										&& !loader.client.getInterface(387)
												.getChild(14)
												.interact("Operate"); i++) {
									if (loader
											.closestObjectForName("Abyssal rift") != null
											&& distanceTo(loader
													.closestObjectForName(
															"Abyssal rift")
													.getPosition()) < 17.5) {
										state = State.ENTER_RIFT;
										return random(500, 600);
									}
									loader.sleep(random(500, 600));
								}

							}
							for (int i = 0; i < 10
									&& loader.client.getInterface(234) == null; i++) {
								loader.sleep(random(500, 600));
							}
							if (loader.client.getInterface(234) != null) {
								old = loader.myPosition();
								for (int i = 0; i < 10
										&& !loader.client.getInterface(234)
												.getChild(1).interact(); i++) {
									if (loader
											.closestObjectForName("Abyssal rift") != null
											&& distanceTo(loader
													.closestObjectForName(
															"Abyssal rift")
													.getPosition()) < 17.5) {
										state = State.ENTER_RIFT;
										return random(500, 600);
									}
									loader.sleep(random(500, 600));
								}
								for (int i = 0; i < 10 && distanceTo(old) < 10; i++) {
									if (loader
											.closestObjectForName("Abyssal rift") != null
											&& distanceTo(loader
													.closestObjectForName(
															"Abyssal rift")
													.getPosition()) < 17.5) {
										state = State.ENTER_RIFT;
										return random(500, 600);
									}
									loader.sleep(random(500, 600));
								}
								if (distanceTo(old) > 10) {
									isFilled = false;
									state = State.BANK;
								}
							}
							break;
						case 1: // Varrock Tab
							old = loader.myPosition();
							for (int i = 0; i < 10
									&& !loader.client.getInventory()
											.interactWithId(8007, "Break"); i++) {
								if (loader.closestObjectForName("Abyssal rift") != null
										&& distanceTo(loader
												.closestObjectForName(
														"Abyssal rift")
												.getPosition()) < 17.5) {
									state = State.ENTER_RIFT;
									return random(500, 600);
								}
								loader.sleep(random(500, 600));
							}
							for (int i = 0; i < 10 && distanceTo(old) < 50; i++) {
								loader.sleep(random(500, 600));
							}
							if (distanceTo(old) > 50) {
								pathIndex = random(0, 1);
								state = State.WALK_FROM_VARROCK;
							}
							break;
						case 0:
							old = loader.myPosition();
							for (int i = 0; i < 10
									&& !loader.client.getInventory()
											.interactWithId(8013, "Break"); i++) {
								if (loader.closestObjectForName("Abyssal rift") != null
										&& distanceTo(loader
												.closestObjectForName(
														"Abyssal rift")
												.getPosition()) < 17.5) {
									state = State.ENTER_RIFT;
									return random(500, 600);
								}
								loader.sleep(random(500, 600));
							}
							for (int i = 0; i < 10 && distanceTo(old) < 50; i++) {
								loader.sleep(random(500, 600));
							}
							if (distanceTo(old) > 50) {
								state = State.TELEPORT_HOUSE;
								loader.sleep(random(1000, 1100));
								for (int i = 0; i < 20
										&& loader.client.getInterface(399) != null; i++) {
									loader.sleep(random(250, 300));
								}
								loader.sleep(random(250, 300));
							}
							break;
						}
						break;
					case 2:
						old = loader.myPosition();
						for (int i = 0; i < 10
								&& !loader.client.getInventory()
										.interactWithId(8007, "Break"); i++) {
							if (loader.closestObjectForName("Abyssal rift") != null
									&& distanceTo(loader.closestObjectForName(
											"Abyssal rift").getPosition()) < 17.5) {
								state = State.ENTER_RIFT;
								return random(500, 600);
							}
							loader.sleep(random(500, 600));
						}
						for (int i = 0; i < 10 && distanceTo(old) < 50; i++) {
							loader.sleep(random(500, 600));
						}
						if (distanceTo(old) > 50) {
							pathIndex = random(0, 1);
							state = State.WALK_FROM_VARROCK;
						}
					}
					if (poisoned
							&& (teleportMethod == 0 || teleportMethod == 1
									&& emergencyTeleportMethod == 0)) {
						poisoned = false;
						/*
						 * loader.log("Poisoned, waiting to die in house"); for
						 * (int i = 0; i < 1000 &&
						 * loader.client.getSkills().getCurrentLevel(
						 * Skill.HITPOINTS) != 0 &&
						 * loader.myPlayer().getHealth() != 0; i++) {
						 * loader.sleep(random(600, 700)); }
						 */
					}
					if (poisoned) {
						loader.log("You've been poisoned. Logging out.");
						while (loader.client.getGameState() != 0) {
							loader.stop();
							loader.sleep(random(1000, 1200));
						}
						poisoned = false;
						loader.stop();
						break;
					}
					if (hopWorlds) {
						loader.log("Anti-PK measure: hopping worlds");
						for (int i = 0; i < 100; i++)
							loader.sleep(random(100, 120));
						hopWorlds = false;
						loader.worldHopper.hopWorld(getRandomWorld());
					}
					break;
				case TELEPORT_HOUSE:
					doneLoading = false;
					tThread = new Thread(new Runnable() {
						public void run() {
							try {
								if (loader.closestObjectForName("Abyssal rift") != null
										&& distanceTo(loader
												.closestObjectForName(
														"Abyssal rift")
												.getPosition()) < 40) {
									blacklistX.clear();
									blacklistY.clear();
									getNew = true;
									state = State.ENTER_WALL;
									bestObject = null;
									doneLoading = true;
									return;
								}
								if (lumbyArea.currentlyInArea()) {
									walkI = 0;
									state = State.ASCEND;
									doneLoading = true;
									return;
								}
								RS2Object door = closestObjectForName("Door",
										"Open");
								if (door != null
										&& !loader
												.canReach(closestObjectForName(
														"Amulet of Glory",
														"Rub"))) {
									for (int i = 0; i < 10
											&& !door.interact("Open"); i++) {
										loader.sleep(random(500, 600));
									}
									for (int i = 0; i < 5
											&& !loader.myPlayer().isMoving(); i++) {
										loader.sleep(random(500, 600));
									}
									for (int i = 0; i < 10
											&& loader.myPlayer().isMoving(); i++)
										loader.sleep(random(500, 600));
								} else {
									RS2Object glory = closestObjectForName(
											"Amulet of Glory", "Rub");
									Position targetPosition = null;
									if (getObjectAt(glory.getX(),
											glory.getY() - 1) == null
											&& getObjectAt(glory.getX(),
													glory.getY() - 7) != null) {
										targetPosition = new Position(glory
												.getX(), glory.getY() - 3, 0);
									} else if (getObjectAt(glory.getX(),
											glory.getY() + 1) == null
											&& getObjectAt(glory.getX(),
													glory.getY() + 7) != null) {
										targetPosition = new Position(glory
												.getX(), glory.getY() + 3, 0);
									} else if (getObjectAt(glory.getX() - 1,
											glory.getY()) == null
											&& getObjectAt(glory.getX() - 7,
													glory.getY()) != null) {
										targetPosition = new Position(glory
												.getX() - 3, glory.getY(), 0);
									} else if (getObjectAt(glory.getX() + 1,
											glory.getY()) == null
											&& getObjectAt(glory.getX() + 7,
													glory.getY()) != null) {
										targetPosition = new Position(glory
												.getX() + 3, glory.getY(), 0);
									}
									if (glory != null) {
										if (targetPosition != null) {
											for (int i = 0; i < 10
													&& loader.myPlayer()
															.isMoving(); i++) {
												loader.sleep(random(500, 600));
											}
											if (distanceTo(targetPosition) > 3) {
												loader.walk(targetPosition);
											}
											if (useNewRotate) {
												direction = (int) Math.toDegrees((Math.atan2(
														glory.getY()
																- targetPosition
																		.getY(),
														glory.getX()
																- targetPosition
																		.getX()))) - 90;
												direction += 360;
												direction %= 360;
												System.out.println((glory
														.getY() - targetPosition
														.getY())
														+ " "
														+ (glory.getX() - targetPosition
																.getX()));
												System.out.println(Math.atan2(
														glory.getY()
																- targetPosition
																		.getY(),
														glory.getX()
																- targetPosition
																		.getX()));
												System.out.println(direction);
												if (direction <= 10
														&& loader.client
																.getCameraYawAngle() > direction
														&& loader.client
																.getCameraYawAngle() < (360 - direction))
													rotateCameraToAngle(direction);
												else if (Math.abs(loader.client
														.getCameraYawAngle()
														- direction) > 5)
													rotateCameraToAngle(direction);
											} else {
												loader.client
														.moveCameraToEntity(glory);
											}
										} else {
											if (!glory.isVisible()) {
												walk2(glory.getPosition(),
														false);
											}
											for (int i = 0; i < 10
													&& !glory.isVisible(); i++) {
												walkMiniMap2(glory);
												loader.sleep(random(500, 600));
											}
										}
										new Thread(new Runnable() {
											public void run() {
												Thread t = new Thread(
														new Runnable() {
															public void run() {
																try {
																	if (loader.client
																			.getCameraPitchAngle() > 30)
																		loader.client
																				.typeKey(
																						(char) KeyEvent.VK_DOWN,
																						0,
																						random(2600,
																								3400),
																						false);
																} catch (InterruptedException e) {
																	// TODO
																	// Auto-generated
																	// catch
																	// block
																	e.printStackTrace();
																}
															}
														});
												t.start();
												if (state != State.TELEPORT_HOUSE) {
													t.interrupt();
													t.stop();
												}
											}
										}).start();
										try {
											for (int i = 0; i < 200; i++) {
												if (loader.myPlayer()
														.isMoving()) {
													i--;
													continue;
												}
												if (glory.interact("Rub"))
													break;
												if (i % 50 == 0 && i >= 20) {
													new Thread(new Runnable() {
														public void run() {
															try {
																rotateCameraToAngle((loader.client
																		.getCameraYawAngle() + random(
																		80, 100)) % 360);
																loader.client
																		.typeKey(
																				(char) KeyEvent.VK_DOWN,
																				0,
																				random(2600,
																						3400),
																				false);
															} catch (InterruptedException e) {
																// TODO
																// Auto-generated
																// catch
																// block
																e.printStackTrace();
															}
														}
													}).start();
												}
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
										for (int i = 0; i < 3
												&& (loader.client
														.getInterface(234) == null
														|| !loader.client
																.getInterface(
																		234)
																.isValid() || !loader.client
														.getInterface(234)
														.isVisible()); i++) {
											if (loader.myPlayer().isMoving())
												i--;
											try {
												loader.sleep(random(500, 600));
											} catch (InterruptedException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										}
										if (!(loader.client.getInterface(234) == null
												|| !loader.client.getInterface(
														234).isValid() || !loader.client
												.getInterface(234).isVisible())) {
											try {
												for (int i = 0; i < 10
														&& !loader.client
																.getInterface(
																		234)
																.getChild(1)
																.interact(); i++) {
													loader.sleep(random(500,
															600));
												}
											} catch (InterruptedException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
											for (int i = 0; i < 10
													&& loader
															.closestObjectForName("Bank booth") == null
													|| loader.distance(loader
															.closestObjectForName("Bank booth")) > 20; i++) {
												try {
													loader.sleep(random(1000,
															1100));
												} catch (InterruptedException e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
											}
										}
									}
								}
							} catch (Exception e) {

							} finally {
								doneLoading = true;
							}
						}
					});
					tThread.start();
					long start = System.currentTimeMillis();
					while (!doneLoading) {
						if (bankArea.currentlyInArea()) {
							tThread.interrupt();
							tThread.stop();
							state = State.BANK;
							isFilled = false;
							break;
						}
						if (System.currentTimeMillis() - start > 10000) {
							if (loader.closestObjectForName("Portal") != null
									&& loader
											.closestObjectForName("Amulet of Glory") == null
									&& loader
											.closestObjectForName("Bank booth") == null) {
								tThread.interrupt();
								tThread.stop();
								state = State.ENTER_PORTAL;
								break;
							}
						}
						loader.sleep(random(500, 600));
					}
					break;
				case ENTER_PORTAL:
					doneLoading = false;
					Thread pThread = new Thread(new Runnable() {
						public void run() {
							RS2Object portal = loader
									.closestObjectForName("Portal");
							if (portal != null) {
								try {
									for (int i = 0; i < 10
											&& !portal.interact("Enter"); i++) {

										loader.sleep(random(300, 400));
									}
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								for (int i = 0; i < 10
										&& (loader.client.getInterface(232) == null
												|| !loader.client.getInterface(
														232).isValid() || !loader.client
												.getInterface(232).isVisible()); i++) {
									try {
										loader.sleep(random(500, 600));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								if (!(loader.client.getInterface(232) == null
										|| !loader.client.getInterface(232)
												.isValid() || !loader.client
										.getInterface(232).isVisible())) {
									try {
										for (int i = 0; i < 10
												&& !loader.client
														.getInterface(232)
														.getChild(2).interact(); i++)
											loader.sleep(random(500, 600));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								Position oldPos = loader.myPlayer()
										.getPosition();
								for (int i = 0; i < 10
										&& distanceTo(oldPos) < 20; i++) {
									try {
										loader.sleep(random(1000, 1100));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								for (int i = 0; i < 10
										&& !(loader.client.getInterface(399) == null
												|| !loader.client.getInterface(
														399).isValid() || !loader.client
												.getInterface(399).isVisible()); i++)
									try {
										loader.sleep(random(500, 600));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								if (loader.distance(oldPos) > 20)
									state = State.TELEPORT_HOUSE;
								doneLoading = true;
							}
						}
					});
					pThread.start();
					while (!doneLoading) {
						if (bankArea.currentlyInArea()) {
							pThread.interrupt();
							pThread.stop();
							state = State.BANK;
							break;
						} else if (generalStoreArea.currentlyInArea()) {
							pThread.interrupt();
							pThread.stop();
							loader.walkMiniMap(new Position(3087, 3501, 0));
							state = State.BANK;
							break;
						}
					}
					break;
				case WALK_FROM_VARROCK:

					if (loader.closestObjectForName("Abyssal rift") != null
							&& distanceTo(loader.closestObjectForName(
									"Abyssal rift").getPosition()) < 40) {
						blacklistX.clear();
						blacklistY.clear();
						getNew = true;
						state = State.ENTER_WALL;
						bestObject = null;
						doneLoading = true;
						break;
					}
					if (walkI == 0) {

						walkMiniMap2(randomize(pathFromVarrock[pathIndex][walkI++]));
						break;
					}
					if (walkI == pathFromVarrock[pathIndex].length) {
						walkI = 0;
						isFilled = false;
						state = State.BANK;
						break;
					}

					if (distanceTo(pathFromVarrock[pathIndex][walkI - 1]) <= 8.5) {
						walkMiniMap2(randomize(pathFromVarrock[pathIndex][walkI++]));
					} else
						walkMiniMap2(randomize(pathFromVarrock[pathIndex][walkI - 1]));
					break;
				case ASCEND:

					if (loader.myPlayer().getZ() == 0) {
						RS2Object stair = loader
								.closestObjectForName("Staircase");
						if (stair != null) {
							for (int i = 0; i < 10
									&& !stair.interact("Climb-up"); i++)
								loader.sleep(random(500, 600));
							for (int i = 0; i < 10
									&& loader.myPlayer().getZ() != 1; i++) {
								loader.sleep(random(500, 600));
							}
						}
					} else if (loader.myPlayer().getZ() == 1) {
						loader.log("Finding staircase!");
						RS2Object stair = loader
								.closestObjectForName("Staircase");
						if (stair != null) {
							for (int i = 0; i < 10
									&& !stair.interact("Climb-up"); i++)
								loader.sleep(random(500, 600));
							for (int i = 0; i < 10
									&& loader.myPlayer().getZ() != 2; i++) {
								loader.sleep(random(500, 600));
							}
						}
					} else if (loader.myPlayer().getZ() == 2) {
						state = State.LUMBY_BANK;
					}
					break;
				case LUMBY_BANK:

					if (loader.client.getInventory().contains(
							"Varrock teleport")
							|| loader.client.getInventory().contains(
									"Teleport to house")) {
						state = State.TELEPORT2;
						break;
					}
					b = loader.client.getBank();
					for (int i = 0; i < 10 && !b.isOpen(); i++) {
						RS2Object booth = loader
								.closestObjectForName("Bank booth");

						loader.sleep(random(300, 400));
						if (booth == null)
							break;

						for (int j = 0; j < 10 && !booth.interact("Bank"); j++)
							loader.sleep(random(500, 600));
						loader.sleep(random(500, 700));
						for (int j = 0; j < 10 && !b.isOpen(); j++) {
							loader.sleep(random(200, 300));
							if (loader.myPlayer().isMoving())
								j--;
						}
					}

					for (int x = 0; x < 10
							&& !loader.client.getInventory().isEmpty(); x++) {
						b.depositAllExcept(pickIDs);
						loader.sleep(random(900, 1100));
					}

					if ((teleportMethod == 0 || teleportMethod == 1
							&& emergencyTeleportMethod == 0))
						for (int x = 0; x < 10 && !b.withdraw1(8013); x++) {
							loader.sleep(random(500, 600));

						}
					if ((teleportMethod == 2 || teleportMethod == 1
							&& emergencyTeleportMethod == 1))
						for (int x = 0; x < 10 && !b.withdraw1(8007); x++) {
							loader.sleep(random(500, 600));
						}
					for (int i = 0; i < 5 && b.isOpen(); i++) {

						b.close();
						loader.sleep(random(300, 400));
					}
					break;
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return random(500, 600);
	}

	private EquipmentSlot getRingSlot() {
		EquipmentSlot ring = null;
		try {
			ring = loader.equipmentTab
					.getSlotForNameThatContains("Ring of life");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ring;
	}

	private Object getObjectAt(int x, int y) {
		for (RS2Object object : loader.client.getCurrentRegion().getObjects()) {
			if (object.getX() == x && object.getY() == y)
				return object;
		}
		return null;
	}

	private boolean interact(final RS2Object bestObject, String string)
			throws InterruptedException {
		loader.client.setMouseSpeed(random(15, 20));
		if (!string.equals("Rub") && !string.equals("Exit-through")) {
			new Thread(new Runnable() {
				public void run() {
					Thread t = new Thread(new Runnable() {
						public void run() {
							try {
								if (loader.client.getCameraPitchAngle() < 55)
									loader.client.typeKey(
											(char) KeyEvent.VK_UP, 0,
											random(2600, 3400), false);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					});
					t.start();
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					t.interrupt();
					t.stop();
				}
			}).start();
		}
		if (!bestObject.isVisible()) {
			loader.sleep(random(500, 600));
			return false;
		}
		if (loader.client.getMenu().get(0).action.contains(string)) {
			loader.client.clickMouse(false);
			return true;
		}
		Thread t = new Thread(new Runnable() {
			public void run() {
				try {
					loader.client.moveMouseTo(bestObject.getMouseDestination(),
							false, false, false);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		t.start();
		double oldx = loader.client.getMousePosition().x;
		double oldy = loader.client.getMousePosition().y;
		for (int i = 0; i < 10 && oldx == loader.client.getMousePosition().x
				&& oldy == loader.client.getMousePosition().y; i++)
			loader.sleep(random(30, 40));
		while (oldx != loader.client.getMousePosition().x
				|| oldy != loader.client.getMousePosition().y) {
			if (loader.client.getMenu().get(0).action.contains(string)
					&& bestObject.getMouseDestination().getArea()
							.contains(oldx, oldy)) {
				loader.client.clickMouse(false);
				loader.client.setMouseSpeed(random(15, 20));
				return true;
			}
			oldx = loader.client.getMousePosition().x;
			oldy = loader.client.getMousePosition().y;
		}
		if (loader.client.getMenu().get(0).action.contains(string)) {
			loader.client.clickMouse(false);
			loader.client.setMouseSpeed(random(15, 20));
			t.interrupt();
			t.stop();
			return true;
		}
		loader.client.setMouseSpeed(random(15, 20));
		t.interrupt();
		t.stop();
		return false;
	}

	private void addPKERBlacklist(final Player p) {
		Thread t = new Thread(new Runnable() {
			public void run() {
				PlayerData pdata = null;
				try {
					pdata = PlayerData.lookup(p.getName());
				} catch (IOException e) {
					// log("Unable to get player data.");
				}
				if (pdata != null) {
					SkillData attack = pdata.getSkillData(Skill.ATTACK);
					SkillData strength = pdata.getSkillData(Skill.STRENGTH);
					SkillData defence = pdata.getSkillData(Skill.DEFENCE);
					SkillData magic = pdata.getSkillData(Skill.MAGIC);
					SkillData range = pdata.getSkillData(Skill.RANGED);
					SkillData hitpoints = pdata.getSkillData(Skill.HITPOINTS);
					SkillData prayer = pdata.getSkillData(Skill.PRAYER);
					double base = 0.25 * (defence.level() + hitpoints.level() + (prayer
							.level() / 2));
					double meelee = 0.325 * (attack.level() + strength.level());
					double ranged = 0.325 * (range.level() / 2 + range.level());
					double mage = 0.325 * (magic.level() / 2 + magic.level());
					int finalLevel = (int) (base + Math.max(
							Math.max(ranged, meelee), mage));
					addBlacklist(p.getName(), finalLevel);
				} else {
					addBlacklist(p.getName(), -100);
				}
			}
		});
		t.start();
	}

	private boolean interact2(final RS2Object bestObject, String string)
			throws InterruptedException {
		loader.client.setMouseSpeed(random(15, 20));
		if (!string.equals("Rub") && !string.equals("Exit-through")) {
			new Thread(new Runnable() {
				public void run() {
					Thread t = new Thread(new Runnable() {
						public void run() {
							try {
								if (loader.client.getCameraPitchAngle() < 55)
									loader.client.typeKey(
											(char) KeyEvent.VK_UP, 0,
											random(2600, 3400), false);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					});
					t.start();
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					t.interrupt();
					t.stop();
				}
			}).start();
		}
		if (!bestObject.isVisible()) {
			loader.sleep(random(500, 600));
			return false;
		}
		if (loader.client.getMenu().get(0).action.contains(string)) {
			loader.client.clickMouse(false);
			return true;
		}
		return bestObject.interact(string);
	}

	private void toggleMouse() {
		try {
			for (int i = 0; i < 10 && !loader.openTab(Tab.SETTINGS); i++) {
				sleep(random(500, 600));
			}

			RS2Interface inte = loader.client.getInterface(261);
			if (inte != null) {
				RS2InterfaceChild child = inte.getChild(3);
				if (child != null) {
					child.hover();
					sleep(random(1000, 1100));
					child.interact();
					loader.sleep(random(500, 600));
				}
			}
		} catch (Exception e) {

		}
		return;
	}

	private HashMap<Integer, Integer> tempMap;

	private void getBlacklist() {
		new Thread(new Runnable() {
			public void run() {
				map = new HashMap<String, PKerPair>();
				try {
					URL url = new URL(
							"http://cmhabyss.cmhscripts.com/getblacklist2.php");
					BufferedReader br = new BufferedReader(
							new InputStreamReader(url.openStream()));
					String s = null;
					PKerPair pp = new PKerPair();
					String name = null;
					int count = 0;
					while (true) {
						while ((s = br.readLine()) == null) {
						}
						if (s.equals("DONE")) {
							break;
						} else {
							count++;
							System.out.println(s + " " + count);
							if (count == 1) {
								name = s;
								System.out.println("Name: " + name);
							} else if (count == 2) {
								pp.level = Integer.parseInt(s);
								System.out.println("Level: " + pp.level);
							} else if (count == 3) {
								pp.delta = Integer.parseInt(s);
								System.out.println("Delta: " + pp.delta);
								count = 0;
								map.put(name, pp);
								pp = new PKerPair();
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					map = null;
				}
			}
		}).start();
	}

	private void addBlacklist(String name, int finalLevel) {
		double base = 0.25 * (client.getSkills().getLevel(Skill.DEFENCE)
				+ client.getSkills().getLevel(Skill.HITPOINTS) + (client
				.getSkills().getLevel(Skill.PRAYER) / 2));
		double meelee = 0.325 * (client.getSkills().getLevel(Skill.ATTACK) + client
				.getSkills().getLevel(Skill.STRENGTH));
		double ranged = 0.325 * (client.getSkills().getLevel(Skill.RANGED) / 2 + client
				.getSkills().getLevel(Skill.RANGED));
		double mage = 0.325 * (client.getSkills().getLevel(Skill.MAGIC) / 2 + client
				.getSkills().getLevel(Skill.MAGIC));
		int myLevel = (int) (base + Math.max(Math.max(ranged, meelee), mage));
		int delta = 6;
		if (Math.abs(finalLevel - myLevel) > 7) {
			finalLevel = myLevel;
			delta = 12;
		}
		try {
			URL add = new URL(
					"http://cmhabyss.cmhscripts.com/addblacklist.php?Username="
							+ name + "&Level=" + finalLevel + "&Delta=" + delta);
			add.openConnection();
			add.openStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void updateServer(final int crafted) {
		System.out.println("Update called");
		new Thread(new Runnable() {
			public void run() {
				try {
					System.out.println("AOEUAOEUAOEUAOEUAOEUAOEUAOEUAOEUAOEU");
					int gainedTotal = gainedExp;
					URL url = new URL(
							"http://scriptstracker.no-ip.org/update.php?script=CMHAbyss"
									+ "&scripter=ericthecmh"
									+ "&community=OSBot"
									+ "&hash=0f04265f374dfef4c766fddf5e6d6d27"
									+ "&OSBotName="
									+ loader.getBot().getUsername()
											.replace(" ", "_")
									+ "&Runtime="
									+ (System.currentTimeMillis() - lastRuntimeUpdate)
									+ "&RunesMade=" + crafted + "&ExpGained="
									+ (gainedTotal - lastExpUpdate)
									+ "&TripsMade=1");
					lastRuntimeUpdate = System.currentTimeMillis();
					lastExpUpdate = gainedTotal;
					url.openConnection().getContent();
				} catch (Exception e) {

				}
			}
		}).start();
	}

	private void walkMiniMap2(Position p) throws InterruptedException {
		final int mapX = 643, mapY = 83;
		double distance = distanceTo(p);
		double angle2 = Math.toDegrees(Math.atan2(p.getY()
				- loader.myPosition().getY(), p.getX()
				- loader.myPosition().getX())) - 90;
		double dY = distance * Math.sqrt(17);
		double rdX = -1
				* Math.sin(Math.toRadians(angle2
						- loader.client.getCameraYawAngle())) * dY;
		double rdY = Math.cos(Math.toRadians(angle2
				- loader.client.getCameraYawAngle()))
				* dY;
		RectangleDestination rd = new RectangleDestination(
				(int) (mapX + rdX - 1), (int) (mapY - rdY - 1), 3, 3);
		loader.client.moveMouseTo(rd, false, false, false);
		for (int i = 0; i < 5
				&& (Math.abs(loader.client.getMousePosition().getX()
						- (int) (mapX + rdX)) > 2 || Math.abs(loader.client
						.getMousePosition().getY() - (int) (mapY - rdY)) > 2); i++) {
			loader.sleep(random(500, 600));

		}
		if ((Math.abs(loader.client.getMousePosition().getX()
				- (int) (mapX + rdX)) <= 2 && Math.abs(loader.client
				.getMousePosition().getY() - (int) (mapY - rdY)) <= 2)) {
			loader.sleep(random(500, 600));
			loader.client.clickMouse(false);
			for (int i = 0; i < 2 && !loader.myPlayer().isMoving(); i++) {
				loader.sleep(random(500, 600));
			}
			for (int i = 0; i < 60 && loader.myPlayer().isMoving()
					&& distanceTo(p) > (loader.isRunning() ? 10 : 7); i++) {
				loader.sleep(random(500, 600));
			}
		}
	}

	private void walkMiniMap2NoBlock(Position p) throws InterruptedException {
		final int mapX = 643, mapY = 83;
		double distance = distanceTo(p);
		double angle2 = Math.toDegrees(Math.atan2(p.getY()
				- loader.myPosition().getY(), p.getX()
				- loader.myPosition().getX())) - 90;
		double dY = distance * Math.sqrt(17);
		double rdX = -1
				* Math.sin(Math.toRadians(angle2
						- loader.client.getCameraYawAngle())) * dY;
		double rdY = Math.cos(Math.toRadians(angle2
				- loader.client.getCameraYawAngle()))
				* dY;
		// System.out.println((p.getY() - loader.myPosition().getY()) + ","
		// + (p.getX() - loader.myPosition().getX()));
		// System.out.println(angle2 + loader.client.getCameraYawAngle() + " "
		// + rdX + " " + rdY);
		RectangleDestination rd = new RectangleDestination(
				(int) (mapX + rdX - 1), (int) (mapY - rdY - 1), 3, 3);
		loader.client.moveMouseTo(rd, false, false, false);
		for (int i = 0; i < 5
				&& (Math.abs(loader.client.getMousePosition().getX()
						- (int) (mapX + rdX)) > 2 || Math.abs(loader.client
						.getMousePosition().getY() - (int) (mapY - rdY)) > 2); i++) {
			loader.sleep(random(500, 600));

		}
		if ((Math.abs(loader.client.getMousePosition().getX()
				- (int) (mapX + rdX)) <= 2 && Math.abs(loader.client
				.getMousePosition().getY() - (int) (mapY - rdY)) <= 2)) {
			loader.sleep(random(500, 600));
			loader.client.clickMouse(false);
			for (int i = 0; i < 2 && !loader.myPlayer().isMoving(); i++) {
				loader.sleep(random(500, 600));
			}
			for (int i = 0; i < 60 && loader.myPlayer().isMoving()
					&& distanceTo(p) > (loader.isRunning() ? 10 : 7); i++) {
				loader.sleep(random(500, 600));
			}
		}
	}

	private void walkMiniMap2(Entity e) throws InterruptedException {
		walkMiniMap2(e.getPosition());
	}

	private boolean containsPouches() {
		for (int i = 0; i < pouchIDs.length; i++) {
			if (loader.client.getInventory().contains(pouchIDs[i]))
				return true;
		}
		for (int i = 0; i < degradedPouchIDs.length; i++) {
			if (loader.client.getInventory().contains(degradedPouchIDs[i]))
				return true;
		}
		return false;
	}

	private boolean containsDecayedPouches() {
		for (int i = 0; i < degradedPouchIDs.length; i++) {
			if (loader.client.getInventory().contains(degradedPouchIDs[i]))
				return true;
		}
		return false;
	}

	private EquipmentSlot getGlorySlot() {
		EquipmentSlot glory = null;
		for (int i = 1; i <= 4; i++) {
			try {
				glory = loader.equipmentTab
						.getSlotForNameThatContains("Amulet of glory(" + i
								+ ")");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (glory != null)
				break;
		}
		return glory;
	}

	private boolean playerFacingMe() {
		for (Player p : loader.client.getLocalPlayers()) {
			if (p.isFacing(loader.myPlayer()))
				return true;
		}
		return false;
	}

	private Player getPlayerFacingMe() {
		for (Player p : loader.client.getLocalPlayers()) {
			if (p.isFacing(loader.myPlayer()))
				return p;
		}
		return null;
	}

	private boolean pkerNearBy() {
		if (map == null)
			return false;
		for (Player p : loader.client.getLocalPlayers()) {
			if (map.containsKey(p.getName().hashCode())) {
				int playerLevel = map.get(p.getName()).level;
				int myLevel = getCombatLevel();
				if (Math.abs(playerLevel - myLevel) <= map.get(p.getName()).delta)
					return true;
			}
		}
		return false;
	}

	private int getCombatLevel() {
		Skills skills = loader.client.getSkills();
		double base = 0.25 * (skills.getLevel(Skill.DEFENCE)
				+ skills.getLevel(Skill.HITPOINTS) + (skills
				.getLevel(Skill.PRAYER) / 2));
		double meelee = 0.325 * (skills.getLevel(Skill.ATTACK) + skills
				.getLevel(Skill.STRENGTH));
		double ranged = 0.325 * (skills.getLevel(Skill.RANGED) / 2 + skills
				.getLevel(Skill.RANGED));
		double mage = 0.325 * (skills.getLevel(Skill.MAGIC) / 2 + skills
				.getLevel(Skill.MAGIC));
		return (int) (base + Math.max(Math.max(ranged, meelee), mage));
	}

	private void fillPouches() {
		isFilled = true;
		int pouchfillcount = 0;
		for (int i = 0; i < pouchIDs.length; i++) {
			if (loader.client.getBank().isOpen()) {
				for (int j = 0; j < 10; j++) {
					try {
						if (loader.client.getBank().close())
							break;
					} catch (Exception e) {
					}
				}

			}
			if (loader.client.getInventory().contains(pouchIDs[i])) {
				pouchfillcount++;
				if (isFilled1 && i != 3)
					continue;
				if (i == 3 && !isFilled1
						&& loader.client.getInventory().getAmount(essID) < 12) {
					isFilled = false;
					isFilled1 = true;
					return;
				}
				long old = loader.client.getInventory().getAmount(essID);
				try {
					for (int j = 0; j < 10
							&& !loader.client.getInventory().interactWithId(
									pouchIDs[i], "Fill"); j++) {
						if (loader.client.getBank().isOpen()) {
							loader.client.getBank().close();

						}
						loader.sleep(random(500, 600));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int j = 0; j < 10
						&& loader.client.getInventory().getAmount(essID) == old; j++)
					try {
						loader.sleep(random(100, 150));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
		for (int i = 0; i < degradedPouchIDs.length; i++) {
			if (loader.client.getInventory().contains(degradedPouchIDs[i])) {
				pouchfillcount++;
				if (isFilled1 && i != 2)
					continue;
				if (i == 2 && !isFilled1
						&& loader.client.getInventory().getAmount(essID) < 12) {
					isFilled = false;
					isFilled1 = true;
					return;
				}
				long old = loader.client.getInventory().getAmount(essID);
				try {
					for (int j = 0; j < 10
							&& !loader.client.getInventory().interactWithId(
									degradedPouchIDs[i], "Fill"); j++) {

						if (loader.client.getBank().isOpen()) {
							loader.client.getBank().close();

						}
						loader.sleep(random(500, 600));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int j = 0; j < 10
						&& loader.client.getInventory().getAmount(essID) == old; j++)
					try {
						loader.sleep(random(100, 150));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
	}

	private void emptyPouches() {
		while (loader.myPlayer().getAnimation() != -1) {
			try {
				loader.sleep(random(500, 600));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		for (int i = 0; i < pouchIDs.length; i++) {
			if (loader.client.getInventory().contains(pouchIDs[i])) {
				if (i < 3 && !isFilled)
					continue;
				if (i == 3 && isFilled1
						&& loader.client.getInventory().getEmptySlots() < 12) {
					isFilled = false;
					return;
				}
				long old = loader.client.getInventory().getAmount(essID);
				try {
					for (int j = 0; j < 10
							&& !loader.client.getInventory().interactWithId(
									pouchIDs[i], "Empty"); j++) {
						loader.sleep(random(500, 600));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int j = 0; j < 10
						&& loader.client.getInventory().getAmount(essID) == old; j++)
					try {
						loader.sleep(random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
		for (int i = 0; i < degradedPouchIDs.length; i++) {
			if (loader.client.getInventory().contains(degradedPouchIDs[i])) {
				if (i < 2 && !isFilled)
					continue;
				if (i == 2 && isFilled1
						&& loader.client.getInventory().getEmptySlots() < 12) {
					isFilled = false;
					return;
				}
				long old = loader.client.getInventory().getAmount(essID);
				try {
					for (int j = 0; j < 10
							&& !loader.client.getInventory().interactWithId(
									degradedPouchIDs[i], "Empty"); j++) {
						loader.sleep(random(500, 600));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int j = 0; j < 10
						&& loader.client.getInventory().getAmount(essID) == old; j++)
					try {
						loader.sleep(random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
		isFilled = false;
		isFilled1 = false;
	}

	private int getRunesCount() {
		int count = 0;
		for (Item i : loader.client.getInventory().getItems()) {
			if (i == null)
				continue;
			if (i.getName().contains("rune")) {
				count += i.getAmount();
			}
		}
		return count;
	}

	private RS2Object getBestObject() {
		RS2Object bestObject = null;
		try {
			double dist = 2000000000.0;
			RS2Object rock = closestObjectForName("Rock", "Mine");
			RS2Object tendrils = closestObjectForName("Tendrils", "Chop");
			RS2Object eyes = closestObjectForName("Eyes", "Distract");
			RS2Object boil = closestObjectForName("Boil", "Burn-down");
			RS2Object gap = closestObjectForName("Gap", "Squeeze-through");
			RS2Object rift = loader.closestObjectForName("Abyssal rift");
			if (rift == null)
				return null;
			if (mineRocks && rock != null && distanceTo(rock) < dist
					&& distanceBetween(rift, rock) < 30 && !blackListed(rock)) {
				bestObject = rock;
				dist = distanceTo(bestObject);
				best = 0;
			}
			// if (debug)
			// System.out.println(dist + "ROCK");
			if (chopTendrils && tendrils != null && distanceTo(tendrils) < dist
					&& distanceBetween(rift, tendrils) < 30
					&& !blackListed(tendrils)) {
				bestObject = tendrils;
				dist = distanceTo(bestObject);
				best = 1;
			}
			// if (debug)
			// System.out.println(dist + "Tendril");
			if (distractEyes && eyes != null && distanceTo(eyes) < dist
					&& distanceBetween(rift, eyes) < 30 && !blackListed(eyes)) {
				bestObject = eyes;
				dist = distanceTo(bestObject);
				best = 2;
			}
			// if (debug)
			// System.out.println(dist + "boil");
			if (burnBoils && boil != null && distanceTo(boil) < dist
					&& distanceBetween(rift, boil) < 30 && !blackListed(boil)) {
				bestObject = boil;
				dist = distanceTo(bestObject);
				best = 3;
			}
			// if (debug)
			// System.out.println(dist + "Gap");
			if (squeezeGaps && gap != null && distanceTo(gap) < dist
					&& distanceBetween(rift, gap) < 30 && !blackListed(gap)) {
				bestObject = gap;
				dist = distanceTo(bestObject);
				best = 4;
			}
			// if (debug)
			// System.out.println(dist);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bestObject;
	}

	private boolean blackListed(Entity e) {
		/*
		 * try { for (int i = 0; i < blacklistX.size(); i++) if (e.getX() ==
		 * blacklistX.get(i) && e.getY() == blacklistY.get(i)) return true; }
		 * catch (Exception ex) { ex.printStackTrace(); }
		 */
		return false;
	}

	private void withdrawOne(int... ids) {
		for (int i = 0; i < ids.length; i++) {
			if (loader.client.getBank().contains(ids[i])) {
				try {
					for (int t = 0; t < 10
							&& !loader.client.getBank().withdraw1(ids[i]); t++)
						loader.sleep(random(500, 600));
					for (int t = 0; t < 10
							&& !loader.client.getInventory().contains(ids[i]); i++) {
						loader.sleep(random(500, 600));
					}
					break;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private double distanceTo(Entity e) {
		return Math.sqrt((loader.myX() - e.getX()) * (loader.myX() - e.getX())
				+ (loader.myY() - e.getY()) * (loader.myY() - e.getY()));
	}

	private RS2Object closestObjectForName(String name, String action) {
		RS2Object ret = null;
		try {
			double dist = 2000000000;
			for (RS2Object o : loader.client.getCurrentRegion().getObjects()) {
				if (debug && o != null && o.getName().equals(name)
						&& o.getDefinition() != null
						&& o.getDefinition().getActions() != null) {
					// System.out.print("Found " + o.getName() +
					// " with actions ");
					// printList(o.getDefinition().getActions());
					// System.out.println(" and distance " + distanceTo(o));
				}
				if (o != null
						&& o.getName().equals(name)
						&& distanceTo(o) < dist
						&& containsInArray(o.getDefinition().getActions(),
								action)) {
					dist = distanceTo(o);
					ret = o;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (ret == null) {
			ret = closestObjectForName2(name, action);
		}
		return ret;
	}

	private RS2Object closestObjectForName2(String name, String action) {

		RS2Object ret = null;
		try {
			double dist = 2000000000;
			for (RS2Object o : loader.client.getCurrentRegion().getObjects()) {
				if (o != null
						&& distanceTo(o) < dist
						&& containsInArray(o.getDefinition().getActions(),
								action)) {
					dist = distanceTo(o);
					ret = o;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	private boolean containsInArray(String[] arr, String needle) {
		// if (debug)
		// System.out.print("Searching for " + needle + " in list ");
		// if (debug)
		// printList(arr);
		// if (debug)
		// System.out.println();
		for (String s : arr) {
			if (s != null
					&& s.contains(needle.substring(1, needle.length() - 1))) {
				// if (debug)
				// System.out.println("Found");
				return true;
			}
		}
		return false;
	}

	private RS2Object getNearestObstacle() {
		// if (debug)
		// System.out.println("Scanning...");
		for (RS2Object o : loader.client.getCurrentRegion().getObjects()) {
			if (o.getName().equals("Rock") || o.getName().equals("Boil")
					|| o.getName().equals("Tendrils")
					|| o.getName().equals("Gap") || o.getName().equals("Eyes")) {
				if (debug)
					System.out.print("Found " + o.getName() + " with ID "
							+ o.getId() + " with actions ");
				printList(o.getDefinition().getActions());
				// if (debug)
				// System.out.println(" and id " + o.getDefinition().getId()
				// + " " + o.getDefinition().getRealId());
			}
		}
		return null;
	}

	private void printList(String[] arr) {
		for (String s : arr) {
			if (s != null) {
				System.out.print(" " + s + ", ");
			}
		}
	}

	private boolean containsOneOf(int... ids) {
		for (int i = 0; i < ids.length; i++) {
			try {
				if (loader.client.getInventory().contains(ids[i])
						|| loader.equipmentTab.isWieldingWeapon(ids[i]))
					return true;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	private boolean jagexModNearby() {

		List<Player> players = loader.client.getLocalPlayers();
		for (Player p : players) {
			if (p.getName().contains("Mod") && loader.distance(p) < 20)
				return true;
		}
		return false;
	}

	private String pad(int i) {
		String ret = i + "";
		if (ret.length() == 1) {
			return "0" + ret;
		}
		return ret;
	}

	public void onPaint(Graphics g) {
		if (canStart) {
			if (hide) {
				g.setColor(Color.BLUE);
				g.fillOval(492, 346, 28, 28);
				return;
			}
			switch (paintI) {
			case 0:
				g.drawImage(paintImages[paintI], -20, 205, null);
				g.setColor(Color.RED);
				String status = state.toString();
				if (antiban)
					status = "ANTIBAN";
				g.drawString("Current State: " + status, 12, 400);
				g.drawString("Logged in as: " + loader.myPlayer().getName()
						+ ".", 12, 415);
				double hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
				int minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
				int seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
				g.drawString("Runtime: " + "Running for: " + pad((int) (hours))
						+ ":" + pad(minutes) + ":" + pad(seconds), 12, 430);

				gainedExp = loader.client.getSkills().getExperience(
						Skill.RUNECRAFTING)
						- startExp;
				g.drawString("Rate: " + (int) (crafted / hours) + " Runes/HR",
						12, 445);
				g.drawString("Crafted " + (crafted) + " Runes", 12, 460);
				break;
			case 1:
				g.drawImage(paintImages[1], -20, 205, null);
				g.setColor(Color.RED);
				hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
				minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
				seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
				gainedExp = loader.client.getSkills().getExperience(
						Skill.RUNECRAFTING)
						- startExp;
				g.drawString("Current Runecrafting Level: "
						+ loader.client.getSkills()
								.getLevel(Skill.RUNECRAFTING), 12, 400);
				g.drawString("Experience Gained: " + gainedExp, 12, 415);
				g.drawString("Experience to Level: "
						+ experienceToNextLevel(Skill.RUNECRAFTING), 12, 430);
				double r = ((gainedExp) / (seconds + minutes * 60 + hours * 3600.0));
				if (r == 0)
					r = .00000001;
				int TTL = (int) (experienceToNextLevel(Skill.RUNECRAFTING) / r);
				TTL /= 2;
				hours = (TTL) / 3600;
				minutes = (int) (((TTL) / 60.0) % 60);
				seconds = (int) ((TTL) % 60);
				g.drawString("Time To Level: " + pad((int) hours) + ":"
						+ pad(minutes) + ":" + pad(seconds), 12, 445);
				double perc = percentToNextLevel(Skill.RUNECRAFTING);
				g.setColor(new Color(0, 1, 0, 0.75f));
				g.fillRect(15 + offsetX, 475 + offsetY, (int) (500 * perc), 17);
				g.setColor(new Color(1, 1, 1, 0.75f));
				g.fillRect(15 + offsetX, 475 + offsetY, 490, 8);
				g.setColor(new Color(.5f, .5f, .5f, 0.75f));
				g.fillRect(15 + offsetX, 484 + offsetY, 490, 8);
				g.setColor(Color.RED);
				g.drawString((int) (perc * 100) + "% : "
						+ experienceToNextLevel(Skill.RUNECRAFTING) + " TNL",
						200, 489 + offsetY);
				break;
			case 2:
				g.drawImage(paintImages[2], -20, 205, null);
				g.setColor(Color.RED);
				g.drawString("CMHAbyss Version " + currentVersion, 12, 400);
				g.drawString("Script Developed by Ericthecmh", 12, 415);
				break;
			case 3:
				g.drawImage(paintImages[3], -20, 205, null);
				g.setColor(Color.RED);
				g.drawString("My Email: info@cmhscripts.com", 12, 400);
				g.drawString(
						"or leave feedback on the OSBot Forum Thread under SDN->Runecrafting->CMHAbyss",
						12, 430);
				g.drawString("Any and all feedback is welcome.", 12, 445);
				break;
			}
			// g.drawString((antiban ? "ANTIBAN" : state.toString()), 100, 100);
			// gainedExp =
			// loader.client.getSkills().getExperience(Skill.RUNECRAFTING)
			// - startExp;
			// g.drawString("Experience Gained: " + gainedExp, 100, 180);
			// mined = gainedExp / 5;
			// double hours = (System.currentTimeMillis() - startTime) / 1000.0
			// / 3600;
			// g.drawString("" + (int) (mined / hours) + " ESS/HR", 100, 120);
			// g.drawString("" + (mined) + " Mined ess", 100, 140);
			// int minutes = (int) (((System.currentTimeMillis() - startTime) /
			// 1000.0 / 60.0) % 60);
			// int seconds = (int) (((System.currentTimeMillis() - startTime) /
			// 1000.0) % 60);
			// g.drawString("Running for: " + (int) (hours) + ":" + minutes +
			// ":"
			// + seconds, 100, 160);
			// g.drawString("Current Mining Level: "
			// + loader.client.getSkills().getLevel(Skill.RUNECRAFTING), 100,
			// 200);
		}
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getX() >= 9 && e.getX() <= 75 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 0;
		} else if (e.getX() >= 75 && e.getX() <= 152 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 1;
		} else if (e.getX() >= 152 && e.getX() <= 220 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 2;
		} else if (e.getX() >= 220 && e.getX() <= 296 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 3;
		}
		if (loader.distance(e.getX(), e.getY(), 506, 358) <= 14) {
			hide = !hide;
		}
	}

	// Custom Methods

	private void loadSettings(CMHAbyssGUI gui) {
		String filename;
		if (System.getProperty("os.name").startsWith("Window")) {
			filename = "\\OSBot\\scripts\\CMHAbyss.settings" + settingsVersion;
		} else {
			filename = "/OSBot/scripts/CMHAbyss.settings" + settingsVersion;
		}
		try {
			Scanner scan = new Scanner(new File(System.getProperty("user.home")
					+ filename));
			gui.mineRocks.setSelected(Boolean.parseBoolean(scan.nextLine()));
			gui.chopTendrils.setSelected(Boolean.parseBoolean(scan.nextLine()));
			gui.distractEyes.setSelected(Boolean.parseBoolean(scan.nextLine()));
			gui.squeezeGaps.setSelected(Boolean.parseBoolean(scan.nextLine()));
			gui.burnBoils.setSelected(Boolean.parseBoolean(scan.nextLine()));
			gui.craftAt.setSelectedIndex(Integer.parseInt(scan.nextLine()));
			gui.teleportMethod.setSelectedIndex(Integer.parseInt(scan
					.nextLine()));
			gui.eatFood.setSelected(Boolean.parseBoolean(scan.nextLine()));
			gui.foodName.setText(scan.nextLine());
			gui.hpHealed.setText(scan.nextLine());
			gui.unknown.setSelected(Boolean.parseBoolean(scan.nextLine()));
			gui.unknown.setEnabled(gui.eatFood.isEnabled());
			gui.hpEatAt.setText(scan.nextLine());
			gui.hpHealed.setEnabled(gui.eatFood.isSelected()
					&& !gui.unknown.isSelected());
			gui.foodName.setEnabled(gui.eatFood.isSelected());
			gui.hpEatAt.setEnabled(gui.eatFood.isSelected());
			gui.pureEss.setSelected(Boolean.parseBoolean(scan.nextLine()));
			gui.runAt.setText(scan.nextLine());
			gui.emTeleport.setText(scan.nextLine());
			gui.toggleRunInner
					.setSelected(Boolean.parseBoolean(scan.nextLine()));
			gui.drinkPots.setSelected(Boolean.parseBoolean(scan.nextLine()));
			gui.energyDrinkAt.setText(scan.nextLine());
			gui.energyDrinkAt.setEnabled(gui.drinkPots.isSelected());
			gui.takeFood.setSelected(Boolean.parseBoolean(scan.nextLine()));
			gui.takeFoodNum.setText(scan.nextLine());
			gui.takeFoodNum.setEnabled(gui.takeFood.isSelected());
			scan.close();
		} catch (Exception e) {

		}
	}

	private void saveSettings(CMHAbyssGUI gui) {
		String filename;
		if (System.getProperty("os.name").startsWith("Window")) {
			filename = "\\OSBot\\scripts\\CMHAbyss.settings" + settingsVersion;
		} else {
			filename = "/OSBot/scripts/CMHAbyss.settings" + settingsVersion;
		}
		try {
			PrintStream ps = new PrintStream(System.getProperty("user.home")
					+ filename);
			ps.println(gui.mineRocks.isSelected());
			ps.println(gui.chopTendrils.isSelected());
			ps.println(gui.distractEyes.isSelected());
			ps.println(gui.squeezeGaps.isSelected());
			ps.println(gui.burnBoils.isSelected());
			ps.println(gui.craftAt.getSelectedIndex());
			ps.println(gui.teleportMethod.getSelectedIndex());
			ps.println(gui.eatFood.isSelected());
			ps.println(gui.foodName.getText());
			ps.println(gui.hpHealed.getText());
			ps.println(gui.unknown.isSelected());
			ps.println(gui.hpEatAt.getText());
			ps.println(gui.pureEss.isSelected());
			ps.println(gui.runAt.getText());
			ps.println(gui.emTeleport.getText());
			ps.println(gui.toggleRunInner.isSelected());
			ps.println(gui.drinkPots.isSelected());
			ps.println(gui.energyDrinkAt.getText());
			ps.println(gui.takeFood.isSelected());
			ps.println(gui.takeFoodNum.getText());
			ps.close();
		} catch (Exception e) {

		}
	}

	private double distanceTo(Position p) {
		Position myP = loader.myPosition();
		return Math.sqrt((p.getX() - myP.getX()) * (p.getX() - myP.getX())
				+ (p.getY() - myP.getY()) * (p.getY() - myP.getY()));
	}

	private RS2Object closestObjectTo(int[] id, Position p) {
		List<RS2Object> objects = loader.client.getCurrentRegion().getObjects();
		RS2Object object = null;
		int dist = 20000000;
		for (RS2Object o : objects) {
			if (o.getId() == id[0] || o.getId() == id[1]) {
				if (object == null) {
					object = o;
					dist = loader.distance(p.getX(), p.getY(), object.getX(),
							object.getY());
				} else {
					if (loader.distance(p.getX(), p.getY(), o.getX(), o.getY()) < dist) {
						dist = loader.distance(p.getX(), p.getY(), o.getX(),
								o.getY());
						object = o;
					}
				}
			}
		}
		return object;
	}

	private void walkLong(Position p, boolean emEnabled) {
		if (debug)
			System.out
					.println("Walking to (" + p.getX() + "," + p.getY() + ")");
		while (distanceTo(p) > 4) {
			walk2(p, emEnabled);
			if (emEnabled
					&& loader.client.getSkills().getCurrentLevel(
							Skill.HITPOINTS) < emTeleportThresh) {
				if (loader.client.getInventory().contains(foodName)) {
					try {
						for (int i = 0; i < 3
								&& !loader.client.getInventory()
										.interactWithName(foodName, "Eat"); i++)
							loader.sleep(random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					loader.log("Emergency Teleport");
					state = State.TELEPORT2;
				}
				return;
			}
		}

	}

	private boolean bankContainsPouches() {
		for (int id : pouchesToUse) {
			if (loader.client.getBank().contains(id))
				return true;
		}
		return false;
	}

	private void getPouches() {
		try {
			loader.client.getBank().depositAll();
			for (int id : pouchesToUse) {
				if (loader.client.getBank().contains(id))
					loader.client.getBank().withdraw1(id);
			}
		} catch (Exception e) {
		}
	}

	private void walkLong(Entity e, boolean emEnabled) {
		if (debug)
			System.out
					.println("Walking to (" + e.getX() + "," + e.getY() + ")");
		while (distanceTo(e) > 4) {
			walk2(e, emEnabled);
			if (emEnabled
					&& loader.client.getSkills().getCurrentLevel(
							Skill.HITPOINTS) < emTeleportThresh) {
				if (loader.client.getInventory().contains(foodName)) {
					try {
						for (int i = 0; i < 3
								&& !loader.client.getInventory()
										.interactWithName(foodName, "Eat"); i++)
							loader.sleep(random(500, 600));
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else {
					loader.log("Emergency Teleport");
					state = State.TELEPORT2;
				}
				return;
			}
		}

	}

	private void walk2(Entity e, boolean emEnabled) {
		double distance = 2000000000.0;
		Position closest = null;
		int radius = random(9, 13);
		for (int dx = -radius; dx < radius; dx++) {
			for (int dy = -radius; dy < radius; dy++) {
				if (dx * dx + dy * dy > radius * radius)
					continue;
				Position p = new Position(loader.myX() + dx, loader.myY() + dy,
						0);
				if (loader.canReach(p) && distanceBetween(p, e) < distance) {
					distance = distanceBetween(p, e);
					closest = p;
				}
			}
		}
		try {
			walkMiniMap2NoBlock(closest);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for (int i = 0; i < 10 && !loader.myPlayer().isMoving(); i++) {
			try {
				loader.sleep(random(250, 300));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		if (emEnabled
				&& loader.client.getSkills().getCurrentLevel(Skill.HITPOINTS) < emTeleportThresh) {
			if (loader.client.getInventory().contains(foodName)) {
				try {
					for (int j = 0; j < 3
							&& !loader.client.getInventory().interactWithName(
									foodName, "Eat"); j++)
						loader.sleep(random(500, 600));
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				loader.log("Emergency Teleport");
				state = State.TELEPORT2;
			}
			return;
		}
		for (int i = 0; i < 10
				&& distanceTo(closest) > (loader.isRunning() ? 10 : 7); i++) {
			try {
				loader.sleep(random(500, 600));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (emEnabled
					&& loader.client.getSkills().getCurrentLevel(
							Skill.HITPOINTS) < emTeleportThresh) {
				if (loader.client.getInventory().contains(foodName)) {
					try {
						for (int j = 0; j < 3
								&& !loader.client.getInventory()
										.interactWithName(foodName, "Eat"); j++)
							loader.sleep(random(500, 600));
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else {
					loader.log("Emergency Teleport");
					state = State.TELEPORT2;
				}
				return;
			}
		}
	}

	private void walk2Avoid(Entity e, boolean emEnabled) {
		double distance = 2000000000.0;
		Position closest = null;
		int radius = random(9, 13);
		for (int dx = -radius; dx < radius; dx++) {
			for (int dy = -radius; dy < radius; dy++) {
				if (dx * dx + dy * dy > radius * radius)
					continue;
				Position p = new Position(loader.myX() + dx, loader.myY() + dy,
						0);
				if (loader.canReach(p)
						&& distanceBetween(p, e) < distance
						&& distanceBetween(
								loader.closestObjectForName("Abyssal rift")
										.getPosition(), p) > 7) {
					distance = distanceBetween(p, e);
					closest = p;
				}
			}
		}
		try {
			walkMiniMap2(closest);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	private void walk2(Position e, boolean emEnabled) {
		double distance = 2000000000.0;
		Position closest = null;
		int radius = random(9, 13);
		for (int dx = -radius; dx < radius; dx++) {
			for (int dy = -radius; dy < radius; dy++) {
				if (dx * dx + dy * dy > radius * radius)
					continue;
				Position p = new Position(loader.myX() + dx, loader.myY() + dy,
						0);
				if (loader.canReach(p) && distanceBetween(p, e) < distance) {
					distance = distanceBetween(p, e);
					closest = p;
				}
			}
		}
		try {
			walkMiniMap2NoBlock(closest);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for (int i = 0; i < 10 && !loader.myPlayer().isMoving(); i++) {
			try {
				loader.sleep(random(250, 300));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		if (emEnabled
				&& loader.client.getSkills().getCurrentLevel(Skill.HITPOINTS) < emTeleportThresh) {
			if (loader.client.getInventory().contains(foodName)) {
				try {
					for (int j = 0; j < 3
							&& !loader.client.getInventory().interactWithName(
									foodName, "Eat"); j++)
						loader.sleep(random(500, 600));
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				loader.log("Emergency Teleport");
				state = State.TELEPORT2;
			}
			return;

		}
		for (int i = 0; i < 10
				&& distanceTo(closest) > (loader.isRunning() ? 10 : 7); i++) {
			try {
				loader.sleep(random(500, 600));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (emEnabled
					&& loader.client.getSkills().getCurrentLevel(
							Skill.HITPOINTS) < emTeleportThresh) {
				if (loader.client.getInventory().contains(foodName)) {
					try {
						for (int j = 0; j < 3
								&& !loader.client.getInventory()
										.interactWithName(foodName, "Eat"); j++)
							loader.sleep(random(500, 600));
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else {
					loader.log("Emergency Teleport");
					state = State.TELEPORT2;
				}
				return;
			}
		}
	}

	private void walkWide(Position e, boolean emEnabled) {
		double distance = 2000000000.0;
		Position closest = null;
		int radius = random(9, 13);
		for (int dx = -radius; dx < radius; dx++) {
			for (int dy = -radius; dy < radius; dy++) {
				if (dx * dx + dy * dy > radius * radius)
					continue;
				Position p = new Position(loader.myX() + dx, loader.myY() + dy,
						0);
				if (loader.canReach(p) && distanceBetween(p, e) < distance) {
					distance = distanceBetween(p, e);
					closest = p;
				}
			}
		}
		try {
			walkMiniMap2(closest);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for (int i = 0; i < 10 && !loader.myPlayer().isMoving(); i++) {
			try {
				loader.sleep(random(250, 300));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (emEnabled
					&& loader.client.getSkills().getCurrentLevel(
							Skill.HITPOINTS) < emTeleportThresh) {
				if (loader.client.getInventory().contains(foodName)) {
					try {
						for (int j = 0; j < 3
								&& !loader.client.getInventory()
										.interactWithName(foodName, "Eat"); j++)
							loader.sleep(random(500, 600));
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else {
					loader.log("Emergency Teleport");
					state = State.TELEPORT2;
				}
				return;
			}
		}
		for (int i = 0; i < 10
				&& distanceTo(closest) > (loader.isRunning() ? 10 : 7); i++) {
			try {
				loader.sleep(random(500, 600));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (emEnabled
					&& loader.client.getSkills().getCurrentLevel(
							Skill.HITPOINTS) < emTeleportThresh) {
				if (loader.client.getInventory().contains(foodName)) {
					try {
						for (int j = 0; j < 3
								&& !loader.client.getInventory()
										.interactWithName(foodName, "Eat"); j++)
							loader.sleep(random(500, 600));
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else {
					loader.log("Emergency Teleport");
					state = State.TELEPORT2;
				}
				return;
			}
		}
	}

	private double distanceBetween(Position p, Position e) {
		return Math.sqrt((p.getX() - e.getX()) * (p.getX() - e.getX())
				+ (p.getY() - e.getY()) * (p.getY() - e.getY()));
	}

	private double distanceBetween(Entity p, Entity e) {
		return Math.sqrt((p.getX() - e.getX()) * (p.getX() - e.getX())
				+ (p.getY() - e.getY()) * (p.getY() - e.getY()));
	}

	private double distanceBetween(Position p, Entity e) {
		return Math.sqrt((p.getX() - e.getX()) * (p.getX() - e.getX())
				+ (p.getY() - e.getY()) * (p.getY() - e.getY()));
	}

	private RS2Object closestObjectTo(int id, Position p) {
		List<RS2Object> objects = loader.client.getCurrentRegion().getObjects();
		RS2Object object = null;
		int dist = 20000000;
		for (RS2Object o : objects) {
			if (o.getId() == id) {
				if (object == null) {
					object = o;
					dist = loader.distance(p.getX(), p.getY(), object.getX(),
							object.getY());
				} else {
					if (loader.distance(p.getX(), p.getY(), o.getX(), o.getY()) < dist) {
						dist = loader.distance(p.getX(), p.getY(), o.getX(),
								o.getY());
						object = o;
					}
				}
			}
		}
		return object;
	}

	private RS2Object findObjectByName(String name) {
		List<RS2Object> objects = loader.client.getCurrentRegion().getObjects();
		RS2Object object = null;
		int dist = 20000000;
		for (RS2Object p : objects) {
			if (p.getName().contains(name)) {
				if (object == null) {
					object = p;
					dist = loader.distance(loader.myX(), loader.myY(),
							object.getX(), object.getY());
				} else {
					if (loader.distance(loader.myX(), loader.myY(), p.getX(),
							p.getY()) < dist) {
						dist = loader.distance(loader.myX(), loader.myY(),
								p.getX(), p.getY());
						object = p;
					}
				}
			}
		}
		return object;
	}

	private double percentToNextLevel(Skill skill) {
		int level = loader.client.getSkills().getLevel(skill);
		double total = 0.0;
		total = xpForLevels[level] - xpForLevels[level - 1];
		return 1 - ((experienceToNextLevel(skill) + 0.0) / total);
	}

	/***************** MADE BY ABIBOT *******************/
	int[] xpForLevels = new int[] { 0, 83, 174, 276, 388, 512, 650, 801, 969,
			1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470,
			5018, 5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363,
			14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648,
			37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014,
			91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040,
			203254, 224466, 247886, 273742, 302288, 333804, 368599, 407015,
			449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257,
			992895, 1096278, 1210421, 1336443, 1475581, 1629200, 1798808,
			1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792,
			3972294, 4385776, 4842295, 5346332, 5902831, 6517253, 7195629,
			7944614, 8771558, 9684577, 10692629, 11805606, 13034431 };

	private int experienceToNextLevel(Skill skill) {
		int xp = loader.client.getSkills().getExperience(skill);
		for (int i = 0; i < 99; i++) {
			if (xp < xpForLevels[i]) {
				return (xpForLevels[i] - xp);
			}
		}
		return (200000000 - xp);
	}

	/***************** END MADE BY ABIBOT *******************/

	private Position randomize(Position p) {
		return new Position(p.getX() + random(1, 2 * CONST + 1) - (CONST + 1),
				p.getY() + random(1, 2 * CONST + 1) - (CONST + 1), 0);
	}

	private Position randomize2(Position p) {
		return new Position(p.getX() + random(1, 2 * 2 + 1) - (2), p.getY()
				+ random(1, 2 * 2 + 1) - (2), 0);
	}

	private void setRunning2(boolean on) {
		try {
			for (int i = 0; i < 10 && !loader.openTab(Tab.SETTINGS); i++) {
				sleep(random(500, 600));
			}

			RS2Interface inte = loader.client.getInterface(261);
			if (inte != null) {
				RS2InterfaceChild child = inte.getChild(53);
				if (child != null) {
					child.hover();
					sleep(random(1000, 1100));
					child.interact();
					for (int i = 0; i < 3 && !loader.isRunning(); i++)
						sleep(random(350, 500));
				}
			}
		} catch (Exception e) {

		}
		return;
	}

	// Custom Classes

	private class PortalThread implements Runnable {
		private Position p;

		public PortalThread(Position p) {
			this.p = p;
		}

		public void run() {
			try {
				if (!p.interact(loader.getBot(), "Exit"))
					p.interact(loader.getBot(), "Use");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private class AntibanThread implements Runnable {

		@Override
		public void run() {
			while (true) {
				if (scriptStopped) {
					break;
				}/*
				 * if (state == State.MINE && !locked &&
				 * loader.myPlayer().getAnimation() != -1 && Math.random() <
				 * antibanTrigger &&
				 * (loader.client.getInventory().getAmount(essID) +
				 * loader.client.getInventory().getAmount( pureEssID) < 25)) {
				 * antiban = true; locked = true; double action = Math.random();
				 * if (action < 0.3) {
				 * 
				 * try { if (loader.currentTab() != Tab.SKILLS) {
				 * loader.openTab(Tab.SKILLS); loader.sleep(random(500, 700)); }
				 * RS2Interface inte = loader.client.getInterface(320);
				 * RS2InterfaceChild mining = inte.getChild((Math .random() >
				 * 0.5 ? 5 : 6)); mining.hover(); loader.sleep(random(2500,
				 * 5000)); } catch (InterruptedException e) {
				 * e.printStackTrace(); }
				 * 
				 * } else if (action < 0.65) { try {
				 * loader.client.rotateCameraPitch(random(80, 90));
				 * loader.client.rotateCameraToAngle(random(0, 360));
				 * loader.sleep(random(1000, 2000)); } catch
				 * (InterruptedException e) { // TODO Auto-generated catch block
				 * e.printStackTrace(); } } else { Rectangle k = new Rectangle(1
				 * + random(515), 1 + random(330), 10, 10); try {
				 * loader.client.moveMouseTo(new RectangleDestination( k),
				 * false, false, false); } catch (InterruptedException e) { //
				 * TODO Auto-generated catch block e.printStackTrace(); } }
				 * locked = false; antiban = false; } try { Thread.sleep(1000);
				 * } catch (InterruptedException e) { // TODO Auto-generated
				 * catch block e.printStackTrace(); }
				 */
			}
		}
	}

	private class Vector {
		private double x, y;

		public Vector(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public double dotProd(Vector v) {
			return this.x * v.x + this.y * v.y;
		}

		public double degreeBetween(Vector v) {
			double dot = dotProd(v);
			double cos = dot / (mag() + v.mag());
			if (this.x == 0 && this.y < 0)
				return 180;
			if (this.x == 0 && this.y > 0)
				return 0;
			if (this.x < 0 && this.y > 0)
				return Math.toDegrees(Math.acos(cos));
			if (this.x < 0 && this.y < 0)
				return Math.toDegrees(Math.acos(cos));
			if (this.x > 0 && this.y < 0)
				return 360 - Math.toDegrees(Math.acos(cos));
			if (this.x > 0 && this.y > 0)
				return 360 - Math.toDegrees(Math.acos(cos));
			return 0;
		}

		public double mag() {
			return Math.sqrt(x * x + y * y);
		}
	}

	private class Area {
		private double minX, minY;
		private double maxX, maxY;

		private Area(double d, double e, double f, double g) {
			if (d > f) {
				minX = f;
				maxX = d;
			} else {
				maxX = f;
				minX = d;
			}
			if (e > g) {
				minY = g;
				maxY = e;
			} else {
				maxY = g;
				minY = e;
			}
			// if (debug)
			// System.out.println("Max X: " + maxX + " Min X: " + minX
			// + " Min Y: " + minY + " Max Y: " + maxY);
		}

		public boolean currentlyInArea() {
			return isInArea(loader.myPosition());
		}

		public boolean isInArea(Position p) {
			return (p.getX() >= minX && p.getX() <= maxX && p.getY() >= minY && p
					.getY() <= maxY);
		}

	}

	char rot = '\0';
	int time = 0;

	private void rotateCameraToAngle(int angle) {
		boolean right = false;
		for (int di = 0; di < 180; di++) {
			if ((loader.client.getCameraYawAngle() + di) % 360 == angle)
				right = true;
		}
		if (right) {
			rot = (char) KeyEvent.VK_RIGHT;
			int di;
			for (di = 0; di < 180; di++) {
				if ((loader.client.getCameraYawAngle() + di) % 360 == angle) {
					break;
				}
			}
			time = (int) (di / ROT * 1000);
		} else {
			rot = (char) KeyEvent.VK_LEFT;
			int di;
			for (di = 0; di <= 180; di++) {
				if ((loader.client.getCameraYawAngle() - di + 720) % 360 == angle) {
					break;
				}
			}
			time = (int) (di / ROT * 1000);
		}
		new Thread(new Runnable() {
			public void run() {
				try {
					loader.client.typeKey(rot, 0, time + 20, false);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
	}

	class CMHAbyssGUI extends javax.swing.JFrame {

		/**
		 * Creates new form CMHAbyssGUI
		 */

		public boolean ready;

		public CMHAbyssGUI() {
			initComponents();
		}

		/**
		 * This method is called from within the constructor to initialize the
		 * form. WARNING: Do NOT modify this code. The content of this method is
		 * always regenerated by the Form Editor.
		 */
		@SuppressWarnings("unchecked")
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		private void initComponents() {

			jLabel1 = new javax.swing.JLabel();
			mineRocks = new javax.swing.JCheckBox();
			jLabel2 = new javax.swing.JLabel();
			chopTendrils = new javax.swing.JCheckBox();
			jLabel3 = new javax.swing.JLabel();
			distractEyes = new javax.swing.JCheckBox();
			squeezeGaps = new javax.swing.JCheckBox();
			burnBoils = new javax.swing.JCheckBox();
			jLabel4 = new javax.swing.JLabel();
			jLabel5 = new javax.swing.JLabel();
			craftAt = new javax.swing.JComboBox();
			jLabel6 = new javax.swing.JLabel();
			teleportMethod = new javax.swing.JComboBox();
			eatFood = new javax.swing.JCheckBox();
			jLabel7 = new javax.swing.JLabel();
			foodName = new javax.swing.JTextField();
			hpHealed = new javax.swing.JTextField();
			jLabel8 = new javax.swing.JLabel();
			unknown = new javax.swing.JCheckBox();
			jButton1 = new javax.swing.JButton();
			hpEatAt = new javax.swing.JTextField();
			jLabel9 = new javax.swing.JLabel();
			pureEss = new javax.swing.JCheckBox();
			jLabel10 = new javax.swing.JLabel();
			runAt = new javax.swing.JTextField();
			emTeleport = new javax.swing.JTextField();
			jLabel11 = new javax.swing.JLabel();
			toggleRunInner = new javax.swing.JCheckBox();
			drinkPots = new javax.swing.JCheckBox();
			energyDrinkAt = new javax.swing.JTextField();
			jLabel12 = new javax.swing.JLabel();
			takeFood = new javax.swing.JCheckBox();
			jLabel13 = new javax.swing.JLabel();
			takeFoodNum = new javax.swing.JTextField();

			setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

			jLabel1.setText("Obstacles");

			mineRocks.setText("Mine through Rock");

			jLabel2.setText("Will take the cheapest pickaxe in bank");

			chopTendrils.setText("Chop through tendrils");

			jLabel3.setText("Will take the cheapest hatchet in bank");

			distractEyes.setText("Distract Eyes");

			squeezeGaps.setText("Squeeze through gaps");

			burnBoils.setText("Burn away boils");

			jLabel4.setText("Will take a tinderbox from the bank");

			jLabel5.setText("Craft at:");

			craftAt.setModel(new javax.swing.DefaultComboBoxModel(new String[] {
					"Nature rift", "Cosmic rift", "Blood rift", "Fire rift",
					"Earth rift", "Body rift", "Mind rift", "Air rift",
					"Soul rift", "Water rift", "Death rift", "Law rift",
					"Chaos rift" }));

			jLabel6.setText("Teleport Method:");

			teleportMethod.setModel(new javax.swing.DefaultComboBoxModel(
					new String[] { "Mounted Amulet of Glory (House teletab)",
							"Worn Amulet of Glory", "Varrock teletab" }));
			teleportMethod
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(
								java.awt.event.ActionEvent evt) {
							teleportMethodActionPerformed(evt);
						}
					});

			eatFood.setText("Eat food to restore health");
			eatFood.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					eatFoodActionPerformed(evt);
				}
			});

			jLabel7.setText("Name of Food:");

			foodName.setEnabled(false);

			hpHealed.setEnabled(false);

			jLabel8.setText("# HP healed by food:");

			unknown.setText("Unknown");
			unknown.setEnabled(false);
			unknown.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					unknownActionPerformed(evt);
				}
			});

			jButton1.setText("Start");
			jButton1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton1ActionPerformed(evt);
				}
			});

			hpEatAt.setEnabled(false);

			jLabel9.setText("HP to eat at:");

			pureEss.setText("Use Pure Essence");

			jLabel10.setText("Turn on run at (turns on before entering Wildy):");

			runAt.setText("30");

			emTeleport.setText("20");

			jLabel11.setText("HP to emergency teleport:");

			toggleRunInner
					.setText("Turn off run when entering inner loop of Abyss");

			drinkPots.setText("");
			drinkPots.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					drinkPotsActionPerformed(evt);
				}
			});

			drinkPots.setEnabled(true);

			energyDrinkAt.setText("70");
			energyDrinkAt.setEnabled(true);

			jLabel12.setText("Energy to drink at:");

			takeFood.setText("Take food:");
			takeFood.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					takeFoodActionPerformed(evt);
				}
			});

			jLabel13.setText("# to take:");

			takeFoodNum.setText("0");
			takeFoodNum.setEnabled(false);

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
					getContentPane());
			getContentPane().setLayout(layout);
			layout.setHorizontalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addContainerGap()
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.LEADING)
													.addGroup(
															layout.createSequentialGroup()
																	.addComponent(
																			jLabel5)
																	.addGap(44,
																			44,
																			44)
																	.addComponent(
																			craftAt,
																			0,
																			javax.swing.GroupLayout.DEFAULT_SIZE,
																			Short.MAX_VALUE))
													.addComponent(
															jButton1,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															Short.MAX_VALUE)
													.addGroup(
															layout.createSequentialGroup()
																	.addComponent(
																			jLabel6)
																	.addPreferredGap(
																			javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																	.addComponent(
																			teleportMethod,
																			0,
																			javax.swing.GroupLayout.DEFAULT_SIZE,
																			Short.MAX_VALUE))
													.addGroup(
															layout.createSequentialGroup()
																	.addComponent(
																			jLabel11)
																	.addGap(21,
																			21,
																			21)
																	.addComponent(
																			emTeleport))
													.addGroup(
															layout.createSequentialGroup()
																	.addComponent(
																			jLabel10)
																	.addPreferredGap(
																			javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																	.addComponent(
																			runAt))
													.addGroup(
															layout.createSequentialGroup()
																	.addGap(21,
																			21,
																			21)
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING)
																					.addComponent(
																							jLabel7)
																					.addComponent(
																							jLabel8)
																					.addComponent(
																							jLabel9))
																	.addPreferredGap(
																			javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING)
																					.addComponent(
																							hpEatAt)
																					.addGroup(
																							layout.createSequentialGroup()
																									.addComponent(
																											hpHealed)
																									.addPreferredGap(
																											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																									.addComponent(
																											unknown))
																					.addComponent(
																							foodName)))
													.addGroup(
															layout.createSequentialGroup()
																	.addComponent(
																			drinkPots)
																	.addGap(34,
																			34,
																			34)
																	.addComponent(
																			jLabel12)
																	.addPreferredGap(
																			javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																	.addComponent(
																			energyDrinkAt))
													.addGroup(
															layout.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
																	.addGroup(
																			layout.createSequentialGroup()
																					.addComponent(
																							takeFood)
																					.addGap(18,
																							18,
																							18)
																					.addComponent(
																							jLabel13)
																					.addGap(10,
																							10,
																							10)
																					.addComponent(
																							takeFoodNum))
																	.addComponent(
																			jLabel1)
																	.addGroup(
																			layout.createSequentialGroup()
																					.addGap(10,
																							10,
																							10)
																					.addGroup(
																							layout.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																									.addComponent(
																											distractEyes)
																									.addGroup(
																											layout.createSequentialGroup()
																													.addGroup(
																															layout.createParallelGroup(
																																	javax.swing.GroupLayout.Alignment.LEADING)
																																	.addComponent(
																																			chopTendrils)
																																	.addComponent(
																																			burnBoils)
																																	.addComponent(
																																			mineRocks))
																													.addGap(18,
																															18,
																															18)
																													.addGroup(
																															layout.createParallelGroup(
																																	javax.swing.GroupLayout.Alignment.LEADING,
																																	false)
																																	.addComponent(
																																			jLabel2,
																																			javax.swing.GroupLayout.DEFAULT_SIZE,
																																			javax.swing.GroupLayout.DEFAULT_SIZE,
																																			Short.MAX_VALUE)
																																	.addComponent(
																																			jLabel4)
																																	.addComponent(
																																			jLabel3,
																																			javax.swing.GroupLayout.PREFERRED_SIZE,
																																			1,
																																			Short.MAX_VALUE)))
																									.addComponent(
																											squeezeGaps)))
																	.addComponent(
																			pureEss)
																	.addComponent(
																			eatFood)
																	.addComponent(
																			toggleRunInner)))
									.addContainerGap()));
			layout.setVerticalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addContainerGap()
									.addComponent(jLabel1)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(mineRocks)
													.addComponent(jLabel2))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(chopTendrils)
													.addComponent(jLabel3))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(distractEyes)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(squeezeGaps)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(burnBoils)
													.addComponent(jLabel4))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel5)
													.addComponent(
															craftAt,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(pureEss)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel6)
													.addComponent(
															teleportMethod,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addGap(6, 6, 6)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(
															jLabel11,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															17,
															javax.swing.GroupLayout.PREFERRED_SIZE)
													.addComponent(
															emTeleport,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(eatFood)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel7)
													.addComponent(
															foodName,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(
															hpHealed,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE)
													.addComponent(jLabel8)
													.addComponent(unknown))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel9)
													.addComponent(
															hpEatAt,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel10)
													.addComponent(
															runAt,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(toggleRunInner)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(drinkPots)
													.addComponent(jLabel12)
													.addComponent(
															energyDrinkAt,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(takeFood)
													.addComponent(jLabel13)
													.addComponent(
															takeFoodNum,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(jButton1)
									.addContainerGap(
											javax.swing.GroupLayout.DEFAULT_SIZE,
											Short.MAX_VALUE)));

			pack();
		}// </editor-fold>

		private void eatFoodActionPerformed(java.awt.event.ActionEvent evt) {
			foodName.setEnabled(eatFood.isSelected());
			hpHealed.setEnabled(eatFood.isSelected() && !unknown.isSelected());
			unknown.setEnabled(eatFood.isSelected());
			hpEatAt.setEnabled(eatFood.isSelected());
		}

		private void unknownActionPerformed(java.awt.event.ActionEvent evt) {
			hpHealed.setEnabled(eatFood.isSelected() && !unknown.isSelected());
		}

		private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
			ready = true;
		}

		private void teleportMethodActionPerformed(
				java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void drinkPotsActionPerformed(java.awt.event.ActionEvent evt) {
			energyDrinkAt.setEnabled(drinkPots.isSelected());
		}

		private void takeFoodActionPerformed(java.awt.event.ActionEvent evt) {
			takeFoodNum.setEnabled(takeFood.isSelected());
		}

		// Variables declaration - do not modify
		private javax.swing.JCheckBox burnBoils;
		private javax.swing.JCheckBox chopTendrils;
		private javax.swing.JComboBox craftAt;
		private javax.swing.JCheckBox distractEyes;
		private javax.swing.JCheckBox drinkPots;
		private javax.swing.JCheckBox eatFood;
		private javax.swing.JTextField emTeleport;
		private javax.swing.JTextField energyDrinkAt;
		private javax.swing.JTextField foodName;
		private javax.swing.JTextField hpEatAt;
		private javax.swing.JTextField hpHealed;
		private javax.swing.JButton jButton1;
		private javax.swing.JLabel jLabel1;
		private javax.swing.JLabel jLabel10;
		private javax.swing.JLabel jLabel11;
		private javax.swing.JLabel jLabel12;
		private javax.swing.JLabel jLabel13;
		private javax.swing.JLabel jLabel2;
		private javax.swing.JLabel jLabel3;
		private javax.swing.JLabel jLabel4;
		private javax.swing.JLabel jLabel5;
		private javax.swing.JLabel jLabel6;
		private javax.swing.JLabel jLabel7;
		private javax.swing.JLabel jLabel8;
		private javax.swing.JLabel jLabel9;
		private javax.swing.JCheckBox mineRocks;
		private javax.swing.JCheckBox pureEss;
		private javax.swing.JTextField runAt;
		private javax.swing.JCheckBox squeezeGaps;
		private javax.swing.JCheckBox takeFood;
		private javax.swing.JTextField takeFoodNum;
		private javax.swing.JComboBox teleportMethod;
		private javax.swing.JCheckBox toggleRunInner;
		private javax.swing.JCheckBox unknown;
		// End of variables declaration
	}

	class CMHAbyssLogin extends javax.swing.JPanel {

		public boolean isAuth;
		public boolean isPrem;
		public BufferedReader in;
		public PrintWriter out;
		public String username;
		public final String name = "CMHAbyss";
		public boolean ready;

		/**
		 * Creates new form CMHStrongholdLogin
		 */
		public CMHAbyssLogin() {
			initComponents();
			String filename;
			if (System.getProperty("os.name").startsWith("Window")) {
				filename = "\\OSBot\\scripts\\CMHAbyss.user";
			} else {
				filename = "/OSBot/scripts/CMHAbyss.user";
			}
			try {
				Scanner scan = new Scanner(new File(
						System.getProperty("user.home") + filename));
				usernameField.setText(scan.nextLine());
				passwordField.setText(scan.nextLine());
				scan.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		/**
		 * This method is called from within the constructor to initialize the
		 * form. WARNING: Do NOT modify this code. The content of this method is
		 * always regenerated by the Form Editor.
		 */
		@SuppressWarnings("unchecked")
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		private void initComponents() {

			jLabel1 = new javax.swing.JLabel();
			usernameField = new javax.swing.JTextField();
			jLabel2 = new javax.swing.JLabel();
			passwordField = new javax.swing.JPasswordField();
			jButton1 = new javax.swing.JButton();
			jButton2 = new javax.swing.JButton();

			setName("Login"); // NOI18N

			jLabel1.setText("Username:");

			jLabel2.setText("Password:");

			jButton1.setText("Login");
			jButton1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton1ActionPerformed(evt);
				}
			});

			jButton2.setText("Create Account");
			jButton2.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton2ActionPerformed(evt);
				}
			});

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
			this.setLayout(layout);
			layout.setHorizontalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							javax.swing.GroupLayout.Alignment.TRAILING,
							layout.createSequentialGroup()
									.addContainerGap(81, Short.MAX_VALUE)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.LEADING,
													false)
													.addComponent(
															jButton1,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															Short.MAX_VALUE)
													.addGroup(
															layout.createSequentialGroup()
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING)
																					.addComponent(
																							jLabel1)
																					.addComponent(
																							jLabel2))
																	.addGap(18,
																			18,
																			18)
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING,
																					false)
																					.addComponent(
																							usernameField)
																					.addComponent(
																							passwordField,
																							javax.swing.GroupLayout.PREFERRED_SIZE,
																							162,
																							javax.swing.GroupLayout.PREFERRED_SIZE)))
													.addComponent(
															jButton2,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															255,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addGap(67, 67, 67)));
			layout.setVerticalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addGap(72, 72, 72)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel1)
													.addComponent(
															usernameField,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel2)
													.addComponent(
															passwordField,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
									.addComponent(jButton1)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(jButton2)
									.addContainerGap(82, Short.MAX_VALUE)));
		}// </editor-fold>

		private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {

			username = usernameField.getText();
			String password = new String(passwordField.getPassword());
			String filename;
			if (System.getProperty("os.name").startsWith("Window")) {
				filename = "\\OSBot\\scripts\\CMHEssMiner.user";
			} else {
				filename = "/OSBot/scripts/CMHEssMiner.user";
			}
			try {
				PrintStream ps = new PrintStream(
						System.getProperty("user.home") + filename);
				ps.println(username);
				ps.println(password);
				ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Socket sock = null;
			try {
				if (username.equals("ericthecmh1")
						|| username.equals("marvin57"))
					sock = new Socket("54.211.36.47", 8088);
				else
					sock = new Socket(((Math.random() > 0.5) ? "54.226.90.112"
							: "54.211.36.47"), 8088);
			} catch (UnknownHostException e) {

				return;
			} catch (IOException ex) {

				return;
			}
			try {
				in = new BufferedReader(new InputStreamReader(
						sock.getInputStream()));
				out = new PrintWriter(sock.getOutputStream(), true);
			} catch (IOException e) {
			}

			long apple = Encrypt.seconds();
			String ack = null;
			double sh = 0.0, rh;
			out.println("login");
			out.println(name);
			out.println(username);
			out.println(apple + "");
			try {
				while ((ack = in.readLine()) == null) {
				}
				try {
					rh = Encrypt.getHash(apple);
					sh = Double.parseDouble(ack);
					if (Math.abs(rh - sh) > .00001)
						throw (new Exception());
				} catch (Exception e) {
					username = null;
					isAuth = false;
					return;
				}
			} catch (Exception e) {
			}
			try {
				out.println(Encrypt.encrypt(sh + "", password));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {

				while ((ack = in.readLine()) == null) {
				}
				ack = Encrypt.decrypt(sh + "", ack);
				if (ack.startsWith("AuthFail")) {
					isAuth = false;
					isPrem = false;
				} else if (ack.startsWith("AuthAck")) {
					int i = ack.charAt(ack.length() - 1) - 48;
					isPrem = (i == 1);
					isAuth = true;
					ready = true;
				} else {
					isAuth = false;
					isPrem = false;
				}
			} catch (Exception e) {
			}
			if (isAuth) {
				JOptionPane.showMessageDialog(null,
						"You have been successfully logged in as a "
								+ (isPrem ? "premium user." : "normal user."));
			} else {
				JOptionPane.showMessageDialog(null,
						"Please check your username and password");
				username = null;
			}
			try {
				in.close();
			} catch (Exception e) {
			}
			try {
				out.close();
			} catch (Exception e) {
			}
			try {
				sock.close();
			} catch (Exception e) {
			}
		}

		private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
			username = usernameField.getText();
			if (username.length() == 0) {
				JOptionPane.showMessageDialog(null, "Your username is empty!");
				return;
			}
			String password = new String(passwordField.getPassword());
			// Using a JPanel as the message for the JOptionPane
			JPanel userPanel = new JPanel();
			userPanel.setLayout(new GridLayout(1, 2));
			JLabel passwordLbl = new JLabel("Password:");

			JPasswordField passwordFld = new JPasswordField();
			userPanel.add(passwordLbl);
			userPanel.add(passwordFld);

			JOptionPane.showMessageDialog(null, userPanel,
					"Confirm your password:", JOptionPane.PLAIN_MESSAGE);

			String confirmPassword = new String(passwordFld.getPassword());
			String email = JOptionPane.showInputDialog(null,
					"Email (Don't worry I won't sell it):");
			if (!password.equals(confirmPassword)) {
				JOptionPane.showMessageDialog(null, "Passwords don't match!");
				return;
			}
			Socket sock = null;
			try {
				sock = new Socket(((Math.random() > 0.5) ? "54.226.90.112"
						: "54.211.36.47"), 8088);
			} catch (UnknownHostException e) {
				return;
			} catch (IOException ex) {
				// System.out.println("IOException when connecting to server");
				return;
			}
			try {
				in = new BufferedReader(new InputStreamReader(
						sock.getInputStream()));
				out = new PrintWriter(sock.getOutputStream(), true);
			} catch (IOException e) {
			}
			out.println("create");
			out.println(name);
			out.println(username);
			out.println(password);
			out.println(email);
			String ack = null;
			try {
				while ((ack = in.readLine()) == null) {
				}
				if (ack.startsWith("CreateFailed")) {
					isAuth = false;
					isPrem = false;
					while ((ack = in.readLine()) == null) {
					}
					JOptionPane.showMessageDialog(null, ack);
				} else {
					isPrem = false;
					isAuth = false;
					ready = false;
					JOptionPane.showMessageDialog(null,
							"Your account has been created.");
				}
			} catch (IOException e) {
			}
		}

		// Variables declaration - do not modify
		private javax.swing.JButton jButton1;
		private javax.swing.JButton jButton2;
		private javax.swing.JLabel jLabel1;
		private javax.swing.JLabel jLabel2;
		private javax.swing.JPasswordField passwordField;
		private javax.swing.JTextField usernameField;
		// End of variables declaration
	}

}