public abstract class Action implements Runnable {

	private final String ID;

	protected int priority;
	protected boolean mustFinish;
	protected boolean allowLower;
	protected CMHScript script;
	protected String interrupter;
	protected long interrupterSetTime;
	private LogWindow log;

	protected boolean isRunning;

	protected Action(String id, int p, boolean al, LogWindow log) {
		ID = id;
		priority = p;
		allowLower = al;
		this.log = log;
		log.addAction(ID);
	}

	public int getPriority() {
		return this.priority;
	}

	public boolean mustFinish() {
		return this.mustFinish;
	}

	public boolean allowLower() {
		return this.allowLower;
	}
	
	public String getID(){
		return this.ID;
	}
	
	public void setScript(CMHScript script){
		this.script = script;
	}
	
	public void setRunning(boolean running){
		isRunning = running;
		log.setRunning(getID(), running);
	}
	
	public boolean isRunning(){
		return isRunning;
	}
	
	public void setInterrupter(String ID){
		interrupter = ID;
		interrupterSetTime = System.currentTimeMillis();
	}
	
	public abstract boolean shouldActivate(CMHScript script);
}
