import java.util.ArrayList;

public class ActionsManager {

	private ArrayList<Action> actions;
	private ActionThread[] runnables;
	private Thread[] threads;
	private CMHScript script;
	private boolean interrupted;

	public ActionsManager(CMHScript s) {
		this.script = s;
		runnables = new ActionThread[5];
		threads = new Thread[5];
		for (int i = 0; i < 5; i++) {
			runnables[i] = new ActionThread(script, i);
			threads[i] = new Thread(runnables[i]);
			threads[i].start();
		}
		actions = new ArrayList<Action>();
	}

	public boolean isBlocked() {
		return interrupted;
	}

	private ArrayList<Action> getNextActions() {
		if (interrupted) {
			return null;
		}
		boolean freeThreads = false;
		for (ActionThread t : runnables) {
			if (!t.isRunning()
					|| (t.getAction() == null || !t.getAction().mustFinish()))
				freeThreads = true;
		}
		if (!freeThreads) {
			return null;
		}
		ArrayList<Action> nextActions = new ArrayList<Action>();
		int maxPriority = -1;
		boolean allowLower = true;
		for (Action a : actions) {
			if (a.shouldActivate(script)) {
				System.out.println(a.getID() + " should activate. Running: "
						+ a.isRunning());
				/*
				 * if (a.getPriority() > maxPriority) { if (a.allowLower()) {
				 * nextActions.add(a); } else { allowLower = false; nextActions
				 * = new ArrayList<Action>(); nextActions.add(a); } } else if
				 * (a.getPriority() == maxPriority) { if (!a.allowLower())
				 * allowLower = false; nextActions.add(a); } else if
				 * (allowLower) { nextActions.add(a); } String s =
				 * "Current actions: "; for (Action action : nextActions) s +=
				 * action.getID() + ","; if (script.log.debug.isSelected()) { //
				 * script.log.logString(s); }
				 */
				if (!a.isRunning())
					nextActions.add(a);
			}
		}
		return nextActions;
	}

	public void releaseInterrupt() {
		interrupted = false;
	}

	public void requestInterruptBlock(String ID) {
		interrupted = true;
		for (ActionThread s : runnables) {
			try {
				s.notifyInterrupt(ID);
			} catch (Exception e) {

			}
		}
		for (int i = 0; i < threads.length; i++) {
			if (runnables[i].getAction() != null
					&& runnables[i].getAction().getID().equals(ID))
				continue;
			script.log.removeThreadAction(i, "");
			threads[i].interrupt();
		}
	}

	public void requestInterrupt(String ID) {
		for (ActionThread s : runnables) {
			try {
				s.notifyInterrupt(ID);
			} catch (Exception e) {

			}
		}
		for (int i = 0; i < threads.length; i++) {
			if (runnables[i].getAction() != null
					&& runnables[i].getAction().getID().equals(ID))
				continue;
			script.log.removeThreadAction(i, "");
			threads[i].interrupt();
		}
	}

	public void executeNextActions() {
		// If an action has requested interrupt, hold all other actions
		System.out.println("Determining next actions... Blocked: "
				+ interrupted);
		if (interrupted) {
			return;
		}
		if (script.s.randomManager.getCurrent() != null)
			return;
		ArrayList<Action> nextActions = getNextActions();
		System.out.println("nextActions == null: " + (nextActions == null));
		if (nextActions != null)
			System.out.println("Next Actions #: " + nextActions.size());
		if (nextActions == null || nextActions.size() == 0) {
			return;
		}
		String s = "Next actions are: ";
		for (Action a : nextActions) {
			s += a.getID();
		}
		System.out.println(s);
		int ai = 0;
		if (nextActions != null && nextActions.size() > 0) {
			for (int i = 0; i < runnables.length && ai < nextActions.size(); i++) {
				if ((!runnables[i].isRunning() || (runnables[i].getAction() == null || !runnables[i]
						.getAction().mustFinish())) && !interrupted) {
					System.out.println("Assigning "
							+ nextActions.get(ai).getID() + " to thread " + i);
					threads[i].interrupt();
					runnables[i].setAction(nextActions.get(ai++));
				}
			}
		}
	}

	public void notifyStopped() {
		for (int i = 0; i < runnables.length; i++) {
			runnables[i].notifyStopped();
		}
		requestInterrupt("STOP");
		for (Thread s : threads) {
			s.interrupt();
			s.stop();
		}
	}

	public void addAction(Action a) {
		actions.add(a);
	}

}
