import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;

import org.osbot.script.rs2.skill.Skill;

public class Test {
	static class Print implements Runnable {

		@Override
		public void run() {
			try {
				while (true) {
					System.out.println("AAOEU");
				}
			} catch (Exception e) {
			}
		}
	}

	static Print p;

	static HashMap<String, PKerPair> blacklist;

	private static void getBlacklist() {
		blacklist = new HashMap<String, PKerPair>();
		try {
			URL url = new URL(
					"http://cmhabyss.cmhscripts.com/getblacklist2.php");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String s = null;
			PKerPair pp = new PKerPair();
			String name = null;
			int count = 0;
			while (true) {
				while ((s = br.readLine()) == null) {
				}
				if (s.equals("DONE")) {
					break;
				} else {
					count++;
					System.out.println(s + " " + count);
					if (count == 1) {
						name = s;
						System.out.println("Name: " + name);
					} else if (count == 2) {
						pp.level = Integer.parseInt(s);
						System.out.println("Level: " + pp.level);
					} else if (count == 3) {
						pp.delta = Integer.parseInt(s);
						System.out.println("Delta: " + pp.delta);
						count = 0;
						blacklist.put(name, pp);
						pp = new PKerPair();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			blacklist = null;
		}
	}

	public static void main(String args[]) {
		blacklist = new HashMap<String, PKerPair>();
		getBlacklist();
		for(String name : blacklist.keySet()){
			System.out.println(name + " " + blacklist.get(name).level + " " + blacklist.get(name).delta);
		}
	}
}
