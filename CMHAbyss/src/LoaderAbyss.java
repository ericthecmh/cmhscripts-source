import java.awt.Graphics;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.osbot.script.Script;
import org.osbot.script.ScriptManifest;

@ScriptManifest(author = "Ericthecmh", info = "Crafts runes using the Abyss.", name = "CMHAbyss", version = 0)
public class LoaderAbyss extends Script {

	Script abyss = null;

	final String[] versions = new String[] { "3.9.0 (Newest)", "Rewrite 1.1.0"};

	public void onStart() {
		JFrame frame = new JFrame("Version Select");
		String scriptVersion = (String) JOptionPane.showInputDialog(frame,
				"Which version of the script do you want to use?",
				"Version Select", JOptionPane.QUESTION_MESSAGE, null, versions,
				versions[0]);
		if (scriptVersion.contains("3.9.0"))
			abyss = new CMHAbyss(this);
		else if (scriptVersion.contains("Rewrite"))
			abyss = new CMHScript(this);
		abyss.onStart();
	}

	public int onLoop() {
		// while (1 == 1) {
		//
		// log(myPlayer().getX() + "," + myPlayer().getY());
		// log(myPosition().getX() + "," + myPosition().getY());
		// try {
		// sleep(random(500,600));
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }

		try {
			return abyss.onLoop();
		} catch (Exception e) {
		}

		return random(300, 400);
	}

	public void onExit() throws InterruptedException {
		abyss.onExit();
	}

	public void onPaint(Graphics g) {
		abyss.onPaint(g);
	}

	public void mouseClicked(MouseEvent e) {
		abyss.mouseClicked(e);
	}

	public void onMessage(String s) {
		try {
			abyss.onMessage(s);
		} catch (Exception e) {

		}
	}

	public void onPlayerMessage(String s, String message)
			throws InterruptedException {
		abyss.onPlayerMessage(s, message);
	}

}
