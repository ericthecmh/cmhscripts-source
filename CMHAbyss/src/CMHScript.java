import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import org.osbot.script.MethodProvider;
import org.osbot.script.Script;
import org.osbot.script.mouse.MouseDestination;
import org.osbot.script.mouse.RectangleDestination;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.model.Item;
import org.osbot.script.rs2.model.NPC;
import org.osbot.script.rs2.model.Player;
import org.osbot.script.rs2.model.RS2Object;
import org.osbot.script.rs2.randoms.RandomBehaviourHook;
import org.osbot.script.rs2.randoms.RandomManager;
import org.osbot.script.rs2.skill.Skill;
import org.osbot.script.rs2.ui.Bank;
import org.osbot.script.rs2.ui.EquipmentSlot;
import org.osbot.script.rs2.ui.RS2Interface;
import org.osbot.script.rs2.ui.RS2InterfaceChild;
import org.osbot.script.rs2.ui.Tab;

//@ScriptManifest(author = "Ericthecmh", info = "Crafts runes using the Abyss.", name = "CMHAbyss", version = 0)
public class CMHScript extends Script {

	private final int pureEssID = 7936;
	private final int regEssID = 1436;
	private final int[][] allPotIDs = new int[][] {
			new int[] { 3014, 3012, 3010, 3008 },
			new int[] { 3022, 3020, 3018, 3016 } };
	private final int smallPouchID = 5509;
	private final int mediumPouchID = 5510, degradedMediumPouchID = 5511;
	private final int largePouchID = 5512, degradedLargePouchID = 5513;
	private final int giantPouchID = 5514, degradedGiantPouchID = 5515;
	private final int pickaxeID = 1265;
	private final int hatchetID = 1351;
	private final int tinderboxID = 590;
	// Abyss Obstacles
	private final int rockRealID = 25422;
	private final int tendrilsRealID = 25425;
	private final int boilRealID = 25590;
	private final int eyesRealID = 26146;
	private final int gapRealID = 25428;
	private final int passageRealID = 25381;
	private final int rockModelID = 7438;
	private final int passageModelID = 7434;
	private final int gapModelID = 7429;
	private final int eyesModelID = 7435;
	private final int boilModelID = 7431;
	private final int tendrilsModelID = 7442;
	private final int[] talkInterfaceParentIDs = new int[] { 64, 242, 232, 65,
			243, 228, 65 };
	private final String settingsVersion = "0.0";
	private final String currentVersion = "1.1.0";

	// Rifts
	private final String[] riftNames = new String[] { "Nature rift",
			"Fire rift", "Earth rift", "Cosmic rift", "Blood rift",
			"Body rift", "Air rift", "Mind rift", "Soul rift", "Water rift",
			"Death rift", "Law rift", "Chaos rift" };
	private final int[] rotationAngles = new int[] { 180, 270, 270, 270, 270,
			0, 0, 0, 90, 90, 90, 90, 180 };

	// World Hopper
	private final int[] blacklistedWorlds = new int[] { 308, 316, 307, 315,
			323, 324, 325, 331, 332, 337, 339, 340, 347, 348, 355, 356, 363,
			364, 371, 372 };

	private State state;
	private ActionsManager am;
	private int essID;
	private int healAt;
	private String foodName;
	private int[] energyPotIDs;
	private int energyRestoreAt;
	private boolean bringPots;
	private boolean useROL;
	private int teleportMethod;
	private int emergencyTeleportMethod;
	private boolean useSmallPouch;
	private boolean useMediumPouch;
	private boolean useLargePouch;
	private boolean useGiantPouch;
	private int riftIndex;
	private boolean bringFood;
	private int foodToBring;
	private int healthToTeleportAt;
	private long startTime;
	private int crafted;

	// PAINT
	private int paintI;
	private int gainedExp;
	private int startExp;
	private BufferedImage[] paintImages = new BufferedImage[4];
	private final int offsetX = -3, offsetY = -20;
	private boolean hide;

	// BLACKLIST
	HashMap<String, PKerPair> blacklist;

	// VARIABLES USED BY BANK ACTION
	private boolean smallPouchFilled;
	private boolean mediumPouchFilled;
	private boolean largePouchFilled;
	private boolean giantPouchFilled;

	// VARIABLES USED BY WALK_TO_DITCH AND WALK_TO_MAGE ACTION
	private Position destination;
	private int destinationi;

	// VARIABLES USED BY ENTER_WALL
	private int rotation;

	// VARIABLES USED BY MONITOR_STATUS
	private int healthEatAt;
	private int drinkPotsAt;

	// VARIABLES USED BY PKER_MONITOR
	private boolean teleblocked;
	private boolean hopWorlds;

	private boolean mineRocks;
	private boolean chopTendrils;
	private boolean burnBoils;
	private boolean squeezeGap;
	private boolean distractEyes;

	private boolean canStart;
	private boolean stopScript;
	public LogWindow log;

	private Thread randomMonitorThread;
	private boolean inventoryContainsPickaxeID;
	private boolean inventoryContainsHatchetID;
	private boolean inventoryContainsFoodName;
	private boolean inventoryContainsEnergyPotIDs;
	private boolean inventoryContainsRingOfLife;

	public Script s;

	public CMHScript(Script s) {
		this.s = s;
	}

	public State getState() {
		return this.state;
	}

	private void loadSettings(CMHScriptGUI gui) {
		String filename;
		if (System.getProperty("os.name").startsWith("Window")) {
			filename = "\\OSBot\\scripts\\CMHAbyss.settings" + settingsVersion;
		} else {
			filename = "/OSBot/scripts/CMHAbyss.settings" + settingsVersion;
		}
		try {
			Scanner scan = new Scanner(new File(System.getProperty("user.home")
					+ filename));
			gui.mineRocks.setSelected(scan.nextInt() == 1);
			gui.chopTendrils.setSelected(scan.nextInt() == 1);
			gui.distractEyes.setSelected(scan.nextInt() == 1);
			gui.squeezeGap.setSelected(scan.nextInt() == 1);
			gui.burnBoil.setSelected(scan.nextInt() == 1);
			gui.pureEss.setSelected(scan.nextInt() == 1);
			gui.regEss.setSelected(scan.nextInt() == 1);
			gui.riftSelection.setSelectedIndex(scan.nextInt());
			gui.teleportMethod.setSelectedIndex(scan.nextInt());
			gui.emergencyTeleportMethod.setSelectedIndex(scan.nextInt());
			scan.nextLine();
			gui.healthToTeleportAt.setText(scan.nextLine());
			gui.eatFood.setSelected(scan.nextInt() == 1);
			gui.bringFood.setSelected(scan.nextInt() == 1);
			scan.nextLine();
			gui.foodToBring.setText(scan.nextLine());
			gui.foodName.setText(scan.nextLine());
			gui.foodEatAt.setText(scan.nextLine());
			gui.drinkPots.setSelected(scan.nextInt() == 1);
			gui.bringPots.setSelected(scan.nextInt() == 1);
			gui.drinkRegPot.setSelected(scan.nextInt() == 1);
			gui.drinkSuperPot.setSelected(scan.nextInt() == 1);
			scan.nextLine();
			gui.drinkPotAt.setText(scan.nextLine());
			gui.emergencyTeleportMethod.setEnabled(gui.teleportMethod
					.getSelectedIndex() == 1);
			gui.jLabel9.setEnabled(gui.eatFood.isSelected());
			gui.jLabel10.setEnabled(gui.eatFood.isSelected());
			gui.foodName.setEnabled(gui.eatFood.isSelected());
			gui.foodEatAt.setEnabled(gui.eatFood.isSelected());
			gui.bringFood.setEnabled(gui.eatFood.isSelected());
			gui.foodToBring.setEnabled(gui.eatFood.isSelected()
					&& gui.bringFood.isSelected());
			gui.bringPots.setEnabled(gui.drinkPots.isSelected());
			gui.drinkRegPot.setEnabled(gui.drinkPots.isSelected());
			gui.drinkSuperPot.setEnabled(gui.drinkPots.isSelected());
			gui.drinkPotAt.setEnabled(gui.drinkPots.isSelected());
			gui.jLabel12.setEnabled(gui.drinkPots.isSelected());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveSettings(CMHScriptGUI gui) {
		String filename;
		if (System.getProperty("os.name").startsWith("Window")) {
			filename = "\\OSBot\\scripts\\CMHAbyss.settings" + settingsVersion;
		} else {
			filename = "/OSBot/scripts/CMHAbyss.settings" + settingsVersion;
		}
		try {
			PrintStream ps = new PrintStream(new File(
					System.getProperty("user.home") + filename));
			ps.println(gui.mineRocks.isSelected() ? 1 : 0);
			ps.println(gui.chopTendrils.isSelected() ? 1 : 0);
			ps.println(gui.distractEyes.isSelected() ? 1 : 0);
			ps.println(gui.squeezeGap.isSelected() ? 1 : 0);
			ps.println(gui.burnBoil.isSelected() ? 1 : 0);
			ps.println(gui.pureEss.isSelected() ? 1 : 0);
			ps.println(gui.regEss.isSelected() ? 1 : 0);
			ps.println(gui.riftSelection.getSelectedIndex());
			ps.println(gui.teleportMethod.getSelectedIndex());
			ps.println(gui.emergencyTeleportMethod.getSelectedIndex());
			ps.println(gui.healthToTeleportAt.getText());
			ps.println(gui.eatFood.isSelected() ? 1 : 0);
			ps.println(gui.bringFood.isSelected() ? 1 : 0);
			ps.println(gui.foodToBring.getText());
			ps.println(gui.foodName.getText());
			ps.println(gui.foodEatAt.getText());
			ps.println(gui.drinkPots.isSelected() ? 1 : 0);
			ps.println(gui.bringPots.isSelected() ? 1 : 0);
			ps.println(gui.drinkRegPot.isSelected() ? 1 : 0);
			ps.println(gui.drinkSuperPot.isSelected() ? 1 : 0);
			ps.println(gui.drinkPotAt.getText());
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onStart() {
		JOptionPane
				.showMessageDialog(
						null,
						"This is a new CMHAbyss script. Please make sure you have read the instructions in the original post on the forums");
		// Initialize and show log
		log = new LogWindow();
		log.setVisible(true);
		new Thread(new Runnable() {
			public void run() {
				getBlacklist();
			}
		}).start();
		log.logString("Loading paint...");
		new Thread(new Runnable() {
			public void run() {
				try {
					paintImages[0] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/main.png"));
					paintImages[1] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/stats.png"));
					paintImages[2] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/script.png"));
					paintImages[3] = ImageIO
							.read(new URL(
									"http://cmhscripts.no-ip.org/osbot/paint/feedback.png"));
				} catch (Exception e) {
				}
			}
		}).start();
		try {
			for (int i = 0; i < 10; i++)
				s.sleep(s.random(400, 500));
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// Initialize and show GUI
		CMHScriptGUI gui = new CMHScriptGUI();
		loadSettings(gui);
		gui.setVisible(true);
		// Wait until gui is finished
		while (gui.isVisible() && !gui.ready) {
			try {
				s.sleep(s.random(500, 600));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
		}
		if (gui.ready) {
			if (gui.regEss.isSelected()) {
				essID = regEssID;
			} else {
				essID = pureEssID;
			}
			if (gui.eatFood.isSelected()) {
				try {
					healAt = Integer.parseInt(gui.foodEatAt.getText());
				} catch (Exception e) {
					s.log("Error, cannot parse \"Health to eat at\" box.");
					log.logString("ERROR. CANNOT PARSE \"Health to eat at\" BOX.");
					stopScript = true;
				}
			} else {
				healAt = -100;// Essentially the same as saying
								// "Don't eat food"
			}
			foodName = gui.foodName.getText();
			if (gui.drinkPots.isSelected()) {
				try {
					energyRestoreAt = Integer
							.parseInt(gui.drinkPotAt.getText());
				} catch (Exception e) {
					s.log("Error, cannot parse \"Energy to drink at\" box.");
					log.logString("ERROR. CANNOT PARSE \"Energy to drink at\" BOX.");
					stopScript = true;
				}
			} else {
				energyRestoreAt = -100; // Essentially the same as saying
										// "Don't drink potions"
			}
			if (gui.drinkRegPot.isSelected()) {
				energyPotIDs = allPotIDs[0];
			} else {
				energyPotIDs = allPotIDs[1];
			}
			if (gui.bringFood.isSelected()) {
				try {
					foodToBring = Integer.parseInt(gui.foodToBring.getText());
				} catch (Exception e) {
					log.logString("Error, cannot parse GUI at \"Food to Bring\".");
					stopScript = true;
				}
			}
			try {
				healthToTeleportAt = Integer.parseInt(gui.healthToTeleportAt
						.getText());
			} catch (Exception e) {
				log.logString("Error, cannot parse GUI at \"Health to Teleport At\".");
				stopScript = true;
			}
			bringFood = gui.bringFood.isSelected();
			bringPots = gui.bringPots.isSelected();
			teleportMethod = gui.teleportMethod.getSelectedIndex();
			emergencyTeleportMethod = gui.emergencyTeleportMethod
					.getSelectedIndex();
			try {
				useROL = s.equipmentTab.getItemInSlot(EquipmentSlot.RING) != null
						&& s.equipmentTab.getItemInSlot(EquipmentSlot.RING)
								.getName().equals("Ring of Life");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mineRocks = gui.mineRocks.isSelected();
			chopTendrils = gui.chopTendrils.isSelected();
			burnBoils = gui.burnBoil.isSelected();
			distractEyes = gui.distractEyes.isSelected();
			squeezeGap = gui.squeezeGap.isSelected();
			riftIndex = gui.riftSelection.getSelectedIndex();
			saveSettings(gui);
			healthEatAt = s.client.getSkills().getLevel(Skill.HITPOINTS) / 2
					- 5 + MethodProvider.random(0, 10);
			drinkPotsAt = 50;
			startExp = s.client.getSkills().getExperience(Skill.RUNECRAFTING);
			canStart = true;
			gui.dispose();
		} else {
			canStart = true;
			stopScript = true;
		}
		if (s.client.getInventory().contains(smallPouchID)) {
			useSmallPouch = true;
		}
		if (s.client.getInventory().contains(mediumPouchID)
				|| s.client.getInventory().contains(degradedMediumPouchID)) {
			useMediumPouch = true;
		}
		if (s.client.getInventory().contains(largePouchID)
				|| s.client.getInventory().contains(degradedLargePouchID)) {
			useLargePouch = true;
		}
		if (s.client.getInventory().contains(giantPouchID)
				|| s.client.getInventory().contains(degradedGiantPouchID)) {
			useGiantPouch = true;
		}
		am = new ActionsManager(this);
		if (closestNPCForName("Dark mage") != null) {
			state = State.INNER_LOOP;
			if (useSmallPouch)
				smallPouchFilled = true;
			if (useMediumPouch)
				mediumPouchFilled = true;
			if (useLargePouch)
				largePouchFilled = true;
			if (useGiantPouch)
				giantPouchFilled = true;
		} else if (closestObjectForName("Amulet of Glory") != null) {
			state = State.HOUSE_GLORY;
		} else if (s.myY() > 3521) {
			state = State.WALK_TO_MAGE;
		} else
			state = State.BANK;
		// ADD BANK ACTION
		am.addAction(new Action("BANK", 1, false, log) {

			@Override
			public void run() {
				teleblocked = false;
				if (hopWorlds) {
					hopWorlds = false;
					hopWorlds();
				}
				try {
					// If there is an Amulet of glory, equip it
					if (script.getInventoryGlory(true) != null) {
						while (script.s.client.getBank().isOpen()) {
							try {
								script.s.client.getBank().close();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
							try {
								Thread.sleep(MethodProvider.random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						String glory = script.getInventoryGlory(false);
						try {
							for (int i = 0; i < 10
									&& !script.s.client.getInventory()
											.interactWithName(glory, "Wear"); i++) {
								script.s.sleep(MethodProvider.random(300, 600));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						for (int i = 0; i < 10
								&& script.s.equipmentTab
										.getItemInSlot(EquipmentSlot.AMULET) == null
								|| !script.s.equipmentTab
										.getItemInSlot(EquipmentSlot.AMULET)
										.getName().equals(glory); i++) {
							try {
								script.s.sleep(MethodProvider.random(300, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
					}

					// If there is a pickaxe in your inventory, equip it
					if (inventoryContainsPickaxeID) {
						// If the bank is open, close it!
						while (script.s.client.getBank().isOpen()) {
							try {
								script.s.client.getBank().close();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
							try {
								Thread.sleep(MethodProvider.random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						try {
							for (int i = 0; i < 10
									&& !script.s.client.getInventory()
											.interactWithId(pickaxeID, "Wield"); i++) {
								script.s.sleep(MethodProvider.random(300, 600));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						for (int i = 0; i < 10
								&& script.s.equipmentTab
										.getItemInSlot(EquipmentSlot.WEAPON) == null
								|| script.s.equipmentTab.getItemInSlot(
										EquipmentSlot.WEAPON).getId() != pickaxeID; i++) {
							try {
								script.s.sleep(MethodProvider.random(300, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
					}

					// If there is a hatchet in your inventory, and we aren't
					// mining, equip it
					if (inventoryContainsHatchetID && !mineRocks) {
						// If the bank is open, close it!
						while (script.s.client.getBank().isOpen()) {
							try {
								script.s.client.getBank().close();
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								// e1.printStackTrace();
							}
							try {
								Thread.sleep(MethodProvider.random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						try {
							for (int i = 0; i < 10
									&& !script.s.client.getInventory()
											.interactWithId(hatchetID, "Wield"); i++) {
								script.s.sleep(MethodProvider.random(300, 600));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						for (int i = 0; i < 10
								&& script.s.equipmentTab
										.getItemInSlot(EquipmentSlot.WEAPON) == null
								|| script.s.equipmentTab.getItemInSlot(
										EquipmentSlot.WEAPON).getId() != hatchetID; i++) {
							try {
								script.s.sleep(MethodProvider.random(300, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
					}

					// If there's food in your inventory, maybe we should eat
					// it?
					if (inventoryContainsFoodName && getHealth() <= healAt) {
						// While you still need to eat and there is still
						// food...
						while (getHealth() < script.s.client.getSkills()
								.getLevel(Skill.HITPOINTS)
								&& script.s.client.getInventory().contains(
										foodName)) {
							// Get the current health before eating
							int oldHealth = getHealth();
							// Boolean to see whether you successfully ate. If
							// true,
							// script will wait for your health to update. Else
							// it
							// won't
							boolean successfullyEaten = false;
							// If the bank is open, close it!
							while (script.s.client.getBank().isOpen()) {
								try {
									script.s.client.getBank().close();
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
								try {
									Thread.sleep(MethodProvider
											.random(500, 600));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
							// Try up to 10 times to eat the food
							for (int j = 0; j < 10; j++) {
								try {
									if (script.s.client.getInventory()
											.interactWithName(foodName, "Eat")) {
										successfullyEaten = true;
										break;
									}
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
								try {
									Thread.sleep(MethodProvider
											.random(300, 400));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
						}
					}

					// If there's a energy restore potion in your inventory,
					// maybe
					// we should drink it?
					if (inventoryContainsEnergyPotIDs
							&& script.s.client.getRunEnergy() <= energyRestoreAt) {
						// While you still need to drink potions and there are
						// still
						// potions...
						while (script.s.client.getRunEnergy() <= energyRestoreAt
								&& inventoryContainsOneOf(energyPotIDs)) {
							// Get the current energy level before drinking
							int oldEnergy = script.s.client.getRunEnergy();
							// Boolean to see whether you successfully drank. If
							// true, script will wait for your energy level to
							// update.
							// Else it won't
							boolean successfullyDrank = false;
							// If the bank is open, close it!
							while (script.s.client.getBank().isOpen()) {
								try {
									script.s.client.getBank().close();
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
								try {
									Thread.sleep(MethodProvider
											.random(500, 600));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
							// Try up to 10 times to drink the potion
							for (int i = 0; i < 10; i++) {
								try {
									if (script.s.client.getInventory()
											.interactWithId(
													getLowestEnergyPotID(),
													"Drink")) {
										successfullyDrank = true;
										break;
									}
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									// e1.printStackTrace();
								}
								try {
									Thread.sleep(MethodProvider
											.random(300, 400));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
							// If you have successfully eaten the food, wait
							// from 6
							// to 10 seconds for your health to update
							if (successfullyDrank) {
								for (int j = 0; j < 20
										&& script.s.client.getRunEnergy() == oldEnergy; j++) {
									try {
										Thread.sleep(MethodProvider.random(300,
												500));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
								}
							}
						}
					}

					// If there is ring of life in inventory, equip it.
					if (inventoryContainsRingOfLife) {
						try {
							for (int i = 0; i < 10
									&& !script.s.client.getInventory()
											.interactWithName("Ring of Life",
													"Equip"); i++)
								Thread.sleep(MethodProvider.random(300, 500));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
					}

					// If there are more than 3 ess in your inventory, and the
					// small
					// pouch hasn't been filled, fill it
					if (script.s.client.getInventory().getAmount(essID) >= 3
							&& useSmallPouch
							&& !smallPouchFilled
							&& script.s.client.getInventory().contains(
									"Small pouch")) {
						// If the bank is open, close it!
						while (script.s.client.getBank().isOpen()) {
							try {
								script.s.client.getBank().close();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
							try {
								Thread.sleep(MethodProvider.random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						// Get the amount of ess before filling the pouch
						int oldAmount = (int) script.s.client.getInventory()
								.getAmount(essID);
						try {
							for (int i = 0; i < 5
									&& !script.s.client.getInventory()
											.interactWithName("Small pouch",
													"Fill"); i++) {
								Thread.sleep(MethodProvider.random(300, 500));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						// Wait 2-4 seconds for the pouch to fill
						for (int i = 0; i < 10
								&& script.s.client.getInventory().getAmount(
										essID) == oldAmount; i++) {
							try {
								Thread.sleep(MethodProvider.random(200, 400));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						smallPouchFilled = true;
					}

					// If there are more than 6 ess in your inventory, and the
					// medium
					// pouch hasn't been filled, fill it
					if (script.s.client.getInventory().getAmount(essID) >= 6
							&& useMediumPouch
							&& !mediumPouchFilled
							&& script.s.client.getInventory().contains(
									"Medium pouch")) {
						// If the bank is open, close it!
						while (script.s.client.getBank().isOpen()) {
							try {
								script.s.client.getBank().close();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
							try {
								Thread.sleep(MethodProvider.random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						// Get the amount of ess before filling the pouch
						int oldAmount = (int) script.s.client.getInventory()
								.getAmount(essID);
						try {
							for (int i = 0; i < 5
									&& !script.s.client.getInventory()
											.interactWithName("Medium pouch",
													"Fill"); i++) {
								Thread.sleep(MethodProvider.random(300, 500));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						// Wait 2-4 seconds for the pouch to fill
						for (int i = 0; i < 10
								&& script.s.client.getInventory().getAmount(
										essID) == oldAmount; i++) {
							try {
								Thread.sleep(MethodProvider.random(200, 400));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						mediumPouchFilled = true;
					}

					// If there are more than 9 ess in your inventory, and the
					// large
					// pouch hasn't been filled, fill it
					if (script.s.client.getInventory().getAmount(essID) >= 9
							&& useLargePouch
							&& !largePouchFilled
							&& script.s.client.getInventory().contains(
									"Large pouch")) {
						// If the bank is open, close it!
						while (script.s.client.getBank().isOpen()) {
							try {
								script.s.client.getBank().close();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
							try {
								Thread.sleep(MethodProvider.random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						// Get the amount of ess before filling the pouch
						int oldAmount = (int) script.s.client.getInventory()
								.getAmount(essID);
						try {
							for (int i = 0; i < 5
									&& !script.s.client.getInventory()
											.interactWithName("Large pouch",
													"Fill"); i++) {
								Thread.sleep(MethodProvider.random(300, 500));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						// Wait 2-4 seconds for the pouch to fill
						for (int i = 0; i < 10
								&& script.s.client.getInventory().getAmount(
										essID) == oldAmount; i++) {
							try {
								Thread.sleep(MethodProvider.random(200, 400));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						largePouchFilled = true;
					}

					// If there are more than 12 ess in your inventory, and the
					// giant
					// pouch hasn't been filled, fill it
					if (script.s.client.getInventory().getAmount(essID) >= 12
							&& useGiantPouch
							&& !giantPouchFilled
							&& script.s.client.getInventory().contains(
									"Giant pouch")) {
						// If the bank is open, close it!
						while (script.s.client.getBank().isOpen()) {
							try {
								script.s.client.getBank().close();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
							try {
								Thread.sleep(MethodProvider.random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						// Get the amount of ess before filling the pouch
						int oldAmount = (int) script.s.client.getInventory()
								.getAmount(essID);
						try {
							for (int i = 0; i < 5
									&& !script.s.client.getInventory()
											.interactWithName("Giant pouch",
													"Fill"); i++) {
								Thread.sleep(MethodProvider.random(300, 500));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						// Wait 2-4 seconds for the pouch to fill
						for (int i = 0; i < 10
								&& script.s.client.getInventory().getAmount(
										essID) == oldAmount; i++) {
							try {
								Thread.sleep(MethodProvider.random(200, 400));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						giantPouchFilled = true;
					}

					// If the bank isn't open, open it
					if (!script.s.client.getBank().isOpen()) {
						// Get the closest bank booth object
						RS2Object bank = script
								.closestObjectForName("Bank booth");
						// If the bank booth isn't null, try up to 10 times to
						// open
						// it
						if (bank != null) {
							try {
								if (bank.interact("Bank")) {
									// If it's here, that means it either
									// failed, or
									// it's moving towards the bank booth.
									// We shall wait for it to stop moving
									for (int j = 0; j < 5
											&& !script.s.myPlayer().isMoving(); j++) {
										try {
											Thread.sleep(MethodProvider.random(
													200, 300));
										} catch (InterruptedException e) {

										}
									}
									while (script.s.myPlayer().isMoving()) {
										try {
											Thread.sleep(MethodProvider.random(
													200, 300));
										} catch (InterruptedException e) {

										}
									}
									for (int i = 0; i < 2
											&& !script.s.client.getBank()
													.isOpen(); i++) {
										Thread.sleep(MethodProvider.random(300,
												500));
									}
								}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
					}
					// If the bank is open
					if (script.s.client.getBank().isOpen()) {
						// If need to eat, then withdraw 10 food
						if (getHealth() <= healAt) {
							if (!script.s.client.getBank().contains(
									getIDFromBank(foodName))) {
								stopScript = true;
								script.s.log("Out of food");
								script.log.logString("Out of food");
								return;
							}
							while (script.s.client.getInventory()
									.getEmptySlots() < 20)
								try {
									script.s.client.getBank().depositAllExcept(
											smallPouchID, mediumPouchID,
											degradedMediumPouchID,
											largePouchID, degradedLargePouchID,
											giantPouchID, degradedGiantPouchID);
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									// e1.printStackTrace();
								}
							try {
								script.s.client.getBank().withdraw10(
										getIDFromBank(foodName));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						// If need to use restore potion, then w up to 5,
						// starting
						// at lowest dose
						if (script.s.client.getRunEnergy() <= energyRestoreAt) {
							// Counter to count number of potions withdrawn
							int withdrew = 0;
							boolean hasPotions = false;
							for (int i = 0; i < energyPotIDs.length
									&& withdrew < 5; i++) {
								if (script.s.client.getBank().contains(
										energyPotIDs[i])) {
									hasPotions = true;
								} else
									continue;
								try {
									script.s.log("Withdrawing "
											+ energyPotIDs[i]);
									script.s.client.getBank().withdraw10(
											energyPotIDs[i]);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
								withdrew += script.s.client.getInventory()
										.getAmount(energyPotIDs[i]);
							}
							if (!hasPotions) {
								stopScript = true;
								script.s.log("Out of potions");
								script.log.logString("Out of potions");
								return;
							}
						}

						if (!script.s.equipmentTab.isWearingItem(
								EquipmentSlot.RING, "Ring of Life") && useROL) {
							if (!script.s.client.getBank().contains(
									getIDFromBank("Ring of Life"))) {
								stopScript = true;
								script.s.log("Out of ROLs");
								script.log.logString("Out of ROLs");
								return;
							}
							if (script.s.client.getInventory().isFull())
								try {
									depositUnneededItems();
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									// e1.printStackTrace();
								}
							try {
								for (int i = 0; i < 5
										&& !script.s.client
												.getBank()
												.withdraw1(
														getIDFromBank("Ring of Life")); i++) {
									Thread.sleep(MethodProvider
											.random(300, 500));
								}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						// If needs to eat of restore run energy, restart the
						// bank
						// action and eat/restore
						if (getHealth() <= healAt
								|| script.s.client.getRunEnergy() <= energyRestoreAt
								|| !script.s.equipmentTab.isWearingItem(
										EquipmentSlot.RING, "Ring of Life")
								&& useROL) {
							return;
						}

						if (teleportMethod == 1 && !script.isWearingGlory()) {
							if (script.getBankGlory() == null) {
								stopScript = true;
								script.s.log("Out of glories");
								script.log.logString("Out of glories");
								return;
							}
							Item glory = script.getBankGlory();
							if (script.s.client.getInventory().isFull())
								try {
									depositUnneededItems();
								} catch (InterruptedException e) {
								}
							try {
								for (int i = 0; i < 5
										&& !script.s.client.getBank()
												.withdraw1(glory.getId()); i++) {
									Thread.sleep(MethodProvider
											.random(300, 500));
								}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}

						// Else, get the necessary materials
						// Get the pickaxe if mining rocks and you don't have it
						if (mineRocks
								&& (script.s.equipmentTab
										.getItemInSlot(EquipmentSlot.WEAPON) == null || script.s.equipmentTab
										.getItemInSlot(EquipmentSlot.WEAPON)
										.getId() != pickaxeID)
								&& !script.s.client.getInventory().contains(
										pickaxeID)) {
							if (!script.s.client.getBank().contains(pickaxeID)) {
								stopScript = true;
								s.log("Out of pickaxes");
								script.log.logString("Out of pickaxes");
								return;
							}
							if (script.s.client.getInventory().isFull())
								try {
									depositUnneededItems();
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									// e1.printStackTrace();
								}
							try {
								for (int i = 0; i < 5
										&& !script.s.client.getBank()
												.withdraw1(pickaxeID); i++) {
									Thread.sleep(MethodProvider
											.random(300, 500));
								}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}

						if (chopTendrils
								&& (script.s.equipmentTab
										.getItemInSlot(EquipmentSlot.WEAPON) == null || script.s.equipmentTab
										.getItemInSlot(EquipmentSlot.WEAPON)
										.getId() != hatchetID)
								&& !script.s.client.getInventory().contains(
										hatchetID)) {
							if (!script.s.client.getBank().contains(hatchetID)) {
								stopScript = true;
								script.s.log("Out of hatchets");
								script.log.logString("Out of hatchets");
								return;
							}
							if (script.s.client.getInventory().isFull()) {
								try {
									depositUnneededItems();
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
							try {
								for (int i = 0; i < 5
										&& !script.s.client.getBank()
												.withdraw1(hatchetID); i++) {
									Thread.sleep(MethodProvider
											.random(300, 500));
								}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}

						if (burnBoils
								&& !script.s.client.getInventory().contains(
										tinderboxID)) {
							if (!script.s.client.getBank()
									.contains(tinderboxID)) {
								stopScript = true;
								script.s.log("Out of tinderboxes");
								script.log.logString("Out of tinderboxes");
								return;
							}
							if (script.s.client.getInventory().isFull()) {
								try {
									depositUnneededItems();
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
							try {
								for (int i = 0; i < 5
										&& !script.s.client.getBank()
												.withdraw1(tinderboxID); i++) {
									Thread.sleep(MethodProvider
											.random(300, 500));
								}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}

						if (script.s.client.getInventory().contains(pickaxeID)
								|| script.s.client.getInventory().contains(
										hatchetID)
								&& !mineRocks
								|| script.getInventoryGlory(true) != null
								|| script.s.client.getInventory().contains(
										"Ring of Life")) {
							return;
						}

						// Finally, get the teleport method and the essence
						switch (teleportMethod) {
						// If using the Teleport to house method
						case 0:
							// and there are no teletabs in inventory
							if (!script.s.client.getInventory().contains(
									"Teleport to house")) {
								try {
									depositUnneededItems();
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									// e1.printStackTrace();
								}
								if (!script.s.client.getBank().contains(
										getIDFromBank("Teleport to house"))) {
									stopScript = true;
									script.s.log("Out of teletabs");
									script.log.logString("Out of teletabs");
									return;
								}
								try {
									for (int i = 0; i < 10
											&& !script.s.client
													.getBank()
													.withdraw1(
															getIDFromBank("Teleport to house")); i++) {
										Thread.sleep(MethodProvider.random(300,
												500));
									}
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
								break;
							}
							break;
						case 1:
							switch (emergencyTeleportMethod) {
							case 0:
								if (!script.s.client.getInventory().contains(
										"Varrock teleport")) {
									// TODO: Deposit unneeded items. Temporarily
									// depositing all items for now
									try {
										depositUnneededItems();
									} catch (InterruptedException e1) {
										// TODO Auto-generated catch block
										// e1.printStackTrace();
									}
									if (!script.s.client.getBank().contains(
											getIDFromBank("Varrock teleport"))) {
										stopScript = true;
										script.s.log("Out of teletabs");
										script.log.logString("Out of teletabs");
										return;
									}
									try {
										for (int i = 0; i < 10
												&& !script.s.client
														.getBank()
														.withdraw1(
																getIDFromBank("Varrock teleport")); i++) {
											Thread.sleep(MethodProvider.random(
													300, 500));
										}
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
									break;
								}
								break;
							case 1:
								if (!script.s.client.getInventory().contains(
										"Teleport to house")) {
									// TODO: Deposit unneeded items. Temporarily
									// depositing all items for now
									try {
										depositUnneededItems();
									} catch (InterruptedException e1) {
										// TODO Auto-generated catch block
										// e1.printStackTrace();
									}
									if (!script.s.client.getBank().contains(
											getIDFromBank("Teleport to house"))) {
										stopScript = true;
										script.s.log("Out of teletabs");
										script.log.logString("Out of teletabs");
										return;
									}
									try {
										for (int i = 0; i < 10
												&& !script.s.client
														.getBank()
														.withdraw1(
																getIDFromBank("Teleport to house")); i++) {
											Thread.sleep(MethodProvider.random(
													300, 500));
										}
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
									break;
								}
								break;
							}
						}

						// If you're bringing a potion, and there isn't one in
						// the
						// inventory, get one
						if (bringPots && !inventoryContainsOneOf(energyPotIDs)) {
							if (script.s.client.getInventory().isFull()) {
								try {
									depositUnneededItems();
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
							for (int i = energyPotIDs.length - 1; i >= 0; i--) {
								if (script.s.client.getBank().contains(
										energyPotIDs[i])) {
									try {
										script.s.client.getBank().withdraw1(
												energyPotIDs[i]);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
									break;
								}
							}
						}

						// If you're bringing a potion, and there isn't one in
						// the
						// inventory, get one
						if (bringFood
								&& script.s.client.getInventory().getAmount(
										foodName) < foodToBring) {
							if (script.s.client.getInventory().getEmptySlots()
									+ script.s.client.getInventory().getAmount(
											foodName) < foodToBring) {
								try {
									depositUnneededItems();
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
							script.s.client.getBank().withdrawX(
									getIDFromBank(foodName),
									foodToBring
											- (int) script.s.client
													.getInventory().getAmount(
															foodName));
						}

						// Get essence, if the inventory isn't full, or there
						// aren't
						// many essence in inventory (what are the other things
						// then...?)
						if ((!script.s.client.getInventory().isFull() || script.s.client
								.getInventory().getAmount(essID) < 15)
								&& inventoryContainsTeleportMethod()) {
							if (!script.s.client.getBank().contains(essID)) {
								stopScript = true;
								script.s.log("Out of essence");
								script.log.logString("Out of essence");
								return;
							}
							try {
								depositUnneededItems();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
							try {
								for (int i = 0; i < 10
										&& !script.s.client.getBank()
												.withdrawAll(essID); i++) {
									Thread.sleep(MethodProvider
											.random(300, 500));
								}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
							return;
						}
					} else {
						// Otherwise, just return
						return;
					}
				} catch (Exception e) {
					setRunning(false);
				}
			}

			@Override
			public boolean shouldActivate(CMHScript script) {
				if (script.state == State.BANK && !shouldBank(script)) {
					// We should go and walk to the DITCH now
					script.state = State.WALK_TO_DITCH;
					destination = new Position(0, 0, 0);
					destinationi = 0;
					am.requestInterrupt("State changed");
				}
				return script.state == State.BANK && shouldBank(script);
			}

			private boolean shouldBank(CMHScript script) {
				if (useSmallPouch && !smallPouchFilled) {
					return true;
				}
				if (useMediumPouch && !mediumPouchFilled) {
					return true;
				}
				if (useLargePouch && !largePouchFilled) {
					return true;
				}
				if (useGiantPouch && !giantPouchFilled) {
					return true;
				}
				if (!inventoryContainsTeleportMethod()) {
					return true;
				}
				try {
					if (mineRocks
							&& (script.s.equipmentTab
									.getItemInSlot(EquipmentSlot.WEAPON) == null || script.s.equipmentTab
									.getItemInSlot(EquipmentSlot.WEAPON)
									.getId() != pickaxeID)) {
						return true;
					}
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					if (chopTendrils
							&& ((script.s.equipmentTab
									.getItemInSlot(EquipmentSlot.WEAPON) == null || script.s.equipmentTab
									.getItemInSlot(EquipmentSlot.WEAPON)
									.getId() != hatchetID))
							&& !script.s.client.getInventory().contains(
									hatchetID)) {
						return true;
					}
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (burnBoils
						&& !script.s.client.getInventory()
								.contains(tinderboxID)) {
					return true;
				}
				if (script.s.client.getInventory().getAmount(essID) < 15
						|| !script.s.client.getInventory().isFull()) {
					return true;
				}
				try {
					if (useROL
							&& ((script.s.equipmentTab
									.getItemInSlot(EquipmentSlot.RING) == null || !script.s.equipmentTab
									.getItemInSlot(EquipmentSlot.WEAPON)
									.getName().equals("Ring of Life")))) {
						return true;
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (getHealth() < healAt) {
					return true;
				}
				if (script.s.client.getRunEnergy() < energyRestoreAt) {
					return true;
				}
				return false;
			}
		});

		// ADD WALK_TO_DITCH ACTION
		destination = new Position(0, 0, 0);
		destinationi = 0;
		am.addAction(new Action("WALK_TO_DITCH", 1, false, log) {

			// List of several tiles along the way
			Position[] tiles = new Position[] { new Position(3093, 3497, 0),
					new Position(3094, 3502, 0), new Position(3099, 3503, 0),
					new Position(3103, 3507, 0), new Position(3103, 3513, 0),
					new Position(3103, 3516, 0), new Position(3100, 3520, 0) };

			@Override
			public void run() {
				try {

					// Random threshold for the tile distance from current
					// position
					int threshold = 14;

					// Find the furthest tile that is within threshold
					int i = destinationi;
					for (; i < tiles.length; i++) {
						if (calcDistance(tiles[i]) > threshold) {
							break;
						}
					}
					i--;
					destination = new Position(tiles[i].getX()
							+ MethodProvider.random(0, 4) - 2, tiles[i].getY()
							+ MethodProvider.random(0, 4) - 2, 0);
					destinationi = i;
					// Click on minimap
					try {
						if (script.s.walkMiniMap(destination)) {
							if (script.s.client.getRunEnergy() > 30) {
								script.setRunning2(true);
							}
							// If successfully clicked destination, wait 3-5
							// seconds for player to start moving
							for (i = 0; i < 10
									&& !script.s.myPlayer().isMoving(); i++) {
								try {
									Thread.sleep(MethodProvider
											.random(300, 500));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
							if (script.s.client.getCameraYawAngle() > 10
									&& script.s.client.getCameraYawAngle() < 350) {
								new Thread(new Runnable() {
									public void run() {
										try {
											script.s.client
													.rotateCameraToAngle(0);
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
								}).start();
							}
							if (script.s.client.getCameraPitchAngle() < 40) {
								new Thread(new Runnable() {
									public void run() {
										try {
											script.s.client
													.rotateCameraPitch(45);
										} catch (Exception e) {
										}
									}
								}).start();
							}
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
				} catch (Exception e) {

				} finally {
					setRunning(false);
				}
			}

			@Override
			public boolean shouldActivate(CMHScript script) {
				if (script.state == State.WALK_TO_DITCH) {
					if (script.s.myPlayer().getY() >= 3512) {
						script.state = State.HOP_DITCH;
						am.requestInterrupt("State changed");
					}
				}
				return script.state == State.WALK_TO_DITCH
						&& (calcDistance(destination) < 8 || !s.myPlayer()
								.isMoving());
			}

		});

		am.addAction(new Action("HOP_DITCH", 1, false, log) {

			@Override
			public void run() {
				// Turn the camera to the north
				try {
					try {
						if (script.s.client.getCameraPitchAngle() < 45)
							script.s.client.rotateCameraPitch(50);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
					RS2Object ditch = closestObjectForName("Wilderness Ditch");
					if (ditch != null) {
						try {
							for (int i = 0; i < 10 && !ditch.interact("Cross"); i++) {
								Thread.sleep(MethodProvider.random(300, 500));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						for (int i = 0; i < 10
								&& !script.s.myPlayer().isMoving()
								&& script.s.myPlayer().getAnimation() == -1; i++) {
							try {
								Thread.sleep(MethodProvider.random(300, 500));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						if (script.s.myPlayer().getAnimation() != -1
								|| script.s.myPlayer().isMoving()) {
							script.s.client.rotateCameraPitch(script.s.client
									.getLowestAvailableCameraPitchAngle());
							for (int i = 0; i < 20
									&& script.s.myPlayer().getAnimation() == -1
									&& calcDistance(new Position(3105, 3556, 0)) < 100; i++) {
								try {
									Thread.sleep(MethodProvider
											.random(300, 500));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
							for (int i = 0; i < 10
									&& script.s.myPlayer().getAnimation() != -1
									&& calcDistance(new Position(3105, 3556, 0)) < 100; i++) {
								try {
									Thread.sleep(MethodProvider
											.random(300, 500));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
						}
					}
				} catch (Exception e) {

				} finally {
					setRunning(false);
				}
			}

			@Override
			public boolean shouldActivate(CMHScript script) {
				if (script.state == State.HOP_DITCH) {
					if (script.s.myY() > 3521) {
						destination = new Position(0, 0, 0);
						destinationi = 0;
						script.state = State.WALK_TO_MAGE;
						am.requestInterrupt("State changed");
					}
				}
				return script.state == State.HOP_DITCH && script.s.myY() < 3521;
			}

		});

		am.addAction(new Action("WALK_TO_MAGE", 1, false, log) {

			// List of several tiles along the way
			Position[] tiles = new Position[] { new Position(3103, 3529, 0),
					new Position(3104, 3529, 0), new Position(3099, 3529, 0),
					new Position(3101, 3532, 0), new Position(3102, 3532, 0),
					new Position(3103, 3532, 0), new Position(3105, 3536, 0),
					new Position(3102, 3536, 0), new Position(3100, 3540, 0),
					new Position(3103, 3540, 0), new Position(3101, 3540, 0),
					new Position(3102, 3543, 0), new Position(3102, 3546, 0),
					new Position(3102, 3550, 0), new Position(3102, 3551, 0),
					new Position(3102, 3555, 0) };

			@Override
			public void run() {
				try {
					if (s.myX() < 3079) {
						s.walkMiniMap(new Position(3089, 3529, 0));
					} else if (s.myX() < 3089) {
						s.walkMiniMap(new Position(3099, 3529, 0));
					}
					/*
					 * try { script.s.client.rotateCameraPitch(0); } catch
					 * (InterruptedException e2) { // TODO Auto-generated catch
					 * block e2.printStackTrace(); }
					 */
					System.out.println("AOEU");
					if (script.s.client.getCameraYawAngle() > 10
							&& script.s.client.getCameraYawAngle() < 350)
						try {
							script.s.client.rotateCameraToAngle(0);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							// e1.printStackTrace();
						}
					// Random threshold for the tile distance from current
					// position

					int threshold = 14;
					System.out.println("Threshold");
					// Find the furthest tile that is within threshold
					int i = 0;
					for (; i < tiles.length; i++) {
						if (tiles[i].getY() > s.myY()) {
							break;
						}
					}
					System.out.println(i);
					for (; i < tiles.length; i++) {
						if (calcDistance(tiles[i]) > threshold) {
							break;
						}
					}
					i--;
					destination = new Position(tiles[i].getX()
							+ MethodProvider.random(0, 4) - 2, tiles[i].getY()
							+ MethodProvider.random(0, 4) - 2, 0);
					destinationi = i;
					System.out.println(destinationi);
					// Click on minimap
					try {
						if (script.s.walkMiniMap(destination)) {
							// If successfully clicked destination, wait 3-5
							// seconds for player to start moving
							for (i = 0; i < 10
									&& !script.s.myPlayer().isMoving(); i++) {
								try {
									Thread.sleep(MethodProvider
											.random(300, 500));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
				} catch (Exception e) {

				} finally {
					setRunning(false);
				}

			}

			@Override
			public boolean shouldActivate(CMHScript script) {
				if (calcDistance(new Position(3105, 3556, 0)) > 100
						&& (script.state == State.WALK_TO_MAGE || script.state == State.TELEPORT_MAGE)) {
					rotation = Math.random() < 0.5 ? -1 : 1;
					state = State.ENTER_WALL;
					s.randomManager.registerHook(new RandomBehaviourHook(
							s.randomManager.SANDWICH_LADY) {
						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}
					});

					s.randomManager.registerHook(new RandomBehaviourHook(
							s.randomManager.TALKERS) {

						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}

					});

					s.randomManager.registerHook(new RandomBehaviourHook(
							s.randomManager.SECURITY_BOOK) {

						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}

					});

					s.randomManager.registerHook(new RandomBehaviourHook(
							s.randomManager.MILES_GILES_NILES) {

						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}

					});

					s.randomManager.registerHook(new RandomBehaviourHook(
							s.randomManager.RUN_AWAY_FROM_COMBAT) {

						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}

					});
					am.requestInterrupt("State changed");
				}
				System.out.println(script.state == State.WALK_TO_MAGE);
				System.out.println(calcDistance(destination) < 8);
				System.out.println(s.myPlayer().isMoving());
				System.out.println((calcDistance(destination) < 8 || !s
						.myPlayer().isMoving()));
				System.out.println(script.s.myY() <= 3550);
				System.out.println(script.state == State.WALK_TO_MAGE
						&& script.s.myY() < 3567
						&& (calcDistance(destination) < 8 || !s.myPlayer()
								.isMoving()) && (script.s.myY() <= 3550));
				System.out.println(am.isBlocked());
				return script.state == State.WALK_TO_MAGE
						&& script.s.myY() < 3567
						&& (calcDistance(destination) < 8 || !s.myPlayer()
								.isMoving()) && (script.s.myY() <= 3550);
			}
		});

		am.addAction(new Action("TELEPORT_MAGE", 2, true, log) {

			@Override
			public void run() {
				try {
					NPC mage = script.s.closestNPCForName("Mage of Zamorak");
					if (mage != null && (script.s.myY() > 3550)) {
						// Interrupt other threads
						am.requestInterruptBlock(this.getID());
						// Attempt to teleport
						Position oldPosition = script.s.myPosition();
						try {
							if (mage.interact("Teleport")) {
								for (int i = 0; i < 10
										&& !script.s.myPlayer().isMoving()
										&& calcDistance(new Position(3105,
												3556, 0)) < 100; i++)
									try {
										Thread.sleep(MethodProvider.random(300,
												500));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
								for (int i = 0; i < 20
										&& script.s.myPlayer().isMoving()
										&& calcDistance(new Position(3105,
												3556, 0)) < 100; i++) {
									try {
										Thread.sleep(MethodProvider.random(300,
												500));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
								}
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
					}
				} catch (Exception e) {

				} finally {
					am.releaseInterrupt();
					setRunning(false);
				}
			}

			@Override
			public boolean shouldActivate(CMHScript script) {
				if (calcDistance(new Position(3105, 3556, 0)) > 100
						&& (script.state == State.WALK_TO_MAGE || script.state == State.TELEPORT_MAGE)) {
					rotation = Math.random() < 0.5 ? -1 : 1;
					state = State.ENTER_WALL;
					s.randomManager.registerHook(new RandomBehaviourHook(
							s.randomManager.SANDWICH_LADY) {
						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}
					});

					s.randomManager.registerHook(new RandomBehaviourHook(
							s.randomManager.TALKERS) {

						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}

					});

					s.randomManager.registerHook(new RandomBehaviourHook(
							s.randomManager.SECURITY_BOOK) {

						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}

					});

					s.randomManager.registerHook(new RandomBehaviourHook(
							s.randomManager.MILES_GILES_NILES) {

						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}

					});

					s.randomManager.registerHook(new RandomBehaviourHook(
							s.randomManager.RUN_AWAY_FROM_COMBAT) {

						@Override
						public boolean shouldActivate() {
							return false;
						}

						@Override
						public boolean shouldActivatePreLoop() {
							return false;
						}

						@Override
						public int preLoop() {
							return 0;
						}

						@Override
						public int onLoop() {
							return 0;
						}

						@Override
						public boolean shouldActivatePostLoop() {
							return false;
						}

						@Override
						public int postLoop() {
							return 0;
						}

					});
					am.requestInterrupt("State changed");
				}
				return (script.state == State.WALK_TO_MAGE || script.state == State.TELEPORT_MAGE)
						&& script.s.myY() < 3567 && (script.s.myY() > 3550);
			}

		});

		am.addAction(new Action("PKER_MONITOR", 3, true, log) {

			@Override
			public void run() {
				try {
					Player p = playerFacingMe();
					if (p != null) {
						hopWorlds = true;
					}
					if (p != null && !teleblocked) { // There is a player facing
														// me!!! PKER
						addPKERBlacklist(p);
						am.requestInterruptBlock(this.getID());
						switch (teleportMethod) {
						case 0:
							Position oldPosition = s.myPosition();
							while (calcDistance(oldPosition) < 30
									&& s.client.getInventory().contains(
											"Teleport to house")
									&& !teleblocked) {
								try {
									s.client.getInventory().interactWithName(
											"Teleport to house", "Break");
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
								try {
									s.sleep(MethodProvider.random(10, 100));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
							if (calcDistance(oldPosition) > 30) {
								script.state = State.HOUSE_GLORY;
								am.releaseInterrupt();
							}
							break;
						case 1:
							switch (emergencyTeleportMethod) {
							case 0:
								oldPosition = s.myPosition();
								while (calcDistance(oldPosition) < 30
										&& s.client.getInventory().contains(
												"Varrock teleport")
										&& !teleblocked) {
									try {
										s.client.getInventory()
												.interactWithName(
														"Varrock teleport",
														"Break");
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
									try {
										s.sleep(MethodProvider.random(10, 100));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
								}
								if (calcDistance(oldPosition) > 30) {
									script.state = State.TELEPORT;
									am.releaseInterrupt();
								}
								break;
							case 1:
								oldPosition = s.myPosition();
								while (calcDistance(oldPosition) < 30
										&& s.client.getInventory().contains(
												"Teleport to house")
										&& !teleblocked) {
									try {
										s.client.getInventory()
												.interactWithName(
														"Teleport to house",
														"Break");
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
									try {
										s.sleep(MethodProvider.random(10, 100));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
								}
								if (calcDistance(oldPosition) > 30) {
									script.state = State.HOUSE_GLORY;
									am.releaseInterrupt();
								}
								break;
							case 2:
								try {
									for (int i = 0; i < 5
											&& !s.equipmentTab.open(); i++) {
										Thread.sleep(MethodProvider.random(300,
												500));
									}
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								if (isValidInterface(387)) {
									try {
										for (int i = 0; i < 10
												&& !s.client.getInterface(387)
														.getChild(14)
														.interact("Operate"); i++) {
											Thread.sleep(MethodProvider.random(
													500, 600));
										}
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								for (int i = 0; i < 10
										&& !isValidInterface(234); i++)
									try {
										Thread.sleep(MethodProvider.random(500,
												600));
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								if (isValidInterface(234)) {
									try {
										for (int i = 0; i < 5
												&& !s.client.getInterface(234)
														.getChild(1).interact(); i++) {
											Thread.sleep(MethodProvider.random(
													500, 600));
										}
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								am.releaseInterrupt();
								break;
							}
						}
					}
				} catch (Exception e) {

				} finally {
					am.releaseInterrupt();
				}
			}

			@Override
			public boolean shouldActivate(CMHScript script) {
				if (script.state == State.WALK_TO_MAGE
						|| script.state == State.TELEPORT_MAGE) {
					if (script.s.closestObjectForName("Bank booth") != null
							&& calcDistance(script.s.closestObjectForName(
									"Bank booth").getPosition()) < 16)
						script.state = State.BANK;
				}
				return (script.state == State.WALK_TO_MAGE || script.state == State.TELEPORT_MAGE);
			}

		});

		am.addAction(new Action("ENTER_WALL", 1, false, log) {

			@Override
			public void run() {
				try {
					RS2Object closest = null;
					double distance = 2000000000.0;
					String action = null;
					// Rocks
					if (mineRocks) {
						RS2Object temp = closestObjectForRealIDModelID(
								rockRealID, rockModelID, "Mine", "Rock");
						if (temp != null
								&& calcDistance(temp.getPosition()) < distance) {
							distance = calcDistance(temp.getPosition());
							closest = temp;
							action = "Mine";
						}
					}

					// Tendrils
					if (chopTendrils) {
						RS2Object temp = closestObjectForRealIDModelID(
								tendrilsRealID, tendrilsModelID, "Chop",
								"Tendrils");
						if (temp != null
								&& calcDistance(temp.getPosition()) < distance) {
							distance = calcDistance(temp.getPosition());
							closest = temp;
							action = "Chop";
						}
					}

					// Eyes
					if (distractEyes) {
						RS2Object temp = closestObjectForRealIDModelID(
								eyesRealID, eyesModelID, "Distract", "Eyes");
						if (temp != null
								&& calcDistance(temp.getPosition()) < distance) {
							distance = calcDistance(temp.getPosition());
							closest = temp;
							action = "Distract";
						}
					}

					// Boil
					if (burnBoils) {
						RS2Object temp = closestObjectForRealIDModelID(
								boilRealID, boilModelID, "Burn-down", "Boil");
						if (temp != null
								&& calcDistance(temp.getPosition()) < distance) {
							distance = calcDistance(temp.getPosition());
							closest = temp;
							action = "Burn-down";
						}
					}

					// Gap
					if (squeezeGap) {
						RS2Object temp = closestObjectForRealIDModelID(
								gapRealID, gapModelID, "Squeeze-through", "Gap");
						if (temp != null
								&& calcDistance(temp.getPosition()) < distance) {
							distance = calcDistance(temp.getPosition());
							closest = temp;
							action = "Squeeze-through";
						}
					}

					// Passage
					RS2Object temp = closestObjectForRealIDModelID(
							passageRealID, passageModelID, "Go-through",
							"Passage");
					if (temp != null
							&& calcDistance(temp.getPosition()) < distance) {
						distance = calcDistance(temp.getPosition());
						closest = temp;
						action = "Go-through";
					}

					// If we are close to the obstacle, then interact with it
					if (distance <= 16
							|| script.getClosestAbyssalRift() == null) {
						closest.interact(action);
					} else {
						// Otherwise, if we know where the obstacle is, walk to
						// it.
						RS2Object center = script.getClosestAbyssalRift();
						if (center != null) {
							int dx = script.s.myX() - center.getX();
							int dy = script.s.myY() - center.getY();
							System.out.println(dx + " " + dy);
							double dx2 = rotateX(dx, dy);
							double dy2 = rotateY(dx, dy);
							Position newPosition = new Position((int) (center
									.getX() + dx2),
									(int) (center.getY() + dy2), 0);
							System.out.println("My position: " + s.myX() + " "
									+ s.myY());
							System.out.println("Rift location: "
									+ center.getX() + " " + center.getY());
							System.out.println("Walk position: "
									+ newPosition.getX() + " "
									+ newPosition.getY());
							Position newPosition2 = findClosestReachablePosition(newPosition);
							try {
								script.s.walkMiniMap(newPosition);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						} else {
							System.out.println("NULL???");
						}
					}
				} catch (Exception e) {
				} finally {
					setRunning(false);
				}
			}

			@Override
			public boolean shouldActivate(CMHScript script) {
				NPC mage = script.s.closestNPCForName("Dark mage");
				if (script.state == State.ENTER_WALL && mage != null
						&& s.canReach(mage)) {
					script.state = State.INNER_LOOP;
					s.randomManager.unregisterHook(RandomManager.SANDWICH_LADY);

					s.randomManager.unregisterHook(RandomManager.TALKERS);

					s.randomManager.unregisterHook(RandomManager.SECURITY_BOOK);

					s.randomManager
							.unregisterHook(RandomManager.MILES_GILES_NILES);

					s.randomManager
							.unregisterHook(RandomManager.RUN_AWAY_FROM_COMBAT);
					am.requestInterrupt("State changed");
				}
				if (script.state == State.ENTER_WALL
						&& closestObjectForName("Amulet of Glory") != null) {
					script.state = State.HOUSE_GLORY;
					s.randomManager.unregisterHook(RandomManager.SANDWICH_LADY);

					s.randomManager.unregisterHook(RandomManager.TALKERS);

					s.randomManager.unregisterHook(RandomManager.SECURITY_BOOK);

					s.randomManager
							.unregisterHook(RandomManager.MILES_GILES_NILES);

					s.randomManager
							.unregisterHook(RandomManager.RUN_AWAY_FROM_COMBAT);
					am.requestInterrupt("State changed");
				}
				if (script.state == State.ENTER_WALL
						&& closestObjectForName("Bank") != null) {
					script.state = State.BANK;
					s.randomManager.unregisterHook(RandomManager.SANDWICH_LADY);

					s.randomManager.unregisterHook(RandomManager.TALKERS);

					s.randomManager.unregisterHook(RandomManager.SECURITY_BOOK);

					s.randomManager
							.unregisterHook(RandomManager.MILES_GILES_NILES);

					s.randomManager
							.unregisterHook(RandomManager.RUN_AWAY_FROM_COMBAT);
					am.requestInterrupt("State changed");
				}
				return script.state == State.ENTER_WALL;
			}

		});

		am.addAction(new Action("MONITOR_STATUS", 2, false, log) {

			@Override
			public void run() {
				try {
					am.requestInterruptBlock(this.getID());
					if (getHealth() < healthEatAt) {
						try {
							script.s.client.getInventory().interactWithName(
									foodName, "Eat");
							healthEatAt = script.s.client.getSkills().getLevel(
									Skill.HITPOINTS)
									/ 2 - 5 + MethodProvider.random(0, 10);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
					}
					if (script.s.client.getRunEnergy() < drinkPotsAt) {
						try {
							int potsID = -1;
							for (int i = 0; i < energyPotIDs.length; i++) {
								if (script.s.client.getInventory().contains(
										energyPotIDs[i])) {
									potsID = energyPotIDs[i];
									break;
								}
							}
							if (potsID != -1) {
								script.s.client.getInventory().interactWithId(
										potsID, "Drink");
							}
						} catch (InterruptedException e) {

						}
					}
					NPC darkMage = closestNPCForName("Dark mage");
					if (getHealth() < healthToTeleportAt
							&& (darkMage == null || !s.canReach(darkMage))) {
						script.emergencyTeleport();
					}
				} catch (Exception e) {
				} finally {
					am.releaseInterrupt();
					setRunning(false);
				}
			}

			@Override
			public boolean shouldActivate(CMHScript script) {
				// TODO Auto-generated method stub
				return script.state == State.ENTER_WALL
						&& (getHealth() < healthEatAt
								&& script.s.client.getInventory().contains(
										foodName)
								|| script.s.client.getRunEnergy() < drinkPotsAt
								&& script.inventoryContainsPots() || getHealth() < healthToTeleportAt);
			}

		});

		am.addAction(new Action("INNER_LOOP", 1, false, log) {

			@Override
			public void run() {
				try {
					if (script.containsDegradedPouches()) {
						NPC darkMage = script.s.closestNPCForName("Dark mage");
						if (darkMage != null) {
							try {
								for (int i = 0; i < 10
										&& !darkMage.interact("Repairs"); i++) {
									Thread.sleep(MethodProvider
											.random(500, 700));
								}
								for (int i = 0; i < 2
										&& !script.s.myPlayer().isMoving(); i++) {
									script.s.sleep(random(500, 600));
								}
								while (script.s.myPlayer().isMoving())
									script.s.sleep(random(500, 600));
								for (int i = 0; i < 10
										&& script.containsDegradedPouches(); i++)
									script.s.sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
					} else {
						if (script.s.isRunning()
								&& script.s.client.getRunEnergy() < 50) {
							System.out.println("ACTION " + 1);
							script.setRunning2(false);
						}

						/*
						 * try { s.client.rotateCameraPitch(90); } catch
						 * (InterruptedException e1) { // TODO Auto-generated
						 * catch block //e1.printStackTrace(); }
						 */
						System.out.println("ACTION " + 2);
						RS2Object rift = script
								.closestObjectForName(riftNames[riftIndex]);
						if (rift != null) {
							if (calcDistance(rift.getPosition()) < 1.416) {
								new Thread(new Runnable() {
									public void run() {
										// Rotate the camera
										if (rotationAngles[riftIndex] == 0) {
											if (s.client.getCameraYawAngle() < 350
													&& s.client
															.getCameraYawAngle() > 10) {
												try {
													s.client.rotateCameraToAngle(0);
												} catch (InterruptedException e) { //
													e.printStackTrace();
												}
											}
										} else if (Math.abs(rotationAngles[riftIndex]
												- s.client.getCameraYawAngle()) > 10) {
											try {
												s.client.rotateCameraToAngle(rotationAngles[riftIndex]);
											} catch (InterruptedException e) {
											}
										}
										if (script.s.client
												.getCameraPitchAngle() < 35)
											try {
												script.s.client
														.rotateCameraPitch(40);
											} catch (InterruptedException e) { // TODO
																				// Auto-generatedcatch
																				// block
												e.printStackTrace();
											}
									}
								}).start();
							}

							System.out.println("ACTION " + 3);
							try {
								rift.interact("Exit-through");
								System.out.println("ACTION " + 6);
								if (rift.isVisible()) {
									MouseDestination md = rift
											.getMouseDestination();
									RectangleDestination rd = new RectangleDestination(
											(int) md.getPointDestination()
													.getX() - 1, (int) md
													.getPointDestination()
													.getY() - 1, 3, 3);
									if (script.s.client.getMenu().get(0).action != null
											&& script.s.client.getMenu().get(0).action
													.equals("Exit-through"))
										script.s.client.clickMouse(false);
									System.out.println("ACTION " + 7);
									script.s.client.moveMouse(rd, false);
									System.out.println("ACTION " + 8);
									if (script.s.client.getMenu().get(0).action != null
											&& script.s.client.getMenu().get(0).action
													.equals("Exit-through"))
										script.s.client.clickMouse(false);
								}
							} catch (Exception e) {
							}

							System.out.println("ACTION " + 9);
							// Rotate the camera
							/*
							 * if (rotationAngles[riftIndex] == 0) { if
							 * (s.client.getCameraYawAngle() < 355 &&
							 * s.client.getCameraYawAngle() > 5) { try {
							 * s.client.rotateCameraToAngle(0); } catch
							 * (InterruptedException e) { //
							 * e.printStackTrace(); } } } else if
							 * (Math.abs(rotationAngles[riftIndex] -
							 * s.client.getCameraYawAngle()) > 5) { try {
							 * s.client.rotateCameraToAngle
							 * (rotationAngles[riftIndex]); } catch
							 * (InterruptedException e) { } }
							 */
							System.out.println("DONE");
						}
					}
				} catch (Exception e) {
				} finally {
					setRunning(false);
				}
			}

			@Override
			public boolean shouldActivate(CMHScript script) {
				if (script.state == State.INNER_LOOP
						&& script.s.closestNPCForName("Dark mage") == null
						&& script.s.closestObjectForName("Altar") != null) {
					script.state = State.CRAFT;
					am.requestInterrupt("State changed");
				}
				if (script.state == State.INNER_LOOP
						&& script.s.closestObjectForName("Amulet of Glory") != null) {
					script.state = State.HOUSE_GLORY;
					am.requestInterrupt("State changed");
				}
				return script.state == State.INNER_LOOP;
			}

		});

		am.addAction(new Action("CRAFT", 1, false, log) {

			@Override
			public void run() {
				try {
					if (script.s.client.getInventory().contains(essID)) {
						RS2Object altar = script.s
								.closestObjectForName("Altar");
						if (altar != null) {
							try {
								altar.interact("Craft-rune");
								for (int i = 0; i < 10
										&& script.s.myPlayer().getAnimation() == -1; i++) {
									Thread.sleep(MethodProvider
											.random(500, 600));
								}
								if (smallPouchFilled || mediumPouchFilled
										|| largePouchFilled || giantPouchFilled) {
									for (int i = 0; i < 10
											&& script.s.myPlayer()
													.getAnimation() != -1; i++) {
										Thread.sleep(MethodProvider.random(500,
												600));
									}
									Thread.sleep(MethodProvider.random(1000,
											1200));
								}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
					} else {
						if (smallPouchFilled || mediumPouchFilled
								|| largePouchFilled || giantPouchFilled) {
							if (useSmallPouch
									&& smallPouchFilled
									&& script.s.client.getInventory()
											.getEmptySlots() >= 3) {
								if (script.s.client.getInventory()
										.interactWithName("Small Pouch",
												"Empty")) {
									smallPouchFilled = false;
									for (int i = 0; i < 5
											&& !script.s.client.getInventory()
													.contains(essID); i++) {
										Thread.sleep(MethodProvider.random(400,
												600));
									}
								}
							}
							if (useMediumPouch
									&& mediumPouchFilled
									&& script.s.client.getInventory()
											.getEmptySlots() >= 6) {
								int oldAmount = (int) s.client.getInventory()
										.getAmount(essID);
								if (script.s.client.getInventory()
										.interactWithName("Medium Pouch",
												"Empty")) {
									mediumPouchFilled = false;
									for (int i = 0; i < 5
											&& script.s.client.getInventory()
													.getAmount(essID) == oldAmount; i++) {
										Thread.sleep(MethodProvider.random(400,
												600));
									}
								}
							}
							if (useLargePouch
									&& largePouchFilled
									&& script.s.client.getInventory()
											.getEmptySlots() >= 9) {
								int oldAmount = (int) s.client.getInventory()
										.getAmount(essID);
								if (script.s.client.getInventory()
										.interactWithName("Large Pouch",
												"Empty")) {
									largePouchFilled = false;
									for (int i = 0; i < 5
											&& script.s.client.getInventory()
													.getAmount(essID) == oldAmount; i++) {
										Thread.sleep(MethodProvider.random(400,
												600));
									}
								}
							}
							if (useGiantPouch
									&& giantPouchFilled
									&& script.s.client.getInventory()
											.getEmptySlots() >= 12) {
								int oldAmount = (int) s.client.getInventory()
										.getAmount(essID);
								if (script.s.client.getInventory()
										.interactWithName("Giant Pouch",
												"Empty")) {
									giantPouchFilled = false;
									for (int i = 0; i < 5
											&& script.s.client.getInventory()
													.getAmount(essID) == oldAmount; i++) {
										Thread.sleep(MethodProvider.random(400,
												600));
									}
								}
							}
							setRunning(false);
							return;
						}
						String name = riftNames[riftIndex].substring(0, 1)
								+ riftNames[riftIndex].toLowerCase().substring(
										1, riftNames[riftIndex].indexOf(' '))
								+ " rune";
						Thread.sleep(MethodProvider.random(100, 200));
						crafted += script.s.client.getInventory().getAmount(
								name);
						script.state = State.TELEPORT;
						am.requestInterrupt("State changed");
					}
				} catch (Exception e) {

				} finally {
					setRunning(false);
				}
			}

			@Override
			public boolean shouldActivate(CMHScript script) {
				if (script.state == State.CRAFT
						&& closestObjectForName("Amulet of Glory") != null) {
					script.state = State.HOUSE_GLORY;
				}
				return script.state == State.CRAFT;
			}

		});

		am.addAction(new Action("TELEPORT", 1, false, log) {

			@Override
			public void run() {
				try {
					if (script.s.client.getBank().isOpen()) {
						script.s.client.getBank().close();
						Thread.sleep(1000);
						return;
					}
					teleblocked = false;
					if (hopWorlds) {
						hopWorlds = false;
						hopWorlds();
					}
					switch (teleportMethod) {
					case 0:
						try {
							script.s.client.getInventory().interactWithName(
									"Teleport to house", "Break");
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						break;
					case 1:
						for (int i = 0; i < 5 && !script.s.equipmentTab.open(); i++) {
							Thread.sleep(MethodProvider.random(300, 500));
						}
						if (script.isValidInterface(387)) {
							for (int i = 0; i < 10
									&& !script.s.client.getInterface(387)
											.getChild(14).interact("Operate"); i++) {
								Thread.sleep(MethodProvider.random(500, 600));
							}
						}
						for (int i = 0; i < 10 && !script.isValidInterface(234); i++)
							Thread.sleep(MethodProvider.random(500, 600));
						if (script.isValidInterface(234)) {
							for (int i = 0; i < 5
									&& !script.s.client.getInterface(234)
											.getChild(1).interact(); i++) {
								Thread.sleep(MethodProvider.random(500, 600));
							}
						}
						break;
					}
				} catch (Exception e) {
				} finally {
					setRunning(false);
				}
			}

			@Override
			public boolean shouldActivate(CMHScript script) {
				if (script.state == State.TELEPORT
						&& script.s.closestObjectForName("Altar") == null
						&& calcDistance(lumbridgeTile) > 100) {
					switch (teleportMethod) {
					case 0:
						script.state = State.HOUSE_GLORY;
						am.requestInterrupt("State changed");
						break;
					case 1:
						script.state = State.BANK;
						am.requestInterrupt("State changed");
					}
				}
				System.out.println("DISTANCE: " + calcDistance(lumbridgeTile));
				return script.state == State.TELEPORT;
			}

		});

		am.addAction(new Action("HOUSE_GLORY", 1, false, log) {

			@Override
			public void run() {
				try {
					teleblocked = false;
					if (hopWorlds) {
						hopWorlds = false;
						hopWorlds();
					}
					RS2Object glory = script
							.closestObjectForName("Amulet of Glory");
					if (glory != null) {
						Position northP = new Position(glory.getX(), glory
								.getY() + 1, 0);
						Position southP = new Position(glory.getX(), glory
								.getY() - 1, 0);
						Position eastP = new Position(glory.getX() + 1, glory
								.getY(), 0);
						Position westP = new Position(glory.getX() - 1, glory
								.getY(), 0);
						int dx = 0, dy = 0;
						int rotation = -1;
						if (!script.noOtherObjectsAt(northP, "")) {
							dx = 0;
							dy = -3;
							rotation = 0;
						} else if (!script.noOtherObjectsAt(southP, "")) {
							dx = 0;
							dy = 3;
							rotation = 180;
						} else if (!script.noOtherObjectsAt(eastP, "")) {
							dx = -3;
							dy = 0;
							rotation = 270;
						} else if (!script.noOtherObjectsAt(westP, "")) {
							dx = 3;
							dy = 0;
							rotation = 90;
						}
						if (rotation != -1) {
							script.s.client.rotateCameraToAngle(rotation);
							/*
							 * Position newP = new Position(glory.getX() + dx,
							 * glory.getY() + dy, 0); if (calcDistance(newP) > 4
							 * && !glory.isVisible()) { try { script.s.walk(new
							 * Position(newP.getX() + random(0, 2) - 1,
							 * newP.getY() + random(0, 2) - 1, 0)); if (rotation
							 * != 0 ? Math.abs(script.s.client
							 * .getCameraYawAngle() - rotation) > 10 :
							 * script.s.client.getCameraYawAngle() > 10 &&
							 * script.s.client .getCameraYawAngle() < 350)
							 * script.s.client .rotateCameraToAngle(rotation);
							 * Thread.sleep(MethodProvider.random(800, 1200)); }
							 * catch (InterruptedException e) { // TODO
							 * Auto-generated catch block //
							 * e.printStackTrace(); } } else {
							 */
							try {
								if (rotation != 0 ? Math.abs(script.s.client
										.getCameraYawAngle() - rotation) > 10
										: script.s.client.getCameraYawAngle() > 10
												&& script.s.client
														.getCameraYawAngle() < 350)
									script.s.client
											.rotateCameraToAngle(rotation);
								script.s.client.rotateCameraPitch(script.s.client
										.getLowestAvailableCameraPitchAngle());
								Thread.sleep(MethodProvider.random(800, 1200));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
							if (!script.s.myPlayer().isMoving()) {
								try {
									if (glory.interact("Rub")) {
										// Wait for interface to appear
										for (int i = 0; i < 5
												&& !isValidInterface(234); i++) {
											if (script.s.myPlayer().isMoving())
												i--;
											try {
												Thread.sleep(MethodProvider
														.random(300, 500));
											} catch (InterruptedException e) {

											}
										}
										if (isValidInterface(234)) {
											if (script.s.client
													.getInterface(234)
													.getChild(1).interact()) {
												for (int i = 0; i < 10
														&& script
																.closestObjectForName("Bank booth") == null; i++) {
													Thread.sleep(MethodProvider
															.random(500, 600));
												}
											}
										}
									}
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
							// }
						} else {
							try {
								if (glory.interact("Rub")) {
									// Wait for interface to appear
									for (int i = 0; i < 5
											&& !isValidInterface(234); i++) {
										if (script.s.myPlayer().isMoving())
											i--;
										try {
											Thread.sleep(MethodProvider.random(
													300, 500));
										} catch (InterruptedException e) {

										}
									}
									if (isValidInterface(234)) {
										if (script.s.client.getInterface(234)
												.getChild(1).interact()) {
											for (int i = 0; i < 10
													&& script
															.closestObjectForName("Bank booth") == null; i++) {
												Thread.sleep(MethodProvider
														.random(500, 600));
											}
										}
									}
								}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}

						}
					} else {
						if (closestObjectForName("Portal") != null) {
							System.out.println("NULL. SLEEP");
							try {
								Thread.sleep(5000);
							} catch (Exception e) {
							}
							for (int i = 0; i < 10
									&& !(script.s.client.getInterface(399) == null
											|| !script.s.client.getInterface(
													399).isValid() || !script.s.client
											.getInterface(399).isVisible()); i++)
								try {
									Thread.sleep(s.random(500, 600));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							System.out.println("DONE SLEEP");
							if (closestObjectForName("Amulet of Glory") == null
									&& closestObjectForName("Portal") != null) {
								RS2Object portal = closestObjectForName("Portal");
								if (portal != null) {
									try {
										for (int i = 0; i < 10
												&& !portal.interact("Enter"); i++) {

											Thread.sleep(s.random(300, 400));
										}
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									for (int i = 0; i < 10
											&& (script.s.client
													.getInterface(232) == null
													|| !script.s.client
															.getInterface(232)
															.isValid() || !script.s.client
													.getInterface(232)
													.isVisible()); i++) {
										try {
											Thread.sleep(s.random(500, 600));
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
									if (!(script.s.client.getInterface(232) == null
											|| !script.s.client.getInterface(
													232).isValid() || !script.s.client
											.getInterface(232).isVisible())) {
										try {
											for (int i = 0; i < 10
													&& !script.s.client
															.getInterface(232)
															.getChild(2)
															.interact(); i++)
												Thread.sleep(s.random(500, 600));
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
									Thread.sleep(2000);
									for (int i = 0; i < 10
											&& !(script.s.client
													.getInterface(399) == null
													|| !script.s.client
															.getInterface(399)
															.isValid() || !script.s.client
													.getInterface(399)
													.isVisible()); i++)
										try {
											Thread.sleep(s.random(500, 600));
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
								}

							}
						}
					}
				} catch (Exception e) {

				} finally {
					setRunning(false);
				}
			}

			@Override
			public boolean shouldActivate(CMHScript script) {
				if (script.state == State.HOUSE_GLORY
						&& script.s.closestObjectForName("Bank booth") != null) {
					script.state = State.BANK;
					am.requestInterrupt("State changed");
				}
				return script.state == State.HOUSE_GLORY;
			}

		});

		am.addAction(new Action("DEATHWALK_LUMBRIDGE", 1, false, log) {

			@Override
			public void run() {
				try {
					if (script.getInventoryGlory(true) != null) {
						while (script.s.client.getBank().isOpen()) {
							try {
								script.s.client.getBank().close();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
							try {
								Thread.sleep(MethodProvider.random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						String glory = script.getInventoryGlory(false);
						try {
							for (int i = 0; i < 10
									&& !script.s.client.getInventory()
											.interactWithName(glory, "Wear"); i++) {
								script.s.sleep(MethodProvider.random(300, 600));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						for (int i = 0; i < 10
								&& script.s.equipmentTab
										.getItemInSlot(EquipmentSlot.AMULET) == null
								|| !script.s.equipmentTab
										.getItemInSlot(EquipmentSlot.AMULET)
										.getName().equals(glory); i++) {
							try {
								script.s.sleep(MethodProvider.random(300, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						}
						return;
					}
					if (script.s.myPlayer().getZ() == 0) {
						RS2Object stair = script
								.closestObjectForName("Staircase");
						if (stair != null) {
							try {
								for (int i = 0; i < 10
										&& !stair.interact("Climb-up"); i++)
									script.s.sleep(s.random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							for (int i = 0; i < 10
									&& script.s.myPlayer().getZ() != 1; i++) {
								try {
									script.s.sleep(s.random(500, 600));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					} else if (script.s.myPlayer().getZ() == 1) {
						script.s.log("Finding staircase!");
						RS2Object stair = script
								.closestObjectForName("Staircase");
						if (stair != null) {
							try {
								for (int i = 0; i < 10
										&& !stair.interact("Climb-up"); i++)
									script.s.sleep(s.random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							for (int i = 0; i < 10
									&& script.s.myPlayer().getZ() != 2; i++) {
								try {
									script.s.sleep(s.random(500, 600));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					} else if (script.s.myPlayer().getZ() == 2) {
						Bank b = script.s.client.getBank();
						for (int i = 0; i < 10 && !b.isOpen(); i++) {
							RS2Object booth = script
									.closestObjectForName("Bank booth");

							try {
								script.s.sleep(s.random(300, 400));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							if (booth == null)
								break;

							try {
								for (int j = 0; j < 10
										&& !booth.interact("Bank"); j++)
									script.s.sleep(s.random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							try {
								script.s.sleep(s.random(500, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							for (int j = 0; j < 10 && !b.isOpen(); j++) {
								try {
									script.s.sleep(s.random(200, 300));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								if (script.s.myPlayer().isMoving())
									j--;
							}
						}
						if (b.isOpen()) {
							switch (teleportMethod) {
							case 0:
								try {
									if (!b.contains(8013)) {
										stopScript = true;
										script.s.log("Out of teletabs");
										script.log.logString("Out of teletabs");
										return;
									}
									b.withdraw1(8013);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								break;
							case 1:
								Item glory = script.getBankGlory();
								if (glory == null) {
									stopScript = true;
									script.s.log("Out of glories");
									script.log.logString("Out of glories");
									return;
								}
								try {
									for (int i = 0; i < 5
											&& !script.s.client.getBank()
													.withdraw1(glory.getId()); i++) {
										Thread.sleep(MethodProvider.random(300,
												500));
									}
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
							}
						}
					}
				} catch (Exception e) {

				} finally {
					script.setRunning2(false);
				}
			}

			@Override
			public boolean shouldActivate(CMHScript script) {
				if (script.state == State.DEATHWALK_LUMBRIDGE) {
					switch (teleportMethod) {
					case 0:
						if (script.s.client.getInventory().contains(8013)) {
							script.state = State.TELEPORT;
							break;
						}
						break;
					case 1:
						if (script.isWearingGlory())
							script.state = State.TELEPORT;
					}
				}
				return script.state == State.DEATHWALK_LUMBRIDGE;
			}

		});

		// Thread to monitor random handler
		randomMonitorThread = new Thread(new Runnable() {
			public void run() {
				while (true) {
					if (s.randomManager.getCurrent() != null) {
						am.requestInterrupt("RANDOM");
					}
					try {
						Thread.sleep(MethodProvider.random(100, 200));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
				}
			}
		});
		randomMonitorThread.start();
		startTime = System.currentTimeMillis();
	}

	private boolean blacklisted(int world) {
		for (int i = 0; i < blacklistedWorlds.length; i++)
			if (blacklistedWorlds[i] == world)
				return true;
		return false;
	}

	private void hopWorlds() {
		int world;
		do {
			world = s.random(305, 375);
		} while (blacklisted(world) || world == s.client.getCurrentWorld());
		try {
			s.worldHopper.hopWorld(world);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void addPKERBlacklist(final Player p) {
		Thread t = new Thread(new Runnable() {
			public void run() {
				log.logString("PKer detected! Adding to blacklist");
				log.logString("PKer rs name: " + p.getName());
				log.logString("Attempting to look up player stats...");
				PlayerData pdata = null;
				try {
					pdata = PlayerData.lookup(p.getName());
				} catch (IOException e) {
					// log("Unable to get player data.");
				}
				if (pdata != null) {
					SkillData attack = pdata.getSkillData(Skill.ATTACK);
					SkillData strength = pdata.getSkillData(Skill.STRENGTH);
					SkillData defence = pdata.getSkillData(Skill.DEFENCE);
					SkillData magic = pdata.getSkillData(Skill.MAGIC);
					SkillData range = pdata.getSkillData(Skill.RANGED);
					SkillData hitpoints = pdata.getSkillData(Skill.HITPOINTS);
					SkillData prayer = pdata.getSkillData(Skill.PRAYER);
					double base = 0.25 * (defence.level() + hitpoints.level() + (prayer
							.level() / 2));
					double meelee = 0.325 * (attack.level() + strength.level());
					double ranged = 0.325 * (range.level() / 2 + range.level());
					double mage = 0.325 * (magic.level() / 2 + magic.level());
					int finalLevel = (int) (base + Math.max(
							Math.max(ranged, meelee), mage));
					log.logString("Caculated pker level: " + finalLevel);
					addBlacklist(p.getName(), finalLevel);
				} else {
					log.logString("Couldn't load data for player! Using your combat level as baseline");
					addBlacklist(p.getName(), -100);
				}
			}
		});
		t.start();
	}

	private Player playerFacingMe() {
		double base = 0.25 * (s.client.getSkills().getLevel(Skill.DEFENCE)
				+ s.client.getSkills().getLevel(Skill.HITPOINTS) + (s.client
				.getSkills().getLevel(Skill.PRAYER) / 2));
		double meelee = 0.325 * (s.client.getSkills().getLevel(Skill.ATTACK) + s.client
				.getSkills().getLevel(Skill.STRENGTH));
		double ranged = 0.325 * (s.client.getSkills().getLevel(Skill.RANGED) / 2 + s.client
				.getSkills().getLevel(Skill.RANGED));
		double mage = 0.325 * (s.client.getSkills().getLevel(Skill.MAGIC) / 2 + s.client
				.getSkills().getLevel(Skill.MAGIC));
		int myLevel = (int) (base + Math.max(Math.max(ranged, meelee), mage));
		List<Player> players = s.client.getLocalPlayers();
		for (Player p : players) {
			if (p != null && p.isFacing(s.myPlayer()))
				return p;
			if (p != null && blacklist.keySet().contains(p.getName())) {
				PKerPair pp = blacklist.get(p.getName());
				if (Math.abs(pp.level - myLevel) <= pp.delta) {
					return p;
				}
			}
		}
		return null;
	}

	private Item getBankGlory() {
		if (s.client.getBank().isOpen()) {
			Item[] items = s.client.getBank().getItems();
			for (int num = 4; num > 0; num--) {
				for (Item i : items) {
					if (i == null)
						continue;
					if (i.getName().equals("Amulet of glory(" + num + ")"))
						return i;
				}
			}
		}
		return null;
	}

	private String getInventoryGlory(boolean bank) {
		if (!bank) {
			for (Item i : s.client.getInventory().getItems()) {
				if (i != null && i.getName().startsWith("Amulet of glory("))
					return i.getName();
			}
			return null;
		} else {
			String ret = null;
			for (Item i : s.client.getInventory().getItems()) {
				if (i == null)
					continue;
				if (i.getId() == pickaxeID)
					inventoryContainsPickaxeID = true;
				if (i.getId() == hatchetID)
					inventoryContainsHatchetID = true;
				if (contains(energyPotIDs, i.getId()))
					inventoryContainsEnergyPotIDs = true;
				if (i.getName().equals("Ring of life"))
					inventoryContainsRingOfLife = true;
				if (i.getName().equals(foodName))
					inventoryContainsFoodName = true;
				if (i.getName().startsWith("Amulet of glory("))
					ret = i.getName();
			}
			return ret;
		}
	}

	private boolean isWearingGlory() {
		try {
			return (s.equipmentTab.getItemInSlot(EquipmentSlot.AMULET) != null && s.equipmentTab
					.getItemInSlot(EquipmentSlot.AMULET).getName()
					.startsWith("Amulet of glory("));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	protected boolean existsTalkingInterface() {
		for (int parentID : talkInterfaceParentIDs) {
			if (isValidInterface(parentID))
				return true;
		}
		return false;
	}

	private Position findClosestReachablePosition(Position p) {
		double dist = 2000000000.0;
		Position node = null;
		for (int dx = -18; dx < 18; dx++) {
			for (int dy = -18; dy < 18; dy++) {
				if (dx * dx + dy * dy > 15 * 15)
					continue;
				Position temp = new Position(s.myX() + dy, s.myY() + dy, 0);
				if (canReach_local(temp)) {
					if (distanceBetween(temp, p) < dist) {
						dist = distanceBetween(temp, p);
						node = temp;
					}
				}
			}
		}
		return node;
	}

	private void addBlacklist(String name, int finalLevel) {
		double base = 0.25 * (s.client.getSkills().getLevel(Skill.DEFENCE)
				+ s.client.getSkills().getLevel(Skill.HITPOINTS) + (s.client
				.getSkills().getLevel(Skill.PRAYER) / 2));
		double meelee = 0.325 * (s.client.getSkills().getLevel(Skill.ATTACK) + s.client
				.getSkills().getLevel(Skill.STRENGTH));
		double ranged = 0.325 * (s.client.getSkills().getLevel(Skill.RANGED) / 2 + s.client
				.getSkills().getLevel(Skill.RANGED));
		double mage = 0.325 * (s.client.getSkills().getLevel(Skill.MAGIC) / 2 + s.client
				.getSkills().getLevel(Skill.MAGIC));
		int myLevel = (int) (base + Math.max(Math.max(ranged, meelee), mage));
		int delta = 6;
		if (Math.abs(finalLevel - myLevel) > 7) {
			log.logString("Calculation for pker seems wrong... using your combat level as baseline");
			finalLevel = myLevel;
			delta = 12;
		}
		try {
			URL add = new URL(
					"http://cmhabyss.cmhscripts.com/addblacklist.php?Username="
							+ name + "&Level=" + finalLevel + "&Delta=" + delta);
			add.openConnection();
			add.openStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private boolean clickContinue() {
		int parentInterface = -1;
		for (int parentID : talkInterfaceParentIDs) {
			if (isValidInterface(parentID)) {
				for (RS2InterfaceChild child : s.client.getInterface(parentID)
						.getChildren()) {
					if (child != null
							&& child.getMessage().contains("continue")) {
						parentInterface = parentID;
						try {
							child.interact();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						for (int i = 0; i < 10
								&& (!existsTalkingInterface() || isValidInterface(parentID)); i++)
							try {
								Thread.sleep(MethodProvider.random(500, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
							}
						return true;
					}
				}
			}
		}
		return false;
	}

	public void onMessage(String message) {
		if (message.contains("tele"))
			teleblocked = true;
	}

	private void emergencyTeleport() {
		NPC mage = closestNPCForName("Dark mage");
		switch (teleportMethod) {
		case 0:
			Position oldPosition = s.myPosition();
			while (calcDistance(oldPosition) < 30
					&& s.client.getInventory().contains("Teleport to house")
					&& !s.canReach(mage)) {
				try {
					s.client.getInventory().interactWithName(
							"Teleport to house", "Break");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}
				try {
					s.sleep(MethodProvider.random(10, 100));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}
			}
			break;
		case 1:
			switch (emergencyTeleportMethod) {
			case 0:
				oldPosition = s.myPosition();
				while (calcDistance(oldPosition) < 30
						&& s.client.getInventory().contains("Varrock teleport")
						&& !s.canReach(mage)) {
					try {
						s.client.getInventory().interactWithName(
								"Varrock teleport", "Break");
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
					try {
						s.sleep(MethodProvider.random(10, 100));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
				}
				break;
			case 1:
				oldPosition = s.myPosition();
				while (calcDistance(oldPosition) < 30
						&& s.client.getInventory()
								.contains("Teleport to house")
						&& !s.canReach(mage)) {
					try {
						s.client.getInventory().interactWithName(
								"Teleport to house", "Break");
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
					try {
						s.sleep(MethodProvider.random(10, 100));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
				}
				break;
			case 2:
				try {
					for (int i = 0; i < 5 && !s.equipmentTab.open(); i++) {
						Thread.sleep(MethodProvider.random(300, 500));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (isValidInterface(387) && !s.canReach(mage)) {
					try {
						for (int i = 0; i < 10
								&& !s.client.getInterface(387).getChild(14)
										.interact("Operate"); i++) {
							Thread.sleep(MethodProvider.random(500, 600));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				for (int i = 0; i < 10 && !isValidInterface(234); i++)
					try {
						Thread.sleep(MethodProvider.random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if (isValidInterface(234)) {
					try {
						for (int i = 0; i < 5
								&& !s.client.getInterface(234).getChild(1)
										.interact(); i++) {
							Thread.sleep(MethodProvider.random(500, 600));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			}
		}
	}

	private boolean inventoryContainsPots() {
		for (int i : energyPotIDs) {
			if (s.client.getInventory().contains(i)) {
				return true;
			}
		}
		return false;
	}

	private RS2Object closestObjectForName(String string) {
		List<RS2Object> objects = s.client.getCurrentRegion().getObjects();
		double dist = 2000000000;
		RS2Object closest = null;
		for (RS2Object o : objects) {
			if (o != null && o.getName().equals(string)) {
				if (calcDistance(o.getPosition()) < dist) {
					dist = calcDistance(o.getPosition());
					closest = o;
				}
			}
		}
		return closest;
	}

	private boolean noOtherObjectsAt(Position p, String name) {
		List<RS2Object> objects = s.client.getCurrentRegion().getObjects();
		for (RS2Object o : objects) {
			if (o != null && o.getX() == p.getX() && o.getY() == p.getY()
					&& !o.getName().equals(name)) {
				return false;
			}
		}
		return true;
	}

	private boolean isValidInterface(int ID) {
		RS2Interface inte = s.client.getInterface(ID);
		if (inte == null || !inte.isValid() || !inte.isVisible())
			return false;
		return true;
	}

	private RS2Object closestObjectForRealIDModelID(int ID, int modelID,
			String action, String name) {
		RS2Object ret = null;
		try {
			double dist = 2000000000;
			for (RS2Object o : s.client.getCurrentRegion().getObjects()) {
				if (o != null && o.getDefinition().getRealId() == ID
						&& o.getName().equals(name)
						&& calcDistance(o.getPosition()) < dist
						&& contains(o.getDefinition().getActions(), action)
						&& o.getDefinition().getModelIds().length > 0
						&& o.getDefinition().getModelIds()[0] == modelID) {
					dist = calcDistance(o.getPosition());
					ret = o;
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return ret;
	}

	private NPC closestNPCForName(String name) {
		List<NPC> npcs = s.client.getLocalNPCs();
		double distance = 2000000000.0;
		NPC closest = null;
		for (NPC n : npcs) {
			if (n != null && n.getName().equals(name)
					&& calcDistance(n.getPosition()) < distance) {
				closest = n;
			}
		}
		return closest;
	}

	private boolean containsDegradedPouches() {
		return s.client.getInventory().contains(degradedMediumPouchID)
				|| s.client.getInventory().contains(degradedLargePouchID)
				|| s.client.getInventory().contains(degradedGiantPouchID);
	}

	private RS2Object closestObjectForName2(int ID, String action) {
		RS2Object ret = null;
		try {
			double dist = 2000000000;
			for (RS2Object o : s.client.getCurrentRegion().getObjects()) {
				if (o != null && calcDistance(o.getPosition()) < dist
						&& contains(o.getDefinition().getActions(), action)) {
					dist = calcDistance(o.getPosition());
					ret = o;
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return ret;
	}

	private void getBlacklist() {
		blacklist = new HashMap<String, PKerPair>();
		try {
			URL url = new URL(
					"http://cmhabyss.cmhscripts.com/getblacklist2.php");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String s = null;
			PKerPair pp = new PKerPair();
			String name = null;
			int count = 0;
			while (true) {
				while ((s = br.readLine()) == null) {
				}
				if (s.equals("DONE")) {
					break;
				} else {
					count++;
					System.out.println(s + " " + count);
					if (count == 1) {
						name = s;
						System.out.println("Name: " + name);
					} else if (count == 2) {
						pp.level = Integer.parseInt(s);
						System.out.println("Level: " + pp.level);
					} else if (count == 3) {
						pp.delta = Integer.parseInt(s);
						System.out.println("Delta: " + pp.delta);
						count = 0;
						blacklist.put(name, pp);
						pp = new PKerPair();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			blacklist = null;
		}
	}

	private RS2Object closestObjectForRealID(int ID) {
		List<RS2Object> objects = s.client.getCurrentRegion().getObjects();
		double dist = 2000000000;
		RS2Object closest = null;
		for (RS2Object o : objects) {
			if (o != null && o.getDefinition().getRealId() == ID
					&& noOtherObjectsAt(o.getPosition(), o.getName())) {
				if (calcDistance(o.getPosition()) < dist) {
					dist = calcDistance(o.getPosition());
					closest = o;
				}
			}
		}
		return closest;
	}

	private RS2Object closestObjectForName(String name, String action) {
		List<RS2Object> objects = s.client.getCurrentRegion().getObjects();
		double dist = 2000000000;
		RS2Object closest = null;
		for (RS2Object o : objects) {
			if (o != null && o.getName().equals(name)
					&& contains(o.getDefinition().getActions(), action)) {
				if (calcDistance(o.getPosition()) < dist) {
					dist = calcDistance(o.getPosition());
					closest = o;
				}
			}
		}
		return closest;
	}

	private void depositUnneededItems() throws InterruptedException {
		ArrayList<Integer> doNotDepositList = new ArrayList<Integer>();
		// Check the potions
		if (bringPots) {
			// Figure out which energy potion has the greatest dosage and bring
			// that
			int greatestDoseIndex = -1;
			for (int i = energyPotIDs.length - 1; i >= 0; i--) {
				if (s.client.getBank().contains(energyPotIDs[i])) {
					greatestDoseIndex = i;
					break;
				}
			}
			// While there's more than one, try up to 5 times to deposit the
			// rest.
			if (greatestDoseIndex != -1) {
				while (s.client.getInventory().getAmount(tinderboxID) > 1) {
					int oldAmount = (int) s.client.getInventory().getAmount(
							tinderboxID);
					for (int i = 0; i < 5
							&& !s.client.getBank().depositX(
									tinderboxID,
									(int) (s.client.getInventory().getAmount(
											tinderboxID) - 1)); i++) {
						s.sleep(MethodProvider.random(300, 500));
					}
					// Wait 2-4 seconds for the inventory to update
					for (int i = 0; i < 10
							&& s.client.getInventory().getAmount(tinderboxID) == oldAmount; i++) {
						s.sleep(MethodProvider.random(200, 400));
					}
				}
			}
			// Now add the remaining potion id to the doNotDepositList so it
			// doesn't get deposited
			doNotDepositList.add(tinderboxID);
		}
		// If bring food, deposit until amount is correct
		if (bringFood) {
			if (s.client.getInventory().getAmount(foodName) > foodToBring) {
				int oldAmount = (int) s.client.getInventory().getAmount(
						foodName);
				for (int i = 0; i < 5
						&& !s.client.getBank().depositX(
								getIDFromBank(foodName),
								(int) (s.client.getInventory().getAmount(
										foodName) - foodToBring)); i++) {
					s.sleep(MethodProvider.random(300, 500));
				}
				// Wait 2-4 seconds for the inventory to update
				for (int i = 0; i < 10
						&& s.client.getInventory().getAmount(foodName) == oldAmount; i++) {
					s.sleep(MethodProvider.random(200, 400));
				}
			}
			doNotDepositList.add(getIDFromBank(foodName));
		}
		// If we're using pouches, don't deposit them
		if (useSmallPouch) {
			doNotDepositList.add(smallPouchID);
		}
		if (useMediumPouch) {
			doNotDepositList.add(mediumPouchID);
			doNotDepositList.add(degradedMediumPouchID);
		}
		if (useLargePouch) {
			doNotDepositList.add(largePouchID);
			doNotDepositList.add(degradedLargePouchID);
		}
		if (useGiantPouch) {
			doNotDepositList.add(giantPouchID);
			doNotDepositList.add(degradedGiantPouchID);
		}

		// Add tinderbox to doNotDepositList if you're using burn boils
		// method
		if (burnBoils) {
			doNotDepositList.add(tinderboxID);
			// If there is more than one, deposit it
			while (s.client.getInventory().getAmount(tinderboxID) > 1) {
				int oldAmount = (int) s.client.getInventory().getAmount(
						tinderboxID);
				for (int i = 0; i < 5
						&& !s.client.getBank().depositX(
								tinderboxID,
								(int) (s.client.getInventory().getAmount(
										tinderboxID) - 1)); i++) {
					s.sleep(MethodProvider.random(300, 500));
				}
				// Wait 2-4 seconds for the inventory to update
				for (int i = 0; i < 10
						&& s.client.getInventory().getAmount(tinderboxID) == oldAmount; i++) {
					s.sleep(MethodProvider.random(200, 400));
				}
			}
		}

		// Only one of (hatchet, pickaxe) can be equipped. Hatchet will remain
		// in inventory
		if (chopTendrils && mineRocks) {
			doNotDepositList.add(hatchetID);
			while (s.client.getInventory().getAmount(hatchetID) > 1) {
				int oldAmount = (int) s.client.getInventory().getAmount(
						hatchetID);
				for (int i = 0; i < 5
						&& !s.client.getBank().depositX(
								hatchetID,
								(int) (s.client.getInventory().getAmount(
										hatchetID) - 1)); i++) {
					s.sleep(MethodProvider.random(300, 500));
				}
				// Wait 2-4 seconds for the inventory to update
				for (int i = 0; i < 10
						&& s.client.getInventory().getAmount(hatchetID) == oldAmount; i++) {
					s.sleep(MethodProvider.random(200, 400));
				}
			}
		}

		// Add essence to the list
		doNotDepositList.add(essID);

		// Add the teleport tabs to the list
		switch (teleportMethod) {
		case 0:
			doNotDepositList.add(8013);
			break;
		case 1:
			switch (emergencyTeleportMethod) {
			case 0:
				doNotDepositList.add(8007);
				break;
			case 1:
				doNotDepositList.add(8013);
				break;
			}
		}
		String string = "Depositing everything except: ";
		// Now, deposit everything else that's not on the list
		int[] list = new int[doNotDepositList.size()];
		for (int i = 0; i < doNotDepositList.size(); i++) {
			list[i] = doNotDepositList.get(i);
			string += (list[i] + ",");
		}
		int count = 0;
		for (int i : list) {
			count += s.client.getInventory().getAmount(i);
		}
		if (count == 0) {
			for (int i = 0; i < 5 && !s.client.getBank().depositAll(); i++) {
				s.sleep(MethodProvider.random(300, 600));
			}
		} else {
			for (int i = 0; i < 5 && !s.client.getBank().depositAllExcept(list); i++)
				s.sleep(MethodProvider.random(300, 600));
		}
	}

	private boolean inventoryContainsTeleportMethod() {
		switch (teleportMethod) {
		case 0:
			return s.client.getInventory().contains("Teleport to house");
		case 1:
			if (!isWearingGlory())
				return false;
			switch (emergencyTeleportMethod) {
			case 0:
				return s.client.getInventory().contains("Varrock teleport");
			case 1:
				return s.client.getInventory().contains("Teleport to house");
			case 2:
				return true;
			}
		}
		return false;
	}

	private int getHealth() {
		return s.client.getSkills().getCurrentLevel(Skill.HITPOINTS);
	}

	private int getIDFromBank(String name) {
		Item[] items = s.client.getBank().getItems();
		for (Item i : items) {
			if (i != null && i.getName().equals(name)) {
				return i.getId();
			}
		}
		return -1;
	}

	private boolean[][] floodFill = new boolean[37][37];
	private boolean[][] hasFlooded = new boolean[37][37];
	private boolean[][] goodMatrix = new boolean[37][37];
	private Position floodFillCenter;

	private void runFloodFill(int x, int y) {
		floodFill[x][y] = true;
		hasFlooded[x][y] = true;
		if (x + 1 < 37 && !hasFlooded[x + 1][y] && goodMatrix[x + 1][y]) {
			runFloodFill(x + 1, y);
		}
		if (y + 1 < 37 && !hasFlooded[x][y + 1] && goodMatrix[x][y + 1]) {
			runFloodFill(x, y + 1);
		}
		if (x - 1 >= 0 && !hasFlooded[x - 1][y] && goodMatrix[x - 1][y]) {
			runFloodFill(x - 1, y);
		}
		if (y - 1 >= 0 && !hasFlooded[x][y - 1] && goodMatrix[x][y - 1]) {
			runFloodFill(x, y - 1);
		}
	}

	private RS2Object getClosestAbyssalRift() {
		for (int i = -18; i <= 18; i++) {
			for (int j = -18; j <= 18; j++) {
				floodFill[i + 18][j + 18] = false;
				hasFlooded[i + 18][j + 18] = false;
				goodMatrix[i + 18][j + 18] = true;
			}
		}
		floodFillCenter = s.myPosition();
		floodFill[18][18] = true;
		ArrayList<RS2Object> rifts = new ArrayList<RS2Object>();
		for (RS2Object o : s.client.getCurrentRegion().getObjects()) {
			if (o != null) {
				if (o.getName().equals("Abyssal rift"))
					rifts.add(o);
				if (o.getId() / 10 == 2615) {
					try {
						goodMatrix[o.getPosition().getX()
								- floodFillCenter.getX() + 18][o.getPosition()
								.getY() - floodFillCenter.getY() + 18] = false;
					} catch (Exception e) {
					}
				}
			}
		}
		runFloodFill(18, 18);
		System.out.println("GOOD MATRIX");
		for (int i = 0; i < goodMatrix.length; i++) {
			for (int j = 0; j < goodMatrix[i].length; j++) {
				System.out.print(goodMatrix[i][j] ? 0 : 1);
			}
			System.out.println();
		}

		System.out.println("FLOODFILL");
		for (int i = 0; i < goodMatrix.length; i++) {
			for (int j = 0; j < goodMatrix[i].length; j++) {
				System.out.print(floodFill[i][j] ? 0 : 1);
			}
			System.out.println();
		}

		// Find the furthest tile that can still be reached
		double dist = 0.0;
		Position p = null;
		for (int dx = -16; dx <= 16; dx++) {
			for (int dy = -16; dy <= 16; dy++) {
				if (dx * dx + dy * dy < 100)
					continue;
				Position temp = new Position(s.myX() + dx, s.myY() + dy,
						s.myZ());
				if (canReach_local(temp) && calcDistance(temp) > dist) {
					dist = calcDistance(temp);
					p = temp;
				}
			}
		}
		if (p == null)
			return null;
		dist = 2000000000.0;
		RS2Object ret = null;
		for (RS2Object o : rifts) {
			System.out.println("Rift found at "
					+ o.getX()
					+ " "
					+ o.getY()
					+ " with distance: "
					+ distanceBetween(o.getPosition(), p)
					+ "+"
					+ calcDistance(o.getPosition())
					+ "="
					+ (distanceBetween(o.getPosition(), p) + calcDistance(o
							.getPosition())));
			if (distanceBetween(o.getPosition(), p)
					+ calcDistance(o.getPosition()) < dist) {
				dist = distanceBetween(o.getPosition(), p)
						+ calcDistance(o.getPosition());
				ret = o;
			}
		}
		return ret;
	}

	public boolean canReach_local(Position p) {
		int dx = p.getX() - floodFillCenter.getX();
		dx += 18;
		if (dx < 0 || dx > floodFill.length) {
			return false;
		}
		int dy = p.getY() - floodFillCenter.getY();
		dy += 18;
		if (dy < 0 || dy > floodFill.length) {
			return false;
		}
		return floodFill[dx][dy];
	}

	private double distanceBetween(Position p, Position p2) {
		return Math.sqrt((p.getX() - p2.getX()) * (p.getX() - p2.getX())
				+ (p.getY() - p2.getY()) * (p.getY() - p2.getY()));
	}

	private boolean inventoryContainsOneOf(int[] ids) {
		for (int i : ids)
			if (s.client.getInventory().contains(i))
				return true;
		return false;
	}

	private int getLowestEnergyPotID() {
		for (int i : energyPotIDs) {
			if (s.client.getInventory().contains(i)) {
				return i;
			}
		}
		return -1;
	}

	private boolean contains(int[] stack, int needle) {
		for (int s : stack) {
			if (s == needle)
				return true;
		}
		return false;
	}

	private boolean contains(String[] stack, String needle) {
		for (String s : stack) {
			if (s != null && s.contains(needle))
				return true;
		}
		return false;
	}

	private double calcDistance(Position p) {
		int x = s.myX();
		int y = s.myY();
		int x2 = p.getX();
		int y2 = p.getY();
		return Math.sqrt((x - x2) * (x - x2) + (y - y2) * (y - y2));
	}

	private double rotateX(double dx, double dy) {
		// Rotate by 10 degrees, direction depends on rotation variable. Returns
		// x-coord
		return Math.cos(Math.PI / 9 * rotation) * dx
				- Math.sin(Math.PI / 9 * rotation) * dy;
	}

	private double rotateY(double dx, double dy) {
		// Rotate by 10 degrees, direction depends on rotation variable. Returns
		// y-coord
		return Math.sin(Math.PI / 9 * rotation) * dx
				+ Math.cos(Math.PI / 9 * rotation) * dy;
	}

	private final Position lumbridgeTile = new Position(3222, 3219, 0);

	public int onLoop() {
		double hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
		int minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
		int seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
		log.setRuntime(pad((int) (hours)) + ":" + pad(minutes) + ":"
				+ pad(seconds));
		log.setRunesCrafted(this.crafted + "");
		log.setRunesHr("" + (int) (this.crafted / hours));
		if (blacklist != null)
			log.setBlacklistStatus("Loaded " + blacklist.size() + " players");
		if (calcDistance(lumbridgeTile) < 20 && s.myZ() == 0) {
			state = State.DEATHWALK_LUMBRIDGE;
		}
		s.client.setMouseSpeed(MethodProvider.random(12, 15));
		if (stopScript) {
			am.notifyStopped();
			try {
				s.stop();
			} catch (InterruptedException e) {
				return 0;
			}
		}
		try {
			am.executeNextActions();
			s.sleep(MethodProvider.random(900, 1100));
		} catch (InterruptedException e) {
			am.requestInterrupt("Client");
		}
		return 0;
	}

	private double percentToNextLevel(Skill skill) {
		int level = s.client.getSkills().getLevel(skill);
		double total = 0.0;
		total = xpForLevels[level] - xpForLevels[level - 1];
		return 1 - ((experienceToNextLevel(skill) + 0.0) / total);
	}

	/***************** MADE BY ABIBOT *******************/
	int[] xpForLevels = new int[] { 0, 83, 174, 276, 388, 512, 650, 801, 969,
			1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470,
			5018, 5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363,
			14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648,
			37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014,
			91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040,
			203254, 224466, 247886, 273742, 302288, 333804, 368599, 407015,
			449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257,
			992895, 1096278, 1210421, 1336443, 1475581, 1629200, 1798808,
			1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792,
			3972294, 4385776, 4842295, 5346332, 5902831, 6517253, 7195629,
			7944614, 8771558, 9684577, 10692629, 11805606, 13034431 };

	private int experienceToNextLevel(Skill skill) {
		int xp = s.client.getSkills().getExperience(skill);
		for (int i = 0; i < 99; i++) {
			if (xp < xpForLevels[i]) {
				return (xpForLevels[i] - xp);
			}
		}
		return (200000000 - xp);
	}

	private String pad(int i) {
		String ret = i + "";
		if (ret.length() == 1) {
			return "0" + ret;
		}
		return ret;
	}

	public void onPaint(Graphics g) {
		if (canStart) {
			if (hide) {
				g.setColor(Color.BLUE);
				g.fillOval(492, 346, 28, 28);
				return;
			}
			switch (paintI) {
			case 0:
				g.drawImage(paintImages[paintI], -20, 205, null);
				g.setColor(Color.RED);
				String status = state.toString();
				g.drawString("Current State: " + status, 12, 400);
				g.drawString("Logged in as: " + s.myPlayer().getName() + ".",
						12, 415);
				double hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
				int minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
				int seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
				g.drawString("Runtime: " + "Running for: " + pad((int) (hours))
						+ ":" + pad(minutes) + ":" + pad(seconds), 12, 430);

				gainedExp = s.client.getSkills().getExperience(
						Skill.RUNECRAFTING)
						- startExp;
				g.drawString("Rate: " + (int) (crafted / hours) + " Runes/HR",
						12, 445);
				g.drawString("Crafted " + (crafted) + " Runes", 12, 460);
				break;
			case 1:
				g.drawImage(paintImages[1], -20, 205, null);
				g.setColor(Color.RED);
				hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
				minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
				seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
				gainedExp = s.client.getSkills().getExperience(
						Skill.RUNECRAFTING)
						- startExp;
				g.drawString("Current Runecrafting Level: "
						+ s.client.getSkills().getLevel(Skill.RUNECRAFTING),
						12, 400);
				g.drawString("Experience Gained: " + gainedExp + " ("
						+ (int) (gainedExp / hours) + ")", 12, 415);
				g.drawString("Experience to Level: "
						+ experienceToNextLevel(Skill.RUNECRAFTING), 12, 430);
				double r = ((gainedExp) / (seconds + minutes * 60 + hours * 3600.0));
				if (r == 0)
					r = .00000001;
				int TTL = (int) (experienceToNextLevel(Skill.RUNECRAFTING) / r);
				TTL /= 2;
				hours = (TTL) / 3600;
				minutes = (int) (((TTL) / 60.0) % 60);
				seconds = (int) ((TTL) % 60);
				g.drawString("Time To Level: " + pad((int) hours) + ":"
						+ pad(minutes) + ":" + pad(seconds), 12, 445);
				double perc = percentToNextLevel(Skill.RUNECRAFTING);
				g.setColor(new Color(0, 1, 0, 0.75f));
				g.fillRect(15 + offsetX, 475 + offsetY, (int) (500 * perc), 17);
				g.setColor(new Color(1, 1, 1, 0.75f));
				g.fillRect(15 + offsetX, 475 + offsetY, 490, 8);
				g.setColor(new Color(.5f, .5f, .5f, 0.75f));
				g.fillRect(15 + offsetX, 484 + offsetY, 490, 8);
				g.setColor(Color.RED);
				g.drawString((int) (perc * 100) + "% : "
						+ experienceToNextLevel(Skill.RUNECRAFTING) + " TNL",
						200, 489 + offsetY);
				break;
			case 2:
				g.drawImage(paintImages[2], -20, 205, null);
				g.setColor(Color.RED);
				g.drawString("CMHAbyss Version " + currentVersion, 12, 400);
				g.drawString("Script Developed by Ericthecmh", 12, 415);
				break;
			case 3:
				g.drawImage(paintImages[3], -20, 205, null);
				g.setColor(Color.RED);
				g.drawString("My Email: info@cmhscripts.com", 12, 400);
				g.drawString(
						"or leave feedback on the OSBot Forum Thread under SDN->Runecrafting->CMHAbyss",
						12, 430);
				g.drawString("Any and all feedback is welcome.", 12, 445);
				break;
			}
			// g.drawString((antiban ? "ANTIBAN" : state.toString()), 100, 100);
			// gainedExp =
			// s.client.getSkills().getExperience(Skill.RUNECRAFTING)
			// - startExp;
			// g.drawString("Experience Gained: " + gainedExp, 100, 180);
			// mined = gainedExp / 5;
			// double hours = (System.currentTimeMillis() - startTime) / 1000.0
			// / 3600;
			// g.drawString("" + (int) (mined / hours) + " ESS/HR", 100, 120);
			// g.drawString("" + (mined) + " Mined ess", 100, 140);
			// int minutes = (int) (((System.currentTimeMillis() - startTime) /
			// 1000.0 / 60.0) % 60);
			// int seconds = (int) (((System.currentTimeMillis() - startTime) /
			// 1000.0) % 60);
			// g.drawString("Running for: " + (int) (hours) + ":" + minutes +
			// ":"
			// + seconds, 100, 160);
			// g.drawString("Current Mining Level: "
			// + s.client.getSkills().getLevel(Skill.RUNECRAFTING), 100,
			// 200);
		}
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getX() >= 9 && e.getX() <= 75 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 0;
		} else if (e.getX() >= 75 && e.getX() <= 152 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 1;
		} else if (e.getX() >= 152 && e.getX() <= 220 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 2;
		} else if (e.getX() >= 220 && e.getX() <= 296 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 3;
		}
		if (s.distance(e.getX(), e.getY(), 506, 358) <= 14) {
			hide = !hide;
		}
	}

	private void setRunning2(boolean on) {
		try {
			System.out.println("AOEUAEOUAOEUAOEUAOEUAOEUAEOUAEOUAOEUAEO" + on
					+ " " + s.isRunning());
			if (on == s.isRunning())
				return;
			for (int i = 0; i < 10 && !s.openTab(Tab.SETTINGS); i++) {
				s.sleep(s.random(500, 600));
			}
			RS2Interface inte = s.client.getInterface(261);
			if (inte != null) {
				RS2InterfaceChild child = inte.getChild(53);
				if (child != null) {
					child.hover();
					s.sleep(s.random(1000, 1100));
					child.interact();
					for (int i = 0; i < 3 && s.isRunning() != on; i++)
						s.sleep(random(350, 500));
				}
			}
		} catch (Exception e) {

		}
		return;
	}

	public void onExit() {
		am.notifyStopped();
		randomMonitorThread.interrupt();
		randomMonitorThread.stop();
	}
}
