public class ActionThread implements Runnable {
	private Action action;
	private boolean isRunning;
	private boolean scriptActive = true;
	private CMHScript script;
	int index;

	public ActionThread(CMHScript script, int i) {
		this.script = script;
		index = i;
	}

	public boolean isRunning() {
		return this.isRunning;
	}

	public void setAction(Action a) {
		action = a;
		isRunning = true;
	}

	public Action getAction() {
		return action;
	}

	public void notifyStopped() {
		scriptActive = false;
	}

	public void notifyInterrupt(String ID) {
		if (action == null)
			return;
		if (action.getID().equals(ID))
			return;
		if (action != null) {
			action.setInterrupter(ID);
			action.setRunning(false);
		}
		script.log.removeThreadAction(index, "");
	}

	public void run() {
		
		while (scriptActive) {
			try {
				while (scriptActive) {
					if (action != null) {
						try {
							System.out.println(0);
							isRunning = true;
							System.out.println(1);//1
							action.setScript(script);
							System.out.println(2);//2
							script.log.setThreadAction(index, action.getID());
							System.out.println(3);//3
							action.setRunning(true);
							System.out.println(4);//4
							action.run();
							System.out.println(5);//5
						} catch (Exception e) {

						} finally {
							isRunning = false;
							//System.out.println(6);//6
							if (action != null)
								action.setRunning(false);
							action = null;
							//System.out.println(7);
							script.log.removeThreadAction(index, "");
							//System.out.println(8);
						}
					}
					//System.out.println(9);
					while (action == null && scriptActive) {
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							//e.printStackTrace();
						}
					}
					//System.out.println(10);
				}
			} catch (Exception e) {

			} finally {
				//System.out.println(11);
				isRunning = false;
				//System.out.println(12);
				if (action != null)
					action.setRunning(false);
				//System.out.println(13);
				action = null;
				//System.out.println(14);
				script.log.removeThreadAction(index, "");
				//System.out.println(15);
			}
		}
	}
}