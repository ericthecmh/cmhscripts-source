import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import org.osbot.script.RandomEventSolver;
import org.osbot.script.Script;
import org.osbot.script.mouse.RectangleDestination;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.model.Entity;
import org.osbot.script.rs2.model.Item;
import org.osbot.script.rs2.model.NPC;
import org.osbot.script.rs2.model.Player;
import org.osbot.script.rs2.model.RS2Object;
import org.osbot.script.rs2.randoms.RandomBehaviourHook;
import org.osbot.script.rs2.randoms.RandomManager;
import org.osbot.script.rs2.skill.Skill;
import org.osbot.script.rs2.ui.Bank;
import org.osbot.script.rs2.ui.RS2Interface;
import org.osbot.script.rs2.ui.RS2InterfaceChild;
import org.osbot.script.rs2.ui.Tab;

public class CMHEssMiner extends Script {
	private boolean shouldHop = false;
	private Script loader;
	private final String currentVersion = "2.2.0";
	private final String settingsVersion = "1.4.0";
	private boolean canStart = false;
	private State state;
	private int walkI = 0;
	private int mined = 0;
	private long startTime;
	private final int CONST = 1;
	private boolean locked = false;
	private int startExp = 0;
	private int gainedExp = 0;
	private Thread antibanThread;
	private double antibanTrigger = 0.1;
	private boolean scriptStopped;
	private boolean antiban = false;
	private LoginForm gui;
	private CMHEssMiner2 newgui;
	private BufferedImage[] paintImages = new BufferedImage[4];
	private boolean doneLoading;
	private int paintI = 0;
	private final int offsetX = -3, offsetY = -20;
	private boolean hide;
	private final boolean gui2 = true;
	private int tileindex;
	private boolean givePrem;
	private int stopMined, stopLevel;
	private boolean updated;
	private int turnOnRunAt;
	private boolean teleLummy;
	private int responsesI;
	private ArrayList<String> responses;
	private CMHCleverbot cleverbot;
	private boolean useCleverbot;
	private int messageCount;
	private int threshold;
	private RandomEventSolver loginHandler;
	private boolean solvingRandom;
	private boolean switchingWorlds;
	private boolean hopMod;
	private long lastLoggedIn;
	private boolean[] filledPouch = new boolean[4];
	private boolean[] filledPouch2 = new boolean[3];
	private boolean hasEmptied;
	private long lastRuntimeUpdate;
	private int lastExpUpdate, lastMinedUpdate;

	/****************************** RANDOM HANDLERS ******************************/
	CMHEvilBobHandler bobHandler;

	/****************************** GENERAL VARIABLES ******************************/
	private final int rockID = 25378; // UPDATE
	private final int[] portalIDs = { 26253, 25384 }; // UPDATE
	private final int closedDoor = 12643; // UPDATE
	private final int essID = 1436;
	private final int pureEssID = 7936;
	private final int[] pickIDs = { 1265, 1267, 1269, 1273, 1271, 1275, 15259,
			5509, 5510, 5512, 5514 };
	private final int arburyID = 1290; // UPDATE
	private final int[] blacklistedWorlds = new int[] { 308, 316, 307, 315,
			323, 324, 331, 332, 339, 340, 347, 348, 355, 356, 363, 364, 371,
			372 };
	private final int[] pouchIDs = new int[] { 5509, 5510, 5512, 5514 };
	private final int[] degradedPouchIDs = new int[] { 5511, 5513, 5515 };

	/****************************** VARROCK VARIABLES ******************************/
	private final Area varrockBank = new Area(3257, 3423, 3250, 3416);
	private final Position[][] pathToArbury = new Position[][] {
			new Position[] { new Position(3262, 3419, 0),
					new Position(3259, 3411, 0), new Position(3258, 3403, 0),
					new Position(3253, 3402, 0) },
			new Position[] { new Position(3245, 3419, 0),
					new Position(3244, 3411, 0), new Position(3244, 3402, 0),
					new Position(3252, 3398, 0), new Position(3253, 3402, 0) },
			new Position[] { new Position(3246, 3419, 0),
					new Position(3250, 3412, 0), new Position(3254, 3407, 0),
					new Position(3252, 3398, 0), new Position(3253, 3402, 0) } };
	private final Position[] pathToRock1 = new Position[] {
			new Position(8444, 12209, 0), new Position(8453, 12202, 0) };
	private final Position[] pathToRock2 = new Position[] {
			new Position(8442, 12222, 0), new Position(8448, 12225, 0),
			new Position(8452, 12231, 0) };
	private final Position[] pathToRock3 = new Position[] {
			new Position(8435, 12221, 0), new Position(8430, 12226, 0),
			new Position(8423, 12230, 0) };
	private final Position[] pathToRock4 = new Position[] {
			new Position(8436, 12210, 0), new Position(8429, 12206, 0),
			new Position(8424, 12201, 0) };
	private final Position[][] pathToVarrock = new Position[][] {
			new Position[] { new Position(3258, 3407, 0),
					new Position(3262, 3415, 0), new Position(3255, 3420, 0) },
			new Position[] { new Position(3245, 3401, 0),
					new Position(3244, 3407, 0), new Position(3245, 3414, 0),
					new Position(3252, 3421, 0) } };
	private final Position arburyTile = new Position(3253, 3399, 0);
	private final Position[] pathFromLumby = new Position[] {
			new Position(3228, 3218, 0), new Position(3235, 3222, 0),
			new Position(3243, 3225, 0), new Position(3251, 3225, 0),
			new Position(3258, 3230, 0), new Position(3262, 3238, 0),
			new Position(3256, 3246, 0), new Position(3250, 3253, 0),
			new Position(3249, 3261, 0), new Position(3247, 3269, 0),
			new Position(3242, 3276, 0), new Position(3238, 3284, 0),
			new Position(3239, 3291, 0), new Position(3239, 3301, 0),
			new Position(3242, 3309, 0), new Position(3249, 3316, 0),
			new Position(3256, 3323, 0), new Position(3265, 3326, 0),
			new Position(3273, 3332, 0), new Position(3282, 3333, 0),
			new Position(3290, 3337, 0), new Position(3290, 3348, 0),
			new Position(3295, 3355, 0), new Position(3295, 3364, 0),
			new Position(3295, 3372, 0), new Position(3294, 3380, 0),
			new Position(3292, 3388, 0), new Position(3291, 3396, 0),
			new Position(3291, 3405, 0), new Position(3288, 3413, 0),
			new Position(3285, 3421, 0), new Position(3279, 3426, 0),
			new Position(3272, 3428, 0), new Position(3264, 3428, 0),
			new Position(3256, 3427, 0), new Position(3254, 3420, 0) };
	private final int varrockStaircaseID = 13717; // UPDATE
	private final Area lumbyArea = new Area(3210, 3230, 3230, 3210);

	/****************************** Yanille Variables ****************************/
	private final int[] doorIDs = new int[] { 20833, 20832 }; // UPDATE
	private final int distentorID = 481; // UPDATE
	private final int yanilleStaircaseID = 1725; // UPDATE
	private final int ladderUpID = 1765; // UPDATE
	private final Position[] pathToDistentor = new Position[] {
			new Position(2605, 3090, 0), new Position(2597, 3088, 0) };
	private final Position[] pathToBank2 = new Position[] {
			new Position(2604, 3089, 0), new Position(2613, 3093, 0) };
	private final Area yanilleBank = new Area(2609, 3097, 2613, 3088);
	private final Position distentorTile = new Position(2591, 3087, 0);
	private long startTeleport;
	private final int[] restoreIDs = new int[] { 2430, 127, 129, 131 };
	private Position[] pathFromCammy = new Position[] {
			new Position(2763, 3468, 0), new Position(2766, 3460, 0),
			new Position(2770, 3453, 0), new Position(2770, 3444, 0),
			new Position(2769, 3436, 0), new Position(2765, 3430, 0),
			new Position(2761, 3423, 0), new Position(2757, 3416, 0),
			new Position(2752, 3409, 0), new Position(2751, 3401, 0),
			new Position(2748, 3394, 0), new Position(2746, 3388, 0),
			new Position(2744, 3379, 0), new Position(2742, 3370, 0),
			new Position(2741, 3362, 0), new Position(2739, 3354, 0),
			new Position(2735, 3347, 0), new Position(2733, 3339, 0),
			new Position(2728, 3334, 0), new Position(2722, 3329, 0),
			new Position(2715, 3323, 0), new Position(2712, 3316, 0),
			new Position(2710, 3310, 0), new Position(2707, 3305, 0),
			new Position(2701, 3300, 0), new Position(2697, 3295, 0),
			new Position(2694, 3287, 0), new Position(2692, 3280, 0),
			new Position(2686, 3275, 0), new Position(2679, 3275, 0),
			new Position(2670, 3273, 0), new Position(2662, 3270, 0),
			new Position(2653, 3269, 0), new Position(2644, 3266, 0),
			new Position(2638, 3260, 0), new Position(2636, 3251, 0),
			new Position(2634, 3243, 0), new Position(2635, 3237, 0),
			new Position(2639, 3230, 0), new Position(2642, 3220, 0),
			new Position(2640, 3212, 0), new Position(2634, 3206, 0),
			new Position(2630, 3199, 0), new Position(2628, 3191, 0),
			new Position(2622, 3184, 0), new Position(2622, 3176, 0),
			new Position(2622, 3166, 0), new Position(2622, 3157, 0),
			new Position(2624, 3146, 0), new Position(2623, 3138, 0),
			new Position(2623, 3129, 0), new Position(2622, 3120, 0),
			new Position(2620, 3113, 0), new Position(2617, 3106, 0),
			new Position(2610, 3099, 0), new Position(2613, 3092, 0)

	};

	Position[] pathToStair = null;

	private enum State {
		BANK, FINDROCK, MINE, PORTAL, // general states
		WALK_ARBURY, TELEPORT_ARBURY, WALK_VARROCK_BANK, LUMBYTOVARROCK, // Varrock
																			// specific
		WALK_DISTENTOR, OPEN_DOOR, TELEPORT_DISTENTOR, OPEN_DOOR2, WALK_YANILLE_BANK, CAMMYTOYANILLE, WALK_TO_STAIRCASE, ASCEND, WALK_LUMBY_BANK, LUMBY_BANK; // Yanille
		// specific

	}

	public CMHEssMiner(Script loader) {
		this.loader = loader;
	}

	public void onStart() {
		turnOnRunAt = random(55, 65);

		try {
			if (!(loader.client.getInterface(548).getChild(85).getPosition()
					.getX() == -1 && loader.client.getInterface(548)
					.getChild(85).getPosition().getY() == -1))
				lastLoggedIn = System.currentTimeMillis();
		} catch (Exception e) {
		}
		log("Loading paint");
		new Thread(new Runnable() {
			public void run() {
				try {
					paintImages[0] = ImageIO.read(new URL(
							"http://54.211.36.47/paint/main.png"));
					paintImages[1] = ImageIO.read(new URL(
							"http://54.211.36.47/paint/stats.png"));
					paintImages[2] = ImageIO.read(new URL(
							"http://54.211.36.47/paint/script.png"));
					paintImages[3] = ImageIO.read(new URL(
							"http://54.211.36.47/paint/feedback.png"));
					doneLoading = true;
				} catch (Exception e) {
				}
			}
		}).start();
		try {
			for (int i = 0; i < 10; i++)
				loader.sleep(random(400, 500));
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		gui = new LoginForm();
		gui.isPrem = true;
		gui.isAuth = true;
		gui.username = loader.myPlayer().getName();
		if (gui2) {
			newgui = new CMHEssMiner2(gui.isPrem);
			loadSettings(newgui);
			newgui.setVisible(true);
			while (!newgui.ready && newgui.isVisible()) {
				try {
					loader.sleep(random(300, 400));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			newgui.setVisible(false);
			saveSettings(newgui);
		}
		if (newgui.locationMenu.getSelectedIndex() == 0) {
			if (newgui.stopAfterCheck.isSelected()) {
				if (newgui.stopMined.isSelected()) {
					stopMined = Integer
							.parseInt(newgui.stopMinedText.getText());
				} else if (newgui.stopLevel.isSelected()) {
					stopLevel = Integer
							.parseInt(newgui.stopLevelText.getText());
				}
			}
		} else {
			if (newgui.stopAfterCheck1.isSelected()) {
				if (newgui.stopMined1.isSelected()) {
					stopMined = Integer.parseInt(newgui.stopMinedText1
							.getText());
				} else if (newgui.stopLevel.isSelected()) {
					stopLevel = Integer.parseInt(newgui.stopLevelText1
							.getText());
				}
			}
		}
		teleLummy = newgui.teleLummy.isSelected();
		try {
			if ((newgui.locationMenu.getSelectedIndex() == 0)) {
				RS2Object rock = loader.closestObject(rockID);
				if (rock != null) {
					if (loader.client.getInventory().isFull())
						state = State.PORTAL;
					else {
						for (int i = 0; i < filledPouch.length; i++)
							filledPouch[i] = false;
						for (int i = 0; i < filledPouch2.length; i++)
							filledPouch2[i] = false;

						state = State.MINE;
					}
				} else if (distanceBetween(loader.myX(), loader.myY(),
						arburyTile.getX(), arburyTile.getY()) < 7)
					if ((loader.client.getInventory().getAmount(essID) + loader.client
							.getInventory().getAmount(pureEssID)) > 14)
						state = State.WALK_VARROCK_BANK;
					else
						state = State.TELEPORT_ARBURY;
				else
					state = State.WALK_ARBURY;
			} else {
				RS2Object rock = loader.closestObject(rockID);
				if (rock != null) {
					if (loader.client.getInventory().isFull())
						state = State.PORTAL;
					else {
						for (int i = 0; i < filledPouch.length; i++)
							filledPouch[i] = false;
						for (int i = 0; i < filledPouch2.length; i++)
							filledPouch2[i] = false;
						state = State.MINE;
					}
				} else if (distanceBetween(loader.myX(), loader.myY(),
						distentorTile.getX(), distentorTile.getY()) < 10)
					if ((loader.client.getInventory().getAmount(essID) + loader.client
							.getInventory().getAmount(pureEssID)) > 14)
						state = State.OPEN_DOOR2;
					else
						state = State.OPEN_DOOR;
				else
					state = State.WALK_DISTENTOR;
			}
		} catch (Exception e) {

		}
		antibanThread = new Thread(new AntibanThread());
		antibanThread.start();
		startTime = System.currentTimeMillis();
		lastRuntimeUpdate = startTime;
		startExp = loader.client.getSkills().getExperience(Skill.MINING);
		bobHandler = new CMHEvilBobHandler(loader);
		canStart = true;
		responses = new ArrayList<String>();
		useCleverbot = newgui.cleverbot.isSelected();
		threshold = Integer.parseInt(newgui.cleverbotThreshold.getText());
		messageCount = 0;
		if (useCleverbot) {
			cleverbot = new CMHCleverbot(responses, 5000);
			new Thread(new CleverbotThread()).start();
		}
		hopMod = newgui.hopMod.isSelected();
		/*
		 * if (newgui.randomEvent.isSelected()) {
		 * loader.randomManager.unregisterHook(RandomManager.EVIL_BOB);
		 * loader.randomManager.registerHook(bobHandler); }
		 */
		/*
		 * loginHandler =
		 * loader.randomManager.forId(RandomManager.LOGIN_SCRIPT);
		 * loader.randomManager.unregisterHook(RandomManager.LOGIN_SCRIPT);
		 * loader.randomManager.registerHook(new RandomBehaviourHook(
		 * RandomManager.LOGIN_SCRIPT) {
		 * 
		 * @Override public boolean shouldActivate() throws InterruptedException
		 * { try { return !switchingWorlds && System.currentTimeMillis() -
		 * lastLoggedIn > 20000 && (loader.client.getInterface(548).getChild(85)
		 * .getPosition().getX() == -1 && loader.client
		 * .getInterface(548).getChild(85) .getPosition().getY() == -1) &&
		 * loader.client.getInterface(378).getChild(17) .getX() == -1; } catch
		 * (Exception e) { return true; } }
		 * 
		 * @Override public boolean shouldActivatePreLoop() { return false; }
		 * 
		 * @Override public int preLoop() { return 0; }
		 * 
		 * @Override public int onLoop() { solvingRandom = true; int pause = 0;
		 * System.out.println("Logging in."); try { loginHandler.onActivate();
		 * loginHandler.onLoop(); } catch (InterruptedException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } try {
		 * 
		 * loader.sleep(random(1000, 2000)); if
		 * (!(loader.client.getInterface(548).getChild(85) .getPosition().getX()
		 * == -1 && loader.client .getInterface(548).getChild(85).getPosition()
		 * .getY() == -1)) lastLoggedIn = System.currentTimeMillis(); } catch
		 * (Exception e) { }
		 * 
		 * solvingRandom = false; return pause; }
		 * 
		 * @Override public boolean shouldActivatePostLoop() { return false; }
		 * 
		 * @Override public int postLoop() { return 0; }
		 * 
		 * });
		 */
	}

	private boolean blacklisted(int world) {
		for (int i = 0; i < blacklistedWorlds.length; i++)
			if (blacklistedWorlds[i] == world)
				return true;
		return false;
	}

	private int getRandomWorld() {
		int world;
		do {
			world = random(305, 375);
		} while (blacklisted(world) || world == loader.client.getCurrentWorld());
		return world;
	}

	public void onLogin(int a) {
		loader.log("Logged in");
	}

	public void onPlayerMessage(String name, String message) {
		if (useCleverbot && !name.equals(loader.myPlayer().getName())
				&& state == State.MINE) {
			cleverbot.getResponse(message);
			if (++messageCount > threshold) {
				cleverbot.recreateSession();
			}
		}
	}

	private class CleverbotThread implements Runnable {

		public void run() {
			while (true) {
				if (scriptStopped)
					break;
				if (responses.size() > responsesI) {
					for (; responsesI < responses.size(); responsesI++) {
						try {
							if (Math.random() < .25)
								loader.type(responses.get(responsesI)
										.toLowerCase());
							else if (Math.random() < .50)
								loader.type(responses
										.get(responsesI)
										.toLowerCase()
										.substring(
												0,
												responses.get(responsesI)
														.length() - 1));
							else if (Math.random() < .75)
								loader.type(responses.get(responsesI));
							else
								loader.type(responses.get(responsesI)
										.substring(
												0,
												responses.get(responsesI)
														.length() - 1));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				try {
					loader.sleep(random(1000));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void onExit() {
		log("Thank you for using CMHEssMiner. You have mined " + mined
				+ " essence.");
		log("and gained " + gainedExp + " EXP.");
		scriptStopped = true;
	}

	public int onLoop() {
		/*
		 * if (abyssHandler.shouldActivate()) { abyssHandler.onStart();
		 * abyssHandler.solveRandom(); abyssHandler.onSolved(); }
		 */

		if (canStart) {
			if (!(loader.client.getInterface(548).getChild(85).getPosition()
					.getX() == -1 && loader.client.getInterface(548)
					.getChild(85).getPosition().getY() == -1))
				lastLoggedIn = System.currentTimeMillis();
			RS2Interface inte = loader.client.getInterface(499);
			if (inte != null) {
				RS2InterfaceChild c = inte.getChild(24);
				if (c != null) {
					try {
						c.hover();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						c.interact();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			if (state == State.BANK
					&& distanceBetween(loader.myX(), loader.myY(),
							distentorTile.getX(), distentorTile.getY()) < 10) {
				state = State.OPEN_DOOR2;
			}
			if (distanceBetween(loader.myX(), loader.myY(), 2584, 3087) < 4) {
				try {
					walkMiniMap2(new Position(2597, 3086, 0));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (lumbyArea.currentlyInArea()) {
				if (newgui.locationMenu.getSelectedIndex() == 0) {
					walkI = 0;
					state = State.LUMBYTOVARROCK;
				} else {
					walkI = 0;
					state = State.ASCEND;
				}
			}
			if (loader.distance(pathFromCammy[0]) < 20
					&& state != State.CAMMYTOYANILLE) {
				walkI = 0;
				state = State.CAMMYTOYANILLE;
			}
			if (loader.client.getConfig(170) == 1) {
				toggleMouse();
			}
			if (loader.myPlayer().getZ() == 1 && state != State.ASCEND) {
				RS2Object object = loader.closestObject((newgui.locationMenu
						.getSelectedIndex() == 0) ? varrockStaircaseID
						: yanilleStaircaseID);
				try {
					object.interact("Climb-down");
				} catch (Exception e) {
				}
				try {
					loader.sleep(random(1000, 1100));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return random(300, 400);
			}
			if (distance(loader.myPosition().getX(),
					loader.myPosition().getY(), 2594, 9486) < 10) {
				RS2Object object = loader.closestObject(ladderUpID);
				try {
					object.interact("Climb-up");
				} catch (Exception e) {
				}
				try {
					loader.sleep(random(1000, 1100));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return random(300, 400);
			}
			switch (state) {
			case LUMBYTOVARROCK:
				try {
					if (walkI == 0) {
						try {
							if (!loader.isRunning()
									&& loader.client.getRunEnergy() > 30) {
								try {
									setRunning2(true);
									loader.sleep(random(500, 700));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							walkMiniMap2(randomize(pathFromLumby[walkI++]));
						} catch (InterruptedException e) {
						}
					}
					if (walkI == pathFromLumby.length) {
						walkI = 0;
						state = State.WALK_ARBURY;
						break;
					}
					try {
						if (loader.realDistance(pathFromLumby[walkI - 1]) <= 6) {
							walkMiniMap2(randomize(pathFromLumby[walkI++]));
						} else
							walkMiniMap2(randomize(pathFromLumby[walkI - 1]));
						if (!loader.isRunning()
								&& loader.client.getRunEnergy() > 30) {
							try {
								setRunning2(true);
								loader.sleep(random(500, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} catch (InterruptedException e) {

					}
				} catch (ArrayIndexOutOfBoundsException e) {

				}
				break;
			case CAMMYTOYANILLE:
				try {
					if (walkI == 0) {
						try {
							if (!loader.isRunning()
									&& loader.client.getRunEnergy() > 30) {
								try {
									setRunning2(true);
									loader.sleep(random(500, 700));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							walkMiniMap2(randomize(pathFromCammy[walkI++]));
						} catch (InterruptedException e) {
						}
					}
					if (walkI == pathFromCammy.length) {
						walkI = 0;
						state = State.WALK_DISTENTOR;
						break;
					}
					try {
						if (loader.realDistance(pathFromCammy[walkI - 1]) <= 6) {
							walkMiniMap2(randomize(pathFromCammy[walkI++]));
						} else
							walkMiniMap2(randomize(pathFromCammy[walkI - 1]));
						if (!loader.isRunning()
								&& loader.client.getRunEnergy() > 30) {
							try {
								setRunning2(true);
								loader.sleep(random(500, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} catch (InterruptedException e) {

					}
				} catch (ArrayIndexOutOfBoundsException e) {

				}
				break;
			case WALK_ARBURY:

				if (walkI == 0) {
					if (gui2) {
						ArrayList<Integer> options = new ArrayList<Integer>();
						if (newgui.pathBox1.isSelected()) {
							options.add(0);
						}
						if (newgui.pathBox2.isSelected()) {
							options.add(1);
						}
						if (newgui.pathBox3.isSelected()) {
							options.add(2);
						}
						tileindex = options.get(random(0, options.size() - 1));
					} else {
						tileindex = 0;
					}
					try {
						if (!loader.isRunning()
								&& loader.client.getRunEnergy() > 0) {
							try {
								setRunning2(true);
								loader.sleep(random(500, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						walkMiniMap2(randomize(pathToArbury[tileindex][walkI++]));
						break;
					} catch (InterruptedException e) {
					}
				}
				if (walkI == pathToArbury[tileindex].length) {
					walkI = 0;
					state = State.TELEPORT_ARBURY;
					break;
				}
				try {
					if (loader.realDistance(pathToArbury[tileindex][walkI - 1]) <= 6) {
						walkMiniMap2(randomize(pathToArbury[tileindex][walkI++]));
					} else
						walkMiniMap2(randomize(pathToArbury[tileindex][walkI - 1]));
					if (!loader.isRunning() && loader.client.getRunEnergy() > 0) {
						try {
							setRunning2(true);
							loader.sleep(random(500, 700));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} catch (InterruptedException e) {

				}
				break;
			case TELEPORT_ARBURY:
				new Thread(new Runnable() {
					public void run() {
						try {
							loader.client.rotateCameraPitch(90);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}).start();
				RS2Object door = closestObjectTo(closedDoor, arburyTile);
				if (door != null
						&& distanceBetween(door.getPosition().getX(), door
								.getPosition().getY(), arburyTile.getX(),
								arburyTile.getY()) < 2) {
					try {
						door.interact("Open");
						loader.sleep(random(300, 400));
						do {
							door = closestObjectTo(closedDoor, arburyTile);
							if (door != null
									&& distanceBetween(door.getPosition()
											.getX(), door.getPosition().getY(),
											arburyTile.getX(),
											arburyTile.getY()) < 2)
								door.interact("Open");
							loader.sleep(random(300, 400));
						} while (door != null
								&& distanceBetween(door.getPosition().getX(),
										door.getPosition().getY(),
										arburyTile.getX(), arburyTile.getY()) < 2);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				NPC arbury = loader.closestNPCForName("Aubury");
				try {
					for (int x = 0; arbury != null
							&& x < 10
							&& (distanceBetween(loader.myX(), loader.myY(),
									arburyTile.getX(), arburyTile.getY()) < 10); x++) {
						if (!arbury.interact("Teleport")) {
							loader.sleep(random(100, 200));
							continue;
						}
						while (loader.myPlayer().isMoving()) {
							loader.sleep(100);
						}
						for (int i = 0; i < 20
								&& ((distanceBetween(loader.myX(),
										loader.myY(), arburyTile.getX(),
										arburyTile.getY()) < 10)); i++) {
							loader.sleep(random(300, 400));
						}
					}
					for (int i = 0; i < 20
							&& ((distanceBetween(loader.myX(), loader.myY(),
									arburyTile.getX(), arburyTile.getY()) < 10)); i++) {

						loader.sleep(random(300, 400));
					}
					for (int x = 0; x < 10; x++) {
						if (findObjectByName("Rune") != null) {
							state = State.FINDROCK;
							break;
						}
						loader.sleep(random(100, 200));
					}
				} catch (Exception e) {
				}
				break;
			case FINDROCK:
				for (int x = 0; x < 10; x++) {
					RS2Object rock = findObjectByName("Rune");
					if (rock == null) {
						try {
							loader.sleep(random(300, 400));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						continue;
					}
					if (!rock.isVisible()) {
						try {
							loader.walk(rock);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						while (loader.myPlayer().isMoving()) {
							try {
								loader.sleep(random(300, 400));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} else {
						Vector v1 = new Vector(0, 1);
						Vector v2 = new Vector(rock.getX() - loader.myX(),
								rock.getY() - loader.myY());
						try {
							loader.client.rotateCameraToAngle(((int) v2
									.degreeBetween(v1)));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						for (int i = 0; i < filledPouch.length; i++)
							filledPouch[i] = false;
						for (int i = 0; i < filledPouch2.length; i++)
							filledPouch2[i] = false;
						state = State.MINE;
						break;
					}
				}
			case MINE:
				if (shouldHop || (jagexModNearby() && hopMod)) {
					shouldHop = false;
					try {
						switchingWorlds = true;
						loader.log("Hopping worlds...");
						loader.worldHopper.hopWorld(getRandomWorld());
					} catch (Exception e) {

					}
					break;
				}
				if (loader.myPlayer().getAnimation() == -1) {
					try {
						RS2Object rock = loader.closestObject(rockID);
						if (rock == null)
							rock = findObjectByName("Rune");
						if (rock == null)
							break;
						Vector v1 = new Vector(0, 1);
						Vector v2 = new Vector(rock.getX() - loader.myX(),
								rock.getY() - loader.myY());
						rock.interact("Mine");
						try {
							loader.client.rotateCameraPitch(90);
						} catch (InterruptedException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
						loader.sleep(random(2000, 3000));
						loader.client.rotateCameraToAngle(((int) v2
								.degreeBetween(v1)));
					} catch (Exception e) {
					}
				} else {
					if (loader.currentTab() != Tab.INVENTORY && !locked) {
						locked = true;
						try {
							loader.openTab(Tab.INVENTORY);
							loader.sleep(random(300, 400));
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						locked = false;
					}
					if (!locked) {
						// locked = true;
						// if (newgui.locationMenu.getSelectedIndex() == 0) {
						// if (loader.client.getRunEnergy() > turnOnRunAt
						// && !loader.isRunning()) {
						// turnOnRunAt = random(55, 65);
						// try {
						// setRunning2(true);
						// loader.sleep(random(500, 700));
						// } catch (InterruptedException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
						// } else if (loader.isRunning()
						// && loader.client.getRunEnergy() < 50) {
						// setRunning2(false);
						// try {
						// loader.sleep(random(500, 700));
						// } catch (InterruptedException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
						// }
						// }
						locked = false;
					}
				}
				if (loader.client.getInventory().isFull()) {
					fillPouches();
					if (loader.client.getInventory().isFull()) {
						updateServer();
						state = State.PORTAL;
						break;
					} else {
						RS2Object rock = loader.closestObject(rockID);
						if (rock == null)
							rock = findObjectByName("Rune");
						if (rock == null)
							break;
						Vector v1 = new Vector(0, 1);
						Vector v2 = new Vector(rock.getX() - loader.myX(),
								rock.getY() - loader.myY());
						try {
							rock.interact("Mine");
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				break;
			case PORTAL:
				List<RS2Object> objects = loader.client.getCurrentRegion()
						.getObjects();
				RS2Object portal = null;
				int dist = 20000000;
				for (RS2Object p : objects) {
					if (p.getId() == portalIDs[0] || p.getId() == portalIDs[1]
							|| p.getName().contains("ortal")) {
						if (portal == null) {
							portal = p;
							dist = loader.distance(portal);
						} else {
							if (loader.distance(p) < dist) {
								dist = loader.distance(p);
								portal = p;
							}
						}
					}
				}
				if (loader.distance(arburyTile) <= 20) {
					state = State.WALK_VARROCK_BANK;
					break;
				}
				if (loader.distance(distentorTile) <= 20) {
					state = State.OPEN_DOOR2;
					break;
				}

				if (portal == null)
					break;
				if (distanceToObject(portal) > 2) {
					try {
						loader.walkMiniMap(portal.getPosition());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					for (int i = 0; i < 10 && (distanceToObject(portal) > 2); i++) {
						try {
							loader.walkMiniMap(portal.getPosition());
							loader.sleep(random(300, 400));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				System.out.println(distanceToObject(portal));
				if (loader.distance(arburyTile) <= 15) {
					state = State.WALK_VARROCK_BANK;
					break;
				}
				if (loader.distance(distentorTile) <= 20) {
					state = State.OPEN_DOOR2;
					break;
				}
				try {
					for (int i = 0; i < 3; i++) {
						Position p = new Position(portal.getX(), portal.getY(),
								0);
						if (loader.distance(arburyTile) <= 15) {
							state = State.WALK_VARROCK_BANK;
							break;
						}

						if (loader.distance(distentorTile) <= 20) {
							state = State.OPEN_DOOR2;
							break;
						}
						new Thread(new PortalThread(p)).start();
						loader.sleep(random(2000, 3000));
						portal = loader.closestObject(portalIDs);
						if (portal == null)
							break;
					}
					if (newgui.locationMenu.getSelectedIndex() == 0) {
						for (int i = 0; i < 10
								&& distanceBetween(loader.myX(), loader.myY(),
										arburyTile.getX(), arburyTile.getY()) > 15; i++) {
							loader.sleep(random(300, 400));
						}
						if (distanceBetween(loader.myX(), loader.myY(),
								arburyTile.getX(), arburyTile.getY()) <= 15)
							state = State.WALK_VARROCK_BANK;
					} else {
						for (int i = 0; i < 10
								&& distanceBetween(loader.myX(), loader.myY(),
										distentorTile.getX(),
										distentorTile.getY()) > 20; i++) {
							loader.sleep(random(300, 400));
						}
						if (loader.distance(distentorTile) <= 20) {
							state = State.OPEN_DOOR2;
							break;
						}
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case WALK_VARROCK_BANK:
				door = closestObjectTo(closedDoor, arburyTile);
				if (door != null
						&& distanceBetween(door.getPosition().getX(), door
								.getPosition().getY(), arburyTile.getX(),
								arburyTile.getY()) < 2
						&& distanceBetween(door.getPosition().getX(), door
								.getPosition().getY(), loader.myX(),
								loader.myY()) <= 7
						&& distanceTo(new Position(3253, 3401, 0)) < 4) {
					try {
						door.interact("Open");
						loader.sleep(random(300, 400));
						do {
							door = closestObjectTo(closedDoor, arburyTile);
							if (door != null
									&& distanceBetween(door.getPosition()
											.getX(), door.getPosition().getY(),
											arburyTile.getX(),
											arburyTile.getY()) < 2)
								door.interact("Open");
							loader.sleep(random(300, 400));
						} while (door != null
								&& distanceBetween(door.getPosition().getX(),
										door.getPosition().getY(),
										arburyTile.getX(), arburyTile.getY()) < 2);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (walkI == 0) {
					if (gui2) {
						ArrayList<Integer> options = new ArrayList<Integer>();
						if (newgui.pathBox1.isSelected()) {
							options.add(0);
						}
						if (newgui.pathBox2.isSelected()
								|| newgui.pathBox3.isSelected()) {
							options.add(1);
						}
						tileindex = options.get(random(0, options.size() - 1));
					} else {
						tileindex = 0;
					}
					try {
						walkMiniMap2(randomize(pathToVarrock[tileindex][walkI++]));
						loader.sleep(random(500, 600));
					} catch (InterruptedException e) {
					}
				}
				if (walkI == pathToVarrock[tileindex].length
						|| varrockBank.currentlyInArea()) {
					walkI = 0;
					hasEmptied = false;
					state = State.BANK;
					break;
				}
				try {
					if (distanceTo(pathToVarrock[tileindex][walkI - 1]) <= 6)
						walkMiniMap2(randomize(pathToVarrock[tileindex][walkI++]));
					else
						walkMiniMap2(randomize(pathToVarrock[tileindex][walkI - 1]));
				} catch (InterruptedException e) {

				}
				break;
			case BANK:
				Bank b = loader.client.getBank();
				for (int it = 0; it < 2
						&& loader.client.getInventory().getAmount(essID)
								+ loader.client.getInventory().getAmount(
										pureEssID) > 0; it++) {
					for (int i = 0; i < 10 && !b.isOpen(); i++) {
						RS2Object booth = loader
								.closestObjectForName("Bank booth");
						if (booth == null) {
							try {
								loader.sleep(random(500, 600));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							continue;
						}
						try {
							booth.interact("Bank");
							for (int j = 0; j < 10 && !b.isOpen(); j++) {
								loader.sleep(random(200, 300));
								if (loader.myPlayer().isMoving())
									j--;
							}
						} catch (Exception e) {
						}
					}
					try {
						for (int x = 0; x < 10
								&& (loader.client.getInventory().getAmount(
										essID) + loader.client.getInventory()
										.getAmount(pureEssID)) > 0; x++) {
							b.depositAllExcept(pickIDs);
							loader.sleep(random(500, 600));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (containsPouches() || containsDecayedPouches()) {
						if (!hasEmptied) {
							for (int i = 0; i < 10; i++) {
								try {
									if (loader.client.getBank().close()) {
										for (int j = 0; j < 5
												&& loader.client.getBank()
														.isOpen(); j++) {
											loader.sleep(random(250, 300));
										}
										break;
									} else
										loader.sleep(random(500, 600));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							hasEmptied = true;
							emptyPouches();
						}
					} else
						break;
				}
				boolean bankAgain = false;
				if (loader.client.getInventory().getAmount(essID)
						+ loader.client.getInventory().getAmount(pureEssID) == 0)
					if (newgui.locationMenu.getSelectedIndex() == 1
							&& loader.client.getSkills().getCurrentLevel(
									Skill.MAGIC) < 66) {
						bankAgain = true;
						for (int i = 0; i < restoreIDs.length; i++) {
							if (b.contains(restoreIDs[i])) {
								try {
									b.withdrawX(restoreIDs[i], 2);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							try {
								loader.sleep(random(1000, 1500));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					}

				for (int i = 0; i < 5
						&& b.isOpen()
						&& (!loader.isRunning()
								&& loader.client.getRunEnergy() > 10 || newgui.locationMenu
								.getSelectedIndex() == 1
								&& loader.client.getSkills().getCurrentLevel(
										Skill.MAGIC) < 66); i++) {
					try {
						b.close();
						loader.sleep(random(300, 400));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				int index = 0;
				while (newgui.locationMenu.getSelectedIndex() == 1
						&& loader.client.getSkills().getCurrentLevel(
								Skill.MAGIC) < 66) {
					Item food = loader.client.getInventory().getItemForId(
							restoreIDs[index]);
					if (food != null) {
						int oldLevel = loader.client.getSkills()
								.getCurrentLevel(Skill.MAGIC);
						try {
							for (int j = 0; j < 10
									&& !loader.client.getInventory()
											.interactWithId(restoreIDs[index],
													"Drink")
									&& loader.client.getSkills().getLevel(
											Skill.MAGIC) < 66; j++) {
								sleep(random(500, 600));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						for (int j = 0; j < 10
								&& oldLevel == loader.client.getSkills()
										.getCurrentLevel(Skill.MAGIC); j++) {
							try {
								loader.sleep(random(200, 300));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} else {
						index++;
					}

					try {
						loader.sleep(random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (newgui.locationMenu.getSelectedIndex() == 0) {
					if (newgui.stopAfterCheck.isSelected()) {
						if (newgui.stopMined.isSelected()) {
							if (mined >= stopMined)
								try {
									scriptStopped = true;
									loader.stop();
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
						} else if (newgui.stopLevel.isSelected()) {
							if (loader.client.getSkills()
									.getLevel(Skill.MINING) > stopLevel)
								try {
									scriptStopped = true;
									loader.stop();
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
						}
					}
				} else {
					if (newgui.stopAfterCheck1.isSelected()) {
						if (newgui.stopMined1.isSelected()) {
							if (mined >= stopMined)
								try {
									scriptStopped = true;
									loader.stop();
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
						} else if (newgui.stopLevel1.isSelected()) {
							if (loader.client.getSkills()
									.getLevel(Skill.MINING) > stopLevel)
								try {
									scriptStopped = true;
									loader.stop();
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
						}
					}
				}
				if (!bankAgain
						&& (loader.client.getInventory().getAmount(essID) + loader.client
								.getInventory().getAmount(pureEssID)) == 0)
					state = (newgui.locationMenu.getSelectedIndex() == 0) ? State.WALK_ARBURY
							: State.WALK_DISTENTOR;
				break;
			case WALK_DISTENTOR:
				if (Math.abs(loader.client.getCameraYawAngle() - 90) > 10) {
					try {
						loader.client.rotateCameraToAngle(90);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (walkI == 0) {
					tileindex = 0;
					try {
						if (!loader.isRunning()
								&& loader.client.getRunEnergy() > 10) {
							try {
								setRunning2(true);
								loader.sleep(random(500, 700));
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						walkMiniMap2(randomize(pathToDistentor[walkI++]));
						break;
					} catch (InterruptedException e) {
					}
				}
				if (walkI == pathToDistentor.length) {
					walkI = 0;
					state = State.OPEN_DOOR;
					break;
				}
				try {
					if (loader.realDistance(pathToDistentor[walkI - 1]) <= 6) {
						walkMiniMap2(randomize(pathToDistentor[walkI++]));
					} else
						walkMiniMap2(randomize(pathToDistentor[walkI - 1]));
					if (!loader.isRunning() && loader.client.getRunEnergy() > 0) {
						try {
							setRunning2(true);
							loader.sleep(random(500, 700));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} catch (InterruptedException e) {

				}
				break;
			case OPEN_DOOR:
				door = loader.closestObject(doorIDs);
				if (door != null) {
					if (loader.distance(door) > 8) {
						break;
					}
					try {
						loader.client.rotateCameraPitch(0);
						for (int i = 0; i < 10 && !door.interact("Open"); i++)
							sleep(random(300, 400));
						for (int i = 0; i < 10 && !loader.myPlayer().isMoving(); i++)
							sleep(random(100, 200));
						for (int i = 0; i < 20 && loader.myPlayer().isMoving(); i++)
							sleep(random(300, 400));
						for (int i = 0; i < 10
								&& !((loader.myX() == 2596 && loader.myY() == 3088) || (loader
										.myX() == 2596 && loader.myY() == 3087)); i++) {
							sleep(random(300, 400));
						}
						for (int i = 0; i < 10 && !loader.myPlayer().isMoving(); i++)
							sleep(random(100, 200));
						for (int i = 0; i < 20 && loader.myPlayer().isMoving(); i++)
							sleep(random(300, 400));
						if ((loader.myX() == 2596 && loader.myY() == 3088)
								|| (loader.myX() == 2596 && loader.myY() == 3087)) {
							state = State.TELEPORT_DISTENTOR;
							startTeleport = System.currentTimeMillis();
							break;
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			case TELEPORT_DISTENTOR:
				if (System.currentTimeMillis() - startTeleport > 40000
						&& distanceBetween(loader.myX(), loader.myY(),
								distentorTile.getX(), distentorTile.getY()) < 20) {
					state = State.OPEN_DOOR;
					break;
				}
				if (loader.myPosition().getZ() == 1) {
					RS2Object stairs = loader.closestObject(yanilleStaircaseID);
					if (stairs != null) {
						try {
							for (int i = 0; i < 5
									&& !stairs.interact("Climb-down"); i++)
								sleep(random(300, 400));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				NPC distentor = loader.closestNPC(distentorID);
				try {
					for (int x = 0; distentor != null
							&& x < 10
							&& (distanceBetween(loader.myX(), loader.myY(),
									distentorTile.getX(), distentorTile.getY()) < 20); x++) {
						if (!distentor.interact("Teleport")) {
							loader.sleep(random(100, 200));
							continue;
						}
						while (loader.myPlayer().isMoving()) {
							loader.sleep(100);
						}
						for (int i = 0; i < 20
								&& ((distanceBetween(loader.myX(),
										loader.myY(), distentorTile.getX(),
										distentorTile.getY()) < 20)); i++) {
							loader.sleep(random(300, 400));
						}
						if (loader.myPosition().getZ() == 1)
							break;
					}
					for (int i = 0; i < 20
							&& ((distanceBetween(loader.myX(), loader.myY(),
									distentorTile.getX(), distentorTile.getY()) < 20)); i++) {

						loader.sleep(random(300, 400));
					}
					for (int x = 0; x < 10; x++) {
						if (findObjectByName("Rune") != null) {
							state = State.FINDROCK;
							break;
						}
						loader.sleep(random(100, 200));
					}
				} catch (Exception e) {
				}
				break;
			case OPEN_DOOR2:

				if (((loader.myX() == 2597 && loader.myY() == 3088) || (loader
						.myX() == 2597 && loader.myY() == 3087))) {
					state = State.WALK_YANILLE_BANK;
					break;
				}
				door = closestObjectTo(doorIDs, new Position(2596, 3087, 0));
				if (door != null) {
					try {
						for (int i = 0; i < 10 && !door.interact("Open"); i++)
							sleep(random(300, 400));
						for (int i = 0; i < 10 && !loader.myPlayer().isMoving(); i++)
							sleep(random(500, 600));
						for (int i = 0; i < 20 && loader.myPlayer().isMoving(); i++)
							sleep(random(300, 400));
						for (int i = 0; i < 10
								&& !((loader.myX() == 2597 && loader.myY() == 3088) || (loader
										.myX() == 2597 && loader.myY() == 3087)); i++) {
							sleep(random(300, 400));
						}
						for (int i = 0; i < 10 && !loader.myPlayer().isMoving(); i++)
							sleep(random(100, 200));
						for (int i = 0; i < 20 && loader.myPlayer().isMoving(); i++)
							sleep(random(300, 400));
						if ((loader.myX() == 2597 && loader.myY() == 3088)
								|| (loader.myX() == 2597 && loader.myY() == 3087)) {
							state = State.WALK_YANILLE_BANK;
							break;
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			case WALK_YANILLE_BANK:
				if (walkI == 0) {
					tileindex = 0;
					try {
						walkMiniMap2(randomize(pathToBank2[walkI++]));
						break;
					} catch (InterruptedException e) {
					}
				}
				if (walkI == pathToBank2.length
						|| yanilleBank.currentlyInArea()) {
					walkI = 0;
					hasEmptied = false;
					state = State.BANK;
					break;
				}
				try {
					if (loader.realDistance(pathToBank2[walkI - 1]) <= 6) {
						walkMiniMap2(randomize(pathToBank2[walkI++]));
					} else
						walkMiniMap2(randomize(pathToBank2[walkI - 1]));
				} catch (InterruptedException e) {

				}
				break;
			case WALK_TO_STAIRCASE:

				if (walkI == 0) {
					tileindex = 0;
					try {
						walkMiniMap2(randomize(pathToStair[walkI++]));
						break;
					} catch (InterruptedException e) {
					}
				}
				if (walkI == pathToStair.length) {
					walkI = 0;
					state = State.ASCEND;
					break;
				}
				try {
					if (loader.realDistance(pathToStair[walkI - 1]) <= 6) {
						walkMiniMap2(randomize(pathToStair[walkI++]));
					} else
						walkMiniMap2(randomize(pathToStair[walkI - 1]));
				} catch (InterruptedException e) {

				}
				break;
			case ASCEND:
				try {
					if (loader.myPlayer().getZ() == 0) {
						RS2Object stair = loader
								.closestObjectForName("Staircase");
						if (stair != null) {
							for (int i = 0; i < 10
									&& !stair.interact("Climb-up"); i++)
								loader.sleep(random(500, 600));
							for (int i = 0; i < 10
									&& loader.myPlayer().getZ() != 1; i++) {
								loader.sleep(random(500, 600));
							}
						}
					} else if (loader.myPlayer().getZ() == 1) {
						loader.log("Finding staircase!");
						RS2Object stair = loader
								.closestObjectForName("Staircase");
						if (stair != null) {
							for (int i = 0; i < 10
									&& !stair.interact("Climb-up"); i++)
								loader.sleep(random(500, 600));
							for (int i = 0; i < 10
									&& loader.myPlayer().getZ() != 2; i++) {
								loader.sleep(random(500, 600));
							}
						}
					} else if (loader.myPlayer().getZ() == 2) {
						state = State.LUMBY_BANK;
					}
				} catch (Exception e) {

				}
			case LUMBY_BANK:
				try {
					if (loader.client.getInventory().contains(
							"Camelot teleport")) {
						for (int i = 0; i < 10
								&& !loader.client.getInventory()
										.interactWithName("Camelot teleport",
												"Break"); i++) {
							loader.sleep(random(500, 600));
						}
						for (int i = 0; i < 10
								&& loader.distance(pathFromCammy[0]) > 30; i++)
							loader.sleep(random(500, 600));
						break;
					}
					b = loader.client.getBank();
					for (int i = 0; i < 10 && !b.isOpen(); i++) {
						RS2Object booth = loader
								.closestObjectForName("Bank booth");
						try {
							loader.sleep(random(300, 400));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (booth == null)
							break;
						try {
							for (int j = 0; j < 10 && !booth.interact("Bank"); j++)
								loader.sleep(random(500, 600));
							loader.sleep(random(500, 700));
							for (int j = 0; j < 10 && !b.isOpen(); j++) {
								loader.sleep(random(200, 300));
								if (loader.myPlayer().isMoving())
									j--;
							}
						} catch (Exception e) {
						}
					}
					try {
						for (int x = 0; x < 10
								&& !loader.client.getInventory().isEmpty(); x++) {
							b.depositAllExcept(pickIDs);
							loader.sleep(random(900, 1100));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						try {
							for (int x = 0; x < 10 && !b.withdraw1(8010); x++) {
								loader.sleep(random(500, 600));
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} catch (Exception e) {
					}
					for (int i = 0; i < 5 && b.isOpen(); i++) {
						try {
							b.close();
							loader.sleep(random(300, 400));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} catch (Exception e) {
				}
				break;
			}
		}
		return random(200, 300);
	}

	private double distanceBetween(int myX, int myY, int x, int y) {
		return Math.sqrt((myX - x) * (myX - x) + (myY - y) * (myY - y));
	}

	private void updateServer() {
		new Thread(new Runnable() {
			public void run() {
				try {
					int gainedTotal = gainedExp;
					int newmined = (gainedExp / 5) - lastMinedUpdate;
					URL url = new URL(
							"http://cmhscripts.no-ip.org/update.php?script=CMHEssMiner&Username="
									+ loader.myPlayer().getName()
									+ "&Runtime="
									+ (System.currentTimeMillis() - lastRuntimeUpdate)
									+ "&ExpGained="
									+ (gainedTotal - lastExpUpdate) + "&Mined="
									+ newmined + "&OSBotName="
									+ loader.getBot().getUsername());
					lastRuntimeUpdate = System.currentTimeMillis();
					lastExpUpdate = gainedTotal;
					lastMinedUpdate = (gainedExp / 5);
					url.openConnection().getContent();
				} catch (Exception e) {

				}
			}
		}).start();
	}

	private boolean containsPouches() {
		for (int i = 0; i < pouchIDs.length; i++) {
			if (loader.client.getInventory().contains(pouchIDs[i]))
				return true;
		}
		for (int i = 0; i < degradedPouchIDs.length; i++) {
			if (loader.client.getInventory().contains(degradedPouchIDs[i]))
				return true;
		}
		return false;
	}

	private boolean containsDecayedPouches() {
		for (int i = 0; i < degradedPouchIDs.length; i++) {
			if (loader.client.getInventory().contains(degradedPouchIDs[i]))
				return true;
		}
		return false;
	}

	private void emptyPouches() {
		for (int i = 0; i < pouchIDs.length; i++) {
			if (loader.client.getInventory().contains(pouchIDs[i])) {
				long old = loader.client.getInventory().getAmount(essID)
						+ loader.client.getInventory().getAmount(pureEssID);
				try {
					for (int j = 0; j < 10
							&& !loader.client.getInventory().interactWithId(
									pouchIDs[i], "Empty"); j++) {
						loader.sleep(random(500, 600));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int j = 0; j < 10
						&& loader.client.getInventory().getAmount(essID)
								+ loader.client.getInventory().getAmount(
										pureEssID) == old; j++)
					try {
						loader.sleep(random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
		for (int i = 0; i < degradedPouchIDs.length; i++) {
			if (loader.client.getInventory().contains(degradedPouchIDs[i])) {
				long old = loader.client.getInventory().getAmount(essID)
						+ loader.client.getInventory().getAmount(pureEssID);
				try {
					for (int j = 0; j < 10
							&& !loader.client.getInventory().interactWithId(
									degradedPouchIDs[i], "Empty"); j++) {
						loader.sleep(random(500, 600));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int j = 0; j < 10
						&& loader.client.getInventory().getAmount(essID)
								+ loader.client.getInventory().getAmount(
										pureEssID) == old; j++)
					try {
						loader.sleep(random(500, 600));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
	}

	private void fillPouches() {

		for (int i = 0; i < pouchIDs.length; i++) {
			if (filledPouch[i])
				continue;
			filledPouch[i] = true;
			if (loader.client.getBank().isOpen()) {
				for (int j = 0; j < 10; j++) {
					try {
						if (loader.client.getBank().close())
							break;
					} catch (Exception e) {
					}
				}

			}
			if (loader.client.getInventory().contains(pouchIDs[i])) {
				long old = loader.client.getInventory().getAmount(essID)
						+ loader.client.getInventory().getAmount(pureEssID);
				try {
					for (int j = 0; j < 10
							&& !loader.client.getInventory().interactWithId(
									pouchIDs[i], "Fill"); j++) {
						loader.sleep(random(500, 600));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int j = 0; j < 10
						&& loader.client.getInventory().getAmount(essID)
								+ loader.client.getInventory().getAmount(
										pureEssID) == old; j++)
					try {
						loader.sleep(random(100, 150));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
		for (int i = 0; i < degradedPouchIDs.length; i++) {
			if (filledPouch2[i])
				continue;
			filledPouch2[i] = true;
			if (loader.client.getInventory().contains(degradedPouchIDs[i])) {
				long old = loader.client.getInventory().getAmount(essID)
						+ loader.client.getInventory().getAmount(pureEssID);
				try {
					for (int j = 0; j < 10
							&& !loader.client.getInventory().interactWithId(
									degradedPouchIDs[i], "Fill"); j++) {
						loader.sleep(random(500, 600));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int j = 0; j < 10
						&& loader.client.getInventory().getAmount(essID)
								+ loader.client.getInventory().getAmount(
										pureEssID) == old; j++)
					try {
						loader.sleep(random(100, 150));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
	}

	private boolean jagexModNearby() {
		List<Player> players = loader.client.getLocalPlayers();
		for (Player p : players) {
			if (p.getName().contains("Mod") && loader.distance(p) < 20)
				return true;
		}
		return false;
	}

	private String pad(int i) {
		String ret = i + "";
		if (ret.length() == 1) {
			return "0" + ret;
		}
		return ret;
	}

	public void onPaint(Graphics g) {
		if (canStart) {
			if (hide) {
				g.setColor(Color.BLUE);
				g.fillOval(492, 346, 28, 28);
				return;
			}
			switch (paintI) {
			case 0:
				g.drawImage(paintImages[paintI], -20, 205, null);
				g.setColor(Color.RED);
				String status = state.toString();
				if (antiban)
					status = "ANTIBAN";
				else if (switchingWorlds)
					status = "SWITCHWORLD";
				g.drawString("Current State: " + status, 12, 400);
				g.drawString("Logged in as: " + loader.myPlayer().getName()
						+ ".", 12, 415);
				double hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
				int minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
				int seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
				g.drawString("Runtime: " + "Running for: " + pad((int) (hours))
						+ ":" + pad(minutes) + ":" + pad(seconds), 12, 430);

				gainedExp = loader.client.getSkills().getExperience(
						Skill.MINING)
						- startExp;
				mined = gainedExp / 5;
				g.drawString("Rate: " + (int) (mined / hours) + " ESS/HR", 12,
						445);
				g.drawString("Mined " + (mined) + " Rune Essence", 12, 460);
				break;
			case 1:
				g.drawImage(paintImages[1], -20, 205, null);
				g.setColor(Color.RED);
				hours = (System.currentTimeMillis() - startTime) / 1000.0 / 3600;
				minutes = (int) (((System.currentTimeMillis() - startTime) / 1000.0 / 60.0) % 60);
				seconds = (int) (((System.currentTimeMillis() - startTime) / 1000.0) % 60);
				gainedExp = loader.client.getSkills().getExperience(
						Skill.MINING)
						- startExp;
				g.drawString("Current Mining Level: "
						+ loader.client.getSkills().getLevel(Skill.MINING), 12,
						400);
				g.drawString("Experience Gained: " + gainedExp, 12, 415);
				g.drawString("Experience to Level: "
						+ experienceToNextLevel(Skill.MINING), 12, 430);
				double r = ((gainedExp) / (seconds + minutes * 60 + hours * 3600.0));
				if (r == 0)
					r = .00000001;
				r *= 2;
				int TTL = (int) (experienceToNextLevel(Skill.MINING) / r);
				hours = (TTL) / 3600;
				minutes = (int) (((TTL) / 60.0) % 60);
				seconds = (int) ((TTL) % 60);
				g.drawString("Time To Level: " + pad((int) hours) + ":"
						+ pad(minutes) + ":" + pad(seconds), 12, 445);
				double perc = percentToNextLevel(Skill.MINING);
				g.setColor(new Color(0, 1, 0, 0.75f));
				g.fillRect(15 + offsetX, 475 + offsetY, (int) (500 * perc), 17);
				g.setColor(new Color(1, 1, 1, 0.75f));
				g.fillRect(15 + offsetX, 475 + offsetY, 490, 8);
				g.setColor(new Color(.5f, .5f, .5f, 0.75f));
				g.fillRect(15 + offsetX, 484 + offsetY, 490, 8);
				g.setColor(Color.RED);
				g.drawString((int) (perc * 100) + "% : "
						+ experienceToNextLevel(Skill.MINING) + " TNL", 200,
						489 + offsetY);
				break;
			case 2:
				g.drawImage(paintImages[2], -20, 205, null);
				g.setColor(Color.RED);
				g.drawString("CMHEssMiner Version " + currentVersion, 12, 400);
				g.drawString("Script Developed by Ericthecmh", 12, 415);
				g.drawString("Main Website: http://cmhscripts.com", 12, 430);
				break;
			case 3:
				g.drawImage(paintImages[3], -20, 205, null);
				g.setColor(Color.RED);
				g.drawString("My Email: cmhscripts@mit.edu", 12, 400);
				g.drawString(
						"or leave feedback at http://cmhscripts.com/contact.html",
						12, 415);
				g.drawString(
						"or on the OSBot Forum Thread under Local->Mining&Smithing->CMHEssMIner",
						12, 430);
				g.drawString("Any and all feedback is welcome.", 12, 445);
				break;
			}
			// g.drawString((antiban ? "ANTIBAN" : state.toString()), 100, 100);
			// gainedExp = loader.client.getSkills().getExperience(Skill.MINING)
			// - startExp;
			// g.drawString("Experience Gained: " + gainedExp, 100, 180);
			// mined = gainedExp / 5;
			// double hours = (System.currentTimeMillis() - startTime) / 1000.0
			// / 3600;
			// g.drawString("" + (int) (mined / hours) + " ESS/HR", 100, 120);
			// g.drawString("" + (mined) + " Mined ess", 100, 140);
			// int minutes = (int) (((System.currentTimeMillis() - startTime) /
			// 1000.0 / 60.0) % 60);
			// int seconds = (int) (((System.currentTimeMillis() - startTime) /
			// 1000.0) % 60);
			// g.drawString("Running for: " + (int) (hours) + ":" + minutes +
			// ":"
			// + seconds, 100, 160);
			// g.drawString("Current Mining Level: "
			// + loader.client.getSkills().getLevel(Skill.MINING), 100, 200);
		}
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getX() >= 9 && e.getX() <= 75 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 0;
		} else if (e.getX() >= 75 && e.getX() <= 152 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 1;
		} else if (e.getX() >= 152 && e.getX() <= 220 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 2;
		} else if (e.getX() >= 220 && e.getX() <= 296 && e.getY() >= 343
				&& e.getY() <= 372) {
			paintI = 3;
		}
		if (distanceBetween(e.getX(), e.getY(), 506, 358) <= 14) {
			hide = !hide;
		}
	}

	// Custom Methods

	private void loadSettings(CMHEssMiner2 gui) {
		String filename;
		if (System.getProperty("os.name").startsWith("Window")) {
			filename = "\\OSBot\\scripts\\CMHEssMiner.settings"
					+ settingsVersion;
		} else {
			filename = "/OSBot/scripts/CMHEssMiner.settings" + settingsVersion;
		}
		try {
			Scanner scan = new Scanner(new File(System.getProperty("user.home")
					+ filename));
			String loc = scan.nextLine();
			if (!loc.equals("Varrock") && this.gui.isPrem) {
				gui.locationMenu.setSelectedIndex(1);
			}
			int i = Integer.parseInt(scan.nextLine());
			gui.pathBox1.setSelected(i == 1);
			i = Integer.parseInt(scan.nextLine());
			if (this.gui.isPrem)
				gui.pathBox2.setSelected(i == 1);
			i = Integer.parseInt(scan.nextLine());
			if (this.gui.isPrem)
				gui.pathBox3.setSelected(i == 1);
			i = Integer.parseInt(scan.nextLine());
			if (this.gui.isPrem) {
				gui.remoteMonitor.setSelected(i == 1);
				gui.screenshot.setEnabled(i == 1);
			}
			i = Integer.parseInt(scan.nextLine());
			if (this.gui.isPrem) {
				gui.remoteMonitor1.setSelected(i == 1);
				gui.screenshot1.setEnabled(i == 1);
			}
			i = Integer.parseInt(scan.nextLine());
			if (this.gui.isPrem)
				gui.screenshot.setSelected(i == 1);
			i = Integer.parseInt(scan.nextLine());
			if (this.gui.isPrem)
				gui.screenshot1.setSelected(i == 1);
			i = Integer.parseInt(scan.nextLine());
			gui.stopAfterCheck.setSelected(i == 1);
			if (i == 1) {
				gui.stopMined.setEnabled(true);
				gui.stopLevel.setEnabled(true);
				gui.stopMinedText.setEnabled(true);
				gui.stopLevelText.setEnabled(true);
			}
			i = Integer.parseInt(scan.nextLine());
			gui.stopAfterCheck1.setSelected(i == 1);
			if (i == 1) {
				gui.stopMined1.setEnabled(true);
				gui.stopLevel1.setEnabled(true);
				gui.stopMinedText1.setEnabled(true);
				gui.stopLevelText1.setEnabled(true);
			}
			i = Integer.parseInt(scan.nextLine());
			gui.stopLevel.setSelected(i == 1);
			i = Integer.parseInt(scan.nextLine());
			gui.stopLevel1.setSelected(i == 1);
			gui.stopLevelText.setText(scan.nextLine());
			gui.stopLevelText1.setText(scan.nextLine());
			i = Integer.parseInt(scan.nextLine());
			gui.stopMined.setSelected(i == 1);
			i = Integer.parseInt(scan.nextLine());
			gui.stopMined1.setSelected(i == 1);
			gui.stopMinedText.setText(scan.nextLine());
			gui.stopMinedText1.setText(scan.nextLine());
			i = Integer.parseInt(scan.nextLine());
			gui.randomEvent.setSelected(i == 1);
			i = Integer.parseInt(scan.nextLine());
			gui.teleLummy.setSelected(i == 1);
			i = Integer.parseInt(scan.nextLine());
			gui.cleverbot.setSelected(i == 1 && this.gui.isPrem);
			gui.cleverbotThreshold.setText(scan.nextLine());
			i = Integer.parseInt(scan.nextLine());
			gui.hopMod.setSelected(i == 1 && this.gui.isPrem);
			scan.close();
		} catch (Exception e) {

		}
	}

	private void saveSettings(CMHEssMiner2 gui) {
		String filename;
		if (System.getProperty("os.name").startsWith("Window")) {
			filename = "\\OSBot\\scripts\\CMHEssMiner.settings"
					+ settingsVersion;
		} else {
			filename = "/OSBot/scripts/CMHEssMiner.settings" + settingsVersion;
		}
		try {
			PrintStream ps = new PrintStream(System.getProperty("user.home")
					+ filename);
			ps.println(gui.locationMenu.getSelectedItem());
			ps.println(gui.pathBox1.isSelected() ? 1 : 0);
			ps.println(gui.pathBox2.isSelected() ? 1 : 0);
			ps.println(gui.pathBox3.isSelected() ? 1 : 0);
			ps.println(gui.remoteMonitor.isSelected() ? 1 : 0);
			ps.println(gui.remoteMonitor1.isSelected() ? 1 : 0);
			ps.println(gui.screenshot.isSelected() ? 1 : 0);
			ps.println(gui.screenshot1.isSelected() ? 1 : 0);
			ps.println(gui.stopAfterCheck.isSelected() ? 1 : 0);
			ps.println(gui.stopAfterCheck1.isSelected() ? 1 : 0);
			ps.println(gui.stopLevel.isSelected() ? 1 : 0);
			ps.println(gui.stopLevel1.isSelected() ? 1 : 0);
			ps.println(gui.stopLevelText.getText());
			ps.println(gui.stopLevelText1.getText());
			ps.println(gui.stopMined.isSelected() ? 1 : 0);
			ps.println(gui.stopMined1.isSelected() ? 1 : 0);
			ps.println(gui.stopMinedText.getText());
			ps.println(gui.stopMinedText1.getText());
			ps.println(gui.randomEvent.isSelected() ? 1 : 0);
			ps.println(gui.teleLummy.isSelected() ? 1 : 0);
			ps.println(gui.cleverbot.isSelected() ? 1 : 0);
			ps.println(gui.cleverbotThreshold.getText());
			ps.println(gui.hopMod.isSelected() ? 1 : 0);
			ps.close();
		} catch (Exception e) {

		}
	}

	private double distanceTo(Position p) {
		Position myP = loader.myPosition();
		return Math.sqrt((p.getX() - myP.getX()) * (p.getX() - myP.getX())
				+ (p.getY() - myP.getY()) * (p.getY() - myP.getY()));
	}

	private RS2Object closestObjectTo(int[] id, Position p) {
		List<RS2Object> objects = loader.client.getCurrentRegion().getObjects();
		RS2Object object = null;
		int dist = 20000000;
		for (RS2Object o : objects) {
			if (o.getId() == id[0] || o.getId() == id[1]) {
				if (object == null) {
					object = o;
					dist = (int) distanceBetween(p.getX(), p.getY(),
							object.getX(), object.getY());
				} else {
					if (distanceBetween(p.getX(), p.getY(), o.getX(), o.getY()) < dist) {
						dist = (int) distanceBetween(p.getX(), p.getY(),
								o.getX(), o.getY());
						object = o;
					}
				}
			}
		}
		return object;
	}

	private RS2Object closestObjectTo(int id, Position p) {
		List<RS2Object> objects = loader.client.getCurrentRegion().getObjects();
		RS2Object object = null;
		int dist = 20000000;
		for (RS2Object o : objects) {
			if (o.getId() == id) {
				if (object == null) {
					object = o;
					dist = (int) distanceBetween(p.getX(), p.getY(),
							object.getX(), object.getY());
				} else {
					if (distanceBetween(p.getX(), p.getY(), o.getX(), o.getY()) < dist) {
						dist = (int) distanceBetween(p.getX(), p.getY(),
								o.getX(), o.getY());
						object = o;
					}
				}
			}
		}
		return object;
	}

	private RS2Object findObjectByName(String name) {
		List<RS2Object> objects = loader.client.getCurrentRegion().getObjects();
		RS2Object object = null;
		int dist = 20000000;
		for (RS2Object p : objects) {
			if (p.getName().contains(name)) {
				if (object == null) {
					object = p;
					dist = (int) distanceBetween(loader.myX(), loader.myY(),
							object.getX(), object.getY());
				} else {
					if (distanceBetween(loader.myX(), loader.myY(), p.getX(),
							p.getY()) < dist) {
						dist = (int) distanceBetween(loader.myX(),
								loader.myY(), p.getX(), p.getY());
						object = p;
					}
				}
			}
		}
		return object;
	}

	private double percentToNextLevel(Skill skill) {
		int level = loader.client.getSkills().getLevel(skill);
		double total = 0.0;
		total = xpForLevels[level] - xpForLevels[level - 1];
		return 1 - ((experienceToNextLevel(skill) + 0.0) / total);
	}

	/***************** MADE BY ABIBOT *******************/
	int[] xpForLevels = new int[] { 0, 83, 174, 276, 388, 512, 650, 801, 969,
			1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470,
			5018, 5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363,
			14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648,
			37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014,
			91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040,
			203254, 224466, 247886, 273742, 302288, 333804, 368599, 407015,
			449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257,
			992895, 1096278, 1210421, 1336443, 1475581, 1629200, 1798808,
			1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792,
			3972294, 4385776, 4842295, 5346332, 5902831, 6517253, 7195629,
			7944614, 8771558, 9684577, 10692629, 11805606, 13034431 };

	private int experienceToNextLevel(Skill skill) {
		int xp = loader.client.getSkills().getExperience(skill);
		for (int i = 0; i < 99; i++) {
			if (xp < xpForLevels[i]) {
				return (xpForLevels[i] - xp);
			}
		}
		return (200000000 - xp);
	}

	/***************** END MADE BY ABIBOT *******************/

	private Position randomize(Position p) {
		return new Position(p.getX() + random(1, 2 * CONST + 1) - (CONST + 1),
				p.getY() + random(1, 2 * CONST + 1) - (CONST + 1), 0);
	}

	private void toggleMouse() {
		try {
			for (int i = 0; i < 10 && !loader.openTab(Tab.SETTINGS); i++) {
				sleep(random(500, 600));
			}

			RS2Interface inte = loader.client.getInterface(261);
			if (inte != null) {
				RS2InterfaceChild child = inte.getChild(24);
				if (child != null) {
					child.hover();
					sleep(random(1000, 1100));
					child.interact();
					loader.sleep(random(500, 600));
				}
			}
		} catch (Exception e) {

		}
		return;
	}

	private void walkMiniMap2(Position p) throws InterruptedException {
		final int mapX = 643, mapY = 83;
		double distance = distanceTo(p);
		double angle2 = Math.toDegrees(Math.atan2(p.getY()
				- loader.myPosition().getY(), p.getX()
				- loader.myPosition().getX())) - 90;
		double dY = distance * Math.sqrt(17);
		double rdX = -1
				* Math.sin(Math.toRadians(angle2
						- loader.client.getCameraYawAngle())) * dY;
		double rdY = Math.cos(Math.toRadians(angle2
				- loader.client.getCameraYawAngle()))
				* dY;
		RectangleDestination rd = new RectangleDestination(
				(int) (mapX + rdX - 1), (int) (mapY - rdY - 1), 3, 3);
		loader.client.moveMouseTo(rd, false, false, false);
		for (int i = 0; i < 5
				&& (Math.abs(loader.client.getMousePosition().getX()
						- (int) (mapX + rdX)) > 2 || Math.abs(loader.client
						.getMousePosition().getY() - (int) (mapY - rdY)) > 2); i++) {
			loader.sleep(random(500, 600));

		}
		if ((Math.abs(loader.client.getMousePosition().getX()
				- (int) (mapX + rdX)) <= 2 && Math.abs(loader.client
				.getMousePosition().getY() - (int) (mapY - rdY)) <= 2)) {
			loader.sleep(random(500, 600));
			loader.client.clickMouse(false);
			for (int i = 0; i < 2 && !loader.myPlayer().isMoving(); i++) {
				loader.sleep(random(500, 600));
			}
			for (int i = 0; i < 60 && loader.myPlayer().isMoving()
					&& distanceTo(p) > 6; i++) {
				loader.sleep(random(500, 600));
			}
		}
	}

	private void walkMiniMap2NoBlock(Position p) throws InterruptedException {
		final int mapX = 643, mapY = 83;
		double distance = distanceTo(p);
		double angle2 = Math.toDegrees(Math.atan2(p.getY()
				- loader.myPosition().getY(), p.getX()
				- loader.myPosition().getX())) - 90;
		double dY = distance * Math.sqrt(17);
		double rdX = -1
				* Math.sin(Math.toRadians(angle2
						- loader.client.getCameraYawAngle())) * dY;
		double rdY = Math.cos(Math.toRadians(angle2
				- loader.client.getCameraYawAngle()))
				* dY;
		System.out.println((p.getY() - loader.myPosition().getY()) + ","
				+ (p.getX() - loader.myPosition().getX()));
		System.out.println(angle2 + loader.client.getCameraYawAngle() + " "
				+ rdX + " " + rdY);
		RectangleDestination rd = new RectangleDestination(
				(int) (mapX + rdX - 1), (int) (mapY - rdY - 1), 3, 3);
		loader.client.moveMouseTo(rd, false, false, false);
		for (int i = 0; i < 5
				&& (Math.abs(loader.client.getMousePosition().getX()
						- (int) (mapX + rdX)) > 2 || Math.abs(loader.client
						.getMousePosition().getY() - (int) (mapY - rdY)) > 2); i++) {
			loader.sleep(random(500, 600));

		}
		if ((Math.abs(loader.client.getMousePosition().getX()
				- (int) (mapX + rdX)) <= 2 && Math.abs(loader.client
				.getMousePosition().getY() - (int) (mapY - rdY)) <= 2)) {
			loader.sleep(random(500, 600));
			loader.client.clickMouse(false);
			for (int i = 0; i < 2 && !loader.myPlayer().isMoving(); i++) {
				loader.sleep(random(500, 600));
			}
			for (int i = 0; i < 60 && loader.myPlayer().isMoving()
					&& distanceTo(p) > 6; i++) {
				loader.sleep(random(500, 600));
			}
		}
	}

	private void walkMiniMap2(Entity e) throws InterruptedException {
		walkMiniMap2(e.getPosition());
	}

	private void setRunning2(boolean on) {
		if (loader.client.getBank().isOpen())
			return;
		try {
			for (int i = 0; i < 10 && !loader.openTab(Tab.SETTINGS); i++) {
				sleep(random(500, 600));
			}

			RS2Interface inte = loader.client.getInterface(261);
			if (inte != null) {
				RS2InterfaceChild child = inte.getChild(36);
				if (child != null) {
					child.hover();
					sleep(random(1000, 1100));
					child.interact();
					for (int i = 0; i < 3 && !loader.isRunning(); i++)
						sleep(random(350, 500));
				}
			}
		} catch (Exception e) {

		}
		return;
	}

	// Custom Classes

	private class PortalThread implements Runnable {
		private Position p;

		public PortalThread(Position p) {
			this.p = p;
		}

		public void run() {
			try {
				for (int i = 0; i < 10 && distanceToPosition(p) > 2; i++) {
					loader.walk(p);
				}
				if (!p.interact(loader.getBot(), "Exit"))
					p.interact(loader.getBot(), "Use");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		private double distanceToPosition(Position p) {
			return Math.sqrt((loader.myX() - p.getX())
					* (loader.myX() - p.getX()) + (loader.myY() - p.getY())
					* (loader.myY() - p.getY()));
		}
	}

	private double distanceToObject(RS2Object p) {
		return Math.sqrt((loader.myX() - p.getX()) * (loader.myX() - p.getX())
				+ (loader.myY() - p.getY()) * (loader.myY() - p.getY()));
	}

	private class AntibanThread implements Runnable {

		@Override
		public void run() {
			while (true) {
				if (scriptStopped) {
					break;
				}
				if (state == State.MINE
						&& !locked
						&& loader.myPlayer().getAnimation() != -1
						&& Math.random() < antibanTrigger
						&& (loader.client.getInventory().getAmount(essID)
								+ loader.client.getInventory().getAmount(
										pureEssID) < 25)) {
					antiban = true;
					locked = true;
					double action = Math.random();
					if (action < 0.3) {

						try {
							if (loader.currentTab() != Tab.SKILLS) {
								loader.openTab(Tab.SKILLS);
								loader.sleep(random(500, 700));
							}
							RS2Interface inte = loader.client.getInterface(320);
							RS2InterfaceChild mining = inte.getChild((Math
									.random() > 0.5 ? 5 : 6));
							mining.hover();
							loader.sleep(random(2500, 5000));
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

					} else if (action < 0.65) {
						try {
							loader.client.rotateCameraPitch(random(80, 90));
							loader.client.rotateCameraToAngle(random(0, 360));
							loader.sleep(random(1000, 2000));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						Rectangle k = new Rectangle(1 + random(515),
								1 + random(330), 10, 10);
						try {
							loader.client.moveMouseTo(new RectangleDestination(
									k), false, false, false);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					locked = false;
					antiban = false;
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private class Vector {
		private double x, y;

		public Vector(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public double dotProd(Vector v) {
			return this.x * v.x + this.y * v.y;
		}

		public double degreeBetween(Vector v) {
			double dot = dotProd(v);
			double cos = dot / (mag() + v.mag());
			if (this.x == 0 && this.y < 0)
				return 180;
			if (this.x == 0 && this.y > 0)
				return 0;
			if (this.x < 0 && this.y > 0)
				return Math.toDegrees(Math.acos(cos));
			if (this.x < 0 && this.y < 0)
				return Math.toDegrees(Math.acos(cos));
			if (this.x > 0 && this.y < 0)
				return 360 - Math.toDegrees(Math.acos(cos));
			if (this.x > 0 && this.y > 0)
				return 360 - Math.toDegrees(Math.acos(cos));
			return 0;
		}

		public double mag() {
			return Math.sqrt(x * x + y * y);
		}
	}

	private class Area {
		private double minX, minY;
		private double maxX, maxY;

		private Area(double d, double e, double f, double g) {
			if (d > f) {
				minX = f;
				maxX = d;
			} else {
				maxX = f;
				minX = d;
			}
			if (e > g) {
				minY = g;
				maxY = e;
			} else {
				maxY = g;
				minY = e;
			}
			System.out.println("Max X: " + maxX + " Min X: " + minX
					+ " Min Y: " + minY + " Max Y: " + maxY);
		}

		public boolean currentlyInArea() {
			return isInArea(loader.myPosition());
		}

		public boolean isInArea(Position p) {
			return (p.getX() >= minX && p.getX() <= maxX && p.getY() >= minY && p
					.getY() <= maxY);
		}

	}

	class LoginForm extends javax.swing.JFrame {

		/**
		 * Creates new form LoginForm
		 */
		public boolean isAuth;
		public boolean isPrem;
		public BufferedReader in;
		public PrintWriter out;
		public String username;
		public final String name = "CMHEssMiner";
		public boolean ready;

		public LoginForm() {
			initComponents();
			String filename;
			if (System.getProperty("os.name").startsWith("Window")) {
				filename = "\\OSBot\\scripts\\CMHEssMiner.user";
			} else {
				filename = "/OSBot/scripts/CMHEssMiner.user";
			}
			try {
				Scanner scan = new Scanner(new File(
						System.getProperty("user.home") + filename));
				usernameField.setText(scan.nextLine());
				passwordField.setText(scan.nextLine());
				scan.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		/**
		 * This method is called from within the constructor to initialize the
		 * form. WARNING: Do NOT modify this code. The content of this method is
		 * always regenerated by the Form Editor.
		 */
		@SuppressWarnings("unchecked")
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		private void initComponents() {

			jLabel1 = new javax.swing.JLabel();
			usernameField = new javax.swing.JTextField();
			passwordField = new javax.swing.JPasswordField();
			jLabel2 = new javax.swing.JLabel();
			jButton1 = new javax.swing.JButton();
			jButton2 = new javax.swing.JButton();

			setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

			jLabel1.setText("Username:");

			jLabel2.setText("Password:");

			jButton1.setText("Login");
			jButton1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton1ActionPerformed(evt);
				}
			});

			jButton2.setText("Create Account");
			jButton2.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton2ActionPerformed(evt);
				}
			});

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
					getContentPane());
			getContentPane().setLayout(layout);
			layout.setHorizontalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addGap(72, 72, 72)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.LEADING,
													false)
													.addComponent(
															jButton1,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															Short.MAX_VALUE)
													.addGroup(
															layout.createSequentialGroup()
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING)
																					.addComponent(
																							jLabel1)
																					.addComponent(
																							jLabel2))
																	.addPreferredGap(
																			javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING,
																					false)
																					.addComponent(
																							passwordField,
																							javax.swing.GroupLayout.DEFAULT_SIZE,
																							152,
																							Short.MAX_VALUE)
																					.addComponent(
																							usernameField)))
													.addComponent(
															jButton2,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															239,
															Short.MAX_VALUE))
									.addContainerGap(89, Short.MAX_VALUE)));
			layout.setVerticalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addGap(82, 82, 82)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel1)
													.addComponent(
															usernameField,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(
															passwordField,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE)
													.addComponent(jLabel2))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
									.addComponent(jButton1)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(jButton2)
									.addContainerGap(78, Short.MAX_VALUE)));

			pack();
		}// </editor-fold>

		private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {

			username = usernameField.getText();
			String password = new String(passwordField.getPassword());
			String filename;
			if (System.getProperty("os.name").startsWith("Window")) {
				filename = "\\OSBot\\scripts\\CMHEssMiner.user";
			} else {
				filename = "/OSBot/scripts/CMHEssMiner.user";
			}
			try {
				PrintStream ps = new PrintStream(
						System.getProperty("user.home") + filename);
				ps.println(username);
				ps.println(password);
				ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Socket sock = null;
			try {
				if (username.equals("ericthecmh1")
						|| username.equals("marvin57"))
					sock = new Socket("54.211.36.47", 8080);
				else
					sock = new Socket(((Math.random() > 0.5) ? "54.226.90.112"
							: "54.211.36.47"), 8080);
			} catch (UnknownHostException e) {

				return;
			} catch (IOException ex) {

				return;
			}
			try {
				in = new BufferedReader(new InputStreamReader(
						sock.getInputStream()));
				out = new PrintWriter(sock.getOutputStream(), true);
			} catch (IOException e) {
			}

			long apple = Encrypt.seconds();
			String ack = null;
			double sh = 0.0, rh;
			out.println("login");
			out.println(name);
			out.println(username);
			out.println(apple + "");
			try {
				while ((ack = in.readLine()) == null) {
				}
				try {
					rh = Encrypt.getHash(apple);
					sh = Double.parseDouble(ack);
					if (Math.abs(rh - sh) > .00001)
						throw (new Exception());
				} catch (Exception e) {
					username = null;
					isAuth = false;
					return;
				}
			} catch (Exception e) {
			}
			try {
				out.println(Encrypt.encrypt(sh + "", password));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {

				while ((ack = in.readLine()) == null) {
				}
				ack = Encrypt.decrypt(sh + "", ack);
				if (ack.startsWith("AuthFail")) {
					isAuth = false;
					isPrem = false;
				} else if (ack.startsWith("AuthAck")) {
					int i = ack.charAt(ack.length() - 1) - 48;
					isPrem = (i == 1);
					isAuth = true;
					ready = true;
				} else {
					isAuth = false;
					isPrem = false;
				}
			} catch (Exception e) {
			}
			if (isAuth) {
				JOptionPane.showMessageDialog(null,
						"You have been successfully logged in as a "
								+ (isPrem ? "premium user." : "normal user."));
			} else {
				JOptionPane.showMessageDialog(null,
						"Please check your username and password");
				username = null;
			}

			try {
				in.close();
			} catch (Exception e) {
			}
			try {
				out.close();
			} catch (Exception e) {
			}
			try {
				sock.close();
			} catch (Exception e) {
			}
		}

		private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
			username = usernameField.getText();
			if (username.length() == 0) {
				JOptionPane.showMessageDialog(null, "Your username is empty!");
				return;
			}
			String password = new String(passwordField.getPassword());
			// Using a JPanel as the message for the JOptionPane
			JPanel userPanel = new JPanel();
			userPanel.setLayout(new GridLayout(1, 2));
			JLabel passwordLbl = new JLabel("Password:");

			JPasswordField passwordFld = new JPasswordField();
			userPanel.add(passwordLbl);
			userPanel.add(passwordFld);

			JOptionPane.showMessageDialog(null, userPanel,
					"Confirm your password:", JOptionPane.PLAIN_MESSAGE);

			String confirmPassword = new String(passwordFld.getPassword());
			String email = JOptionPane.showInputDialog(null,
					"Email (Don't worry I won't sell it):");
			if (!password.equals(confirmPassword)) {
				JOptionPane.showMessageDialog(null, "Passwords don't match!");
				return;
			}
			Socket sock = null;
			try {
				sock = new Socket(((Math.random() > 0.5) ? "54.226.90.112"
						: "54.211.36.47"), 8080);
			} catch (UnknownHostException e) {
				return;
			} catch (IOException ex) {
				System.out.println("IOException when connecting to server");
				return;
			}
			try {
				in = new BufferedReader(new InputStreamReader(
						sock.getInputStream()));
				out = new PrintWriter(sock.getOutputStream(), true);
			} catch (IOException e) {
			}
			out.println("create");
			out.println(name);
			out.println(username);
			out.println(password);
			out.println(email);
			String ack = null;
			try {
				while ((ack = in.readLine()) == null) {
				}
				if (ack.startsWith("CreateFailed")) {
					isAuth = false;
					isPrem = false;
					while ((ack = in.readLine()) == null) {
					}
					JOptionPane.showMessageDialog(null, ack);
				} else {
					isPrem = false;
					isAuth = true;
					ready = true;
				}
			} catch (IOException e) {
			}
		}

		// Variables declaration - do not modify
		private javax.swing.JButton jButton1;
		private javax.swing.JButton jButton2;
		private javax.swing.JLabel jLabel1;
		private javax.swing.JLabel jLabel2;
		private javax.swing.JPasswordField passwordField;
		private javax.swing.JTextField usernameField;
		// End of variables declaration
	}

	class CMHEssMiner2 extends javax.swing.JFrame {

		private boolean isPrem;
		public boolean ready;
		private CardLayout mainLayout;

		public CMHEssMiner2(boolean isPrem) {
			initComponents();
			this.isPrem = isPrem;
			buttonGroup1.add(stopMined);
			buttonGroup1.add(stopLevel);
			buttonGroup2.add(stopMined1);
			buttonGroup2.add(stopLevel1);
			mainLayout = new CardLayout();
			mainPane.setLayout(mainLayout);
			mainPane.add(varrockLayer, "VARROCK");
			mainPane.add(yanilleLayer, "YANILLE");
			mainLayout.show(mainPane, "VARROCK");
		}

		/**
		 * This method is called from within the constructor to initialize the
		 * form. WARNING: Do NOT modify this code. The content of this method is
		 * always regenerated by the Form Editor.
		 */
		@SuppressWarnings("unchecked")
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		private void initComponents() {

			varrockLayer = new javax.swing.JPanel();
			pathBox1 = new javax.swing.JCheckBox();
			pathBox2 = new javax.swing.JCheckBox();
			pathBox3 = new javax.swing.JCheckBox();
			jSeparator1 = new javax.swing.JSeparator();
			jLabel2 = new javax.swing.JLabel();
			stopAfterCheck = new javax.swing.JCheckBox();
			stopMined = new javax.swing.JRadioButton();
			stopLevel = new javax.swing.JRadioButton();
			stopLevelText = new javax.swing.JTextField();
			stopMinedText = new javax.swing.JTextField();
			remoteMonitor = new javax.swing.JCheckBox();
			screenshot = new javax.swing.JCheckBox();
			buttonGroup1 = new javax.swing.ButtonGroup();
			buttonGroup2 = new javax.swing.ButtonGroup();
			yanilleLayer = new javax.swing.JPanel();
			jSeparator2 = new javax.swing.JSeparator();
			stopAfterCheck1 = new javax.swing.JCheckBox();
			stopMined1 = new javax.swing.JRadioButton();
			stopLevel1 = new javax.swing.JRadioButton();
			stopLevelText1 = new javax.swing.JTextField();
			stopMinedText1 = new javax.swing.JTextField();
			remoteMonitor1 = new javax.swing.JCheckBox();
			screenshot1 = new javax.swing.JCheckBox();
			jLabel1 = new javax.swing.JLabel();
			locationMenu = new javax.swing.JComboBox();
			mainPane = new javax.swing.JPanel();
			randomEvent = new javax.swing.JCheckBox();
			jLabel3 = new javax.swing.JLabel();
			Start = new javax.swing.JButton();
			teleLummy = new javax.swing.JCheckBox();
			jLabel5 = new javax.swing.JLabel();
			cleverbot = new javax.swing.JCheckBox();
			jLabel17 = new javax.swing.JLabel();
			jScrollPane7 = new javax.swing.JScrollPane();
			jTextArea1 = new javax.swing.JTextArea();
			cleverbotThreshold = new javax.swing.JTextField();
			hopMod = new javax.swing.JCheckBox();

			pathBox1.setSelected(true);
			pathBox1.setText("Path 1");
			pathBox1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					pathBox1ActionPerformed(evt);
				}
			});

			pathBox2.setText("Path 2 (Premium)");
			pathBox2.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					pathBox2ActionPerformed(evt);
				}
			});

			pathBox3.setText("Path 3 (Premium)");
			pathBox3.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					pathBox3ActionPerformed(evt);
				}
			});

			jLabel2.setText("Randomize Path (Antiban):");

			stopAfterCheck.setText("Stop After:");
			stopAfterCheck
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(
								java.awt.event.ActionEvent evt) {
							stopAfterCheckActionPerformed(evt);
						}
					});

			stopMined.setText("Amount Mined:");
			stopMined.setEnabled(false);

			stopLevel.setText("After Mining Level:");
			stopLevel.setEnabled(false);

			stopLevelText.setEnabled(false);

			stopMinedText.setEnabled(false);

			remoteMonitor.setText("Enable Remote Monitoring (Premium)");
			remoteMonitor
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(
								java.awt.event.ActionEvent evt) {
							remoteMonitorActionPerformed(evt);
						}
					});

			screenshot.setText("Enable screenshots (Premium)");
			screenshot.setEnabled(false);
			screenshot.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					screenshotActionPerformed(evt);
				}
			});

			javax.swing.GroupLayout varrockLayerLayout = new javax.swing.GroupLayout(
					varrockLayer);
			varrockLayer.setLayout(varrockLayerLayout);
			varrockLayerLayout
					.setHorizontalGroup(varrockLayerLayout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addComponent(jSeparator1,
									javax.swing.GroupLayout.Alignment.TRAILING)
							.addGroup(
									varrockLayerLayout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													varrockLayerLayout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addGroup(
																	varrockLayerLayout
																			.createSequentialGroup()
																			.addGap(12,
																					12,
																					12)
																			.addComponent(
																					screenshot))
															.addGroup(
																	varrockLayerLayout
																			.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING,
																					false)
																			.addGroup(
																					varrockLayerLayout
																							.createSequentialGroup()
																							.addComponent(
																									pathBox1)
																							.addGap(18,
																									18,
																									18)
																							.addComponent(
																									pathBox2)
																							.addGap(18,
																									18,
																									18)
																							.addComponent(
																									pathBox3))
																			.addComponent(
																					jLabel2)
																			.addComponent(
																					stopAfterCheck)
																			.addGroup(
																					varrockLayerLayout
																							.createSequentialGroup()
																							.addGap(12,
																									12,
																									12)
																							.addGroup(
																									varrockLayerLayout
																											.createParallelGroup(
																													javax.swing.GroupLayout.Alignment.LEADING)
																											.addComponent(
																													stopLevel)
																											.addComponent(
																													stopMined))
																							.addPreferredGap(
																									javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																							.addGroup(
																									varrockLayerLayout
																											.createParallelGroup(
																													javax.swing.GroupLayout.Alignment.LEADING)
																											.addComponent(
																													stopLevelText)
																											.addComponent(
																													stopMinedText))))
															.addComponent(
																	remoteMonitor))
											.addContainerGap(
													javax.swing.GroupLayout.DEFAULT_SIZE,
													Short.MAX_VALUE)));
			varrockLayerLayout
					.setVerticalGroup(varrockLayerLayout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									varrockLayerLayout
											.createSequentialGroup()
											.addContainerGap()
											.addComponent(jLabel2)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													varrockLayerLayout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	pathBox1)
															.addComponent(
																	pathBox2)
															.addComponent(
																	pathBox3))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
											.addComponent(
													jSeparator1,
													javax.swing.GroupLayout.PREFERRED_SIZE,
													10,
													javax.swing.GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(stopAfterCheck)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													varrockLayerLayout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	stopMined)
															.addComponent(
																	stopMinedText,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
											.addGroup(
													varrockLayerLayout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	stopLevel)
															.addComponent(
																	stopLevelText,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addGap(18, 18, 18)
											.addComponent(remoteMonitor)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(screenshot)
											.addContainerGap(
													javax.swing.GroupLayout.DEFAULT_SIZE,
													Short.MAX_VALUE)));

			stopAfterCheck1.setText("Stop After:");
			stopAfterCheck1
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(
								java.awt.event.ActionEvent evt) {
							stopAfterCheck1ActionPerformed(evt);
						}
					});

			stopMined1.setText("Amount Mined:");
			stopMined1.setEnabled(false);

			stopLevel1.setText("After Mining Level:");
			stopLevel1.setEnabled(false);

			stopLevelText1.setEnabled(false);

			stopMinedText1.setEnabled(false);

			remoteMonitor1.setText("Enable Remote Monitoring (Premium)");
			remoteMonitor1
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(
								java.awt.event.ActionEvent evt) {
							remoteMonitor1ActionPerformed(evt);
						}
					});

			screenshot1.setText("Enable screenshots (Premium)");
			screenshot1.setEnabled(false);

			javax.swing.GroupLayout yanilleLayerLayout = new javax.swing.GroupLayout(
					yanilleLayer);
			yanilleLayer.setLayout(yanilleLayerLayout);
			yanilleLayerLayout
					.setHorizontalGroup(yanilleLayerLayout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addComponent(jSeparator2,
									javax.swing.GroupLayout.Alignment.TRAILING)
							.addGroup(
									yanilleLayerLayout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													yanilleLayerLayout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	stopAfterCheck1)
															.addComponent(
																	remoteMonitor1)
															.addGroup(
																	yanilleLayerLayout
																			.createSequentialGroup()
																			.addGap(12,
																					12,
																					12)
																			.addGroup(
																					yanilleLayerLayout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addComponent(
																									screenshot1)
																							.addGroup(
																									yanilleLayerLayout
																											.createSequentialGroup()
																											.addGroup(
																													yanilleLayerLayout
																															.createParallelGroup(
																																	javax.swing.GroupLayout.Alignment.LEADING)
																															.addComponent(
																																	stopLevel1)
																															.addComponent(
																																	stopMined1))
																											.addPreferredGap(
																													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																											.addGroup(
																													yanilleLayerLayout
																															.createParallelGroup(
																																	javax.swing.GroupLayout.Alignment.LEADING,
																																	false)
																															.addComponent(
																																	stopLevelText1,
																																	javax.swing.GroupLayout.DEFAULT_SIZE,
																																	220,
																																	Short.MAX_VALUE)
																															.addComponent(
																																	stopMinedText1))))))
											.addContainerGap(
													javax.swing.GroupLayout.DEFAULT_SIZE,
													Short.MAX_VALUE)));
			yanilleLayerLayout
					.setVerticalGroup(yanilleLayerLayout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									yanilleLayerLayout
											.createSequentialGroup()
											.addGap(72, 72, 72)
											.addComponent(
													jSeparator2,
													javax.swing.GroupLayout.PREFERRED_SIZE,
													10,
													javax.swing.GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(stopAfterCheck1)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													yanilleLayerLayout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	stopMined1)
															.addComponent(
																	stopMinedText1,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
											.addGroup(
													yanilleLayerLayout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.BASELINE)
															.addComponent(
																	stopLevel1)
															.addComponent(
																	stopLevelText1,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addGap(18, 18, 18)
											.addComponent(remoteMonitor1)
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(screenshot1)
											.addContainerGap(
													javax.swing.GroupLayout.DEFAULT_SIZE,
													Short.MAX_VALUE)));

			setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
			setResizable(false);

			jLabel1.setText("Select Location:");

			locationMenu.setModel(new javax.swing.DefaultComboBoxModel(
					new String[] { "Varrock", "Yanille (Premium)" }));
			locationMenu.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent evt) {
					locationMenuItemStateChanged(evt);
				}
			});

			mainPane.setMaximumSize(null);
			mainPane.setPreferredSize(new java.awt.Dimension(417, 270));

			javax.swing.GroupLayout mainPaneLayout = new javax.swing.GroupLayout(
					mainPane);
			mainPane.setLayout(mainPaneLayout);
			mainPaneLayout.setHorizontalGroup(mainPaneLayout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING).addGap(
							0, 0, Short.MAX_VALUE));
			mainPaneLayout.setVerticalGroup(mainPaneLayout.createParallelGroup(
					javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 279,
					Short.MAX_VALUE));

			randomEvent.setText("Custom Random Event Handler (Premium)");
			randomEvent.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					randomEventActionPerformed(evt);
				}
			});

			jLabel3.setText("Includes: Evil Bob");

			Start.setText("Start");
			Start.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					StartActionPerformed(evt);
				}
			});

			teleLummy.setText("Teleport to Lumbridge on Random fail (Premium)");
			teleLummy.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					teleLummyActionPerformed(evt);
				}
			});

			jLabel5.setText("If mining at Yanille, have Cammy tele in bank");

			cleverbot.setText("Cleverbot Talking");
			cleverbot.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					cleverbotActionPerformed(evt);
				}
			});

			jLabel17.setText("Talk threshold:");

			jTextArea1.setEditable(false);
			jTextArea1.setColumns(20);
			jTextArea1.setLineWrap(true);
			jTextArea1.setRows(5);
			jTextArea1
					.setText("The threshold is the number of responses before ending the current conversation and not responding for a period of time. Set to 0 for never.\n\nRationale: In a room with two cleverbots... they would continue talking forever.");
			jTextArea1.setWrapStyleWord(true);
			jScrollPane7.setViewportView(jTextArea1);

			cleverbotThreshold.setText("6");

			hopMod.setText("Hop Worlds if Jagex Mod nearby");
			hopMod.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					hopModActionPerformed(evt);
				}
			});

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
					getContentPane());
			getContentPane().setLayout(layout);
			layout.setHorizontalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addContainerGap()
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.LEADING)
													.addGroup(
															layout.createSequentialGroup()
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.LEADING)
																					.addGroup(
																							layout.createSequentialGroup()
																									.addComponent(
																											teleLummy)
																									.addGap(0,
																											55,
																											Short.MAX_VALUE))
																					.addGroup(
																							layout.createSequentialGroup()
																									.addComponent(
																											cleverbot)
																									.addGap(36,
																											36,
																											36)
																									.addComponent(
																											jLabel17)
																									.addPreferredGap(
																											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																									.addComponent(
																											cleverbotThreshold)))
																	.addGap(12,
																			12,
																			12))
													.addGroup(
															javax.swing.GroupLayout.Alignment.TRAILING,
															layout.createSequentialGroup()
																	.addGroup(
																			layout.createParallelGroup(
																					javax.swing.GroupLayout.Alignment.TRAILING)
																					.addComponent(
																							jScrollPane7,
																							javax.swing.GroupLayout.Alignment.LEADING)
																					.addComponent(
																							Start,
																							javax.swing.GroupLayout.Alignment.LEADING,
																							javax.swing.GroupLayout.DEFAULT_SIZE,
																							javax.swing.GroupLayout.DEFAULT_SIZE,
																							Short.MAX_VALUE)
																					.addComponent(
																							mainPane,
																							javax.swing.GroupLayout.Alignment.LEADING,
																							javax.swing.GroupLayout.DEFAULT_SIZE,
																							javax.swing.GroupLayout.DEFAULT_SIZE,
																							Short.MAX_VALUE)
																					.addGroup(
																							javax.swing.GroupLayout.Alignment.LEADING,
																							layout.createSequentialGroup()
																									.addComponent(
																											jLabel1)
																									.addPreferredGap(
																											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																									.addComponent(
																											locationMenu,
																											0,
																											javax.swing.GroupLayout.DEFAULT_SIZE,
																											Short.MAX_VALUE))
																					.addGroup(
																							javax.swing.GroupLayout.Alignment.LEADING,
																							layout.createSequentialGroup()
																									.addGroup(
																											layout.createParallelGroup(
																													javax.swing.GroupLayout.Alignment.LEADING)
																													.addComponent(
																															randomEvent)
																													.addGroup(
																															layout.createSequentialGroup()
																																	.addGap(12,
																																			12,
																																			12)
																																	.addGroup(
																																			layout.createParallelGroup(
																																					javax.swing.GroupLayout.Alignment.LEADING)
																																					.addComponent(
																																							jLabel3)
																																					.addComponent(
																																							jLabel5))))
																									.addGap(0,
																											0,
																											Short.MAX_VALUE)))
																	.addContainerGap())
													.addGroup(
															layout.createSequentialGroup()
																	.addComponent(
																			hopMod)
																	.addGap(0,
																			0,
																			Short.MAX_VALUE)))));
			layout.setVerticalGroup(layout
					.createParallelGroup(
							javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(
							layout.createSequentialGroup()
									.addContainerGap()
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(jLabel1)
													.addComponent(
															locationMenu,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(
											mainPane,
											javax.swing.GroupLayout.PREFERRED_SIZE,
											279,
											javax.swing.GroupLayout.PREFERRED_SIZE)
									.addGap(18, 18, 18)
									.addComponent(randomEvent)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(jLabel3)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(teleLummy)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(jLabel5)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addGroup(
											layout.createParallelGroup(
													javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(cleverbot)
													.addComponent(jLabel17)
													.addComponent(
															cleverbotThreshold,
															javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(
											jScrollPane7,
											javax.swing.GroupLayout.PREFERRED_SIZE,
											javax.swing.GroupLayout.DEFAULT_SIZE,
											javax.swing.GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED)
									.addComponent(hopMod)
									.addPreferredGap(
											javax.swing.LayoutStyle.ComponentPlacement.RELATED,
											12, Short.MAX_VALUE)
									.addComponent(Start).addContainerGap()));

			pack();
		}// </editor-fold>

		private void locationMenuItemStateChanged(java.awt.event.ItemEvent evt) {
			if (locationMenu.getSelectedIndex() == 1) {
				if (!isPrem) {
					JOptionPane
							.showMessageDialog(null,
									"Sorry, this location is only for Premium users. Visit http://cmhscripts.com.");
					locationMenu.setSelectedIndex(0);
					return;
				}
				mainLayout.show(mainPane, "YANILLE");
			} else {
				mainLayout.show(mainPane, "VARROCK");
			}
		}

		private void pathBox2ActionPerformed(java.awt.event.ActionEvent evt) {
			if (!isPrem) {
				JOptionPane
						.showMessageDialog(null,
								"Sorry, this action is only for Premium users. Visit http://cmhscripts.com.");
				pathBox2.setSelected(false);
			}
		}

		private void pathBox3ActionPerformed(java.awt.event.ActionEvent evt) {
			if (!isPrem) {
				JOptionPane
						.showMessageDialog(null,
								"Sorry, this action is only for Premium users. Visit http://cmhscripts.com.");
				pathBox3.setSelected(false);
			}
		}

		private void stopAfterCheckActionPerformed(
				java.awt.event.ActionEvent evt) {
			stopMined.setEnabled(stopAfterCheck.isSelected());
			stopLevel.setEnabled(stopAfterCheck.isSelected());
			stopMinedText.setEnabled(stopAfterCheck.isSelected());
			stopLevelText.setEnabled(stopAfterCheck.isSelected());
		}

		private void pathBox1ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void remoteMonitorActionPerformed(java.awt.event.ActionEvent evt) {
			if (remoteMonitor.isSelected() && !isPrem) {
				JOptionPane
						.showMessageDialog(null,
								"Sorry, this action is only for Premium users. Visit http://cmhscripts.com.");
				remoteMonitor.setSelected(false);
			}
			screenshot.setEnabled(remoteMonitor.isSelected());
		}

		private void screenshotActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void stopAfterCheck1ActionPerformed(
				java.awt.event.ActionEvent evt) {
			stopMined1.setEnabled(stopAfterCheck1.isSelected());
			stopLevel1.setEnabled(stopAfterCheck1.isSelected());
			stopMinedText1.setEnabled(stopAfterCheck1.isSelected());
			stopLevelText1.setEnabled(stopAfterCheck1.isSelected());
		}

		private void remoteMonitor1ActionPerformed(
				java.awt.event.ActionEvent evt) {
			if (remoteMonitor1.isSelected() && !isPrem) {
				JOptionPane
						.showMessageDialog(null,
								"Sorry, this action is only for Premium users. Visit http://cmhscripts.com.");
				remoteMonitor1.setSelected(false);
			}
			screenshot1.setEnabled(remoteMonitor1.isSelected());
		}

		private void randomEventActionPerformed(java.awt.event.ActionEvent evt) {
			if (randomEvent.isSelected() && !isPrem) {
				JOptionPane
						.showMessageDialog(null,
								"Sorry, this action is only for Premium users. Visit http://cmhscripts.com.");
				randomEvent.setSelected(false);
				return;
			}
		}

		private void StartActionPerformed(java.awt.event.ActionEvent evt) {
			ready = true;
		}

		private void teleLummyActionPerformed(java.awt.event.ActionEvent evt) {
			if (teleLummy.isSelected() && !isPrem) {
				JOptionPane
						.showMessageDialog(null,
								"Sorry, this action is only for Premium users. Visit http://cmhscripts.com");
				teleLummy.setSelected(false);
				return;
			}
		}

		private void cleverbotActionPerformed(java.awt.event.ActionEvent evt) {
			if (cleverbot.isSelected() && !isPrem) {
				cleverbot.setSelected(false);
				JOptionPane
						.showMessageDialog(null,
								"Sorry, cleverbot is for premium users only. Visit cmhscripts.com");
				return;
			}
			;
			cleverbotThreshold.setEnabled(cleverbot.isSelected());
		}

		private void hopModActionPerformed(java.awt.event.ActionEvent evt) {
			if (hopMod.isSelected() && !isPrem) {
				hopMod.setSelected(false);
				JOptionPane
						.showMessageDialog(null,
								"Sorry, world hopping is for premium users only. Visit cmhscript.com");
				return;
			}
		}

		// Variables declaration - do not modify
		private javax.swing.JButton Start;
		private javax.swing.ButtonGroup buttonGroup1;
		private javax.swing.ButtonGroup buttonGroup2;
		private javax.swing.JCheckBox cleverbot;
		private javax.swing.JTextField cleverbotThreshold;
		private javax.swing.JCheckBox hopMod;
		private javax.swing.JLabel jLabel1;
		private javax.swing.JLabel jLabel17;
		private javax.swing.JLabel jLabel2;
		private javax.swing.JLabel jLabel3;
		private javax.swing.JLabel jLabel5;
		private javax.swing.JScrollPane jScrollPane7;
		private javax.swing.JSeparator jSeparator1;
		private javax.swing.JSeparator jSeparator2;
		private javax.swing.JTextArea jTextArea1;
		private javax.swing.JComboBox locationMenu;
		private javax.swing.JPanel mainPane;
		private javax.swing.JCheckBox pathBox1;
		private javax.swing.JCheckBox pathBox2;
		private javax.swing.JCheckBox pathBox3;
		private javax.swing.JCheckBox randomEvent;
		private javax.swing.JCheckBox remoteMonitor;
		private javax.swing.JCheckBox remoteMonitor1;
		private javax.swing.JCheckBox screenshot;
		private javax.swing.JCheckBox screenshot1;
		private javax.swing.JCheckBox stopAfterCheck;
		private javax.swing.JCheckBox stopAfterCheck1;
		private javax.swing.JRadioButton stopLevel;
		private javax.swing.JRadioButton stopLevel1;
		private javax.swing.JTextField stopLevelText;
		private javax.swing.JTextField stopLevelText1;
		private javax.swing.JRadioButton stopMined;
		private javax.swing.JRadioButton stopMined1;
		private javax.swing.JTextField stopMinedText;
		private javax.swing.JTextField stopMinedText1;
		private javax.swing.JCheckBox teleLummy;
		private javax.swing.JPanel varrockLayer;
		private javax.swing.JPanel yanilleLayer;
		// End of variables declaration
	}

}
