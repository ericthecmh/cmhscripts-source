import java.awt.Graphics;
import java.awt.event.MouseEvent;

import org.osbot.script.Script;
import org.osbot.script.ScriptManifest;

@ScriptManifest(name = "CMHEssMiner", author = "Ericthecmh", version = 1.0D, info = "Mines Rune Essence version 1.8.7")
public class Loader extends Script {
	CMHEssMiner miner = new CMHEssMiner(this);

	public void onStart() {
		miner.onStart();
	}

	public int onLoop() {
		return miner.onLoop();
	}

	public void onExit() {
		miner.onExit();
	}

	public void onPaint(Graphics g) {
		miner.onPaint(g);
	}

	public void mouseClicked(MouseEvent e) {
		miner.mouseClicked(e);
	}

	public void onMessage(String s) {
		try {
			miner.onMessage(s);
		} catch (Exception e) {

		}
	}

	public void onPlayerMessage(String s, String message) {
		miner.onPlayerMessage(s, message);
	}

}
