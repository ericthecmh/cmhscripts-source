import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import org.osbot.script.Script;
import org.osbot.script.ScriptManifest;
import org.osbot.script.mouse.RectangleDestination;
import org.osbot.script.rs2.map.Position;
import org.osbot.script.rs2.model.RS2Object;
import org.osbot.script.rs2.ui.Bank;
import org.osbot.script.rs2.ui.RS2Interface;
import org.osbot.script.rs2.ui.Tab;

//@ScriptManifest(name = "PathMaker", author = "Ericthecmh", version = 1.0D, info = "Path Info")
public class PathMaker extends Script {
	ArrayList<Position> list;
	public State state;
	public Bank b;

	public enum State {
		TELEPORT_LUMBY, WALK_TO_STAIRCASE, ASCEND, LUMBY_BANK, EXIT_PORTAL, ENTER_PORTAL, BUILD, WALK_TO_ROOM;
	}
	private int walkI = 0;
	private int tileindex;

	public void onStart() {
		b = client.getBank();
		list = new ArrayList<Position>();
		state = State.ASCEND;
	}

	public int onLoop() {
		try{
		switch (state) {
		
		case ASCEND:
			try {
				if (myPlayer().getZ() == 0) {
					RS2Object stair = closestObjectForName("Staircase");
					if (stair != null) {
						for (int i = 0; i < 10 && !stair.interact("Climb-up"); i++)
							sleep(random(500, 600));
						for (int i = 0; i < 10 && myPlayer().getZ() != 1; i++) {
							sleep(random(500, 600));
						}
					}
				} else if (myPlayer().getZ() == 1) {
					log("Finding staircase!");
					RS2Object stair = closestObjectForName("Staircase");
					if (stair != null) {
						for (int i = 0; i < 10 && !stair.interact("Climb-up"); i++)
							sleep(random(500, 600));
						for (int i = 0; i < 10 && myPlayer().getZ() != 2; i++) {
							sleep(random(500, 600));
						}
					}
				} else if (myPlayer().getZ() == 2) {
					state = State.LUMBY_BANK;
				}
			} catch (Exception e) {

			}
			break;
		case LUMBY_BANK:
			try {
				if (client.getInventory().getAmount("Plank") > 9) {
					for(int i = 0; i < 10 && !openTab(Tab.MAGIC); i++)
						sleep(random(500,600));
					for(int i = 0; i < 10 && !client.getInterface(192).getChild(23).interact(); i++)
						sleep(random(500,600));
					for (int i = 0; i < 10 && myX() < 7000; i++)
						sleep(random(500, 600));
					sleep(random(7000,8000));
					state = State.EXIT_PORTAL;
					break;
				}
				b = client.getBank();
				for (int i = 0; i < 10 && !b.isOpen(); i++) {
					RS2Object booth = closestObjectForName("Bank booth");
					try {
						sleep(random(300, 400));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (booth == null)
						break;
					try {
						for (int j = 0; j < 10 && !booth.interact("Bank"); j++)
							sleep(random(500, 600));
						sleep(random(500, 700));
						for (int j = 0; j < 10 && !b.isOpen(); j++) {
							sleep(random(200, 300));
							if (myPlayer().isMoving())
								j--;
						}
					} catch (Exception e) {
					}
				}
				try {
					for (int x = 0; x < 10 && !client.getInventory().isEmpty(); x++) {
						b.depositAllExcept(2347, 4820, 8794, 4696, 563);
						sleep(random(900, 1100));
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					try {
						for (int x = 0; x < 10 && !b.withdrawAll(960); x++) {
							sleep(random(500, 600));
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (Exception e) {
				}
				for (int i = 0; i < 5 && b.isOpen(); i++) {
					try {
						b.close();
						sleep(random(300, 400));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
			}
			break;
		case EXIT_PORTAL:
			RS2Object portal = closestObjectForName("Portal");
			if(portal != null){
				Position p = myPosition();
				for(int i = 0; i < 10 && !portal.interact("Enter"); i++)
					sleep(random(500,600));
				for(int i = 0; i < 10 && distance(p) < 10; i++)
					sleep(random(500,600));
				state = State.ENTER_PORTAL;
			}
			break;
		case ENTER_PORTAL:
			portal = closestObjectForName("Portal");
			if(portal != null){
				Position p = myPosition();
				for(int i = 0; i < 10 && !portal.interact("Enter"); i++)
					sleep(random(500,600));
				RS2Interface inte = client.getInterface(232);
				for(int i = 0; i < 10 && inte == null; i++){
					inte = client.getInterface(232);
					sleep(random(500,600));
				}
				for(int i = 0; i < 10 && !inte.getChild(2).interact(); i++)
					sleep(random(500,600));
				for(int i = 0; i < 10 && distance(p) < 10; i++)
					sleep(random(500,600));
				sleep(random(7000,8000));
				state = State.WALK_TO_ROOM;
			}
			break;
		case WALK_TO_ROOM:
			Position roomPos = new Position(myX() - 8, myY() + 9, 0);
			while(distance(roomPos) > 2){
				walk(roomPos);
				sleep(random(500,600));
			}
			state = State.BUILD;
			break;
		case BUILD:
			if(client.getInventory().getAmount("Plank") < 9){
				for(int i = 0; i < 10 && !openTab(Tab.MAGIC); i++)
					sleep(random(500,600));
				for(int i = 0; i < 10 && !client.getInterface(192).getChild(18).interact(); i++)
					sleep(random(500,600));
				for (int i = 0; i < 10 && myX() < 7000; i++)
					sleep(random(500, 600));
				sleep(random(5000,6000));
				state = State.ASCEND;
				break;
			}
			if(closestObjectForName("Larder Space") != null){
				RS2Object larderspace = closestObjectForName("Larder Space");
				for(int i = 0; i < 10 && !larderspace.interact("Build"); i++)
					sleep(random(500,600));
				for(int i = 0; i < 10 && client.getInterface(394) == null || client.getInterface(394).getChild(107) == null; i++)
					sleep(random(500,600));
				RectangleDestination rect = new RectangleDestination(42, 103, 20, 20);
				client.moveMouseTo(rect, false, false, false);
				sleep(random(1000,2000));
				client.clickMouse(false);
				while(closestObjectForName("Larder") == null)
					sleep(random(500,600));
			}else{
				RS2Object larder = closestObjectForName("Larder");
				for(int i = 0; i < 10 && !larder.interact("Remove"); i++)
					sleep(random(500,600));
				for(int i = 0; i < 10 && client.getInterface(228) == null; i++){
					sleep(random(500,600));
				}
				for(int i = 0; i < 10 && client.getInterface(228).getChild(1).interact(); i++)
					sleep(random(500,600));
				while(closestObjectForName("Larder") != null)
					sleep(random(500,600));
			}
		}
		}catch(Exception e){}
		return random(500,600);
	}

	public void onPaint(Graphics g){
		g.drawString(state.toString(), 200, 200);
	}
	
	public void onExit() {
		for (Position p : list) {
			System.out.println("new Position(" + p.getX() + ", " + p.getY()
					+ ", 0),");
		}
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getX() > 9 && e.getX() < 400 && e.getY() > 400) {
			list.add(myPosition());
			log(myX() + " " + myY());
		}
	}
}
